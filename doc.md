---
title: Evidence of publishing printed and manuscript works, prior to 1700 by the Royal Society
author: J. P. Ascher
date: 20 May 2022
citation-style: chicago-fullnote-no-bibliography.csl
bibliography: /Users/james/org/research.bib
...
\input{standard-style.tex.include}
\input{custom-style.tex.include}

# TODO

- [x] finish reviewing CMO/8A (next IMG_4313.JPG)
- [x] review A–H and restore SA references
- [ ] think about indexes: should they be appendixes?
- [x] review CMO/1 index continue at IMG_4346 letter Q
- [ ] review MS/703
- [ ] review RBO/22, 23
- [ ] figure out how to review the JBO
- [ ] think about Birch and how he includes many, many more sources (but focuses on the papers)
- [x] figure out a mechanism to produce an index of CMO/8A phrases
      that go back to the text itself
- [ ] actually index the past items, continue to index willy nily, but
      think about the right way too
- [ ] develop a presentation form: index entries as head notes to pages?

# TODO editing

- [ ] check that the ns/os years are correct on headings
- [ ] add location to headings: Gresham College ca. 1663-1666, Arundel
      house ca. 1666-1673, Gresham ca. 1673-1681; sometimes the
      President’s house
- [ ] add Clerks, amanuenses, etc. when known
  - [ ] Wicks is Clerk in 20 Nov 1676 minute

# TODO at GB-uklors

- [x] check CMO/1 entries with ? in them against CMO/1 (regex query \?.*CMO/1)
- [x] check CMO/8A entries with ? in them against CMO/8A: regex query \?.*CMO/8A
- [x] check CMO/1 against transcriptions below W40
  - [x] CMO/1/1–12 (W40.1)
  - [x] CMO/1/13-24 (W40.2) Continue with CMO/1/15 p. 24
  - [x] CMO/1/25–36 (W40.3) continue with CMO/1/34 p. 45
  - [x] correct all the page numbers on headers
  - [x] continue to get ahead (how far?) continue with p. 56 CMO/1/42
- [x] W41
  - [x] CMO/1/42–54 (W41.1)
  - [x] CMO/1/55–67 (W41.2)
  - [x] CMO/1/68–80 (W41.3) continue with CMO/1/70 p. 84
  - [x] CMO/1/81–93 (W41.4)
  - [x] CMO/1 how many more? CMO/1/42–CMO/1/96 = 55 minutes in about 9 hours = 6 an hour = 10 min each, to CMO/1/100
- [x] W42
  - [x] continue with p. 114 CMO/1/115–CMO/1/128 (W42.2)
  - [x] CMO/1/129–149 (W42.3) continue with CMO/1/131; CMO/1/149 (19 done in one day)
  - [x] CMO/1/150–160 (W42.4)
  - [x] CMO/1/161–179 (W42.5) continue with CMO/1/162 p. 183; continue with CMO/1/180 p. 206 (19 done in one day)
- [x] W43
  - [x] CMO/1/180–199 (W43.1) continue with CMO/1/194 p. 230
  - [x] CMO/1/200–219 (W43.2) continue with CMO/1/218 p. 259
  - [x] CMO/1/220–239 (W43.3)
  - [x] CMO/1/240–259 (W43.4) continue with CMO/1/241 p. 282
  - [x] CMO/1/260–279 (W43.5) continue with CMO/1/262 p. 305
- [x] W44
  - [x] CMO/1/280-288 (W44.1) continue with CMO/1/285 p. 326
- [x] photos: CMO/1 pp. 15–16
- [x] photos: AB/1/1/1 
- [x] photos: MS/629 v.1 (rough minutes for council)
- [x] photos: MS/847 (rough minutes collected by Hooke)
- [x] photos: AB/1/1/2
- [x] photos: 557/1/1
- [x] photos: 557/1/2 
- [x] photos: 557/1/3
- [x] photos: various MM materials
- [ ] examine books mentioned in the minutes
- [ ] review other possible sources to read my notes against

# TODO technical

- [x] update JPDoc rendering pipeline
- [x] ERROR: checkboxes are not checkboxes, fix them
- [ ] review error messages (missing glyphs, etc.)
- [ ] make margins wider
- [ ] glyphs to check 
  - [ ] &bstrok; = bstroke = ƀ —;
  - [ ] bhighoverline =  ;
  - [ ] lhighstroke = ꝉ;
  - [ ] lbar = ƚ;
  - [ ] q3app = ;
  - [ ] checkmark = ✓;
  - [ ] sclose = ;
  - [ ] per = ⅌;
  - [ ] ballot x = ✗;
  - [ ] macute = ḿ;
  - [ ] qbardes = Ꝗ;
  - [ ] ltailstrok = ꝲ;
  - [ ] ptailstroke = ꝑ;
  - [ ] mmacrmed = ;
  - [ ] omacron = ō;
  - [ ] amacron = ā;
  - [ ] dstrok = đ;
  - [ ] emacron = ē;
  - [ ] hstroke = ħ;
  - [ ] cmacr = ; cmacr (not displaying right?)
  - [ ] nmacrmed = ;
- [x] Re-number with CMO last: it’s not a stable reference (According to Rupert: they don’t quite line up.)
- [x] Think about stroke characters: is there a good keyboard shortcut?  (lots! A-- isn’t bad)
- [ ] learn to remove the lower line from multiline simple tables
- [ ] prevent tables from floating between pages: right now p. 145 is
      a little broken.  In ConTeXt, use “location={here,force,split}”
      but figure out how to make this the default

# TODO digitally while in GB-LND (ISO 3166-2 code)

- [x] CMO/4–15 (W40.1) continue with second part of CMO/1/9
- [x] quick check against stub transcription continue with CMO/1/53
- ~~[ ] stub edits CMO/1/53–103 continue with 101~~
- ~~[ ] stub edits CMO/1/104–154~~
- ~~[ ] stub edits CMO/1/155–205~~
- ~~[ ] stub edits CMO/1/206–256~~
- ~~[ ] stub edits CMO/1/256-288~~

# Very useful events to consider

- throughout: classify O’s corrections: what are they doing?
- CMO/1/100: reprinting the lists with typographical instructions
- CMO/1/122: who is Ellen Collet? A charwoman
- CMO/1/143: sticking new material in a printed book
- CMO/1/144: publishing licensor: the ask Martin to publish a translation and pay(!)
- CMO/1/159: reviewing a review to be included in the Transactions: were they all Oldenburg before this point?
- CMO/1/159: also, order to translate ← this is a good example minute of their activities
- CMO/1/167: Oldenburg’s preface to Malpighi approved by Council
- CMO/1/172: books must be returned entire, undefaced and unblotted
- CMO/1/175: Pitt is over-charging, so the Council says subscribers don’t need to buy
- CMO/1/184: do they pay for the copies they send?
- CMO/1/221-2: ordered printed and then licensed, why that order?
- CMO/1/228: Emanuel de Costa librarian to the RS canceled
- CMO/1/241: notes on HOW to keep the books: a particularly full meeting
- CMO/1/243: in the handwriting of the index
- CMO/1/251: leave of the Councel &/or Author

# Accounting related events

- CMO/1/107: money into chest
- CMO/1/118,121,124 (debts and credits): why are we paying Hook and how much? Use of forms, accounting abbreviations
- CMO/1/133: exchanging books with ideas
- CMO/1/134: treasurer’s accounts reported
- CMO/1/140: letter about Colepresse in draft form within EL
- CMO/1/142: letter about Leopold to be sent
- CMO/1/143: ledgers make no sense without the Council minutes
- CMO/1/189–190: reimbursing Mr. Povey by lowering his fees
- CMO/1/195: both £400 and a note managed by the Council
- CMO/1/197: to Thompson and Company banking venture?
- CMO/1/206: ways of disposing of money
- CMO/1/241: notes on HOW to keep this book after Oldenburg passes: a particularly full meeting
- CMO/1/260: copy out all the experiments into the register
- CMO/1/273: record of expenses over the last few years

# Classification related events

- CMO/1/120: “reducing extracts into a Method”
- CMO/1/122a: notes about Digby’s library: what’s there? “Rated at”?
- CMO/1/129 selecting upon every Head one or two instances

# Physical features that evince use

- CMO/1/124: circular ink stain over date
- CMO/1/126: doubled “The P” over the heading, suggesting the rough copies were kept in this book
- underlines of the year, etc. must indicate checking in some sort of
  process; whose? Perhaps the person correcting the first copy? The
  ink is normally a different color.
- black ink shows up pp. 127--132 “con\rlap{\bfa cer}{ t a i}ning”
- black ink (bleed through or blotting?) in lower right of p. 169, upper right p. 171, center p. 205., bottom p. 212
- pen skip on date p. 171, p. 173
- first line ruled with pencil p. 176, perhaps Oldenburg trying to keep things level?
- p. 177 ff. mixes italic and secretary forms: I think this is a new
  writer. Oldenberg on pp. 175-6, new Amanuensis afterward?
- CMO/1/169: list of Queries use different paragraph shapes, out of order too
- CMO/1/255: blotting in gutter
- CMO/1/134 (p. 144) report of the auditor is signed by Brouncker, etc. from 11 Nov 1667, also p. 175 (9 Nov 1668)
- 17 June 1663: Journal book of council mentioned the first time

## Approach

This study initially aimed to find manuscripts used by printers in
preparing printed editions, particularly those manuscripts that have
marks indicating their use for printing.  During preliminary survey, I
identified manuscripts used for printing with distinctive marks within
several archival fonds, the “Classified Papers” (CLP), “Manuscripts
General” (MS), “Early Letters” (EL), and “The Robert Boyle Collection”
(RB).  However, during the survey I saw that some, but not all,
manuscripts were treated as equivalent to printed works produced from
other manuscripts, either given the same weight in debates or treated
as established, common knowledge.  The community within the Royal
Society treated some manuscripts the same way they treated published,
printed books.  Thus the simple distinction between printed and
manuscript texts is anachronistic here.

For example, <!-- Evelyn, rowling press, todo this -->  Harold Love writes about a similar distinction between separates and manuscript books from other forms of writing <!-- summarize this -->

I reviewed the historical indices for references to texts that had
been printed or treated as equivalent to a printed work, then examined
those texts for evidence of use.

Along with the references to texts in the indices, the chronology
below transcribes references to printing within the minutes of the
Council, who had the authority to license and order books printed.
The also frequently purchased books for the society or paid to have
works copied.

The focus of this project is verbal text, but I have inventoried the
images along the way, particularly the images inserted in the Record
books by Dr. Stack <!-- todo get his name right! See CMO/8A under
“Stack, Thomas, M.D.” for a lot of page references--> during his
collation of the society’s records.  Someone with a serious interest
in the transmission of images through publication would need to
consider paintings and exhibition as well, but this inventory will
provide a place to start by considering images that were published
along with verbal texts.

Each entry begins with the best approximation of the accepted creation
date, followed by the text, then the locations in CMO, the Journal
books (JBO), the Record books (RBO), or elsewhere as appropriate.
Administrative delays such as copying the draft or rough minutes into
the official minutes are described as such.  I.e. **NOT POSSIBLE TO DO THIS SYSTEMATICALLY? -jp 2023**

* “Revised account of a meeting held:”

* “A paper prepared for a meeting held:”

* “An order from a meeting held:”

* “An issued number meant to cover work up to:”

* “A book planned to be issued by:”

Since revision of notes after a meeting could take as long as the next
meeting, or even years later, but could happen much quicker, these
marks of the nature of the date aim to help the careful historian in
established what was known by whom, when.

## Scope

This inventory means to list all primary source evidence of several
types: of printing, of acquiring printed materials, of writing
manuscripts that would have been treated as equal to individually
printed works, and of acquiring manuscripts that would have been
treated as equal to individually printed works.  The society in this
period seemed not to treat printing as the only, or even a
particularly important, mode for transmitting the texts of works.
Their work copying out letters, arranging letters, building indices,
and acquiring manuscript papers, attests to their more capacious sense
of written works.  Thus restricting to only things that were actually
printed underestimates the bibliographical range of the early work of
the society.

This scope results in some unusual inclusions: since I include printed
forms and tickets, I furthermore include the written forms and
tickets.  The society treated a printed invitation as equivalent to a
written one.

The acquisition of the Arundel Library seems more administrative at
first, but eventually demands a catalog treating its items as
individual works.  Thus it is included as an example of the
acquisition of printed works, and manuscripts treated as equivalent to
printed works, but perhaps as a marginal case.

The Record books pose philosophical problems, as every text was a
distinct contribution recorded for posterity, but not every one was
printed.  Indeed, the society went through the collections several
times to identify materials to print and ultimately did not print
every single one.  This seems to attest to the increasing importance
of printing for the transmission of works, but also that there is a
distinction between kinds of written documents.  For this inventory,
merely recording a contribution in the Record book does not warrant
inclusion, nor does copying it when duplicating the whole of the
Record book.  However, subsequent printing, individual transcription,
or individual circulation does.

The society frequently requests letters be written or notes sent,
these are typically included since publication by letter constitutes
local publication as well.  Orders to copy are also included as
evidence of the role of the amanuenses.

A great deal of early business concerns the writing and revising of
statues, laws, and indentures.  Considering a lawyer a special kind of
secretary and a legal scrivener a special kind of amanuensis, I have
transcribed these orders as well when the emphasize the writing rather
than the law.  The word “Patent” is a marginal inclusion.  When patent
refers to the writing or creating of an open letter, the minutes are
included.  When patent is a synechdoche for a human process, the
minutes are excluded.  E.g. 5 November 1667 (p. 137) “the state of the
Patent” means its bureaucratic process and the solution is asking Lord
Ashley to nudge it along, not writing another document.  Thus this passage is excluded.

Generally, paperwork of all sorts is included when it is discussed as
paperwork, not when it is discussed by its results.  The logic here is
that the provision of paperwork reflects on how the society used
writing to achieve its aims.

Positions within the society in this period are somewhat fluid.  A
curator or operator may be asked to write or assist in other ways.
Eventually, however, the positions have specific remits outside of the
scope of the minutes transcribed here.  When in doubt, I include, but
when it becomes clearly out of scope I exclude.  E.g. p. 139 when
Dr. Lower is proposed as Curator in Anatomical Experiments, the role
is no longer miscellaneous but constrained to experiments in anatomy,
which do not directly concern my scope here.

The distinction here aims to be one between the documents of the
society’s activities and the works it produced.  These works certainly
included anything published, that is made public by printing or
another form of duplication, but also includes other printed items
generally regarded as ephemera.  Paperwork whose audience was the
public is included as a sort of work produced by the society.  For
purposes of this paper, published works interest us, but those include
anything deliberately made available to the whole membership as a
distinct work, or anything made available to a broader public as an
distinct work.

## Indices

\BiblHangingIndentsStart

**CMO/8A:** *Council Minutes* of the Royal Society of London, volume
8A, which consists of an index for 1663-1811.

**CMO/1:** *Council Minutes* of the Royal Society of London, volume 1
(1663–1681), which has an index at the end.  This is briefer, but
covers different material than CMO/8A, so might have significant items
not mentioned.

**MS/703:** *Index of papers read before the Royal Society 1660-1716*,
presumably assembled by Dr. Stack on the request to find everything
that had been printed.  It includes references to papers stored in the
CLP, LBO, RBO, and what was printed.  Many of the items in CLP were
used by the printers and letters found in LBO are often also in EL
series, items of which were often used by printers.

**RBO/22, 23:** *General Subject Index* to the Journal, Register and
Letter Books by Richard Waller.  The catalog says it covers the dates
1661-1689 and it includes “Books presented” along with “Papers brought
in, read, & mostly left.”

**MS/131:** *Royal Society ‘scrap book’* of illustrations assembled by
Richard Waller according to the catalogue.  Many of these were used to
create the engraved plates used to illustrate copies of the
*Transactions* and other volumes.

**MS/629:** *Minutes of Meetings of the Council, 1665-1870* nineteen
volumes of minutes of the Council of the Royal Society.  The first
volume begins when the meeting resumes after the plague on 21 February
166$\frac{5}{6}$.  These show significant revisions and changes
suggesting they were the written minutes of the meeting reviewed and
entered in the Council’s journal book.

\BiblHangingIndentsStop

## Sources for evidence of printing, writing, or reading

**Orders:** of the Council, which seem to have been recorded in rough
minutes by the Secretary that have not been preserved, aside from a
few exceptional cases <!--todo, fill this in -->.  The *Council
Minutes* (CMO) consist of the deliberate legal record of the activities of
the Council, so represent the way the society wished to be understood.
This is the primary evidence of the administrative activity around
printing, writing, and acquisitions along with every other aspect of
the society’s activities.

**Papers:** brought in to meetings and left, or mailed with an
accompanying letter, most of these are organized in CLP in the 1730s,
though some are in the collections of particular Secretaries or
Presidents, see Birch papers, RB, various MS.  Many of these papers
are manuscripts used by printers, but even in cases where they are
not, they are the source for the transcriptions which would have
become the manuscript.  In some cases, manuscripts may have been
transcribed from the JBO or RBO, but those tend to be after the files
were arranged into guard-books.

Including: MS/366 which are the α-ε guard books mentioned in the minutes.

**Letters:** the original of many of these are present in EL, but also
RB, CLP, and various MS.

**Editorial bundles:** these are the collections of papers, letters,
transcriptions, additions, and other matter that were licensed by the
Council and brought to a printer.  Many of the papers and letters can
be found, but so too can some of the editorial materials in unexpected
places.

## Form of entries NO LONGER CORRECT

Each section of the documentary evidence of CMO, JBO, or rough versions, begins with the nominal date and the page reference.  E.g.

\BiblHangingIndentsStart

**14 Dec 1663 (CMO/1/33, v. 1 pp. 43–44):**

\BiblHangingIndentsStop

Means that the evidence is nominally dated 14 December 1663 and can be located at “CMO/1/33” which corresponds to volume 1 of the Council Minutes (CMO) on pages 43 to 44.  After the heading comes any index terms, e.g.

\BiblHangingIndentsStart

**21 Dec 1663 (CMO/1/34, v. 1 pp. 44–6):**

“[Benefactors & Benefactions]  Lists, or Tables, of them ordered” (CMO/8A)

\BiblHangingIndentsStop

And the index entry is coded for the automatically generated index as
`\index{Benefactors+Lists or tables ordered}`.  Following the nominal
date and locations, a transcription of the documentation itself
follows.  The procedure generally follows Vander Meulen, but indicates omissions with […], thus an entry beginning on the middle of page 43 and continuing to the top of page 44 would be transcribed:

\TranscriptionStart

[**Page 43:**]

[…]

 Ordered , that the Secretary bring in a list of the [**Page 44:**]\
Names of all the Benefactors to the Society , to\rlap{\bf g}{t}ether with [*‘to\rlap{\bf g}{t}ether’ corrected from ‘totether’ by overwriting*]\
their donations , and the time when they promised them .

 Ordered , that the Benefactors be registered , in loose\
Vellum-sheets .

 Ordered , that D^r^. Goddard take care that a Preſs or\
two be provided for the use of the Society .

 Ordered , that M^r^. Palmer consult M^r^. Ellis , whether\
the Charter of the Society , speaks fully enough to impower\
the Council for granting License to their Printers , to — —\
print such books as shall be committed to them by the Society\
concerning their Design and work . [*pen flourish*]

\TranscriptionStop

Since the aim of the reference is to help someone locate the passage,
prefixed omissions are included, but when the transcription leaves off
no sign of omission is used.

In all cases, I use my judgment to try to decide what portion of the
minutes relates directly to the activities of writing and printing.
By directly, I mean the activities themselves, purchase or provision
of materials primarily intended for the activities, or activities that
turn out to be primarily related to the activities.  Necessary for the
activities is not sufficient for inclusion, e.g., having a building in
which to meet is itself necessary for the meetings, but not directly
related; but, building shelves in that building to store books or
writing is directly related.

When the index item gives multiple pages, I simply repeat the item for
each page.  E.g. rather than “(CMO/1, v.1 p. 6,56):” I make two
entries one for p. 6 and one for p. 56 in the form “(CMO/1, v.1
p. 6,56):” for p.6 and “(CMO/1, v.1 p. 56: 6,56):” for p. 56.  The
purpose here is to preserve the cross-references for the time being.

It’s unclear if this judgment will yield a coherent document, but
that’s part of the research question here.

## Editorial conventions

- glyphs transcribed, list them
- When there is a correction write it “corrected[*how:* ‘uncorrected’]” e.g. “Columnes[*over:* ‘Collumnes’].”
- When it is an insertion or addition, simply represent it as such,
  e.g. “the‸^inserted^ text”, but if such an approach doesn’t work,
  give it with a note first, e.g. “[*four words marginal:*] the four
  words here are inserted unusually” Following DVM’s approach, give
  the final form outside of the brackets as much as possible.
- Generally reproduce the typography, but give prefixed notes for
  centered headings and other oddities, e.g. “[*centered:*] The
  Heading”. If it applies to multiple lines, give it on a line by
  itself with the number, e.g. “[*Four lines centered:*] \n The \n
  next \n four \n lines”.  As for pages, capitalize when a paragraph
  by itself, otherwise lowercase.
- Page breaks and structure are given in bold, e.g. [**Page 23:**];
  When a paragraph alone, capitalize.  When part of another text,
  lowercase.
- Should page breaks be repeated entry to entry? I don’t think so, but
  I’m not consistent right now.  Similarly, I don’t think I need the
  ellipses at the beginning *and* end of two entries that but up
  against each other.  Just one should be fine: the end seems better: things should be consistent after CMO/1/36
- Where the amanuensis runs out of ink, dips their pen, and begins
  writing over the *same* letters, I do not note this.  It occurs
  every 11–30 lines or so.
- The description in brackets can have two words, the first is the
  most visible aspect of its appearance, the second explains what the
  thing might be, i.e. [*canceled dittography:* ‘~~them~~’] means that
  it’s a mere repetition due to copying.  Similarly, the description
  can be modified with a comma, i.e. [*canceled, uncertain:* ‘&c.’]
  means that it’s not totally clear if what’s being canceled is the
  ‘&c.’

## Chronology of Council Orders relating to writing or printing, Volume 1 CMO

During October 2023, I completed this restricted portion of the
project.  Because I lacked the funding, I could not complete more.
I’m not yet sure if this should be produced as a stand alone edition
or if I should retain it as part of this larger project.

### Description of the volume

Moderate brown [30.43% 23.2 61.60° 45:0] conserved reversed calf with
combed endpapers with dark red [27.23% 25.01 22.64° 45:0] dominating,
blind filet edges and inner frame with leaf corner pieces inward on
boards; six panel spine, second panel with deep red [26.72 49.20
16.82° 45:0] lettering piece ‘ROYAL SOCIETY \| [*rule*] \| MINUTES \|
OF \| COUNCIL \|’, third panel with grayish brown [26.97% 10.24 55.77°
45:0] lettering piece ‘VOL I \| [*rule*] \| 1663–1681’, sixth panel
blind stamped arms of the Society, loose notes mounted on guards at
front. Various bifolia have been reinforced with either contemporary
or modern paper; the entire volume, other than the preliminary and
ending blank pages, is ruled with vertical strong pink [cf. 68% 42
20°] line 42/55 from the left margin on rectos and versos.  The last
gathering, S, was a gathering of at least four leaves with the final
leaves now missing; paper typically pale orange yellow [P4^r^: 84.99%
13.29 78.83° to B2^r^: 82.62 14.18 78.01° ΔE 1.70 45:0] with soiling
as dark as light yellow brown [A1^r^ edges, 59.43% 20.93 72.18° with
ΔE 19.94 from P4^r^ 45:0]; 322/333 × 209/212 × 38/40.

\BiblHangingIndentsStart

2^o^:\
‌ [A]^10^; 10 leaves, pp. \[[5]{.underline}\]\(*unruled blanks*\) \[[1]{.underline}\]\(*ruled blank*\) 1–14\
‌ [B]^12^; 12 leaves, pp. 15–38\
‌ [C]^10^; 10 leaves, pp. 39–58\
‌ [D]^14^; 14 leaves, pp. 59–86\
‌ [E]^10^; 10 leaves, pp. 87–106\
‌ [F]^14^; 14 leaves, pp. 107–134\
‌ [G]^10^; 10 leaves, pp. 135–154\
‌ [H]^14^; 14 leaves, pp. 155–183\
‌ [I]^10^; 10 leaves, pp. 183–202\
‌ [K]^14^; 14 leaves, pp. 203–230\
‌ [L]^10^; 10 leaves, pp. 231–250\
‌ [M]^14^; 14 leaves, pp. 251–278\
‌ [N]^10^; 10 leaves, pp. 279–298\
‌ [O]^14^; 14 leaves, pp. 299–326\
‌ [P]^10^; 10 leaves, pp. 327–330 \[[5]{.underline}\]\(*ruled blanks*\) \[[11]{.underline}\]\(*index A. to F.*\)\
‌ [Q]^6^; 6 leaves,  \[[12]{.underline}\]\(*index G. to Q.*\)\
‌ [R]^2^; 2 leaves, \[[4]{.underline}\]\(*index R. through S.*\)\
‌ \[S\]\(2 ll.\); 2 leaves, \[[3]{.underline}\]\(*index T. through W.*\) \[[1]{.underline}\]\(*unruled blank*\).

Preceded by: 2^o^: χ^2^ “Council book. Vol.1.”;— unlabeled slip listing names beginning “W. Croone \| L. Rooke \| […]”;— label attached with sealing wax to verso of first leaf “The State of the Original Minutes \| of the Council of R.S.”.

\BiblHangingIndentsStop

### Preliminary note “State of the Original Minutes”

\TranscriptionStart

[*two lines centered:*] The State of the Original Minutes\
of the Council of R .S .

All the Original minutes to March 16 ; 166$\frac{3}{4}$ inclusiue are wanting .

Mar 23 : 63 . all but the first paragraph , miſsing . And instead of y^e^ last\
‌  words of this article , [as to their Novelty, Reality & Usefullneſs :]{.underline} the Orig^l^\
‌  has these : whether they be new true and usefull .

None found till Oct : 5 : 1664: and then one article only : viz :\
‌  [Ordered that M^r^ Hill do speedily &c]{.underline} .

None to Feb : 21 . 166$\frac{5}{6}$ . exclusiue : except the following one :\
[*canceled:* ‘~~There is~~’] an Orig^l^ Minute of May 16 : 1665 not enter’d .

\TranscriptionStop


### 13 May 1663 (CMO/1 pp. 1–3; CMO/1/1)

\TopicsStart
“Debates kept under Secrecy” (CMO/1)\index{Debates kept under Secrecy}
“Extract of the Charter is to be made.” (CMO/1)\index{Extracts+Charter}
“The Forme of the Subscription of the Fellows of the R: Society.” (CMO/1)\index{Forms+Subscription of the Fellows}
“D^r^: Wilkins sworne by the President as Secretary.” (CMO/1)\index{Secretaries+Dr Wilkins sworne}
“Clerk. M^r^. Weeks appointed.” (CMO/8A)\index{Clerks+Mr. Weeks appointed}
“Presidents of the Royal Society \| Lord Viscount Brouncker — 1663” (CMO/8A)\index{Presidents+Lord Brouncker}
\TopicsStop


\TranscriptionStart

[**Page 1:**]

[*five lines centered:*]\
The Journal-Book\
of the Council\
Of the [Royal Society]{.smallcaps}\
of [London]{.smallcaps}\
for Improving Natural Knowledge.

The Council of the Royal Society met the first time, ~\
May 13. 1663 : there being present these persons that\
Follow ;

[…]

‌ The‸^new^ Charter of the Society was read before them , ~ ~\
wherein the Members of the Councill were nominated , as ~\
followeth :

‌ Lord Brouncker President.  D^r^. John Wilkins , Secretary\
[…]\
‌ D^r^. Timothy Clark    Henry Oldenburg Esq^r^. Secretary.\
‌ William Aerskine Esq^r^.

[**Page 2:**]


‌ The Members of the Council present , were ſworne by the\
President : and M^r^. Balle was also sworne as Treasurer ;\
and D^r^. Wilkins and M^r^. Oldenburg , as Secretaries .

‌ M^r^. Wicks was sworn as Clerk.

‌ Ordered , That the debates concerning those , that are\
to be received and admitted into the Society , be kept ~ ~\
under Secrecy .

[…]

‌ The Collecting of Arrars , being thought neceſsary ,\
the following Order was drawn up concerning the ~\
ſame .

‌           Wednesday May 13 : 1663 .\
‌         At a Meeting of the Councill of the Royal Society.

‌ Ordered , That all Persons , that haue been elected or ~\
admitted into the Royal Society , doe pay their whole ~\
Arrears unto this day , according to their Subscription :\
And that the Treasurer , or Collector by him appointed , do\
repair to every such person , and demand the said Arrears,\
shewing unto him this Order , together with the forme\
of the Subscription hereunto annexed .

‌                   Signed by the President .

‌           The forme of the Subscription .

Wee whose names are under written , do consent and ~\
agree , that we will meet together weekly ( if not —\
hindred by neceſsary Occasions ) to consult and debate —\
concerning the promoting of Experimental Learning : [**Page 3:**]\
And that [*canceled:* ‘~~we will~~’] such of us will allow one shilling ~ ~\
weekly , towards the defraying of occasional Charges : ~\
Provided that if any one , or more of us , shall think fit\
at any time to withdraw , he or they shall , after notice ~ ~\
thereof given to the Company at a Meeting , be freed from —\
this Obligation for the future .

‌ Ordered , That a short Extract be made by D^r^. Goddard ,\
of the Heads of the Charter , for the use of the Councill. [*downward flourish*]

\TranscriptionStop

### 20 May 1663 (CMO/1 pp. 3–6; CMO/1/2)

\TopicsStart
“Persons committed to make Report of the Statutes of the R: Society.” (CMO/1) \index{Reports+On Statutes}
“Obligacõn to be Subscribed by every Fellow of the Society” (CMO/1) \index{Subscription+Obligation to}
“Committee. For Statutes.” (CMO/8A) \index{Committees+For Statutes.}
“Obligation of the Fellows. None admitted Fellow ’till he has subscribed to it” (CMO/8A) \index{Obligations of Fellows+Subscription}
\TopicsStop

\TranscriptionStart

[*centered:*] May 20t^th^. [1663]{.underline} .

‌ It was considered again , who should be received as\
Fellows into the Society ; and the following persons were\
unanimously resolved upon , to be published this day in\
the afternoon , at the Meeting of the Society .

[…]

[**Page 6:**]

[…]

‌ Voted , That the persons following be a Committee to ~ ~\
consider and make Report of the Statutes of the Society : —\
viz^t^.

‌ S^r^. Robert Moray      D^r^. Ent .\
‌ M^r^. Boyle .           M^r^. Palmer .\
‌ M^r^. Sling‸^e^sby .    M^r^. Hill .\
‌ D^r^. Wilkins           M^r^. Oldenburg .\
‌ D^r^. Goddard .

and that any three or more of ·them , [*canceled:* ‘~~make~~’]‸^be^ a Quorum .

‌ Resolved , That there shall be an Obligation to be Subscri⸗\
bed by every Fellow of the Society .

‌ Ordered , That the Committee of the Council consider of\
the Obligation to be Subscribed by the Fellows of the ~ ~\
Society .

[…]

‌ Resolved , That no person be Registered Fellow of the —\
Society , untill he hath Subscribed such Obligation  , as the\
Council shall agree upon . [*downward flourish*]

\TranscriptionStop

### 27 May. 1663. (CMO/1 pp. 7–8; CMO/1/3)

\TopicsStart
“Clause concerning the Armes of the Society enroled in the Herald Office.” (CMO/1) \index{Arms, clause concerning}
“The Armes of the Society enroled in the Charter.” (CMO/1) \index{Arms enrolled}
“A Draught of the Statute for the aforesaid Obligation to be ~ \| brought in of the Committee” (CMO/1) \index{Drafts+Statue for the Obligation of Fellows} \index{Statutes+Obligation of Fellows}
“[No person to be Registred as Fellow of the Society till he hath Subscribed the same.] The form of the said Obligation.” (CMO/1) \index{Forms+Obligation of Fellows}
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A) \index{Forms+Obligations of Fellows}
\TopicsStop

\TranscriptionStart

[*centered:*] May 27 . 1663 .

[…]

‌ Ordered , That D^r^. Goddard deliver the Charter of the ~ ~\
Society to M^r^. Elias Ashmole , to enrole the Armes of the —\
Society , and the Clause concerning the same , in the ~ ~ ~\
Herald’s Office .

‌ The Committee having presented to the Council , the ~ ~\
Draughts of the Obligation to be Subscribed by the Fellows\
of the Society ; it was , after some alterations , agreed —\
upon , as followeth :

‌ We , who haue hereunto Subscribed , do hereby promise ~\
each for himselfe , that we will endeavour to promote the\
Good of the Royal Society of London , for Improving Natural\
Knowledge ; and to pursue the Ends for which the same —\
was founded : That we will be present at the Meetings of\
the Society , as often as conveniently we can , especially\
at the Anniversary Elections , and upon extraordinary\
Occasion : And that we will obserue the Statutes and\
Orders of the ſaid Society : Provided , that whensoever\
any of us , shall signify to the President under his —\
hand , that he desireth to withdraw from the Society , He\
shall be free from this Obligation for the future .

‌ Ordered , That the Committee bring in a Draught of\
a Statute for the aforesaid Obligation , at the next\
Meeting of the Council.

[**Page 8:**]

[…]

‌ [*canceled:* ‘~~Ordered ,~~’] Vpon the reading of a letter , sent out of —\
Ireland to the Secretary , concerning the Expectation , ~\
which the Committee , that heretofore had giuen the Society\
an Account of S^r^. William Petty’s new ship , did entertain\
for hearing the sense of the Society thereupon ; it was

‌ Ordered , That the Committee should be put in minde\
by the Secretary , that the Matter of Navigation , ~\
being a State-concern , was not proper to be managed\
by the Society ; And that S^r^: William Petty, for his —\
private ſatisfaction , may , when he pleases, haue the —\
ſense (if he hath it not already ) of particular Members\
of the Society , concerning his new Invention . [*downward flourish*]

\TranscriptionStop

### 3 June 1663 (CMO/1 pp. 8–9; CMO/1/4)

\TopicsStart
“A Draught of Statutes concerning the Obligacon to be ~ \| Subscribed & the weekly payments.” (CMO/1)\index{Drafts+Statute about the obligation to be subscribed}\index{Drafts+Statute about weekly payments} \index{Statutes+Subscription}\index{Statutes+Weekly payments}\seeindex{Draughts}{Drafts}
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A) \index{Forms+Obligation of Fellows}
“A Form of Thanks of the Council drawne up concerning D^r^: Bats.” (CMO/1) \index{Forms+Thanking Dr Bats}
“Making & Repealing of Laws.” (CMO/1)\index{Statutes+Making and repealing laws}\seeindex{Laws}{Statutes}
“Whether, By-Laws, confirmacon be necessary.” (CMO/1)\index{Statutes+Confirmation}\seeindex{Bylaws}{Statutes}
“Order of making a Law, concerning the President.” (CMO/1) \index{Statutes+President’s covering}
“Bye-Laws. Of proper force, and not necessary to be confermed by the Lord Justices” (CMO/8A) \index{Statutes+Proper force} \index{Statutes+Confirmation of Lord Justices}
\TopicsStop


\TranscriptionStart

[**Page 8:**]

[*centered:*]\
June 3. [1663]{.underline}.

[…]

[**Page 9:**]

‌ Ordered , That D^r^, Clark should giue him [Dr. Bats] the thanks\
of the Councill , for his Respect and kindneſse to the ~ —\
Society ;  And that D^r^. Goddard draw up a Forme of —\
this Order.

[…]

‌ Ordered , That the Committee bring in a Law , obliging\
the President to be covered , except when he speaketh to ~\
the whole Society : As also , that they take a reveiw of\
the former Draughts , for the Making and Repealing of\
Laws ; and alter them so , that the Repealing as well as\
the Making of a Law , be considered but twice , on two —\
severall dayes .

‌ Ordered , That M^r^. Palmer informe himselfe of some\
Lawyers , whether it be neceſsary , to haue all the By-Laws\
of the Society confirmed by the Lords Cheif-Justices ; ~\
and if so , whether any of such By-Laws can be repea⹀\
led , without the revising of the said Justices .

‌ The Committee presented the Council with a Draught\
of a Statue for the Obligation to be subscribed , and\
of another , for the Weekly payments ; both which , ~ ~\
were recommitted .

[…]

\TranscriptionStop

### 10 June 1663 (CMO/1 pp. 10–11; CMO/1/5)

\TopicsStart
“Whether, By-Laws, confirmacon be necessary.” (CMO/1)
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A)
“Bye-Laws. Of proper force, and not necessary to be confermed by the Lord Justices” (CMO/8A) \index{Bye-Laws. Of proper force, and not necessary to be confermed by the Lord Justices}
“The Forme of the Subscription of the Fellows of the R: Society.” (CMO/1)
“A Draught of the Statute for the aforesaid Obligation to be ~ \| brought in of the Committee” (CMO/1)
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A)
\TopicsStop

\TranscriptionStart

[**Page 10:**]

[*centered:*] June 10 . 1663 .

[…]

‌ M^r^. Palmer reported that he had consulted with some\
able Lawyers concerning the neceſsity of confirming —\
the By-laws of the Society by the Lords Justices ; and\
that they conceived such a Confirmation to be unneceſs⹀\
ary .

‌ Ordered , That the Arrears should be Collected by\
the following Order :

[*two lines centered:*] Wednesday June 10 . 1663 .\
At a Meeting of the Council of the Royal Society .

‌ Ordered , That all persons , that haue been elected or\
admitted into the Royal Society , do pay their whole ~\
Arrears , unto the 24^th^. of this instant June , according\
to their Subscription : and that the Treasurer, or ~\
Collector by him appointed , do repair to every such\
person , and demand the said Arrears , showing unto\
them this Order , together with the Forme of Sub⹀\
scription hereunto annexed .

‌           Signed by the President .[**page 11:**]

[*centered:*] The Forme of Subscription .

We whose Names are underwritten &c .\
‌ It being found , that an Order of the Council , of — —\
May 13 . concerning the Collection of Arrears , had ~\
been neglected , the Council made this Resolue there⹀\
upon :

‌ Whereas there hath been a neglect in the Execution —\
of an Order of the Council of May 13 . concerning ~\
the Collection of Arrears , whereby it might haue the —\
more clearly appeared , who should be thought fitt to\
be continued Fellows of the Society ; And whereas\
the Time , wherein the Council hath power to elect ~ ~\
Fellows , is neer Expired : ’Tis Resolved , that the Order\
of May 20 . concerning the non-registring the ~ ~\
Members of the Society , untill they haue subscribed —\
such Obligation as the Council shall agree upon , be —\
reversed. [*downward flourish*]

\TranscriptionStop

### 17 June 1663 (CMO/1 pp. 11–13; CMO/1/6)

\TopicsStart
“A Draught concerning the making of Laws.” (CMO/1)
“Statutes \| For the Making & repealing of Laws” (CMO/8A)
“Statutes \| For the Making & repealing of Laws” (CMO/8A)
\TopicsStop

\TranscriptionStart

[*centered:*] June 17 . [1663 :]{.underline}

[…]

‌ The Draught of the Statute for Subscribing the ~\
Obligation , being considered again ; it paſsed as ~ ~\
followeth .

‌ Every Fellow of the Society , and every person ~ ~\
Elected , or to be admitted , shall subscribe the Obligation [**page 12:**]\
in these words following ;

‌ We who haue hereunto Subscribed , doe hereby promiſe\
each for himselfe , that we will endeavour to promote\
the Good of the Royal Society of London for Improving\
Natural Knowledge, and to pursue the Ends for which\
the same was founded : That we will be present at\
the Meetings of the Society , as often as conveniently\
we can , Especially at the Anniversary Elections ,\
and upon Extraordinary Occasions : And that we\
will obserue the Statutes and Orders of the said\
Society : Provided , that whensoever any of us shall\
Signify to the President under his hand , that he\
desireth to withdraw from the Society , He shall\
be free from this Obligation for the future .

‌ And if any Fellow shall [*canceled, unclear:* ‘~~or severil~~’] refuse to\
Subscribe the said Obligation , he shall be ejected —\
out of the Society : And if any person Elected , shall\
refuse to subscribe the same , the Election of the said\
person shall be Voyd . Neither shall any person , refus⹀\
ing to Subscribe , be admitted or Registered among the\
Fellows of the Society .

‌ The Draught concerning the making of Lawes ~\
passed as followeth ;

‌ For the making of any Law or Statute of the ~ —\
Royal Society , the Draught thereof shall be read in\
Council , and put to the Vote on two ſeveral dayes of\
their Meeting : The first day , the Question to be ~ ~ -\
resolved by Vote shall be , whether the Draught of the\
said Statute , then agreed upon , shall be read at another\
Meeting ; The second day , the Question shall be Whe⹀\
ther the Draught of the said Statute then agreed ~ [**page 13:**]\
upon , shall paſse for a Law or not .

‌ The Draught for the Repealing of Laws , passed the\
first time as followeth ;

‌ For the Repealing of any Law or statute , or any part\
thereof , the Repeal shall be propounded and Voted in ~ ~\
Council , on two several dayes of their Meeting : The first\
day the Question to be resolved by Vote , shall be to this\
Effect , Viz^t^. whether the Repeals of such statutes or such\
part thereof , shall be [*canceled:* ‘~~repeated or not~~’] propounded at ~\
another Meeting : The second day , the Question shall be\
to this Effect , viz^t^. whether ſuch statute or such part\
thereof , shall be Repealed or not . And in cases the —\
said Repeal be agreed unto , the same shall be recorded\
in the Journal-book of the Council[*erased:* ‘e’] , and the statute\
or part of the Statute Repealed , shall be Cancelled in\
the Statute-books .

‌ Ordered , That the Council do met henceforth on\
Mundayes about 3 of the Clock in the afternoon .~

[…]

\TranscriptionStop

### 22 June 1663 (CMO/1 pp. 13–15; CMO/1/7)

\TopicsStart
“A Draught to be brought in for a Statute concerning the Laws for Strangers in the Meetings.” (CMO/1)
“[Vice Presidents] Appointed” (CMO/8A)
“Wilkins John D.D. \| Is Secretary. by Charter \| Appointed V.P.” (CMO/8A)
“Statutes \| For the Making & repealing of Laws” (CMO/8A)
“A Draught to be brought in for a Statute concerning the Laws for Strangers in the Meetings.” (CMO/1)
“[Order of making a Law, concerning the President.] A Law concerning the same.” (CMO/1)
\TopicsStop

\TranscriptionStart

[**Page 13:**]

[…]

[*centered:*] June 22 . [1663 .]{.underline}

[…]

[**Page 14:**]

‌ The Draught for Repealing of Laws paſsed this day\
the second time , as it stands recorded Jun 17. 1663 . —\
without any Alteration .

‌ The Draught for the time of the Ordinary Meetings\
of the Society , paſsed this day the first time , as follow⹀\
eth .

‌ The Ordinary Meetings of the Society , shall be ~\
weekly upon Wednesday , beginning about 3 of the Clock ,\
in the afternoon , and continuing untill six ; unleſse the\
Major part of the Fellows present , shall , for that time ,\
resolue to rise sooner , or sit longer : And no Fellow\
shall depart , without giving notice to the President .

‌ Ordered , That the Committee bring in a Draught for\
a Statute concerning the Leaue to be given to thoſe\
that shall desire to be present at the Meeting of the\
Society , both Natiues and Forreigners , that are not\
Fellows thereof .

[…]

[**Page 15:**]

[…]

‌ The Committee offered a Law for the President’s being\
covered ;

‌ The President , being in the Chair , is to be covered ,\ —\
notwithstanding the Fellows of the Society be uncovered ,\
whilst they speak to him .

\TranscriptionStop

### 24 June 1663 (CMO/1 pp. 15–16; CMO/1/8)

\TopicsStart
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A)
“[Obligation of the Fellows. Form of Minutes about] Alterations made therein [to the form, TODO(confirm CMO/1)]”  (CMO/8A)
\TopicsStop

\TranscriptionStart

[**Page 15:**]

[*centered:*] June . 24 . [1663]{.underline} .

[…]


‌ The Committee for Laws , presented the draft of a Statute\
for permitting those that are not of the Society, to be pre⹀\
sent at their Meetings ; and the Members of the Council ~ -\
present approved thereof, as followeth ;

‌  When the President taketh the Chair , the rest of the\
Fellows shall place themselues orderly and conveniently for\
the Busineſse of the Meeting : And those persons , that are —\
not of the Society , shall withdraw ; Excepting that it\
shall be free , for any of his Ma^ties^. Subjects of England , —\
Scotland or Ireland , having the Title and place of a ~ ~\
Baron ; or any of his Ma^ties^. Privy-Council of any of his\
three Kingdoms , and for any Forreigner of eminent ~ ~\
repute , with the Allowance of the President , to stay for\
that time : And that it shall be free for any other\
person upon leaue obtained of the President and Fellows [**page 16:**]\
present , or the major part of them , to stay for that time ;\
And the Name of every person permitted to stay , of any\
person that moved for him , and the allowance shall be —\
entered in the Journal-book .

‌ It was considered ; whether in the Statute for Subscri⹀\
bing the Obligation , which paſsed once (June 17) the ~ ~\
words , [Or to be admitted]{.underline} , were not superfluous , and the\
Members present having judged them to be so, debated ,\
whether those words should be struck out of the said Sta⹀\
tute , already paſsed without repealing them ; Or whether\
they , or their whole Law , should be first repeated .  It was\
thought fit to referr it to a full Council .

‌ They considered also a Statute for electing Noble⹀\
men , and agreed upon that which followeth ;

‌ Every One of His Ma^ties^. Subjects of England , Scotland\
or Ireland , having the Title and Place of a Baron ~\
or having any higher Title and Place ; And every one —\
of his Ma^ties^. Privy-Council in any of the said Kingdoms\
upon his desire to be a Fellow of the Society , shall\
be propounded and put to the Vote for Election , on the\
same day (if there be present , a competent Number\
for making Elections ) if otherwise , it shall be free\
for every such person , to be present at any Meeting\
of the Society , untill there be such a Number —\
present.

‌ Ordered , That the President take care , that ~\
there be always a blanck Deputation remaining —\
with the Council, in case of his or his Deputies —\
absence.[*downward flourish*]

\TranscriptionStop

### 1 July 1663 (CMO/1 pp. 17; CMO/1/9)

\TopicsStart
“[Obligation of the Fellows.] Alterations made therein” (CMO/8A)
“[Obligation of the Fellows.] Form of Minutes about” (CMO/8A)
\TopicsStop

\TranscriptionStart

[**Page 17:**]

[*centered:*] July . 1 . [1663]{.underline} .

[…]

‌ The Statute for the Ordinary Meetings of the Society ,\
paſsed , without alteration , the Second time , having paſsed —\
the first time , the 22 June .

‌ Voted , that the Statute for subscribing the Obligation,\
as it stands recorded June 17^th^. , with these words , [Or to]{.underline}\
[be admitted]{.underline} , be Repealed .

‌ The same Statute (Excepting these words , [or to be ad⹀]{.underline}\
[mitted]{.underline} ) paſsed the first time , inserting these words , [Or to]{.underline}\
[be Elected]{.underline} , twice ; first , between the words , [Elected]{.underline} , and\
[shall subscribe]{.underline} : ſecondly , between the words [Elected]{.underline} and\
[shall refuse]{.underline} .

[…]

\TranscriptionStop

### 6 July 1663 (CMO/1 pp. 18–20; CMO/1/10)

\TranscriptionStart

[**Page 18:**]

[*centered:*] July . 6 . [1663]{.underline} .

[…]

[**Page 19:**]

[…]

‌ M^r^. Hook was charged to show his Microscopial Ob⹀\
ſervations , in a handsome book , to be provided by him for\
that purpose .  Item , to weigh the Air both in the ~ ~\
Engine and abroad .  Item , to break Empty Glaſse-balls ;\
as also to let the Water aſcend into them , after they ~\
haue been emptied .  Item , to provide the Instrument for\
finding the differeing preſsure , of the Atmosphare in the\
same place : as also the Hygroscope made of the beard\
of a wilde Oate. 

‌ Ordered , That M^r^. Slingsby giue directions for the\
Graving of the Society's Arms upon their Mace .[**Page 20:**]

‌ Voted‸^[y^e^\ 2^d^\ time ,]{}^ That the Statute for Subscribing the Obli⹀\
gation , as it stands recorded June 17^th^. with these\
words , [or to be admitted]{.underline} , be repealed . [*canceled:* ‘~~the second~~’]\
[*canceled:* ‘~~time .~~’]

‌ The same Statute (except those words , [or to be]{.underline}\
[admitted]{.underline} ) paſsed the second time , inserting these —\
words , [or to be elected]{.underline} , twice ; first between the ~\
words , [Elected]{.underline} , and , [shall subscribe]{.underline} ;  Secondly ; ~\
between the words , [Elected]{.underline} , and [shall refuse]{.underline}.\
Vide Jun . 17 . et Jul : 1.[*downward flourish*]

\TranscriptionStop


### 13 July 1663 (CMO/1 pp. 20–21; CMO/1/11)

\TranscriptionStart

[*centered:*] July . 13 . [1663]{.underline} .

[…]

‌ The Statute for weekly payments paſsed the first\
time , as followeth ;  Every Fellow of the Society shall [**page 21:**]\
pay One shilling by the week , towards the Charges of\
Experiments and other Expenses of the Society , So long\
as he shall continue a Fellow thereof ; which continu⹀\
ance shall be accounted from the time of his Ad⹀\
miſsion , until such time as he shall signify , to ~\
the President under his hand , that he desireth to\
withdraw from the Society ; or untill , upon any other\
Account , he shall cease to be a Fellow thereof :\
And if any Fellow shall refuse to pay according to\
the Rate aforesaid , he shall be Ejected out of the\
Society , Except the said Payment be remitted in\
whole or in part, by Special Order of the Council.

‌ Voted , the first time , that in the Statute for\
making of Laws already paſsed , bee between the —\
words , [shall be]{.underline} and [whether]{.underline} , inserted in both places\
of the Statute , these words , [To this Effect, Videlicet]{.underline} .

‌ Resolved , That in one of the two Scutcheons of the\
Society's Mace , be engraven the whole Armes of the\
said Society , and in the other, the following Inscriptions ;

[*ten-line, centered pen facsimile:*]\
Ex Munificentia\
Augustiſsimi Monarchæ\
C A R O L E II.\
D. G.\
Magnæ Britanniæ , Franciaæ et Hibern : Regis &c.\
Et\
Societatis Regalis Londini ad Scientiam\
Naturalem promovendam institutæ, Fundatoris\
et Patroni\
A^o^. 1663 .

\TranscriptionStop


**(CMO/1 p. 19,21):** “[The Armes of the Society] Order for the Grauing the said Armes upon y^e^ Mace.” (CMO/1)

**(CMO/1 p. 20: 7,9,10,11,16,17,20):** “[Obligation of the Fellows.] Form of Minutes about” (CMO/8A)

**(CMO/1 p. 21: 19,21):** “[The Armes of the Society] Order for the Grauing the said Armes upon y^e^ Mace.” (CMO/1)

**(CMO/1 p. 21: 12,13,14,21,24,30):** “Statutes \| For the Making & repealing of Laws” (CMO/8A)

**(CMO/1 p. 21,23):** “Voted for inserting few words in the Statute for making of Laws.” (CMO/1)


### 20 July 1663 (CMO/1 pp. 22; CMO/1/12)

\TranscriptionStart

[**Page 22:**]

[*centered:*] July 20 . [1663]{.underline}

[…]

\TranscriptionStop

### 27 July 1663 (CMO/1 pp. 22–23; CMO/1/13)

\TranscriptionStart

[*centered:*] July . 27 . [1663]{.underline} .

[…]

‌ The President , upon the desire of the Council , [**page 23:**]\
charged himselfe with the Survey of the Accompts of the\
Treasurer .

‌ Voted , the sencond time ; That in the Statute for ~ ~\
making of Laws , be , between the words, [shall be]{.underline} , and , ~\
[whether]{.underline} , inserted in both places of the said Statute ,\
these Words , [To this Effect , Videlicet]{.underline} .

‌ The Statute for Weekly payments paſsed the second\
time , as it stands Recorded abue , July 13 . [1663 ]{.underline}.

‌ The 2 . 3 4^th^. and 5^th^. Statutes of the 4^th^. Chapt. concerning\
the Ordinary Meetings of the Society : Item , the 1 . 2 .\
3 . 4 .^th^ 5 . 6 . and 7 .^th^ Statutes of the 6^th^. Chapt. concerning\
the Election and Admiſsion of Fellows , paſsed the first —\
time .[*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 23: 21,23):** “Voted for inserting few words in the Statute for making of Laws.” (CMO/1)

**(CMO/1 p. 23):** “Subscription of the Fellows” (CMO/8A)


### 3 August 1663 (CMO/1 pp. 23–24; CMO/1/14)

\TranscriptionStart

[*centered:*] August 3 . [1663]{.underline} .

[…]

‌ The President acquainted the Council , that S^r^. Gilbert ~\
Talbot had sent to him , without taking any Fees , the\
Mace bestowed by his Ma.^ty^ upon the Society ; and that\
he , the said President , had , in the Book at his Ma^ties^. ~ ~\
Jewell-house , acknowledged the Receipt thereof , for the ~\
Society .

[…]

[**Page 24:**]

[…]

‌ Resolved , that the Clerk and the Operator be —\
Mace-bearers pro tempore .

‌ Ordered , That the Lord Brouncker , D^r^. Wilkins , ~\
D^r^. Goddard , M^r^. Palmer , M^r^. Hill and M^r^. Oldenburg ,\
do meet next Munday next, about 2 of the Clock in D^r^.\
Goddards chamber , to examine the Accounts of the\
Treasurer concerning the Society .

‌ The 2 . 3 . 4 .^th^ and 5^th^. Statutes of the 4^th^. Chapt. Item\
the 1 . 2 . 3 . 4^th^. 5^th^. 6^th^. and Seventh Statutes of the 6^th^\
[*canceled:* ‘~~Statute~~’] Chapt. paſsed the 2^d^. time .

\TranscriptionStop


### 10 August 1663 (CMO/1 p. 24; CMO/1/15)

\TranscriptionStart

[*centered:*] August 10^th^.[ 1663]{.underline} .

This Council-day nothing was done , for want of ~ -\
Company .

\TranscriptionStop


### 17 August 1663 (CMO/1 pp. 24–25; CMO/1/16)

\TranscriptionStart

[*centered:*] August . 17^th^. [1663]{.underline} .

[…]

[**Page 25:**]

‌ The 1 . 3 . 4^th^. and 5^th^. Statute of the 3 . Chapt :  Item , the\
1 . 2 . 3 and 4^th^. Statute of the 8^th^. Chapt . paſsed the first time.

‌ Ordered , to make a Statute , that the President shall ~\
not preside in any Council or Committee transacting any ~\
busineſse , wherein he is a Party .[*downward flourish*]

‌ M^r^. Ashmole presented the Society , with a Picture of their ~\
Armes , for which he received thanks .[*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 24: 12,13,14,21,24,30):** “Statutes \| For the Making & repealing of Laws” (CMO/8A)

**(CMO/1 p. 24,53,55):** “Deputies for examining the Accompts of the \| Treasurer” (CMO/1)

**(CMO/1 p. 24):** “The Clerk & the Operator pro tempore Mace bearers.” (CMO/1)


### 24 August 1663 (CMO/1 p. 25; CMO/1/17)

\TranscriptionStart

[*centered:*] August 24 [. 1663 ]{.underline}.

[…]

‌ The Treasurers Papers were begun to be examined , both as\
to the Payments made by the Felows to him , and the Bills\
of Payments made by him for the use of the Society .

[…]

\TranscriptionStop


**(CMO/1 p. 25):** “[Deputies for examining the Accompts of the \| Treasurer] Examined.” (CMO/1)

**(CMO/1 p. 25):** “Pictures or Portraits. \| Of the Societies Arms, presented by M^r^. Ashmole.” (CMO/8A)

**(CMO/1 p. 25):** “[Deputies for examining the Accompts of the \| Treasurer] Examinacon continued.” (CMO/1)


### 31 August 1663 (CMO/1 p. 26; CMO/1/18)

\TranscriptionStart

[**Page 26:**]

[*centered:*] August 31 . [1663 ]{.underline}.

[…]

‌ The Examination of the Treasurers Accounts was con⹀\
tinued , and the Charge , given him by the President , found\
to agree with what the Treasurer acknowledgeth to haue\
been received by him , and to be yet owing to him by the ~\
Fellows .

[…]

\TranscriptionStop

### 7 September 1663 (CMO/1 pp. 26–27; CMO/1/19)

**(CMO/1 p. 26):** “[Vice Presidents] A Blank Deputation to be always with the Council” [is this a letter or a form TODO(confirm)] (CMO/8A)

\TranscriptionStart

[…]

[*Centered:*] September . 7^th^. [1663]{.underline} .

[…]

[**Page 27:**]

‌ Ordered , That the Amanuensis take care constantly to ~\
provide good paper , pens and Ink , for the Meetings of the ~\
Society and Council .

‌ The Treasurers Accounts being further considered , [*canceled:* ‘~~the~~’]\
the President undertook to set down the Result of all , to ~\
be recorded .

‌ Ordered that an Inventory be made of the Goods of —\
the Society , from the Journalls‸^[of y^e^ Society ,]{}^ and the Bills of Payments —\
made by the Treasurer ;  and that to this End , M^r^. Hill peruse\
the said bills , to see what things the Society had paid for ,\
and the Amanuensis do look over the Journall , to see what\
books and other things had been presented to the Society .

‌ The 1 . 2 . 3 . 4 . and 5^th^. Statutes of the third Chapter : —\
Item , the 1 . 2 . 3 . and 4^th^. Statutes of the Eighth Chapter paſs’d,\
with a few alterations , the second time .

[…]

\TranscriptionStop


**(CMO/1 p. 27,28,39):** “[Inventory to be made of the Goods of the Society] and of the Bills of payments.” (CMO/1)

**(CMO/1 p. 27,38):** “Inventory to be made of the Goods of the Society” (CMO/1)

**(CMO/1 p. 27):** “Bills of payment to be perused.” (CMO/1)

**(CMO/1 p. 27):** “Amanuensis charged to see what Books & other things \| have been presented.” (CMO/1)

**(CMO/1/, v.1 p. 27):** “Amanuensis, to have paper, pens & ink always ready” (CMO/8A) \index{Amanuensis+to have paper, pens & ink always ready}

**(CMO/1/, v.1 p. 27,28,38:)** “Inventory. Orders about the Society’s Goods” (CMO/8A)


### 14 September 1663 (CMO/1 pp. 27–28; CMO/1/20)


\TranscriptionStart

[*centered:*] September . 14[  . 1663]{.underline} .

[…]

‌ The Statutes for the Election of the Council and ~ ~\
Officers were read , but could not be paſse for want of a ~\
Quorum .

[**Page 28:**]

‌ The Inventory , formerly committed to M^r^. Hill, being\
called for , it was found that he could not make it hi⹀\
therto , for want of the Bills , which the Treasurer ~\
had not yet delivered to him .

\TranscriptionStop

### 22 September 1663 (CMO/1 p. 28; CMO/1/(20+1))

\TranscriptionStart

[*centered:*] Septemƀ[er] . 22 [. 1663 .]{.underline}

[…]

‌ D^r^. Goddard was desired to bring in a draught for\
a Statute of laying up all the Societye’s moneys in a ~\
Chest , except 20^~~l~~^ to be left in the Treasurers hands ~\
for occasionall Expenses .

\TranscriptionStop

**(CMO/1/, v.1 p. 28: 27,28,38:)** “Inventory. Orders about the Society’s Goods” (CMO/8A)

**(CMO/1 p. 28):** “Order for collecting of the Arrears renewed” (CMO/1)

**(CMO/1 p. 28):** “Distinct Tables Ordered for the Laws & By Laws.” (CMO/1)


### 28 September 1663 (CMO/1 pp. 28–30; CMO/1/21)

\TranscriptionStart

[*centered:*] Septemƀ[er]. 28 [ . 1663 .]{.underline}

[…]

‌ Voted , that there shall be two distinct Tables ; one for\
the Lawes contained in the Charter , and another for —\
the By-Lawes.

[**Page 29:**]

‌ D^r^. Goddard brought in a draught of the Statute Order⹀\
ed at an precedent Meeting : which was read , and paſsed the\
first time , as followeth :

‌ All Moneys , or summes of Money , whereof there shall not\
be present occasion for Expending or disposing to the use of —\
the Society , shall be Layd up in an Iron chest , having —\
three different Locks and keyes , whereof one shall be in —\
the Custody of the President ;  another , of the Treasurer ; —\
and a third, of one of the Secretaries : And the Treasurer\
shall not haue in his Custody aboue twenty pounds , at once,\
and accordingly he shall pay in moneys into the Chest —\
aforesaid , and take out of the same , by Order of the ~ ~\
Council as they shall judge Expedient.

‌ All the Statutes of the 5^th^. Chapt. concerning Experiments\
and the Reports thereof ;  Of the 7^th^. concerning the Election of\
the Council and Officers ;  Of the 9^th^. concerning the Treasurer\
and his Accounts ;  Of the 11^th^. concerning Curators by Office ;\
Of the 12^th^. concerning the Clerk ;  of the 13^th^. concerning the ~\
Operators ;  Of the 14^th^, concerning the Common-seal and Deeds ;\
Of the 15^th^. concerning the Books of the Society ; Of the 16^th^. ~\
concerning Benefactors ;  Of the 17^th^. concerning the Death or\
receſse of any Fellow ;  Of the 18^th^. concerning the Causes and\
forme of Ejection : all these paſsed the first time , this\
day .

‌ Resolved , That an Order be iſsued, for the Collecting of the\
Weekly payments, as followeth ;

[*two lines centered:*] Munday , Septemƀ^r^. 28 , 1663 .\
At a Meeting of the Council of the Royall Society .

‌ Ordered , That all persons , that haue been Elected and ~ ~\
Admitted into the Royall Society of London &c., do pay their ~\
whole Arrears , unto the 23^th^. day of September [. 1663]{.underline}.\
‌              Signed by the President. 

[**Page 30:**]

‌ The President was desired to giue himselfe the trouble ;\
and bring in the results of the Treausrers Accounts at the\
next meeting of the Council .

‌ The same was desired to take care , that notice may be\
given to M^r^. Symmons , the Graver of the Seal of the Society ,\
to attend the Council, at the next sitting .

\TranscriptionStop


### 5 October 1663 (CMO/1 pp. 30–31; CMO/1/22)

\TranscriptionStart

[*centered:*] October . 5 . [1663 .]{.underline}

[…]

‌ All the Statutes which paſsed on 28 Septemƀ. 28 , the first time ,\
paſsed this day the second time .

‌ The Accompts of the [*canceled:* ‘~~Society~~’] Treasurer , from the —\
beginning of the Society , untill June 24. 1663, paſsed ; by\
which it appeareth , that

|                                                | ƚi                  | s                  | d                |
|------------------------------------------------|---------------------|--------------------|------------------|
| The summe given in Charge , is - - - - - - - - | [697 :]{.underline} | [00 :]{.underline} | [00]{.underline} |
| where of received - - - - - - - - - - -        | 527 :               | 5 :                | 6                |
| In Arrears from divers Members of the          |                     |                    |                  |
| Society, untill June 24 . 1663 - - - - -       | 158 :               | 4 :                | 6                |

[**Page 31:**]

|                                           | ƚ     | s    | d. |
|-------------------------------------------|-------|------|----|
| Disbursed by the Treasurer - - - - - -    | 488 : | 8 :  | 3  |
| Remaining in the Treasurer’s hands - - -  | 38 :  | 17 : | 3  |
| Remitted to severall Members of the ~     |       |      |    |
| Society , by Order of the Council - - - - | 11 :  | 9 :  | —  |

‌ Ordered , That the President , S^r^. Robert Moray and M^r^.\
Boyle , do meet and consider of the draughts , brought in by\
D^r^. Wilkins and D^r^. Goddard , concerning the Entertainment ~\
and allowances of Curators by Office , when the Society shall\
be endowed with a Revenue .

[…]

\TranscriptionStop


**(CMO/1 p. 30):** “Accompts of the Treasurer from the beginning of the Royall Society until June. 24. 1663 passed” (CMO/1)

**(CMO/1 p. 30):** “The President desired concerning Trears Accompts” (CMO/1)

**(CMO/1 p. 30: 12,13,14,21,24,30):** “Statutes \| For the Making & repealing of Laws” (CMO/8A)

**(CMO/1 p. 30,34,67,68,69):** “Seal of the Society. \| Orders about procuring one” (CMO/8A)

**(CMO/1 p. 30):** “The Presidents desired concerning the Grauer of the Seal of the Society.” (CMO/1)


### 12 October 1663 (CMO/1 pp. 31–33; CMO/1/23)

\TranscriptionStart

[*centered:*] October . 12  . [1663 ]{.underline}.

[…]

[**Page 32–33:**]

[…]

‌ Colonel Long ,\
‌ To bring in his Apparatus formerly offered by him , ~\
July. 6. and for that purpose to be written to be M^r^. Oldenburg\

[…]

‌ Ordered , that a Book be provided by the Amanuensis for\
his Ma^ties^, Subscription .  [After he sees all the experiments]

‌ Voted the first time , that in the Statute of the Obligation\
to be subscribed , these words , [Or to be elected]{.underline} , be , in both the\
places wher they occurre in the Statute , repealed .[*downward flourish*]

[*Horizontal flourish*]

\TranscriptionStop

**(CMO/1 p. 31):** “Deputies for considering the Draughts concerning Curator by Office.” (CMO/1)

**(CMO/1 p. 33):** “A Book to be provided for his Ma^ts^: Subscription.” (CMO/1)


### 19 October 1663 (CMO/1 p. 34;  CMO/1/24) 

\TranscriptionStart

[**Page 34:**]

[*centered:*] October . 19. [1663 ]{.underline}.

[…]

‌ Voted the second time , that in the Statute for the Ob⹀\
ligation to be subscribed , these words ,  [Or to be elected ]{.underline}, be ,\
in both the places, where they occurre in this statute, repealed .

‌ The whole Body of the statutes were read , and ordered to\
be written fair by the Anamuensis.

‌ Ordered that M^r^. Hook haue the keeping of the Reposi⹀\
tory of the Society , for which the West-Gallery of Greſham\
College was appointed .

‌ Ordered , that one of the new Instruments for ~ ~\
sounding withou a Line , be by M^r^. Hook made ready amongst\
the other matters designed for his Ma^ties^. reception . [Later described in the *Transactions*]

‌ Vpon a proposal by M^r^. Boyle , concerning some overture\
made by another person , of considerable advantage to the\
Society , which in the nature of it , requires secrecy ,\
it was ,

‌ Ordered , that the Lord Brouncker , S^r^. Robert Moray ,\
M^r^. Boyle , and M^r^. Oldenburg , haue full power to treat and\
agree with the said person , upon such terms as they shall\
think reasonable , for the Good of the Society .

‌ Ordered , that the Common Seal of the Society , be ~\
produced and shown to the Society at their Meeting .[*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 34):** “The whole Body of the Statues hauing been read, & Ordered to be written faire.” (CMO/1)

**(CMO/1 p. 34: 30,34,67,68,69):** “Seal of the Society. \| Orders about procuring one” (CMO/8A)

**(CMO/1 , v. 1 p. 34:)** “[Hook M^r^. Robert.] Appointed keeper of the Repository” (CMO/8A)

**(CMO/1 p. 34):** “M^r^: Hook appointed Keeper of the Repository of the Society” (CMO/1)

**(CMO/1 p. 34: 16,17,34):** “[Obligation of the Fellows. Form of Minutes about] Alterations made therein” [to the form for minutes, right, or to the obligations TODO(confirm)] (CMO/8A)


### 26 October 1663 (CMO/1 p. 35;  CMO/1/25) 

\TranscriptionStart

[**Page 35:**]

[*centered:*] October . 26 . [1663 .]{.underline}

[…]

‌ The first and second Statute of Chapt. 11^th^. concerning ~\
the Allowance and busineſse of Curators by Office , passed ~\
the first time.

‌ Ordered , that D^r^. Wilkins and D^r^. Goddard , draw up a\
Statute for the duty of the Printers to the Society .

[…]

\TranscriptionStop

### 28 October 1663 (CMO/1 p. 35;  CMO/1/26) 

\TranscriptionStart

[*centered:*] October . 28 . [1663]{.underline}.

[…]

‌ The first and second Statute of Chapt. 11^th^. concerning the\
Allowance and Busineſse of Curators by Office, paſsed the ~\
second time.

‌ The Commiſsion for the Printers to the Society was read ,\
and M^r^. Palmer was desired to advise with M^r^. Ellis concern-\
ing the forme thereof .[*downward flourish*] 

\TranscriptionStop


**(CMO/1 p. 35):** “Order for drawing up a Statute concerning the Printer to the Society.” (CMO/1)

**(CMO/1 p. 35):** “The Comission for the Printers to the Society is Read.” (CMO/1)

**(CMO/1 p. 35,36):** “[The Comission for the Printers to the Society is Read.] The Form of the same.” (CMO/1)

**(CMO/1 p. 35):** “The Councils Meeting an hour or two before that of y^e^ Society.” (CMO/1)

**(CMO/1 p. 35,36,37,38):** “Printers to the Society. \| Orders concerning their commiſsion” (CMO/8A)

### 2 November 1663 (CMO/1 pp. 36–38;  CMO/1/27) 

\TranscriptionStart

[**Page 36:**]

[*centered:*] November . 2 . [1663 .]{.underline}

[…]

‌ M^r^. Palmer reported from M^r^. Ellise , that the draught\
of the Commiſsion for the Printers to the Society , was in —\
good forme .

‌ The said Commiſsion paſsed as followeth ,

The President Council and Fellows of —\
the Royall Society of London for improving Naturall\
Knowledge , To all persons who shall read or hear these\
presents, Greeting .  Whereas , by his Ma^ties^. Charter\
under his great Seal of England , is giuen and granted\
to us , full power and Authority to choose One or more\
Printer or Printers , for Matters and affairs relating\
unto , or concerning the said Society ; Wee do by these\
presents declare John Martin and James Allestree ~\
Citizens and Stationers of London , joyntly chosen , in\
due manner and forme , according to the said Charter ,\
to the Office of Printer to the Royall Society aforesaid ,\
and, according to the said Charter , sworne to deale ~\
faithfully and honestly in all things belonging to the\
trust committed‸^to^ them , as Printers to the said Society ,\
during their Imployment in that capacity :  And ~\
Wee do by these presents giue and graunt unto the\
said John Martin and James Allestree , full power ~ [**page 37:**]\
and priviledge , to print all such things, matters and busineſses\
concerning the Royall Society aforesaid , as shall be committed\
unto them , by the President and Council of the said Society , ~\
or any seven or more of them , (whereof the President alwayes\
to be one ) or by the major part of the said seven or more .\
And, Wee do further giue and grant unto the said John ~ ~\
Martin and James Allestree , that no other person , (except⹀\
ing any duely chossen and sworne as aforesaid ) shall print\
any of the said Things Matters and Busineſses concerning the\
Royall Society aforesaid , they , the said John Martin and ~ ~\
James Allestree , duly observing all the Orders and Directions\
of the President and Council , or the said ſeven or more of\
them aforesaid , concerning the Printing of all the said ~\
matters and buisineſses .  In witneſse whereof , Wee\
haue caused our Common Seale to be affixed unto these —\
presents , and the same to be signed by the President of ~\
the Royall Society aforesaid , this thirteenth day of January\
in the fifteenth year of our Soveraigne Lord Charles ~\
the second, by the grace of God of England, Scotland, France\
and Ireland King , Defender of the Faith &c Anno  ~\
Domini [1663]{.underline} .

‌ Ordered , That the Operator desire M^r^. Martin and ~\
M^r^. Allestree , to attend the Councill on Munday next , to\
review their Commiſsion , and to hear the Statutes concern⹀\
ing their duty , and to be sworn as Printers to the ~ ~\
Society , they having no exception against such Com̃iſsion\
and Statutes .

‌ The Statute of the duty of the Printers ; as also the\
Statute of a certain number of Printed Copies , to be ~\
presented by the Printers of the Society , both paſsed the\
first time .

[**Page 38:**]

‌ Ordered that M^r^. Hill survey the Bills of the Treasurer\
and draw out of them an Inventory of the things belonging to\
the Society . [*downward flourish*]

\TranscriptionStop




**(CMO/1 p. 36,37):** “[The Comission for the Printers to the Society is Read. The Form of the same.] Passed” (CMO/1)

**(CMO/1 p. 36: 35,36):** “[The Comission for the Printers to the Society is Read.] The Form of the same.” (CMO/1)

**(CMO/1 p. 36: 35,36,37,38):** “Printers to the Society. \| Orders concerning their commiſsion” (CMO/8A)

**(CMO/1 p. 37,39):** “The Statute of the Printers duty & of a certaine number of printed Copies passed—” (CMO/1)

**(CMO/1 p. 37: 36,37):** “[The Comission for the Printers to the Society is Read. The Form of the same.] Passed” (CMO/1)

**(CMO/1 p. 37):** “The Printers to the Society demanded to attend the Council for receiving their Comission.” (CMO/1)

**(CMO/1 p. 37: 35,36,37,38):** “Printers to the Society. \| Orders concerning their commiſsion” (CMO/8A)

**(CMO/1 p. 37):** Martyn printing, etc. \index{Martyn printing, etc.}

### 9 November 1663 (CMO/1 pp. 38–39;  CMO/1/28) 

\TranscriptionStart

[*centered:*] November . 9 . [1663 .]{.underline}

[…]

‌ Ordered, that the Amanuensis make a List of all the ~\
Fellows of the Society , without the Members of the Council ,\
in four Columnes ;[*over:* ‘Collumnes’] And another of all the Members of the\
Council , in a Columne[*over:* ‘Collumne’] by themselves , and that he deliure\
them to the Printer of the Society , to be printed ~ ~\
accordingly .

‌ Ordered that the Summons for the Anniversary ~ ~\
Election , be iſsued in the manner following ;

‌ These are to giue Notice, that on Munday the 30^th^.\
day of this instant November [1663]{.underline} ; being [S^t^. Andrew's]{.underline} ~\
day, the Council and Officers of the Royall Society , are ~\
to be elected for the year ensuing :  At which Election ,\
your presence is expected , at [Greſham-College ]{.underline}, at nine of\
the Clock in the forenoon precisely.\
‌               Brouncker P.RS

‌ John Martin and James Allestree had the Commiſsion —\
establishing them Printers to the Society , as also the two\
statutes concerning the duty of Printers, delivered to ~\
them , to consider thereof : They having withdrawn for [**page 39:**]\
that purpose , and, upon consideration , having no exception\
against any thing contained in the said Com̃iſsion and ~ ~\
Statutes , ~[*overwriting, uncertain:* ‘&c’] were sworne Printers to the Society .

‌ The Statute of the Duty of the Printers : And the ~ ~\
Statute of a certain number of Printed Copies , to be presented\
by the Printers of the Society , both paſsed the second time

‌ Ordered , that the Treasurer do pay to the Amanuensis ~\
of the Society , seven pounds eleven shillings , according to\
his Bill .[*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 38):** “The Survey of the Bills of the Treasurer demanded.” (CMO/1)

**(CMO/1 p. 38: 35,36,37,38):** “Printers to the Society. \| Orders concerning their commiſsion” (CMO/8A)

**(CMO/1/, v.1 p. 38: 27,28,38):** “Inventory. Orders about the Society’s Goods” (CMO/8A)

**(CMO/1, v.1 p. 38):** “Lists of the Society \| To be printed” (CMO/8A)

**(CMO/1 p. 38):** “Summons. Anniversary. Form of” (CMO/8A)

**(CMO/1 p. 38):** “Forme of the Summons for the Anniversary Election” (CMO/1)

**(CMO/1 p. 38):** “A List of all the Members of the Council to be made & printed.” (CMO/1)

**(CMO/1 p. 38: 27,38):** “Inventory to be made of the Goods of the Society” (CMO/1)

**(CMO/1 p. 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)

### 16 November 1663 (CMO/1 pp. 39–40;  CMO/1/29) 

\TranscriptionStart

[*centered:*] November . 16 . [1663 ]{.underline}.

[…]

‌ Ordered, that a Preſse or two , as bigg as a Porter can ~\
conveniently carry , be provided for the use of the Society ; and\
that D^r^. Goddard direct the Operator accordingly .

‌ M^r^. Hill brought in the Inventory , drawn out of the ~ ~\
Treasurer's Bills , concerning the things belonging to the Society;\
which Inventory was Ordered to be delivered by the ~ ~ ~ [**page 40:**]\
Secretary [Mr Oldenburg] to M^r^. Hook , as Keeper of the Repository .

‌ D^r^. Goddard also brought in a list of the things comitted\
to his Custody ; which Lists also , was ordered to be delivered\
to M^r^. Hook . [*downward flourish*]

\TranscriptionStop



**(CMO/1 p. 39: 37,39):** “The Statute of the Printers duty & of a certaine number of printed Copies passed—” (CMO/1)

**(CMO/1 p. 39):** “Order concerning a Boy to be found out for y^e^ use of y^e^ Society.” (CMO/1)

**(CMO/1/, v.1 p. 39):** “[Amanuensis] his fee for writing” (CMO/8A) \index{Amanuensis+his fee for writing}

**(CMO/1 p. 39):** “Paper. Pens, and Ink to be provided by the Amanuensis.” (CMO/1)

**(CMO/1 p. 39):** “Money paid to the Amanuensis.” (CMO/1)



### 23 November 1663 (CMO/1 pp. 40–41;  CMO/1/30) 

\TranscriptionStart

[*centered:*] November . 23 . [1663 .]{.underline}

[…]

‌ M^r^. Symmonds brought in the manual Seale , containing ~\
the Crest of the Armes of the Society .

‌ D^r^. Goddard had this Seal delivered to him .

‌ Ordered, that the Treasurer pay to M^r^. Symmonds the ~\
sum̃e of twenty five pounds in full of his Bill , for both the\
Seales of the Society , and the other particulars contained ~\
in the said Bill .

‌ The President was desired to let M^r^. Sym̃onds come to\
him , and show the way of making the Impreſsions of the\
Seales .

[…]

\TranscriptionStop

**(CMO/1/, v.1 p. 39,40):** “[Inventory.] For D^r^ Hooke & D^r^. Goddard” (CMO/8A)

**(CMO/1 p. 39,44):** “Order concerning a Presse to be provided.” (CMO/1)

**(CMO/1 p. 39):** “Printers established to the Society, & hauing recieved the two Statutes concerning their duty, & nothing to except, sworne to the Society.” (CMO/1)

**(CMO/1 p. 40):** “List of things committed ot the custody of D^r^: Goddard.” (CMO/1)

**(CMO/1/, v.1 p. 40: 39,40):** “[Inventory.] For D^r^ Hooke & D^r^. Goddard” (CMO/8A)

**(CMO/1 p. 40):** “[Seal of the Society.] Delivered to D^r^. Goddard. & the Expence” (CMO/8A)

**(CMO/1 p. 40):** “Manual Seale of the Society. \| Delivered to etc.” (CMO/1)

**(CMO/1 p. 40):** “President desired to shew the way of making impressions of the Seales.” (CMO/1)

### 30 November 1663 (CMO/1 p. 41;  CMO/1/31) 

\TranscriptionStart

[**Page 41:**]

[…]

[*centered:*] November . 30[‌ 1663]{.underline}.

The Council did not sit , this being the day of the anniversary\
Election of the Council and Officers .

\TranscriptionStop

### 7 December 1663 (CMO/1 pp. 41–42;  CMO/1/32) 

\TranscriptionStart

[*centered:*] December . 7 . [1663]{.underline} .

[…]

‌ Vpon Debate , whether the Eleven, continued of the old ~ ~\
Council , should be now sworne ; the Council , after mature con⹀\
sideration [*canceled:* ‘~~,~~’] of the words of the Charter , concerning this ~ ~\
particular ; was satisfied , that they were not to be sworne\
anew ; [*canceled:* ‘~~Also~~’]‸^But,^ the two secretarys elected again for another ~\
years , were judged to be obliged by the words of the Charter\
to renew their Oath , which they did accordingly .

[**Page 42:**]

‌ Ordered , That the President , S^r^. Anthony Morgan , M^r^. Hill\
M^r^. Colwall , and one of the Secretaries , or any three or more of\
them (whereof the President and one of the Secretaries shall be\
two)[*over:* ‘,’] be a Committee for examining of M^r^. Balle's Accounts\
from Midsum̃er till S^t^. Andrew's day.

[…]

‌ Voted , that the Experiments to be made on Wednesdayes at\
the Ordinary Meetings of the Society , be considered of by the\
Council on Mundayes , whether they are fit and ready : as.\
also , that some considerable Experiments be had in reserue ~\
for extraordinary occasions .

‌ Ordered , that D^r^. Merret , D^r^. Whistler and M^r^.  Hoskins\
doe peruse the Books of the Society , wherein the Experim^ts^.\
and other Philosophical matters , treated of at their Meetings ,\
are recorded , together with their Journal Books ; and ~\
consider , which do relate to and depend upon one another ;\
as also wherein they may be defectiue , and how further ~\
to be prosecuted .[*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 41,82):** “Secretaries to the Society. \| To be sworn anew at every Election” (CMO/8A)

**7 Dec 1663 (CMO/1/32, v. 1 pp. 41–2):** \index{Books+Orders to review}

**(CMO/1/, v. p. 42):** “Inventions. How to secure them to their Authors” (CMO/8A)

**(CMO/1 p. 42):** “A Committee ordered for examining of M^r^. Balls Accompts.” (CMO/1)

**(CMO/1 p. 42):** “Some appointed of the Society to puse their Books of Experi⹀ \| mentall & other Philosophicall Matters for examining the same —” (CMO/1)

**14 Dec 1663 (CMO/1/33, v. 1 pp. 43–44):**

### 14 December 1663 (CMO/1 pp. 43–44;  CMO/1/33) 

\TranscriptionStart

[**Page 43:**]

[*centered:*] December . 14 . [1663 ]{.underline}.

[…]

‌ M^r^. Balle promised to the Society under his hand , as a ~\
present , the sum̃e of one hundred pounds Sterling , to be ~\
paid in , before the first of Aprill next ; and presented ~\
them also with an iron Chest , having three Locks and Keyes.

‌ Ordered that S^r^. Anthony Morgan do peruse the Statutes\
of the Society , and having made his Annotations upon them ,\
as he shall see cause , to communicate them to the rest\
of the new Members of the Council , and upon joynt conside-\
ration had thereof , to make a Report to the Council . 

‌ S^r^. Anthony Morgan took the Statute-book home with\
him accordingly .

[…]

‌ Ordered that the Secretary bring in a List of the [**page 44:**]\
Names of all the Benefactors to the Society , together with\
their Donations , and the time when they presented them . 

‌ Ordered , that the Benefactors be registered , in loose\
Vellum-sheets .

‌ Ordered , that D^r^. Goddard take care that a Preſs or\
two be provided for the use of the Society .

‌ Ordered , that M^r^. Palmer consult M^r^. Ellis , whether\
the Charter of the Society speake fully enough to impower\
the Council for granting Licence to their Printers , to ~ ~\
print such books as shall be committed to them by the Society\
concerning their Design and work . [*downward flourish*]

\TranscriptionStop


“[Donation.] The Iron Chest by M^r^. Ball” (CMO/8A)

**(CMO/1 p. 43):** “Weather-clock, its charge to be estimated by D^r^. Wren” [a written report TODO(confirm)] (CMO/8A)

**(CMO/1 p. 43,49,76):** “[Statutes] Submitted to the perusal of Lawyers, & reported” (CMO/8A)

### 21 December 1663 (CMO/1 pp. 44–46;  CMO/1/34) 

\TranscriptionStart

[*centered:*] December [. 1663 .]{.underline}

[…]

‌ Ordered , that D^r^. Goddard and M^r^. Palmer , be added to\
the Com̃ittee for reveiwing the Statutes ; and that this\
Committee pitch upon particulars , wherein they desire —\
alterations

[**Page 45:**]

‌ M^r^. Palmer reported , it was the Opinion of M^r^. Ellis , ~\
that the Society's Charter speaks fully enough to Autho⹀\
rize the Council for granting Licence to their Printers ,\
to print such Books , as shall be comitted to them by the Society\
concerning their Busineſse .

‌ Ordered, that M^r^. Martin and M^r^. Allestree be sum̃oned\
by the Operator , to attend the Council , on wednesday next , about\
one of the Clock , to receive their Commiſsion Sealed .

‌ Ordered, that M^r^. Evelyn haue leaue to print the fiue\
Discourses concerning Cider , formerly brought in and read\
at severall Meetings of the Society , written by S^r^. Paul\
Neile , D^r^. Smith , M^r^. Beale , M^r^. Newburg , and Captaine\
Taylor : as also D^r^. Goddard’s discourse concerning the\
Textures of Trees .

‌ Reſolved , that no Book be printed by Order of the\
Council , which hath not been perused and considered by[*over:* ‘to’] two\
of the Council , who shall report , that such book containeth\
nothing , but what is sutable to the Deſign and Works of\
the Society.

‌ Ordered that D^r^. Goddard and D^r^. Merrett , peruse ~ ~\
M^r^. Evelyn's Book called Sylva ,  together with the ~\
[Appendix of Fruit-Trees]{.underline} ,  and the [Calendarium Hortense ,]{.underline}\
and make their Report to the Council , according to the —\
next precedent Order .

‌ Resolved , That the Forme of the [Imprimatur]{.underline} giuen by\
the Council of the Society , to their Printer , be as Foll⹀\
oweth.;

‌ By the Council of the [Royal Society]{.smallcaps} of London\
for improving Natural Knowledge, Ordered, that —\
\_\_\_\_\_ Book, be printed by John Martin and James ~\
Allestree , Printers to the said Society .  Dat: \_\_\_die\
\_\_\_\_\_\_\_\_ mensae[*overwriting, uncertain:* ‘mensis’] \_\_\_\_\_\_\_ Anno \_\_\_\_\_ .

‌                    Signed by the President .

[**Page 46:**]

‌ Ordered , that D^r^. Merrett and D^r^. Whistler inquire of —\
some of the College of Physicians , concerning the Forme of\
the Warrant for Bodies to be demanded from the Sheriffs of\
London for dissection , and make report thereof at the next\
meeting of the Council .

[…]

\TranscriptionStop

**(CMO/1/34, v. 1 pp. 44–6):** “[Benefactors & Benefactions]  Lists, or Tables, of them ordered” (CMO/8A) \index{Benefactors+Lists or tables ordered}

**(CMO/1 p. 44: 39,44):** “Order concerning a Presse to be provided.” (CMO/1)

**(CMO/1 p. 44):** “A List of the Benefactors to the Society to be brought in.” (CMO/1)

**(CMO/1 p. 44,45):** “Whether the Charter of the Society enabled the Council of the same to give Lycence to Printers.” (CMO/1)

**(CMO/1 p. 44,45):** “[Printers to the Society.] About Licensing of Books” (CMO/8A)

**(CMO/1 p. 44):** “The Benefactors to the Society to be registred in loose ~ \| vellum ſheets.” (CMO/1)

**(CMO/1 p. 45):** “Form of Imprinmatur given to the Printer—” (CMO/1)

**(CMO/1 p. 45,47,49):** “[The Printers to the Society demanded to attend the Council for receiving their Comission.] Sealed” (CMO/1)

**(CMO/1 p. 45: 44,45):** “Whether the Charter of the Society enabled the Council of the same to give Lycence to Printers.” (CMO/1)

**(CMO/1 p. 45):** “A Discourse of Texture of Trees Licensed.” (CMO/1)

**(CMO/1 p. 45):** “Order concerning M^r^: Evelyns Book to be ꝑ[per]used of Deputies” (CMO/1)

**(CMO/1 p. 45):** “Order concering Books to be printed.” (CMO/1)

**(CMO/1 p. 45):** “Five Discourses concerning Cyder to be printed.” (CMO/1)

**(CMO/1 p. 45):** “Printing of Papers & Books. Discourses read at the Society, by leave from the Council” (CMO/8A)

**(CMO/1 p. 45):** “[Printing of Papers & Books.] Council may order, after being perused by two of them” (CMO/8A)

**(CMO/1 p. 45: 44,45):** “[Printers to the Society.] About Licensing of Books” (CMO/8A)

**(CMO/1, v.1 p. 45):** “Imprimatur. Form of” (CMO/8A)

**(CMO/1 p. 46):** “[Seal of the Society.] Fee for affixing it  (an Angel)” (CMO/8A)

**(CMO/1 p. 46):** “The Fee for affixing the Comon Seale” (CMO/1)

**(CMO/1 p. 46):** “Fee of the Amanuensis for writing in Parchm^t^:” (CMO/1)

### 6 January 166$\frac{3}{4}$ (CMO/1 pp. 46–47; CMO/1/35)

\TranscriptionStart

[*in left margin at head:*] ✓

[*centered:*] January . 6[ . 1663.]{.underline}

[…]

‌ Voted , that the Fee of the Secretaries , for affixing the\
Common Seal of the Society , [*canceled:* ‘~~to~~’] be an Angill .

‌ Voted , that the Fee of the Amanuensis for every ~ —\
writing in Parchment , to which the Common Seale of the\
Society is affixed , be fiue shillings at least ; and more ,\
according as the Writing may be larger , at the ~ ~\
appointment of the Council .

[**Page 47:**]

‌ Ordered , that M^r^. Balle finish his Accounts , against the\
Next Meeting .

‌ D^r^. Merrett and M^r^. Hoskins made some report of the —\
Books of the Society : A fuller report thereof was refe‸^r^ed\
untill the next meeting of the Council .

‌ Resolved that the Commiſsion for the Printers , be ~\
Sealed at the next Meeting : for which they are to be ~ ~\
ſummoned by the Operator , then to attend the Council .

‌ D^r^. Whistler produced the Forme of the Warrant used=\
by the Colledge of Physitians : And it was ordered , that S^r^.\
Anthony Morgan be desired to fit that Warrant for —\
the use of the Society .

‌ D^r^. Merrett was desired to prosecute his Collection\
of the Curious things of Nature , to be found in England ,\
and to present the Society therewith .

[…]

\TranscriptionStop

“[Amanuensis] his fee for writing” (CMO/8A)


### 13 January 166$\frac{3}{4}$ (CMO/1 pp. 47–49;  CMO/1/36) 

\TranscriptionStart

[*centered:*] January . 13 [. 1663 ]{.underline}.

[…]

[**Page 48:**]

‌ M^r^. Balle’s Accompts were referred to the next —\
Meeting .

‌ Ordered , that a Seal-preſse be made , and that the ~\
President and S^r^. Robert Moray take care thereof .

‌ Ordered , that D^r^. Merret and M^r^. Hill , do acquaint —\
the Secondaries of London , with the power , the Royall Society\
hath of demanding Bodies for Diſsection ; and that they\
show then that part of the Society's Charter , which giveth\
them that power .

‌ Ordered , that the Operator attend S^r^. Anthony Morgan\
for the warrant of demanding a Body for diſsection , —\
and carry the same to the President , to sign and seal\
it .

‌ Ordered , that D^r^. Croone and M^r^. Colwall haue a Copy , to\
be drawn by the Amanuensis , of the Experiments and Observations\
to be made in Teneriffa , as they are Regist’red in the Book —\
of the Society , in order to consult with D^r^. Pugh , ~ ~\
concerning the most convenient way of making them .

‌ The Report referring to the Statutes , was referred to the\
next Meeting .

[…]

[**Page 49:**]

[…]

‌ The Commiſsion for the Printers was sealed and delive⹀\
red this day : but left by them with D^r^. Goddard , to be —\
better sealed at the next Meeting , than could be done —\
this time by reason of haste . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 47: 45,47,49):** “[The Printers to the Society demanded to attend the Council for receiving their Comission.] Sealed” (CMO/1)

**(CMO/1 p. 47):** “D^r^: Merret desired concerning curious things of Nature to be found in England” [TODO(check): is this a request to write a book] (CMO/1)

**(CMO/1 p. 47,52):** “[Some appointed of the Society to puse their Books of Experi⹀ \| mentall & other Philosophicall Matters for examining the same —] Reports made of the same.” (CMO/1)

**(CMO/1 p. 47):** “M^r^. Ball. charged to finish his Accompts.” (CMO/1)

**(CMO/1 p. 47):** “[Order concerning Bodies for Dissection.] Form of the Warrant concerning the same” (CMO/1)

**(CMO/1 p. 48):** “Order for a Seal Presse to be made.” (CMO/1)

**(CMO/1 p. 48):** “Order concerning a coppie of the Experiments to be made in Teneriff” (CMO/1)

**(CMO/1 p. 48):** “Order concerning the Warrant of demanding a Body for a Dissection to be Sealed.” (CMO/1)

**(CMO/1 p. 48):** “[Seal of the Society.] Minutes about the Seal preſs.” (CMO/8A)

### 20 January 166$\frac{3}{4}$ (CMO/1 pp. 49–51;  CMO/1/37) 

\TranscriptionStart

[*centered:*] January . 20 [. 1663]{.underline} .

[…]

[**Page 50:**]

‌ The General and particular Warrant , to demand Bodies\
for diſsection , drawn up by S^r^. Anthony Morgan , were —\
read and approved ,

‌ Ordered , That the General Warrant be sealed by the\
President , and the other , signed by the same ; and that a\
Copy of both , be entered in the Journal of the Council ,\
which is as followeth ;

[*centered:*] The General Warrant

By virtue of Letters Patents under the great seal\
of England , the President , Council and Fellows of the Royall\
Society of London for improving Natural Knowledge , do\
hereby will and require all persons concerned , that from\
time to time , dead Bodies of the persons who shall suffer\
death by the hand of the Hangman be delivered and taken\
by such person or persons , as shall, by the said President\
or his Deputy , for the time being , by any writing under —\
his or either of their hands , from time to time, be ~ ~\
nominated and appointed to ask and receiue the same , for\
the use of the said Royall Society , in such and as ample\
manner and Forme , to all intents and purposes , as the like\
dead Bodies haue at any time heretofore been , or at any\
time hereafter may or ought to be lawfully delivered\
or taken for the use and by the direction of the President\
of the College of Physitians and Company of Chirurgians[*over:* ‘Chururgians’]\
of the City of London , by what name soever the said two —\
Corporations or either of them are known .  In witneſse\
whereof , the said President Council and Fellows of the\
Royal Society aforesaid , haue caused their Common Seale to\
be affixed to these presents , this 20^th^. day of January 1663.\
‌                  [*two-line, upward diagonal flourish*]

[**Page 51:**]

[*centered:*] The Forme of the Particular Warrant .

These are to will and require you , that one Body , either Man\
or Woman , executed at Tyburne , this present \_\_\_\_\_ being —\
the \_\_\_\_ day of \_\_\_\_\_\_\_\_ such as the bearer hereof\
R . S . shall chuse , be delivered unto the said \_\_\_\_\_\_\_\_\
at the time and place of the said Execution , for the use of the ~\
Royall Society ; he paying the ordinary Fees for the same . Given\
under my hand the day and year aboue written .\
‌                    Signed by the President .\
To all whom this may concerne .\
‌              [*downward flourish*]

‌ Ordered , that M^r^. Hill pay the Operator's Bill from November\
23 . 1663 . to January 20 . 1663 .

‌ Ordered that the Operator do hencefort bring in his Bills ~\
weekly .

‌ D^r^. Wilkins was desired to speak with the Operator as from\
himselfe , concerning his yearly Sallary , to try, whether fifty —\
pounds a year will content him , comprehending the Ten pounds —\
allowed him for his constant attendance .

‌ D^r^. Merret reported , that he had spoken with the Secondaries\
of London , and given them an Extract of the Society's Charter ,\
concerning their power given them therein , for demanding of\
Bodies to diſsect ; and that they would take notice of it ~\
publickly .

[…]

‌ M^r^. Ball's Accounts again referred to the next Meeting .[*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 49: 45,47,49):** “[The Printers to the Society demanded to attend the Council for receiving their Comission.] Sealed” (CMO/1)

**(CMO/1 p. 49: 43,49,76):** “[Statutes] Submitted to the perusal of Lawyers, & reported” (CMO/8A)

**(CMO/1 p. 48,51,52):** “[M^r^. Ball. charged to finish his Accompts.] The same referred to the etc.” (CMO/1)

**(CMO/1 p. 50):** “A Generall & ᵽ[per]ticular Warrant to demand Bodies appoued. \| A coppy of both entered in the Jornal. B. of the Concel:” (CMO/1)

**(CMO/1 p. 51,52):** “Operators Bills are weekly to be brought in.” (CMO/1)

**(CMO/1 p. 51: 48,51,52):** “[M^r^. Ball. charged to finish his Accompts.] The same referred to the etc.” (CMO/1)


### 27 January 166$\frac{3}{4}$ (CMO/1 p. 52;  CMO/1/38) 

\TranscriptionStart

[**Page 52:**]

[*centered:*] January 27 . [1663]{.underline} .[*over:* ‘1664’]

[…]

‌ Ordered , that the Operator be paid twenty shillings weekly ,[*overwriting, uncertain:* ‘wk’]\
for his work for the Society ; and that he bring his Bills ~\
weekly for the Materialls he useth .

[…]

‌ Ordered , that D^r^. Merret bring in a List of the Instruments\
neceſsary for Chirurgery ,[*over:* ‘Chururgery’] to be added to those of Anatomy , ~\
in the Box lately provided for the Society .

‌ Ordered , that the Porter be allowed , three pounds a year\
for his[*over:* ‘his’] constant attendance , to be paid him quarterly .

[…]

‌ Ordered , that the Operator's Bill from January 21 . 1663.\
to January 27 be paid by M^r^. Hill .

‌ Ordered , that the Report concerning the Statutes and the ~\
Books of Experiments , be made at the next Meeting .

‌ Ordered , that M^r^. Balle finish his Accounts, at the next ~ ~\
Meeting . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 52: 51,52):** “Operators Bills are weekly to be brought in.” (CMO/1)

**(CMO/1 p. 52: 47,52):** “[Some appointed of the Society to puse their Books of Experi⹀ \| mentall & other Philosophicall Matters for examining the same —] Reports made of the same.” (CMO/1)

**(CMO/1 p. 52,53):** “Order for bringing in a List of Chirurgical Instrum^ts^:” (CMO/1)

**(CMO/1 p. 52: 48,51,52):** “[M^r^. Ball. charged to finish his Accompts.] The same referred to the etc.” (CMO/1)


### 3 February 166$\frac{3}{4}$ (CMO/1 p. 53;  CMO/1/39) 

**(CMO/1 p. 53: 24,53,55):** “Deputies for examining the Accompts of the \| Treasurer” (CMO/1)

\TranscriptionStart

[*centered:*] February . 3 [. 1663 .]{.underline}

[…]

‌ D^r^. Merrett brought in his Lists of Chirurgicall[*over:* ‘Chururgicall’] Instruments : He\
was desired , to mark those of them that are of [*canceled dittography:* ‘them’] cheif use ~\
for the Chirurgicall[*over:* ‘Chururgicall’] operations , intended to be made by the Society ~\
upon dead Bodies .

‌ Ordered , the Com̃ittee for examining M^r^. Balle's Account from\
June 24 . to September 23 . 1663. doe meet on Friday next in the ~\
Morning , about 10 of the Clock at the President's house .[*over:* ‘horse’]

‌ S^r^. Robert Moray moved , that every one of the Council might\
think on wayes to raise a Revenue for carrying on the Design[*over:* ‘Desrin’] [*canceled:* ‘~~of~~’]\
and work of the Society .

‌ D^r^. Whistler suggested , severall things for that purpose : vid .[*overwriting, uncertain:* ‘i.e.’]\
that his Ma^ty^. ‸^might^ [*canceled:* ‘~~should~~’] be spoken to , that in every new Grant , ~ ~\
something might be Stipulated for the use of the Society . Item ,\
that if any thing should be found , that at the present yeilds no ~\
revenues ,to the King , the Grant of it might fall to the Society .\
Item , that upon all Philosophical books , printed in England ,\
some imposition might be laid for the benefit of the Society .

[*horizontal flourish*]

\TranscriptionStop

### 10 February 166$\frac{3}{4}$ (CMO/1 pp. 54-55;  CMO/1/40) 

**10 Feb 166**$\frac{3}{4}$ **(CMO/1/ , v.1 pp. 54–5):** “[Dead Bodies.] Form of Summons to see disected” (CMO/8A) \index{Forms+Summons to dissection}

\TranscriptionStart

[*centered:*] February . 10 . 1663.

[…]

 Resolved , that the Forme for the Summons of the Fellows of\
the Society for a diſsection , be in manner following ;

‌” You are desired to take Notice , that there will be an ~\
” Anatomical Administration at Greſham College , to begin at\
” \_\_\_\_\_ day , at 10 of the clock precisely .

[…]

‌ It was desired , that those of the Society that are Lawyers ,\
would informe themselues about the vacancies and Reversions of —\
the places of the Law-Courts ; and M^r^. Colwall , about places of\
the Customehouse &c.

[…]

\TranscriptionStop


**(CMO/1 p. 54):** “A Forme for the Summons of y^e^ Fellows of the Society for a Dissection.” (CMO/1)


### 17 February 166$\frac{3}{4}$ (CMO/1 pp. 55-56;  CMO/1/41) 

**(CMO/1 p. 55: 24,53,55):** “Deputies for examining the Accompts of the \| Treasurer” (CMO/1)

**(CMO/1 p. 55):** “[Printing of Papers & Books.] D^r^. Charlton’s Lecture’s on Muscles, to be considered” (CMO/8A)

**(CMO/1 , v. 1 p. 56:)** “Committee. For Statutes. [*which include printing rights*]” (CMO/8A) \index{Committee+For Statutes. [which include printing rights]}

\TranscriptionStart

[**Page 55:**]

[…]

[*centered:*]  February 17 . [1663 ]{.underline}.

[…]

‌ The Com̃ittee appointed[*over:* ‘appiointed’] Febr : 3 for examining M^r^. Ball's Accounts\
reported that they had examined them , and found M^r^. Ball to haue ~\
diſcharged his trust with care and fidelity : whereupon it was [*canceled:* ‘~~,~~’]\

‌ Ordered , that the Society should this day at their ordinary\
Meeting , be acquainted therewith , to return thanks to M^r^. Ball .

‌ Ordered also, that the Council having approved of M^r^. Ball's\
Accounts , giuen up by him untill September 23 . [1663]{.underline} . as to the\
Receipts of weekly payments , and untill November 30 . [1663]{.underline} . as to\
Admittance money and disbursements , the present Treasurer , M^r^. Hill\
giue an Acquittance to M^r^. Ball , for his full discharge , when\
he shall haue received from him the remainder of his Account ,\
amounting to seven pounds Six shillings Eight pence .

[…]

‌ Ordered , that upon S^r^. Peter Wyche his moving , that D^r^.\
Charleton’s Lectures upon the Muscles might be printed , D^r^. Charleton\
should be desired to giue in the said Lectures to the Council ~[**Page 56:**]\
for further consideration , and order concerning them .

‌ Ordered , that M^r^. Hoskins bring in the next day , a Report\
in writing , concerning the Statutes of the Society .

‌ Ordered , that M^r^. Hook set down in writing , and produce ~\
before the Council , his whole Apparatus and Management for ſppedy\
intelligence .

[…]

\TranscriptionStop

### 24 February 166$\frac{3}{4}$ (CMO/1 p. 56; CMO/1/(41+1))

\TranscriptionStart

The Council sat not this day , being Ashe-Wednesday .[*downward flourish*]

\TranscriptionStop

### 2 March 166$\frac{3}{4}$ (CMO/1 pp. 56-57;  CMO/1/42) 

**(CMO/1 p. 57):** “Proposal made concerning the Printing & Selling Books for the advantage of the Society.” (CMO/1)

**(CMO/1 p. 57):** “[Printing of Papers & Books.] Minutes concerning. Author to affix his name” (CMO/8A)

**(CMO/1 p. 57):** “Order concerning the Title of Fellows of the Society in Books printed.” (CMO/1)

\TranscriptionStart

[*centered:*] March 2 . 1663 .

[…]

‌ M^r^. Hoskins brought in writing the Report of the Com̃ittee  \|\
for reveiwing the Statutes of the Society , Viz^t^.\

[*centered:*] The 19^th^. of December [. 1663 ]{.underline}.

‌    At the Com̃ittee for reviewing[*over:* ‘reveiwing’] the Laws .\
This Committee is of Opinion , that it is the interest of\
the Society , to haue no compleat Body of Laws ; but that [**page 57:**]\
such Orders be from time to time made , as the State of Affairs —\
shall neceſsarily require ; And that the Lawes already made , be kept\
private .

‌                          John [Hos]{.underline}kins .


‌ Ordered upon this Report , that in pursuance of an Order of —\
the Council on December 21 . the same Committee bring in their sence\
upon particular statutes .

[…]

‌ Ordered , that all those of the Society , that shall print any —\
Book of a Philosophical Nature by Order fo the Society , be desired\
to own themselues in the Title-page , Fellows of the Society .

‌ A Motion being made by M^r^. Hoskins , that the Society might\
print and vend to their advantage , the Books to be printed by any\
of their Fellows , and by their Order ; It was resolved , that this\
proposition should be considered of , when any Book of\
note , should be offered to the preſse by any Fellows . [*downward flourish*]

\TranscriptionStop


### 9 March 166$\frac{3}{4}$ (CMO/1 pp. 57-59;  CMO/1/43) 

**(CMO/1 p. 58,60):** “Proposall concerning Mechanicall Invention” (CMO/1)

**(CMO/1 p. 58,60):** “Mechanical inventions \| To be referred to the Royal Society.” (CMO/8A)

\TranscriptionStart

[*centered:*] March . 9 .[‌ 1663 ]{.underline}.

[…]

‌ Ordered , that any 3 . or more of the Council , that shall [**page 58:**]\
first meet next Wednesday next at the usual time , consider\
of an Order to be drawn up and offered to this Council con⹀\
cerning the power that may be giuen to a Committee of the\
Council , to discourse of and prepare matters concerning the\
Society , against the time that a Quorum of the Council be\
aſsembled .

[…]

‌ It was moved that the king might be desired , by the\
President S^r^. Robert Moray and S^r^. Paul Neile , to giue\
a Rule to the two Secretaries of State , that all the pro⹀\
posals that shall be made concerning Mechanical Inventions\
be referred to the Council of this Society , to be by them —\
Examined , whether they be new, true and usefull .

[…]

\TranscriptionStop


### 16 March 166$\frac{3}{4}$ (CMO/1 p. 59;  CMO/1/44) 

\TranscriptionStart

[**Page 59:**]

[…]

[*centered:*]  March 16.[‌ 1664 ]{.underline}.[*over:* ‘1663’]

[…]

‌ Ordered, that D^r^. Goddard and M^r^. Palmer do visit M^r^.  Ellis ,\
to desire him from the Council , to peruse and consider the Statuteſ\
of the Society .

[…]

\TranscriptionStop

### 23 March 166$\frac{3}{4}$ (CMO/1 pp. 60;  CMO/1/45) 


**(CMO/1 p. 60):** “Order for speeding the Picture on a Wall.” (CMO/1)

**(CMO/1 p. 60):** “Order concerning improuement of Philosophy.” [TODO(check): is this about writing] (CMO/1)

**(CMO/1 p. 60: 58,60):** “Proposall concerning Mechanicall Invention” (CMO/1)

\TranscriptionStart

[**Page 60:**]

[*centered:*] March . 23 .[‌ 1663]{.underline}.

[…]

‌ Ordered , that S^r^. Robert Moray and S^r^. Paul Neile be desired ,\
to consult with the Lord Ashley , whether it may be fit to\
desire his Ma^ty^. to giue a Rule to the two Secretaries of State ,\
that all the Proposals , that shall be made concerning Mecha⹀\
nical inventions , be referred to the Council of the Royal\
Society , to be examined by them , as to their Novelty ~ ~\
Reality and Usefulneſse .

[**preliminary note:** ‘Mar 23 : 63 . all but the first paragraph , miſsing . And instead of y^e^ last\
‌  words of this article , [as to their Novelty, Reality & Usefullneſs :]{.underline} the Orig^l^\
‌  has these : whether they be new true and usefull .’]

[…]

‌ Ordered , that M^r^ Hook produce at every Meeting of
the Society , one of his Microscopical diſcourses , in reference\
to be printed by Order of the Society .

‌ Ordered , that Several Committees be appointed for the\
consideration and improuement of Several Subjects of Phi⹀\
losophy . [*downward flourish*]

\TranscriptionStop

### 30 March 1664 (CMO/1 pp. 61-62;  CMO/1/46) 

**(CMO/1 , v. 1 p. 61:)** “[Astronomical] MSS. of Ulug Beig” (CMO/8A)

**(CMO/1 p. 61):** “A draught of Seuſall Subjects offered.” (CMO/1)

**(CMO/1 p. 61,66,66,66):** “[Printing of Papers & Books.] Of Ulug Beig’s Astronomical M.S.” (CMO/8A)

**(CMO/1 p. 61,64,66,67):** “Astronomical Manuscripts of Ulug Beig.” (CMO/1)

**(CMO/1 , v. 1 p. 62:)** “Diploma. For Foreigner’s election, to be drawn” (CMO/8A)

\TranscriptionStart

[**Page 61:**]

[*centered:*] March 30 .[‌ 1664 ]{.underline}.

[…]

‌ A draught of Eight Committees , for the Consideration of Se⹀\
verall Subjects , belonging to the Cognizance of the Society , was\
read , and ordered to be offered to the Society at their meet⹀\
ing this afternoon , for their approbation .

[*centered:*] An Order of the Council .

‌ When any three or more of the Council  ( the President and\
one of the Secretaries being two of the Number ) are met at\
the time and place appointed for any meeting thereof , they are\
desired to examine , consider , debate and prepare any busineſse\
or matter , appointed to be considered or done by the Council,\
and report what is by them agreed upon , to the same , and\
to giue directions for making and preparing Experiments .

‌ It being proposed to M^r^. Martin , whether he would be\
at the Charge of the Translation and printing of the Astro⹀\
nomical Manuscript of [Ulug Beig]{.underline} , he was desired, to send his\
answer within 2 or 3 dayes , to D^r^. Wilkins , to be by him —\
signified to the President .

‌ S^r^. Robert Moray and S^r^. Paul Neile were desired to\
get from the Atturney-General the paper declaring , that it\
is in his Ma^ties^. power , to dispose of the house of Chelsey-college [**page 62:**]\
and to referr the same , together with the Charter of the said\
College , to the consideration of S^r^. Anthony Morgan .

‌ D^r^. Merret proposing a person fit to be entertained by the\
Society , as an aſsistant to the Operator , and as a Collector\
of Englands curiosities , as to plants , Fowle , and Fish , he —\
was desired to addreſse him to D^r^. Wilkins and D^r^. Goddard ,\
for further Examination of his fitneſse for such services .

[…]

\TranscriptionStop

### 6 April 1664 (CMO/1 pp. 62-63;  CMO/1/47) 

**(CMO/1 p. 63: 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)

\TranscriptionStart

[*centered:*] April. 6. 1664 .

[…]

‌ Ordered , that the Secretary draaw up a forme of a Diploma\
testifying strangers to haue been elected into the Society .

‌ The following draught of a Statute , dispensing with For⹀\
reigners, elected into the Society , as to their Subscription and\
Admiſsion , was read , and paſsed the first time .

‌ When any person , residing in remote or Forreign parts ,\
shall be elected into the Society , in the due and accustomed —\
forme and manner ; the said person shall be registered among\
the Fellows of the Society , and be reputed a Fellow thereof ,\
without Subscription and admiſsion in the usual Forme ; any [**page 63:**]\
thing contained in the Statutes , requiring Subscription and ~\
admiſsion , to the contrary notwithstanding : And the said —\
person may haue an Instrument under the Seal of the Society ,\
testifying him to be elected and reputed a Fellow of the ~ ~\
Society accordingly .

‌ Ordered , that the Clerk call upon the Printers of the —\
Society , to bring in their Bill for printing the Lists of the —\
Fellows of the Society .

[…]

‌ Ordered , that the Secretarie[*canceled:* ‘~~Secretaries~~’] desire M^r^. Boyle to informe\
himself by the Lord Privy-Seal , why his Ma^ties^. Grant of ~ ~\
Chelsey-College to M^r^. Sutclif is stopped ?  And what his Lo^p^.\
judgeth of the Causes , alledged by the Petitioners , for stopping —\
the said Grant ?  As also occasionally , and by way of diſcourses ,\
what he thinks of the King's power of giving away Chelsey —\
College and diſsolving that Corporation . [*downward flourish*]

\TranscriptionStop

### 13 April 1664 (CMO/1 pp. 63-65;  CMO/1/48) 

\TranscriptionStart

[*centered:*] April . 13 [. 1664]{.underline} .

[…]

[**Page 64:**]

‌ It was debated , whether the Manuscript of [Ulug-Beig]{.underline} ~\
should be printed by another , if Allestree and Martin would\
not doe it ?  It was not fully resolued upon , by reason of the\
non-attendance of the said persons , who had been appointed to\
be present this day :

‌ Ordered , to giue them notice again to attend the Council —\
the next Meeting .

‌ The Statute dispensing with Forrainers , elected into the\
Society , for their Subscription and Admiſsion , was read again ,\
and paſsed the second time , with some small alterations , as —\
followeth ;

‌ When any person , residing in Forraigne parts , shall be\
— elected into the Society in the due and accustomed forme and\
manner ; the said person shall be registred among the Fellows\
of the Society , and be reputed a Fellow thereof , without Sub⹀\
scription and admiſsion in the Vsual Forme ; any thing con⹀\
tained in the Statutes requiring Subscription and Admiſsion ,\
to the contrary notwithstanding :  And the said person may haue\
an Instrument under the Seal of the Society , testifying him to\
be elected and reputed a Fellow of the Society accordingly . 

‌ The Secretary offered a draught of ſuch a Diploma\
as is mentioned in the aboue written statute,‸ ^[w^th^ relation to Mons^r^ Hevelius : and it]{}^ [*canceled:* ‘~~which~~’] was read ,[*over:* ‘red’]\
and approued with some alterations , as Followeth .

‌ Præses , Concilium et sodales Regalis Soci⹀\
etatis Londini pro Scientia Naturali promovenda ~ ~\
Omnibus et Singulis , ad quos præsentes pervenerint ,\
Salutem .  Cum[*penciled:*]\[ Virtute et Scientijs Mathematicis’ ,\
præcipùe ver̀o laboribus et Scriptis Astronomicis ~ —\
Illustris, Dominus Johannes Hevelius , Celeberrimæ Civi⹀\
tatis Gedanensis Consul . Ampliſsimus , [*penciled:*]\]Singularem suum [**page 65:**]\
in dictæ Societatis conatus et ſtudia affectum huma⹀\
niſsimis Litteris uberrim̀e fuerit testatus , suis[ue] meri⹀\
tis egregiis Rem litterariam , et Solidam inprimis Philo⹀\
sophiam , augere et ornare pro Virili Satagat ; dicta So⹀\
cietas [*penciled:*]\[Laudatum [*penciled:*]\[Dominum Hevelium [*penciled:*]\] die 30 Martij ~\
Anni 1664 in solemni conseſsu , conspirantibus omnium —\
Suffragijs ,[*penciled:*]\] in Sodalium suorum Album cooptavit , in̀ hu⹀\
jus rei Testimonium , Sigillum suum præsentibus affigi —\
curavit . Dat . Londini [*canceled:* ‘~~die undecima Maij~~’] Anno Æræ\
Christianæ[*over:* ‘Christiani’] 1664 .  Regni CAROLUS. II , Augustiſsimi Magnæ\
Britanniæ &c . Regis , dictæ Societatis Fundatoris et Pa⹀\
troni Munificentiſsime , decimo-sexto . [*downward flourish*]

\TranscriptionStop

**(CMO/1 , v. 1 p. 64:)** “[Astronomical] MSS. of Ulug Beig” (CMO/8A) “[Diploma.] Draught of” (CMO/8A)

**(CMO/1 p. 64: 61,64,66,67):** “Astronomical Manuscripts of Ulug Beig.” (CMO/1)

**(CMO/1 , v. 1 p. 65:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)

**(CMO/1 p. 65,66):** “Order about a Recompence to D^r^: Charleton for ~ \| Anatomical Administracon” (CMO/1)


### 20 April 1664 (CMO/1 pp. 65-66;  CMO/1/49) 

\TranscriptionStart

[*centered:*] April 20 [. 1664 ]{.underline}.

[…]

‌ Ordered , that the President keep the paper of Subscriptions\
made for D^r^. Charleton by several persons of the Society , for con⹀\
tributing to a recompence for the care and pains which the —\
said Doctor was willing to take in Anatomical administrations.

[**Page 66:**]

[…]

‌ Ordered , that the Secretary write to M^r^. Hevelius, and ~\
desires him that the Catalogue of the Fixed Starrs of Ulug-Beig\
being sent him for his private Satisfaction , he would not , by —\
printing[*over, dittography:* ‘printintin’] the said Catalogue , anticipate the Society's[*over:* ‘Societies’] purpose\
of printing the whole Book .

\TranscriptionStop


**(CMO/1 p. 66):** “A Promise concerning Insects & other Curiosities of Nature.” [TODO(check): what form is this promise] (CMO/1)

**(CMO/1 p. 66: 61,64,66,67):** “Astronomical Manuscripts of Ulug Beig.” (CMO/1)

**(CMO/1 p. 66: 65,66):** “Order about a Recompence to D^r^: Charleton for ~ \| Anatomical Administracon” (CMO/1)

**(CMO/1 p. 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 , v. 1 p. 66:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)

**(CMO/1 , v. 1 p. 66:)** “[Astronomical] MSS. of Ulug Beig” (CMO/8A)

**(CMO/1 p. 66: 61,66,66,66):** “[Printing of Papers & Books.] Of Ulug Beig’s Astronomical M.S.” [thrice in CMO/8A versus different pages in CMO/1] (CMO/8A)

### 27 April (CMO/1 pp. 66–67;  CMO/1/50)

\TranscriptionStart

[*centered:*] April . 2[7 . 1664 .]{.underline}

[…]

‌ Ordered , that D^r^. Wilkins , D^r^. Goddard , M^r^. Ball, M^r^. Colwall ,\
and the Secretary , be a Committee to examine M^r^. Hill’s Accounts\
of the last Quarter from Christmaſs to our Lady day in March .

‌ Ordered , that the Secretary write to Mon^r^. Hevelius , and\
offer him the printing of Ulug-Beig , either [*canceled:* ‘~~both~~’] in Persian and\
Latine‸^both,^ or in Latine alone .

‌ Ordered , that the Secretary write to D^r^. Wallis , and desire\
him to take an occasion of proposing to M^r^. Hide , to print\
Ulug-Beig in Latine onely , without adding the Persian .

‌ A Letter^\*^ [*in margin:* ‘\* dated .13 Apr . [1664]{.underline}.] of Colonel Long’s , written to the Secretary , was read ,\
wherein he promiseth to send up his Boxes with Insects , and\
other curiosities of Nature , to be put among the rest of the things\
that are preparing for his Ma^ties^. reception .

[…]

[**Page 67:**]

[…]

‌ Ordered , that the Amanuensis do handsomely write the ~\
Diploma for M^r^. Hevelius , upon parchment , and that M^r^. Simmonds\
be desired to attend the Council at their next Meeting , for taking\
off the Seal , and to haue a Silver box ready to put the Seal in[ .]{.underline}

[…]

\TranscriptionStop

### 4 May 1664 (CMO/1 pp. 67-68;  CMO/1/51) 

\TranscriptionStart

[*centered:*] May . 4 .[‌ 166]{.underline}4 .

[…]

‌ Ordered that D^r^. Wallis be written to by the Secretary , and desired\
to preſse M^r^. Hide to the translating of Ulug-Beig , and not to transcribe\
the original till further order .

[…]

\TranscriptionStop


**(CMO/1 p. 67: 61,64,66,67):** “Astronomical Manuscripts of Ulug Beig.” (CMO/1)

**(CMO/1 p. 67):** “Ulug Beig \| To be translated by M^r^. Hyde” (CMO/8A)

**(CMO/1 p. 67: 30,34,67,68,69):** “Seal of the Society. \| Orders about procuring one” (CMO/8A)

**(CMO/1 , v. 1 p. 67:)** “[Astronomical] MSS. of Ulug Beig” (CMO/8A)

**(CMO/1 , v. 1 p. 67:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)

**(CMO/1 p. 68: 30,34,67,68,69):** “Seal of the Society. \| Orders about procuring one” (CMO/8A)

**(CMO/1 , v. 1 p. 68:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)


### 11 May 1664 (CMO/1 pp. 68;  CMO/1/52) 

\TranscriptionStart

[**Page 68:**]

[…]

[*centered:*] May. 11 . 1664 .

[…]

‌ The Treasurer desiring to be directed , what kind of acquittance\
to take from M^r^. Sutcliffe for the Ten poundsn ordered to be paid\
him ; M^r^. Hoskins was desired to draw up a forme thereof .

‌ Ordered , that the President take home with him the Society's\
Seal , to haue it handsomely taken off by M^r^. Simmonds , for —\
Mons^r^. Hevelius his Diploma ; And that the Amanuensis get the\
ornaments of the said Diploma ready against satturday morning ,\
and carry it to the President , in Order of Sealing and Signing\
it ; and that thereupon the Secretary dispatch it away with —\
the first opportunity . [*downward flourish*]

[*horizontal flourish*]

\TranscriptionStop


### 18 May 1664 (CMO/1 pp. 69;  CMO/1/53) 

\TranscriptionStart

[**Page 69:**]

[*centered:*] May . 18[. 1664 .]{.underline}

[…]

‌ The President returned the Society's Seal , which was put ~\
again into the Chest with 3 locks : the Diploma of Mons^r^. Hevelius\
having been sealed therewith , bearing date of May 11^th^. 1664 . and\
being sent away by sea to Dantzick .

‌ A motion being made by the President and S^r^. Robert Moray\
on the behalf of M^r^. Povey , about the reimbursing him the 23^~~lib~~^. —\
which he affirmed to haue expended for the Society upon the Account\
of treating with D^r^. Killegrew for the Savoy , by order of the ~\
Council , for the use of the Society ; though the said Treaty proved\
Succeſsleſse , by reason of some conditions , judged by the Council —\
not fit to be accepted of ; It was ordered , that the Treasurer\
of the Society should reimburse M^r^. Povey of the said 23^~~li~~^.

[…]

\TranscriptionStop

**(CMO/1 p. 69: 30,34,67,68,69):** “Seal of the Society. \| Orders about procuring one” (CMO/8A)

**(CMO/1 , v. 1 p. 69:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)


### 25 May 1664 (CMO/1 pp. 70;  CMO/1/54) 

\TranscriptionStart

[**Page 70:**]

[*centered:*] May . 25 . 1664 .

[…]

‌ Ordered , that the President and S^r^. Robert Moray be desired\
to prosecute the Petition for the Grant of Chelsey-college , and do\
present such a Petition to his Ma^ty^.

[…]

\TranscriptionStop

### 1 June 1664 (CMO/1 pp. 70-71;  CMO/1/55) 

\TranscriptionStart

[*centered:*] June . 1 . 1664 .

[…]

‌ The Petition to his Ma^ty^. for Granting the House of Chelsey⹀\
colledge , and the Land belonging therunto , to the Royal Society ,\
was read , and, after some alterations , agreed upon , as followeth ,

[*centered:*] To the [King’s]{.smallcaps} most excellent Ma^ty^.

‌  The humble Petition of the President , Council and Fellows\
‌  of the Royal Society of London for improving Natural\
‌  Knowledge.,[**page 71:**]\
Humbly sheweth ,

‌ That the House called Chelsey-college , and the Land heretofore\
belonging thereunto by the Donation of your Ma^tys^. Royal Grandfather\
King JAMES of bleſsed memory , are now ( as your Petitioners do ~\
humbly conceiue ) at your Ma^ties^. disposal ,

‌  Your Petitioners do therefore humbly pray , your Ma^ty^. will\
be gratiously pleased to giue Order , that a Bill be prepared for yo^r^.\
Royal Signature , granting to your Petitioners and their Succeſsors\
the said House and Lands , that your Pet^rs^. may thereby be in some\
measure enabled to prosecute the designe , for which your Ma^ty^.\
was pleased to constitute them a Corporation.

‌           And your Petition^rs^. shall ever pray &c.

‌ Ordered , That the President , S^r^. Robert Moray , S^r^. Paul —\
Neile and M^r^. Aerskine be desired to present the said Petition\
to his Ma^ty^., as soon as conveniently may be .

‌ Ordered , that M^r^. Hill do pay to M^r^. Sutcliffe , the summe\
of Ten pounds , and take a receipt for the same , notwithstanding\
former Order . [*downward flourish*]

\TranscriptionStop

### 8 June 1664 (CMO/1 pp. 71-72;  CMO/1/56) 

\TranscriptionStart

[*centered:*] June 8 . 1664 .

[…]

‌ S^r^. Robert Moray acquainted the Council , that the ~\
Petition for the Grant of Chelsey- College was presented to\
his Ma^ty^., and that Secretary Bennet was to receiue his —\
Ma^tyes^. order for a referrence upon it .

[…]

‌ Ordered , that the Keeper of the Repository deliver ~\
nothing out of the same , without a written order . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 72,259):** “[M^r^: Hook appointed Keeper of the Repository of the Society] Order to the same.” (CMO/1)

**(CMO/1 p. 72):** “Repository at Gresham College. Orders to the Keeper” (CMO/8A)


### 15 June 1664 (CMO/1 pp. 72-73;  CMO/1/57) 

\TranscriptionStart

[*centered:*] June 15[ . 1664]{.underline} .

[…]

[**Page 73:**]

‌ S^r^. Robert Moray reported , that M^r^. Secretary Bennet had\
acquainted him , that his Ma^ty^ had referred the Petition of the\
Society concerning the Grant of Chelsey-College to the consideration\
of the Arch-Bishopp of Canterbury , the Lord Chancellor , Lord —\
Privy-Seal ,  Bishop of London ,  and Lord Ashely , or any two\
of them .

‌ Ordered , That the Lord Brouncker , S^r^ Robert Moray , and\
S^r^. Paul Neile take further care of Soliciting this affaire —\
and of attending the Referrees of this Petition . [*downward flourish*]

\TranscriptionStop

### 22 June 1664 (CMO/1 pp. 73;  CMO/1/58) 

\TranscriptionStart

[*centered:*] June 22 . [166]{.underline}4 .

[…]

‌ Ordered that M^r^. Hill and M^r^. Hoskins conferre with Sir —\
William Petty and M^r^. Grant , concerning the manner and forme,\
in which it may be most proper for S^r^. John Cutler to put into\
execution[*over:* ‘executions’] his promise of giving Fifty pounds a year to M^r^. ~\
Hook during his life , for the reading of the Histories of Trades\
in Gresham College .

‌ It being mentioned , that in case of M^r^. Hooks Microscopical\
observations should be printed by Order of the Society , they\
might be perused and Examined by some Members of the Society ;\
The Lord Brouncker was desired to undertake this perusall , to\
whom of the Society he should think fit . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 73,76):** “Order concerning the reading of the Histories of Trades— \| in Gresham Colledge.” (CMO/1)

### 29 June 1664 (CMO/1 pp. 74;  CMO/1/59) 

\TranscriptionStart

[**Page 74:**]

[*centered:*] June . 29 [. 1664]{.underline} .

[…]

‌ M^r^. Sutcliffe soliciting the payment of the 90^~~li~~^: remaining ,\
was desired to haue patience till the Attorney-Generall ~\
had made a Report concerning the King’s power of granting\
Chelsey-College and the Land belonging to the same .

‌ S^r^. Robert Moray reported , that the Attorney-General\
had appointed Satturday next to giue a Report concerning\
the matter before mentioned . [*downward flourish*]

\TranscriptionStop


### 6 July 1664 (CMO/1 pp. 74;  CMO/1/60) 

\TranscriptionStart

[*centered:*] July 6 .[‌ 1664]{.underline} .

[…]

‌ D^r^. Goddard and M^r^. Palmer were put in mind to con⹀\
ferre with M^r^. Ellise concerning the Statutes of the Society.

‌ S^r^. Robert Moray reported , that the Attorney-General ~\
had not yet given an account concerning Chelsey-College.

‌ Ordered , that the Council meet not henceforth , but upon\
summons, to be sent by the President , to every Member of\
the Council . [*downward flourish*]

\TranscriptionStop

### 27 July 1664 (CMO/1 pp. 75-76;  CMO/1/61) 

\TranscriptionStart

[*centered:*] July 27 [. 166]{.underline}4 .

[…]

‌ Voted , that at the first opportunity, M^r^. Hook be put to\
the Scrutiny for a Curators place .

‌ Voted , that M^r^. Hook shall receiue Eighty pounds per Annū\
as Curator[*canceled:
* ‘~~,~~’] ‸^to^ [*canceled:* ‘~~of~~’] the Society , by Subscriptions of particular —\
Members of the said Society , or otherwise .

‌ Ordered , that M^r^. Hook forthwith provide himself of a [**page 76:**]\
Lodging , in or neer Greſham College .

‌ Ordered , that these Orders and Votes be kept secret , ~\
till S^r^. John Cutler haue established M^r^. Hook as Profeſsor\
of the Histories of Trades .

‌ M\r. Palmer reported , that M^r^. Ellis having been con⹀\
sulted with concerning the Statutes of the Society , had —\
declared to D^r^. Goddard and himself , that they were such ,\
both as to matter and Forme , that he found no exceptions at —\
all against them , but must highly approue of them , for —\
the Reason , Convenience and Perspicuity therein ; wishing\
that all other Laws might equall them in these Qualifi⹀\
cations . [*downward flourish*]

\TranscriptionStop

### 10 August 1664 (CMO/1 pp. 76;  CMO/1/62) 

\TranscriptionStart

[*centered:*] August 10 [. 1664]{.underline}.

[…]

‌ The Forme of the King's and the Duke of York's Subscriptions\
was referred to the Com̃ittee that meets on Munday at D^r^.\
Goddards Lodgings . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 76: 73,76):** “Order concerning the reading of the Histories of Trades— \| in Gresham Colledge.” (CMO/1)

**(CMO/1 p. 76):** “The form of the Kings & the Duks of York Subscription referred to etc.” (CMO/1)

**(CMO/1 p. 76: 43,49,76):** “[Statutes] Submitted to the perusal of Lawyers, & reported” (CMO/8A)

### 31 August 1664 (CMO/1 pp. 76-77;  CMO/1/63) 

\TranscriptionStart

[*centered:*] August 31 .[‌ 1664]{.underline} .

[…]

[**Page 77:**]

‌ Resolved , that the Patent for the several Newfashioned ~ —\
Chariots , be drawn for M^r^. Hook and his Assistants . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 77):** “The Forme for the Subscription of Benefactors.” (CMO/1)

**(CMO/1 p. 77):** “[The form of the Kings & the Duks of York Subscription referred to etc.] The Formes of the same. \| and Prince Rupert.” (CMO/1)

**(CMO/1 p. 77,80,86):** “Order concerning direction for prepareing a Charter Book.” (CMO/1)

**(CMO/1 p. 77):** “[Subscription of the Fellows] Form of, for the King &c (Carol. II)” (CMO/8A)


### 5 October 1664 (CMO/1 pp. 77-78;  CMO/1/64) 

\TranscriptionStart

[*centered:*] October 5 . 1664 .

[…]

‌ 1. Resolved , that the Forme Following be offered to the King\
for his Subscription ;

[*Three lines centered:*]\
Charles R.\
Founder , Patron and One of the Royal Society of\
London for improving Natural Knowledge

‌ 2. Resolved , that the Forme following be offered to the Duke of\
York and Prince Rupert , for their Subscriptions ;

[*Four lines centered:*]\
James D.\
Fellow of the Royal Society .\
Rupert P .\
Fellow of the Royal Society .

‌ 3. Resolved , that the Forme following be used for the Sub⹀\
scription of Benefactors ;

‌   I  (N.N. ) giue so much \_\_\_\_\_\_ .

‌ Ordered , that D^r^. Goddard , do giue Directions for the preparing\
of a Book , to be called the Charter - book , wherein forthwith [**page 78:**]\
is fairly to be written , a Copy of the Charter , the Statutes , and\
the Register of the Fellows and Benefactors of the Society &c.\
according as is provided by statute .

[…]

‌ Ordered , that the determination for printing M^r^. Horrox's\
Manuscript , as it is digested by D^r^. Wallis , be differred ,[*over:* ‘deferred’] till\
the President haue perused the Book , and given his judg⹀\
ment of the fitneſse of printing it .

[…]

‌ Ordered , that M^r^. Hook do prepare an Oration , upon\
the account of S^r^. John Cutler's founding of a Mechanical\
Lecture ; and do also , between this and the next meeting ,\
think upon a Method to proceed by , in his Lectures , which\
upon the Council's approbation , he may giue a hint of , in the\
said Oration .

[…]

\TranscriptionStop


**(CMO/1 p. 78,88):** “Order concerning M^r^: Horrexs Manuscript.” (CMO/1)

**(CMO/1 , v. 1 p. 78:)** “Charter Book. Minutes concerning. [*the charter which granted the right to print*]” (CMO/8A)

**(CMO/1 p. 78):** “[Printing of Papers & Books.] Of M^r^. Horrox M. S. considered” (CMO/8A)


### 2 November 1664 (CMO/1 pp. 79-80;  CMO/1/65) 

\TranscriptionStart

[**Page 79:**]

[*centered:*] November 2 [. 1664]{.underline} .

[…]

‌ D^r^. Wilkins related ,  that S^r^. John Cutler had declared to —\
him , that he was firm in his resolution , to settle upon M^r^.\
Hook , 50 . pounds per Annum , for such imployment as the Royal\
Society should put him upon .

‌ Ordered, that D^r^. Wilkins draw up a Forme of Thanks , to\
be given to the said S^r^. John Cutler , for his Donation ; decla⹀\
ring him withall an Honorary Member of the Society , to\
see whether it be according to his sense .

‌ Ordered , that M^r^. Hill prepare the Accompts of the Society\
for next Wednesday ; and that the President , D^r^. Wilkins , D^r^.\
Goddard ,  M^r^. Colwall , M^r^. Oldenburg be a Committee to ~ ~\
examine the same .

[…]

‌ Ordered , that S^r^. Anthony  Morgan be desired to draw\
up a Report for the Attorney-General to signe , concerning —\
the power his Ma^ty^. hath of the Grant of Chelsea- College .

‌ Ordered , that the Lists of the Society and Council be —\
printed for the Anniversary Election .

[…]

\TranscriptionStop


**(CMO/1 p. 79):** “Order for drawing up a Form of Thanks to be given to S^r^: John Cutler.” (CMO/1)

**(CMO/1 p. 79,81):** “List of the Society & Council to be printed:” (CMO/1)

**(CMO/1 p. 79: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 79: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)


### 9 November 1664 (CMO/1 pp. 80;  CMO/1/66) 

\TranscriptionStart

[**Page 80:**]

[*centered:*] November. 9 . [1664]{.underline} .

[…]

‌ Ordered , that M^r^. Hill bring in his Accompts next day .

‌ Ordered , that the Lists fo the Society and the Council be\
printed , for the Anniversary Election day : and that M^r^. Hoar\
M^r^. Godolphin , M^r^. Woodford and M^r^. Beale , though not yet\
admitted , be inserted .

‌ Ordered , that the Charter- Book be made ready with all\
poſsible speed , to be presented to his Ma^ty^.

‌ S^r^. Anthony Morgan promised , to draw up a Report for\
the Attorney General to signe , concerning his Ma^ties^. power\
to giue a Grant of Chelsey- College .

‌ S^r^. Paul Neile reported, that M^r^. Coal had declared to\
him , that he would by no means take any more from the\
Society for the fiue Acres of Land , lying about Chelsey-College\
than he could get from others , without any collusion : and\
that he would cause a lease to be drawn up , for the —\
Society to become his Tennants . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 80):** “[List of the Society & Council to be printed: for the Anniversary Election.] and some not yet admitted, inserted.” (CMO/1)

**(CMO/1 p. 80: 77,80,86):** “Order concerning direction for prepareing a Charter Book.” (CMO/1)

**(CMO/1 , v. 1 p. 80:)** “Charter Book. Minutes concerning. [*the charter which granted the right to print*]” (CMO/8A)

**(CMO/1 p. 80):** “[A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.] Order for the same” (CMO/1)


### 16 November 1664 (CMO/1 pp. 81;  CMO/1/67) 

\TranscriptionStart

[**Page 81:**]

[*centered:*] November 16 .[‌ 1664]{.underline} .

[…]

‌ Ordered , that the Lists and Summons be printed against Monday\
next , and the Summons directed by the Amanuensis , and then by\
the Operator brought to the President to signe them .

‌ Ordered that D^r^. Wilkins draw up a Diploma in English for\
S^r^. John Cutler as Honorary Member of the Society .

‌ Ordered, that the Amanuensis extract out of the Charter , the\
Title of the Society , and what concernes their power to purchase ,\
and to plead and be impleaded , for the use of M^r^. Coale . [Whom the Society plans to pay for the land around Chelsey College and who wishes to deal fairly, cf. 9 November 1664]

‌ Voted , that there shall be a Curator by office ; and that M^r^.\
Hook be propounded to the Society , as such. [*downward flourish*]

\TranscriptionStop


### 23 November 1664 (CMO/1 pp. 81-82;  CMO/1/68) 

\TranscriptionStart

[*centered:*] November 23 [. 1664]{.underline} .

[…]

‌ Ordered, that the President be desired to declare to the — [**page 82:**]\
Society , this afternoon , that the Council thought good to haue a\
Curator by Office , and to allow him pro tempore 30^~~l~~^ per annum .

‌ Ordered, that M^r^. Hook , standing for a Curators place , be\
this afternoon proposed Candidate by D^r^. Wilkins , as he nominated\
him to the Council .

‌ Ordered , that the President be desired to signe a Licence for\
the Printing of M^r^. Hook's Microscopical Book .

‌ Ordered , that M^r^ Hook giue notice in the Dedication of\
his Microscopical Treatise to the Society , that though they\
haue licensed that Book , yet they own no Theory , nor will be\
thought to doe so : also , that the severall Hypotheses and\
Theories , laid down by him therein ; are not delivered as\
certainties , but as conjectures , and that he intends not at\
all to obtrude or Expose them to the world , as the Opinion\
of the Society . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 81):** “A Diploma Ordered for S^r^: John Cutler as a Fellow of y^e^ Society.” (CMO/1)

**(CMO/1 p. 81):** “List & Summons to be printed.” (CMO/1)

**(CMO/1 p. 81: 79,81):** “List of the Society & Council to be printed:” (CMO/1)

**(CMO/1 p. 82: 2,82):** “D^r^: Wilkins sworne by the President as Secretary.” (CMO/1)

**(CMO/1 p. 82,118,162):** “Order concerning a Present to M^r^: Oldenburg.” (CMO/1)

**(CMO/1 p. 82: 41,82):** “Secretaries to the Society. \| To be sworn anew at every Election” (CMO/8A)

**(CMO/1 p. 82):** “[Treasurer to the Royal Society] To give instructions about Arrears” [possibly in letters TODO] (CMO/8A)

**(CMO/1 p. 82):** “Royal-Society. \| Are not responsible for any thing aſserted in the \| books they License” (CMO/8A)

**(CMO/1 p. 82):** “[Printing of Papers & Books.] Hooks Microscopical Observations Licensed” (CMO/8A)


### 7 December 1664 (CMO/1 pp. 82-83;  CMO/1/69) 

\TranscriptionStart

[*centered:*] December 7 [. 1664]{.underline} .

[…]

‌ The Treaurer , M^r^. Hill , and both the Secretaries , D^r^. ~\
Wilkins and M^r^.  Oldenburg , were sworne as such .

[**Page 83:**]

‌ D^r^. Wilkins proposed M^r^. Hook , as candidate for a Curator's\
place .

‌ Voted , that M^r^. Hook be not examined as Curator .

‌ Ordered , That S^r^. W^m^. Petty ,  D^r^. Wilkins and M^r^. Grant draw —\
up a Forme concerning the Name, time and Subject of M^r^. Hook's\
Lecture , and show it to S^r^. John Cutler , the Founder thereof ; to\
receiue his thoughts upon it .

‌ Ordered , that D^r^. Wilkins doe produce the Catalogue of the —\
Entryes of the Society's Register Books , as it was given in by —\
D^r^. Merrett.

‌ It being suggested, that there were several persons of the —\
Society, whose Genius was very proper and inclined to improue\
the English tongue , and particularly for Philosophical purposes ;\
it was Voted , that there be a Committee for improving the English\
Language ; and that they meet at S^r^. Peter Wyche's Lodgings in —\
Gray's Inn , once or twice a moneth , and giue an Account of —\
their proceedings to the Society , when called thereunto .

‌ The persons following , or any three or more of them , were —\
nominated to constitute this Committee .

‌  M^r^. Aerskine . [Erskine]      M^r^. Henshaw .
‌  S^r^. Robert Atkins .           M^r^. Hoskins .
‌  M^r^. Austin .                  M^r^. Neile .
‌  S^r^.‸^John^ Birkenhead .       S^r^. Thomas Nott .
‌  D^r^. Clark .                   M^r^. Spratt .
‌  D^r^. Croone .                  M^r^. Southwell.
‌  M^r^. Dridsen .                 S^r^ Samuel Tuke.
‌  M^r^. Ellis .                   M^r^. Waller .
‌  M^r^. Evelyn .                  M^r^ Williamson .
‌  S^r^. John Finch .              M^r^. Matthew Wren .
‌  M^r^. Godolphin .

‌ Ordered, that this Committee at their first Meeting , doe\
choose a Chair-man out of their number [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 83):** “Order to produce a Catalogue of the Entrys of the Societies Register Booke.” (CMO/1)

**(CMO/1 p. 83):** “Voted upon a Comittee for improueing the Engl language” (CMO/1)

**(CMO/1 , v. 1 p. 83:)** “[Committee.] To consider of Spratt’s Hist. R. S.” (CMO/8A)

**(CMO/1 p. 83,84):** “[Mechanicall Lectures.] Order for a forme concerning the same.” (CMO/1)


### 14 December 1664 (CMO/1 pp. 84-85;  CMO/1/70) 

\TranscriptionStart

[**Page 84:**]

[*centered:*] December 14[ . 1664 .]{.underline}

[…]

‌ S^r^. W^m^. Petty made a report , that S^r^. John Cutler , intending\
a particular kindneſse to M^r^. Hook , in founding the new — —\
Lecture , and bestowing upon him as Reader ,  fifty pounds\
per Annum , did desire , that he might not be more burthened\
that the other Readers of Gresham-College are , but read onely\
as many lectures in the Vacations as they do in Term-time :\
And further , that S^r^. John Cutler had intimated , that the\
management of this Lecture by the Society , during M^r^. Hook's\
life , should be a measure to him , to make it perpetual or\
no.

‌ Hereupon the Subject and number of this Lecture was —\
much debated , and it was agreed upon , that the Subject of it\
should be the [History of Nature and Art]{.underline} ; but as for the—\
number of the Lectures , it was not resolved upon , Some —\
urging that M^r^. Hook should be ordered to read once upon\
all the ordinary weekly Meeting-dayes of the Vacations ,\
Excepting those of the three months of August , September , and\
October , the greater part of half an hour , beginning about\
two of the Clock : Others preſsing , that M^r^. Hook might —\
read but as many Lectures , as other Profeſsors doe , and\
that the rest of the Wednesdayes might be left to be endowed [**page 85:**]\
by some other Benefactor , for another Philosophical Profeſsor. [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 84: 83,84):** “[Mechanicall Lectures.] Order for a forme concerning the same.” (CMO/1)

### 21 December 1664 (CMO/1 pp. 85;  CMO/1/71) 

\TranscriptionStart

[*centered:*] December [21. 1664 .]{.underline}

[…]

‌ The President moved, that Licence might be granted to D^r^. Charleton\
for the Printing of his Book de Puero , Fulmine icto , and de ~ ~\
Cerebro [*Oldenburg’s hand:*], by ye Printers of ye Society .

‌ Ordered , that this[*over:* ‘the’] Licence be granted , [*canceled:* ‘~~to M^r^. Allestree ,~~’] but that\
two passages in the Preface be left out , or altered to the satiſfaction\
of the President , before it was licenced .

‌ Ordered , that the Council be summoned against next Wednesday\
at 6. of the Clock in the Evening , to consider of the recommending of\
a Curator to the Society.

‌ Ordered, that D^r^. Wilkins , D^r^. Goddard , and D^r^. Croone be a —\
Committee to consider of the particulars to be inserted in the\
relation of the Society's Institution . [*downward flourish*]

\TranscriptionStop


### 28 December 1664 (CMO/1 pp. 85-86;  CMO/1/72) 

\TranscriptionStart

[*centered:*] December [28 . 1664 .]{.underline}

[…]

[**Page 86:**]

[…]

‌ The Council approved of M^r^. Hook as Curator , in order to recommend\
him to the Society as such .

‌ Ordered , that the President be desired , at the next Meeting of\
the Society , to recommend M^r^. Hook from the Council to the Society\
as a Curator by Office .

‌ Ordered , that the Amanuensis cause Bill[s] to be printed\
for summoning the Society , against January the 11^th^. for the\
Election of a Curator , and that the Operator carry such Bills\
abroad accordingly . [*downward flourish*]

\TranscriptionStop


### 11 January 166$\frac{4}{5}$ (CMO/1 p. 86;  CMO/1/73) 

\TranscriptionStart

[*centered:*]  January 11[ . 1664 .]{.underline}

[…]

‌ D^r^. Goddard brought in his Bill , for the money , he had laid out\
for the fitting of the Charter-book of the Society : The Treasurer\
was ordered to repay him .

‌ Ordered , That M^r^. Spratt be sent to by the Secretary to meet\
at D^r^. Wilkins his house on Monday next , to consider of certain\
papers , to be inserted in the History of y^e^. Society . [*diagonal slash*]

\TranscriptionStop

**(CMO/1 p. 85):** “Proposal concerning a Booke to be printed de Puero fulmine \|  icto de Cerebro. [i.e. *Inquisitiones II. anatomico-physicae: prior De fulmine; altera De proprietatibus cerebri humani, auctore Gualt.Charletono*]” (CMO/1)

**(CMO/1 p. 85):** “[Printing of Papers & Books.] D^r^. Charlson’s Book De Puero &c to be Licensed, he leaving out \| two paſsages in the, Preface” (CMO/8A)

**(CMO/1 p. 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 86):** “Bills to be printed for Summoning the Society for the Election of a Curator.” (CMO/1)

**(CMO/1 p. 86: 77,80,86):** “Order concerning direction for prepareing a Charter Book.” (CMO/1)

**(CMO/1 , v. 1 p. 86:)** “Charter Book. Minutes concerning. [*the charter which granted the right to print*]” (CMO/8A)

### 18 January 166$\frac{4}{5}$ (CMO/1 pp. 87-88;  CMO/1/74) 

\TranscriptionStart

[*centered:*] January [18^th^. 1664 .]{.underline}

[…]

‌ D^r^ Wilkins made a Report from the Committee formerly —\
appointed to meet and consider of certain papers to be inserted in\
the History of the Society : Viz^l^. That the Committe had met , and\
looked over a number of such papers , as did appear by a List ;  and\
that they had thought good , that such papers might be referred to\
the repectiue Authors thereof , to review them , before they were —\
printed , seing they were to be published with their names prefixed\
thereunto .

‌ It was ordered , that such papers according to the said Liste\
be referred, and reveiwed , and then delivered to M^r^. Spratt to ~ ~\
insert them , and before they are printed , to present them to the —\
perusal of the President .

‌ Ordered , that M^r^. Hill take care of having drawn out of the —\
General Liste of the proposers of Candidates , the Names of every such\
proposer , together with the names of those who were thus respectiuely\
proposed by them , to the End , that the persons proposed , may be\
minded of their Payments , by their Proposers .

‌ M^r^. Hook having made a Proposition of giuing the ~\
Longitude , as he hath conceived it , to the Society , it was ~\
ordered, that he should choose such persons to commit this\
Busineſse to, as he thought good ; and make the Experiment :[*over:* ‘;’]\
that by such persons chosen , the Council might be satisfyed of [**page 88:**]\
the Truth and practicableneſse of his Invention , and proceed accordingly\
to take out a Patent for him .

‌ Ordered , that D^r^. Wilkins do meet the first time (at least)\
with the Committee for improving the English tongue , and that\
particularly he do intimate to them , the way of proceeding in that\
Committee , according to the ſense of the Council ; Viz^l^. cheifly\
to improue the Philosophy of y^e^. Language .

‌ Ordered, that the printing of Horrox's papers be considered\
the next Meeting of the Council . 

\TranscriptionStop


**(CMO/1 p. 87: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 87):** “[Printing of Papers & Books.] for Spratt’s History, to be reviewed by the Authors” (CMO/8A)


### 25 January 166$\frac{4}{5}$ (CMO/1 pp. 88;  CMO/1/75) 

\TranscriptionStart

[*centered:*] January 25 . 1664 .

[…]

‌ M^r^ Hill having been ordered formerly , to take care of having\
drawn out of the general Liste of the Proposers , the Name of\
every Proposer , together with the names of the proposed by them\
for the better collecting the Arrears : but not being now present\
it was thought neceſsary , he should at the next meeting be —\
preſsed to see this order executed with care and diligence .

[…]

\TranscriptionStop


**(CMO/1 p. 88):** “[Voted upon a Comittee for improueing the Engl language] Order concerning the same.” (CMO/1)

**(CMO/1 p. 88: 78,88):** “Order concerning M^r^: Horrexs Manuscript.” (CMO/1)

**(CMO/1 p. 88,159,161,249):** “[The Benefactors to the Society to be registred in loose ~ \| vellum ſheets.] Resolution & Order concerning the same.” (CMO/1)


### 25 February 166$\frac{4}{5}$ (CMO/1 pp. 89;  CMO/1/76) 

\TranscriptionStart

[**Page 89:**]

[*centered:*] February 25 [. 1664 .]{.underline}

[…]

‌ It was ordered , That every Member in Arrears should haue a\
Ticket  sent to him desiring him to pay the said Arrear .

‌ Ordered , That all such Members as haue Sponsors shall not\
haue any such Tickets sent them , but their Sponsors are desired\
to speake to them , and returne their Answer .

[…]

‌ Ordered , That a List of the Persons so in Arrear , and also\
of the Sponsor for each of them , together with every particular\
Arrear , be laid before the President , at every meeting of the\
Council and Society . [*downward flourish*] 

\TranscriptionStop

**(CMO/1 p. 89):** “[Order for making a List concerning Debtors of Arrears] And of the Sponsors.” (CMO/1)

### 1 March 166$\frac{4}{5}$ (CMO/1 pp. 89-90;  CMO/1/77) 

**1 March 166**$\frac{4}{5}$ **(CMO/1/77, v. 1 p. 89–90):**

“[Arrears of contributions] To be wrote for” (CMO/8A)

“[Arrears of contributions] Tickets to be printed for demanding” (CMO/8A)


\TranscriptionStart

[*centered:*] March . 1 .[‌ 1664 ]{.underline}.

[…]

‌ Ordered, that four or fiue hundred Tickets be printed [**page 90:**]\
for demanding the Arrears of the Fellows of the Society ,\
leaving Blanks for the Time and the respectiue Summs .

‌ Ordered , that the *Philosophical Trnasactions* , to\
be composed by M^r^. Oldenburg , be printed the first Munday\
of every moneth , if he haue sufficient matter for it , and\
that that Tract be licensed by the Council of the Society , —\
being first reveiwed by some of the Members of the same .\
And that the President be desired , now to Licence the first\
papers thereof , being written in four sheets in folio , to be —\
printed by John Martyn and James Allestree. [*downward flourish*]

\TranscriptionStop

### 15 March 166$\frac{4}{5}$ (CMO/1 pp. 90;  CMO/1/78) 

\TranscriptionStart

[*centered:*] March 15 . 1664 .

[…]

‌ Ordered ,\
‌   That the Amanuensis do attend the Treasurer , and receiue\
instructions from him for inserting into the printed bills for\
demanding the Arrears , the Sums of them and the time : and ~\
that the Operator be diligent in going about and demanding\
such Arrears . [*downward flourish*]

[*horizontal flourish*]

\TranscriptionStop


**(CMO/1 p. 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 90):** “Tickets Ordered to be printed for demanding Arrears.” (CMO/1)

**(CMO/1 p. 90):** “[Printing of Papers & Books.] Of the Phil. Trans. on the 1^st^ monday of every month.” (CMO/8A)

**(CMO/1 p. 90):** “Philosophical Transactions. \| by Oldenburg, to be License for printing,” (CMO/8A)

### 29 March 1665 (CMO/1 pp. 91;  CMO/1/79) 

\TranscriptionStart

[**Page 91:**]

[*centered:*] March 29.[‌ 1665..]{.underline}[*over:* ‘1664’]

[…]

‌ Ordered\
‌   That the President by desired to licence the Second ~\
Tract of [Philosophical Trnasactions]{.underline} , written in four sheets\
of paper in folio . [*downward flourish*]

\TranscriptionStop

[Query: is this second tract the one discussed in the next minute? It would seem this first request is to peruse the second and that the next meeting reports on a problem.]

### 26 April 1665 (CMO/1 pp. 91;  CMO/1/80) 

\TranscriptionStart

[*centered:*] April 26. 1665 .

[…]

‌ The Papers of the next Philosophical Transactions , having\
been considered of , and the Account therein given concerning\
the Structure and Advantages of S^r^. William Petty's Double ~ —\
bottom’d ship ; it was resolved , that the Publication of them\
should be differred[*over:* ‘defferred’] , till his Ma^ty^. had been made acquainted\
with the particulars , therein , relating to the said Ship . [*downward flourish*]

[*horizontal flourish*]

\TranscriptionStop

**(CMO/1 p. 91: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

### 3 May 1665 (CMO/1 pp. 92;  CMO/1/81) 

\TranscriptionStart

[**Page 92:**]

[*centered:*] May 3 [. 1665 .]{.underline}

[…]

‌ Ordered ,\
‌   That the President be desired to Licence the Third\
Tract of the Philosophical Transactions , written in about\
fiue sheets of paper in folio : deferring to another time , the\
particulars concerning S^r^. William Petty's Ship . [*downward flourish*]

[**Paragraph from preliminary sheet:**]

‌  Wanting in y^e^ minutes of y^e^ Council of May 3 : 1665 .\
Order’d that a letter be drawn up to be signed &c .\| about arrears . w^th^ form of y^e^ letter\
‌          w^ch^ is repetd to Feb .26 . 65 .

\TranscriptionStop

### 16 May 1665 (CMO/1 pp. 92;  CMO/1/82) 

\TranscriptionStart

[*centered:*] May 16 .[‌ 1665]{.underline}.

[…]

‌ Ordered ,
‌   That the President , S^r^. Robert Moray , S^r^ William Petty ,\
and D^r^. Wilkins be a Committee for reveiwing M^r^. Sprat's Relation\
concerning the Institution and Designe of the Royal Society . [*downward flourish*]

[*horizontal flourish*]

\TranscriptionStop

**(CMO/1 p. 92,93):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] 3^d^ Tract & 4^th^ Tract” (CMO/8A)

**(CMO/1 , v. 1 p. 92:)** “[Committee.] To consider of Spratt’s Hist. R. S.” (CMO/8A)

**(CMO/1 p. 92: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

### 29 May 1665 (CMO/1 pp. 93;  CMO/1/83) 

\TranscriptionStart

[*centered:*] May 29 .[1665.]{.underline}

[…]

‌ Ordered ,
‌   That M^r^. Sprat be desired to take Notice in his Book of —\
the Society , what is meant by their Council , when they grant an\
Imprimatur ; and to draw up a draught concerning it , and\
offer it to the Council.

‌ Ordered , that the President be desired to License the —\
Fourth Tract of the Philosophical Transactions , written in —\
six written sheets of paper in folio .

[…]

\TranscriptionStop

**(CMO/1 p. 93: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 93):** “Order that Notice may be taken in the Book of Society ~ \| concerning the word imprimatur.” (CMO/1)

**(CMO/1 p. 93: 92,93):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] 3^d^ Tract & 4^th^ Tract” (CMO/8A)

**(CMO/1 , v. 1 p. 93:)** “[Committee.] To consider of Spratt’s Hist. R. S.” (CMO/8A)


### 20 June 1665 (CMO/1 pp. 93-94;  CMO/1/84) 

\TranscriptionStart

[*centered:*] June [20. 1665.]{.underline}

[…]

[**Page 94:**]

‌ Ordered , that the President be desired to moue it at the\
next Meeting of the Society , that by reason of the present\
Contagion , it will be convenient to intermit their publique\
weekly Meetings , untill the Sickneſse doe cease , and the —\
President , with the advice of the Council , summon them to\
meet again .

‌ Ordered, That the Curator , Amanuensis and Operator , ~\
during this intermiſsion , be imployed by the direction of any\
three of the Council , in busineſse concerning the deſigne and\
work of the Society ; Of which three , the President , Vice ⹀\
President, S^r^. Robert Moray , S^r^. William Petty , D^r^. Goddard ,\
the Treasurer , either of the Secretaries , D^r^. Croone or M^r^. Colwall\
shall be one.

[…]

‌ Ordered ,\
‌   That upon a Report made by S^r^. William Petty , of\
his having perused the Additions of M^r^. Graunt , to his\
Observations upon the Bills of Mortallity , the President be\
desired to license the reprinting of that Book , together\
with such Additions ; and it was licensed accordingly .

‌ Ordered,\
‌   That the President be desired to signe the Amanuensis\
his Bill , from Michaelmas 1664[*over:* ‘1660’] to Midsummer 1665 : being\
21^~~li~~^. 01^ſh^. 6^d^. [*downward flourish*] 

[*horizontal flourish*]

\TranscriptionStop

**(CMO/1 p. 94):** “Order concerning the Curator, Amanuensis, & Operator during the intermission of the Meeting [by reason of the Contagion]” (CMO/1)

**(CMO/1 p. 94):** “Order to procure & License for M^r^: Graunts Book to be reprinted.” (CMO/1)

**(CMO/1 p. 94):** “[Printing of Papers & Books.] Graunt On the bills of Mortality. Licensed” (CMO/8A)

**(CMO/1 p. 94):** “Order concerning the Amanuensis Bill” (CMO/1)

**(CMO/1, v. 1 p. 94):** “[Amanuensis] his fee for writing” (CMO/8A)


### 21 June 1665 (CMO/1 pp. 95;  CMO/1/95) 

\TranscriptionStart

[**Page 95:**]

[*centered:*] June 21[. 1665 .]{.underline}

[…]

‌ S^r^. Paul Neile moved , that the Amanuensis might be —\
ordered to attend S^r^. Anthony Morgan , and to desire of —\
him the Papers which concerne the Society ,  in the matter\
of Chelsey-college , and to deliver them to D^r^. Wilkins , who —\
should be desired to find out a fit person  , that might in\
the absence of S^r^. Anthony Morgan , attend M^r^. Attorny⹀\
General , for the prosecuting this busineſse concerning —\
Chelsey- College.  And it was ordered accordingly . [*downward flourish*] 

\TranscriptionStop

**(CMO/1 p. 95: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 95):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] 5^th^ [Tract]” (CMO/8A)

### 28 June 1665 (CMO/1 pp. 95-96;  CMO/1/96) 

\TranscriptionStart

[*centered:*] June 28 . [1665.]{.underline}

[…]

‌ Ordered , that the President be desired to Licence the\
Fifth Tract of the Philosophical Transactions , written\
in four sheets of Paper in folio.

‌ Ordered, that the President be desired to signe the [**page 96:**]\
allowance for M^r^. Hook , as Curator of the Society, ~ ~\
though the summe do exceed fiue pounds . [*downward flourish*] 

\TranscriptionStop

### 21 February 166$\frac{5}{6}$ (CMO/1 pp. 96;  CMO/1/97) Gresham College

\TranscriptionStart

[*centered:*] February 21 . 166$\frac{5}{6.}$

‌ The Council , after a long interruption , caused by the Contagion\
met again in the usual place in Gresham College , Viz^l^.

[…]

‌ Ordered , that the Philosopohical Transactions Numb. 9 be\
printed by John Martyn and James Allestree .

[**From preliminary sheet:** ‘line 18. C.B. has :[by J^n^ Martyn & J. Allestree]{.underline} not inc^d^ Orig^l^. Minutes .’]

[…]

\TranscriptionStop

**(CMO/1 p. 96: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 96,191):** “Form of Letters concerning y^e^ collecting of Arrears” (CMO/1)

**(CMO/1 p. 96):** “Voted for the Collection of Rarities.” (CMO/1)

**(CMO/1, v. 1 p. 97):** “[Arrears of contributions] Form of notice to be sent”


### 26 February 166$\frac{5}{6}$ (CMO/1 pp. 97-98;  CMO/1/98) 

\TranscriptionStart

[**Page 97:**]

[*centered:*] February 26 . 166$\frac{5}{6}$ .

[…]

‌ Ordered,\
‌   That the several Members of the Council , that haue par⹀\
ticular acquaintance with those Lords , that are of the Society ,\
and in arrears , be desired to recommend to them those Letters ,\
which formerly were drawn up , to put them in mind to satisfy\
such Arrears :  And y^t^ accordingly , the President be desired to\
giue to the Earle of Northampton ,  the letter addreſsed to him ;\
The Lord B^p^. of Exeter and S^r^. Robert Moray , to the Marqueſse\
of Dorchester , his ;  S^r^. Robert Moray , to the Duke of Buckingham,\
his ; D^r^. Wilkins to the Lord Hatton, his .

‌ Ordered , that the Collector make haste to go about with the —\
general List of Arrears , and use all Diligence to gather them\
in ; and that in doing so , he take particular notice of those\
that refuse of delay payment .

‌ Resolved , that the Amanuensis do make several Copies of\
the following Order , for those that are hereafter named , inserting\
their Names , and the sums of their Arrears , and carry them to\
the President to be signed . 

‌    The Forme of the Order is to be this ,

Ordered , that the Collector [*canceled:* ‘~~of~~’]‸^to^ the Treasurer of the Royal ~ —\
Society , do repair to \_\_\_\_\_\_\_\_ and giue him notice ,\
that he is in Arrear tho summe of \_\_\_\_\_ due to the\
Royal Society on the 23^th^. of December last past , according to [**page 98:**]\
his Subscription , and the Statutes of the said Society :  and do\
desire him to pay the said sum̃e unto the said Collector , who in\
case of non payment , is to returne his Answer unto the —\
Council of the said Society .

[…]

\TranscriptionStop

**(CMO/1 p. 97):** “Quorum of Seven requisite for giving Licences for publishing a Booke” (CMO/1)

**(CMO/1 p. 97):** “Letters to be recommended to the Lords that are in Arrears.” (CMO/1)


### 12 March 166$\frac{5}{6}$ (CMO/1 p. 98;  CMO/1/99) 

\TranscriptionStart

[*centered:*] March 12 166$\frac{5}{6}$ .

[…]

‌ It being signified by M^r^. Oldenburg , that the President had\
perused the Philosophical Transactions designed for this Month ,\
Viz^l^. Numb^r^. 10 , and given his consent for the publishing of them ,\
those , that were present , gaue their consent likewise for it , to\
make up the Quorum of seven , requisite for giuing Licence . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 98: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 98):** “[Printing of Papers & Books.] License for printing, by 7 of the Council” (CMO/8A)


### 21 March 166$\frac{5}{6}$ (CMO/1 pp. 98-99;  CMO/1/100) 

\TranscriptionStart

[*centered:*] March 21. 166$\frac{5}{6}$ .

[…]

[**Page 99:**]

‌ The President moving , that it would be neceſsary , the Society\
should proceed to the Election of a New Council and Officers\
for the remaining part of this year ; and it being according\
to the Charter requisite, that there do meet for that purpose , at\
the least Thirty one of the Fellows of the Society , for appointing\
a day for such Election , it was ordered , that Summons should\
be issued , to this effect : Vid :

‌ That there being an Extraordinary occasion , for a full ~ —\
Meeting of the Society , on Wednesday next the 28^th^. of March 1666 .\
the several Fellows were desired not to faile , to be then present\
at the usual time and place .

‌ Ordered also , That the Amanuensis draw up for the next ~ ~\
Meeting of the Council , a fair List of all the Fellows of the Society\
in order to print New Lists for the Election-day ; and that he leaue\
some space on the top of the Paper , for his Ma^ty^., and the Princes ,\
that are of the Society , and some space also at the beginning of\
every Letter for occasion .

[…]

‌ Ordered , that D^r^. Wilkins and D^r^. Goddard do meet , to —\
consider of a Supplement to the Charter of the Society .

‌ D^r^. Wilkins and M^r^. Hook were desired , that they would look\
over the Operator's Bills , and if they find them just , set their hands\
to them

[…]

\TranscriptionStop

**(CMO/1 , v. 1 p. 99:)** “Charter. Additional Articles considered.” (CMO/8A)

**(CMO/1, v.1 p. 99):** “[Lists of the Society] \| Order about drawing it up” (CMO/8A)

**(CMO/1 p. 99: 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)



### 29 March 1666 (CMO/1 pp. 100-101;  CMO/1/101) 

\TranscriptionStart

[**Page 100:**]

[*centered:*] March 29 . [1666 .]{.underline}

[…]

‌ The List of the Fellows of the Royal Society was perfected\
and ordered to be printed against Wednesday next .

‌ Ordered , That a particular written Summons be sent to the\
Princes of the Bloud , and to as many of the Lords of the ~ ~\
Society as are in towne ; in the Forme following

‌ On Wednesday the eleventh of this instant April 1666 , at\
two of the clock in the afternoon , the President, Council and [**page 101:**]\
Officers of the Royal Society , for the remaining part of this\
present year , are to be elected , at the usual place in — — —\
Gresham College .

‌ Ordered also , that the following forme be printed and sent to\
the Fellows ,[*over:* ‘fellows’] for this extraordinary day .

‌ Whereas the usual time for the Annual Election , could not be\
observed this last year , by reason of the Sickneſse : These are to\
giue Notice , that the Royal Society , according to the power given\
to them by Charter , haue appointed the eleventh day of April\
1666 , being Wednesday , for the Election of the Council and Officers\
of the Royal Society , for the remaining part of this year : At\
which Election your presence is expected , at two of the clock\
in the afternoon , at the usual place .

‌ The Amanuensis was appointed , to haue the Lists ready —\
against Wednesday next ; as also the Summons to be ſigned by the\
President.

‌ Ordered, that the Philosophical Transactions Numƀ^r^. 11 . be —\
printed . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 100: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 100: 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)

**(CMO/1 p. 100):** “Particular written Summons to be ſent to the Princes of Blood concerning Election.” (CMO/1)

**(CMO/1 p. 100):** “[Particular written Summons to be ſent to the Princes of Blood concerning Election.] Forme concerning the same to be printed.” (CMO/1)

**(CMO/1 p. 101: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 101: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 101: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**29 March 1666 (CMO/1/101, v. 1 pp. 100–101):** “[Anniversary Meeting] List of Members to be printed” (CMO/8A)

### 9 April 1666 (CMO/1 pp. 101–102;  CMO/1/[102]) 

\TranscriptionStart

[*centered:*] April 9. 1666.

[…]

‌ There was appointed a Committee of the Council to examine —\
the Treasurers Accompts , and to prepare them for the Committee\
of the Society , according to Statue ; Viz^l^. The President , Doct^r^. [**page 102:**]\
D^r^. Wilkins , M^r^. Colwall , M^r^. Graunt, M^r^. Oldenburg .

‌ This Committee of the Council , having examined the Accompts\
and after rectification of some mistakes in the Transcript ~ —\
thereof , made a Report to the Council , what was accepted ;\
the Council delivered the said Accompts to the Committee of the\
Society , appointed for auditing the same .

‌ S^r^. Anthony Morgan brought in from the Lord Chancellour\
the Report concerning Chelsey-College, to the end , that it might\
be read by the Council , before it be signed by his Lords^p^. and\
the other Lords Referees .

‌ The Council having read it , Ordered , That the President ,\
S^r^. Robert Moray and S^r^. Paul Neile , should be desired to returne\
to the Loard Chancellour their humble Thanks , for his Lords^ps^. ~\
favour in giving them a view of this Report before ſigning : —\
And that the [*canceled:* ‘~~B^p^.~~’] Lo^d^. B^p^. of Exeter should be desired to deliver ~\
the Report to the Lo^d^. Arch B^p^. of Canterbury and to the Lo^d^. B^p^.\
of London to signe ; and that then it should be , by the care of\
S^r^. Anthony Morgan , sent to M^r^. Matthew Wren , to be signed —\
also , by the Lord Chancellour

‌ In the mean while , S^r^. Anthony Morgan was desired , to\
speak with M^r^. Attorney-Generall , and to enquire , whether —\
there be not a mistake in his Report , as to the House of\
Chelsey-College. [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 102,104,105,107):** “[Chelsey Colledge.] Report brought in concerning the same.” (CMO/1)

**(CMO/1 p. 102,104):** “[Chelsey Colledge.] Desire about the same—” (CMO/1)

**(CMO/1 p. 102):** “[Reports to the Council] Accepted & delivered to the Comittee of the Society” (CMO/1)


### 23 April 1666 (CMO/1 pp. 103-104;  CMO/1/103) 

\TranscriptionStart

[**Page 103:**]

[*centered:*]  April 23 . [1666]{.underline}.

[…]

‌ M^r^. Colwall was sworne as Treasurer to the Society .

‌ Ordered, that M^r^. Hill do pay M^r^. Colwall , the present —\
Treasurer , the remainder of the Accompt in his hands : Viz^l^.\
Thirty four pounds, two shillings and Eight pence ; and that\
taking the Receipt for the same shall be his discharge.

[…]

‌ S^r^. Robert Moray mentioned, that the Lord Maſsareen ~\
had formerly , by a Letter desired M^r^. Boyle and himself, that\
they would engage themselves for One hundred pounds , which —\
he did giue to the use of the Society .

‌ That Letter being in the President's hands , his Lordsh.^p^ was\
desired to bring it with him , to the next Meeting of the —\
Council .

[**Page 104:**]

[…]

‌ S^r^. Anthony Morgan mentioned , that the Bish^p^. of London\
not having yet signed the Report of the Lords Referrees ~\
concerning Chelsey- College , it had not yet been presented to the\
King . He added that M^r^. Attorny-General had declared , that\
his Ma^ty^. granting onely what right he had to Chelsey - College ,\
M^r^. Cole could not at all be prejudiced by that Grant . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 104: 102,104,105,107):** “[Chelsey Colledge.] Report brought in concerning the same.” (CMO/1)

**(CMO/1 p. 104: 102,104):** “[Chelsey Colledge.] Desire about the same—” (CMO/1)

**(CMO/1 p. 104):** “President to be desired for writeing Letters concerning Arrears.” (CMO/1)

**(CMO/1 p. 104):** “[President to be desired for writeing Letters concerning Arrears.] Treasurer desired about the same.” (CMO/1)


### 9 May 1666 (CMO/1 pp. 104-105;  CMO/1/104) 

\TranscriptionStart

[*centered:*] May . 9 . 1666 .

[…]

‌ Ordered, that the President be asked to write a Letter to\
those of the Nobility that are Members , concerning their Arrears ,\
to this purpose , Viz^l^. that the Occasions of the Society requiring\
a present Supply of Money , they are desired to giue order for\
the immediate payment of their respectiue Arrears to the —\
Treasurer , or to whom he shall appoint to wait upon their\
Lords^ps^. for it .

‌ Ordered , that the Treasurer write to the rest of the Society\
to the like purpose .

[**Page 105:**]

‌ S^r^. Anthony Morgan was desired to get the Papers concerning\
Chelsey-College , and to send them to the President , that, together\
with them , the Report of the Lords Referees may be  his —\
Lords^p^. presented to [*canceled:* ‘~~the Lord Arlington~~’] [*Oldenburg’s hand:*] his Majty .

‌ Ordered , that M^r^. Colwall giue two Guiny peices in Gold to\
M^r^. Billup , for drawing up the Report concerning Chelsey -\
College. [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 105):** “[A Lawyer to be consulted concerning the Deputies of the President appointed in his stead.] Forme of the Oath concerning the same.” (CMO/1)

**(CMO/1 p. 105):** “[Chelsey College. Report.] Two Guinees appointed for drawing up the same Report.” (CMO/1)

**(CMO/1 p. 105: 102,104,105,107):** “[Chelsey Colledge.] Report brought in concerning the same.” (CMO/1)

**(CMO/1 p. 105):** “[Vice Presidents] Their Oath &c referred to a Lawyer” (CMO/8A)

**(CMO/1 p. 105,107):** “A Lawyer to be consulted concerning the Deputies of the President appointed in his stead.” (CMO/1)


### 4 June 1666 (CMO/1 pp. 105-106;  CMO/1/105) 

\TranscriptionStart

[*centered:*] June 4 . [1666]{.underline}.

[…]

‌ M\r. Palmer was desired to consult with a Lawyer about the\
Case following ; Viz^l^.

‌ Whether the President , appointing one Deputy in his Absence ,\
and another in the absence of the said Deputy , and so further ;\
these Deputies be not one in law , and may be all sworne ~ ~\
together , and act with intermiſsion , as occasion serues , with⹀\
out being sworne anew ; the forme of the Oath , being as ~\
followeth ;

‌ I *A.B.* do promise to deale faithfully and honestly in all\
things belonging to the trust committed to me , as Vice-President\
of the Royal Society of London for improving Natural Knowledg\
during my Imployment in that Capacity .

[**Page 106:**]

‌ It was Ordered, that the Clause , which contains the power\
of Deputation , be transcribed by the Amanuensis , out of\
the Charter , and shewed to the same Lawyer , together with\
the recited Case .

‌ Ordered, that the Secretary bring in a Draught of a — —\
Diploma for Mons^r^. Auzout .

‌ Ordered , that the Philosophical Transactions , Num : 13 . be\
be printed .

‌ Ordered , that y^e^[*over:* ‘a’] Committee of the Mercer's Company be\
spoken to by M\r. Harrington , M^r^. Colwall and M^r^. Graunt , ~\
for flooring the West-Gallery of Gresham College . The Forme\
of the Order to be thus ,

[*two lines centered:*]\
June 4 . [1666 .]{.underline}\
At a Meeting of the Council of the Royal Society.

Ordered,\
‌ That M^r^. Daniel Colwall , M^r^. William Harrington , and ~\
M^r^. John Graunt do waite upon the Committee for the affaires of\
Gresham College London , to desire that they would please to ~\
repair the Floor and Windows in the West Gallery of the ~\
said College , where the Societyes Repository is to be . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 106: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 106):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] N^o^. 13” (CMO/8A)

**(CMO/1 p. 106,107):** “A draught of a Diploma to be brought in.” (CMO/1)

**(CMO/1 p. 106):** “The clause, containing the Power of Deputaion in the Presidents Stead, to be transcribed out of the Charter &.” (CMO/1)

### 20 June 1666 (CMO/1 pp. 106-107;  CMO/1/106) 

\TranscriptionStart

[*centered:*] June 20 .[‌ 1666.]{.underline}

[…]

‌ Ordered, that the Porter of Gresham College be paid his yeerly\
wages as formerly . Vide this Journal of An^o^. 1663 . January 27.

[**Page 107:**]

[…]

‌ Ordered, that the consideration of the Lawyer's answer to\
the Case formerly recommended to M^r^. Palmer , as also the\
Draught of the Diploma for M^r^. Auzout , be both deferred, till\
the next meeting of the Council. [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 107: 105,107):** “A Lawyer to be consulted concerning the Deputies of the President appointed in his stead.” (CMO/1)

**(CMO/1 p. 107: 106,107):** “A draught of a Diploma to be brought in.” (CMO/1)

**(CMO/1 p. 107: 102,104,105,107):** “[Chelsey Colledge.] Report brought in concerning the same.” (CMO/1)


### 4 July 1666 (CMO/1 pp. 107-108;  CMO/1/107)

\TranscriptionStart

[*centered:*] July 4 [: 1666. ]{.underline}

[…]

‌ Ordered, That the President , accompanied with S^r^. Robert ~\
Moray and S^r^. Paul Neile , do present to his Ma^ty^. ; when they ~ —\
shall see a convenient time for it , the Report of the Lords ~\
Commiſsioners about the busineſse of Chelsey College ; as also that\
the Treasurer do issue out such sum̃s of money , for the occasions [**page 108:**]\
of the same Busineſse , as shall be thought neceſsary by Sir\
Anthony Morgan

‌ Ordered, That the Answer , brought in by M^r^. Palmer\
to the Case , concerning the Plurality of Deputations of\
Vice-presidents, ( as it was stated June 4.. last ) be recorded\
upon the Journal-book of the Council : Viz^l^.

‌ I do conceiue the President can make but one Deputy at\
one time ; but I do conceiue if he make two Deputations ~\
to two ſeveral persons , to avoyd the inconvenience of —\
the absence of one of them , that such of the Deputies , as\
come first may Act , and the Entry will bee before such an\
one lawfully deputed , and there being no matters of moment\
then acted, it will do well enough : But if one Deputy sit ,\
and after, another , the Deputation to the first ceaseth . [*downward flourish*]

\TranscriptionStop

### 18 July 1666 (CMO/1 pp. 108;  CMO/1/108) 

\TranscriptionStart

[*centered:*] July 18.\[*over:* ‘4’\][‌ 1666.]{.underline}

[…]

‌ There was licenced Num: 15 . of the Philosophical ~ ~\
Transactions , consisting of two written sheets in folio . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 108):** “[A Lawyer to be consulted concerning the Deputies of the President appointed in his stead.] Lawyers Answer concerning the Case proposed.” (CMO/1)

**(CMO/1 p. 108: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 108: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 108,109,110):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] N^os^. 15, 16, 17” (CMO/8A)


### 8 August 1666  (CMO/1 pp. 109;  CMO/1/109) 

\TranscriptionStart

[**Page 109:**]

[*centered:*] August 8.[‌ 1666 .]{.underline}

[…]

‌ Ordered , That the Philosophical Transactions Numƀ^r^. 16 . be\
printed , containing about Eight written sheets in folio .

‌ Resolved , that the original of the late Lord Maſsereen's letter\
concerning the hundred pounds , by his Lords^p^. intended‸^[*Oldenburg’s\ hand:*]\ as\ a\ Donation^ for the\
use of the Royal Society , written to M^r^. Boyle and S^r^. Robert\
Moray , be sent to one fo the Members of the said Society residing\
at Dublin , who should be desired to deliuer a Copy of it to the\
now Lord Maſsereen , to see his inclinations for the performance\
of the said intention . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 109: 108,109,110):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] N^os^. 15, 16, 17” (CMO/8A)

**(CMO/1 p. 109: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 109, v.2 p.54,279, v.3 p.147):** “Presents from the Society \| Minutes” (CMO/8A)

**(CMO/1 , v. 1 p. 109:)** “[Hevelius Mons^r^.] Orders about his Diploma & Presents of Books” (CMO/8A)


### 29 August 1666 (CMO/1 pp. 109-110;  CMO/1/110) 

\TranscriptionStart

[*centered:*] August 29. 1666.

[…]

Ordered,\
‌ 1 . That M^r^. Hevelius having desired such Books , as haue\
been lately published by any of the Fellows of the Society , and\
such others as are curious and Philosophical , lately printed,\
the Secretary should provide them , and show the list of them to [**page 110:**]\
the Council , and that the Treasurer of the Society should ~\
ſatisfy for them .

‌ 2 . That the Amanuensis should make a Copy of the Warrant\
for demanding a Body for dissection ,[*over:* ‘dissecting’] to be performed in —\
Greſham College by some of the Fellows of the Society at their\
own charges .

‌ 3. That the Philosophical Transactions Numƀ^r^. 17 . ~\
consisting of ſix written sheets in folio [*canceled:* ‘~~should~~’] [*inserted comma:*], be printed [*downward flourish*]

\TranscriptionStop


### 12 September 1666 (CMO/1 pp. 110-111;  CMO/1/111) 

\TranscriptionStart

[*centered:*] September 12 [. 1666]{.underline}.

[…]

\TranscriptionStop

### 19 September 1666 (CMO/1 pp. 111;  CMO/1/112) 

\TranscriptionStart

[**Page 111:**]

[…]

[*centered:*] September 19 . 1666.

[…]

The President reported , that M^r^. Charles Howard had very\
freely offered convenient rooms in Arundel house , both for\
the Council and the Society to meet in [*inserted comma:*], if there was ~ —\
occasion for it .

‌ Ordered , that M^r^. Howard be thanked for this great —\
respect and civility

[…]

\TranscriptionStop


**(CMO/1 p. 110):** “A coppy of a Warrant for demanding a Body to be made” (CMO/1)

**(CMO/1 p. 110):** “Order concerning Books to be ſent to M^r^: Hevelius Gedanen” (CMO/1)

**(CMO/1 p. 110: 108,109,110):** “[Philosophical Transactions. \| by Oldenburg, to be License for printing,] N^os^. 15, 16, 17” (CMO/8A)

### 29 October 1666 (CMO/1 pp. 112-113;  CMO/1/113) 

\TranscriptionStart

[**Page 112:**]

[*centered:*] October 29 .\[*over:* ‘19’\][‌ 1666]{.underline} .

[…]

‌ Ordered, that the President , one of the Secretaries , M^r^. Palmer\
M^r^. Neile and M^r^. Creed , or any three or more of them , do\
meet at D^r^. Popes lodgings in Greſham College , on Wednesday\
next , October 31 . as a Committee of the Council , to examine\
the Accounts of the Treasurer , form April 11 . 1666 to ~\
Michaelmas , and to make a report thereof to the Council on\
Monday next Novemƀ^r^. 5^th^.

‌ Ordered , that the Journal of the Society be perused by\
the Secretary , to find out what had been formerly ordered ~\
concerning the payment of the Thirty pounds per Annum to\
M^r^. Hook .

‌ Ordered , that the Sponsors for several Fellows of the\
Society , do speake or write to them for the payment of their\
Arrears , and bring in , if conveniently they can , their —\
answer on ‸^Wedneſ⹀^[*canceled:* ‘~~Mon~~’]day next .

‌ Ordered , that the Lord Lucas , S^r^. John Denham , D^r^. Scarburgh\
M^r^. Driden , M^r^ Vermuyden , be left out of the Account of\
the Arrears .

[…]

‌ Ordered , that the Book for Subscriptions be carried before\
Monday next to the Earles of Manchester and Carlile , to\
subscribe their names .

‌ Ordered , that the List of the present Fellows of the [**page 113:**]\
Society, be drawn up against the same day , and then read ~ —\
before the Council , in order to print the same for the approach ⹀\
ing day of‸^ye^ Anniversary Election. [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 112: 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)

**(CMO/1 p. 112):** “Order to leave some out of the Accompt of y^e^ Arrears.” (CMO/1)

**(CMO/1 p. 112: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 112: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

### 5 November 1666 (CMO/1 pp. 113-114;  CMO/1/114) 

\TranscriptionStart

[*centered:*] November 5[ . 1666.]{.underline}

[…]

[*marginally, Oldenburg:* ‘The report to\
be inſerted here .’]

‌ The Committee appointed by the Council to examine the\
Accompts from April 1666 , to Michaelmas of the same year,\
made a report of their Examination to the Council , which was\
approved by them , who also ordered thanks to be given to the\
Treasurer , both for the justneſse of his Accompts , and his care\
and diligence[*atop, uncertain:* ‘dibigence’] in collecting the Arrears .

‌ Ordered , that the busineſse concerning the moneys pretended\
to be due to M^r^. Hook , be deferred until D^r^. Wilkins returne ;\
and that in the mean time all the Orders entred in the —\
Journals touching the same , be looked out and produced\
upon occasion .

‌ Ordered , that the List of the Society , read this day before\
the [*canceled:* ‘~~Society~~’] Council , be printed , onely altering therein ~ [**page 114:**]\
what shall be ordered on Wednesday next at the meeting of\
the Society , ſome persons being then probably to be left\
out , and some to be inserted .

[…]

\TranscriptionStop

**(CMO/1 p. 113: 38,63,99,100,112,113):** “[List of the Society & Council to be printed:] for the Anniversary Election.” (CMO/1)

**(CMO/1 p. 113: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

**(CMO/1 p. 113: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1, v. 1 p. 113):** “[Anniversary Meeting] List of Members, to be printed” (CMO/8A)

### 13 November 1666 (CMO/1 pp. 114-115;  CMO/1/115) 

\TranscriptionStart

[*centered:*] November 13 . 1666 .

[…]

‌ Ordered , that it be preposed to morrow at the meeting of\
the Society , whether they do think fit to leaue out of their\
List Mons^r^. Sorbiere ; The Council inclining to do so , but [**page 115:**]\
wanting power actually to do it without the Society :

[…]

\TranscriptionStop

**(CMO/1 p. 114: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 114):** “Sorbiere Mon^r^. du Query if not to be left out of the List” (CMO/8A)


### 27 November 1666 (CMO/1 p. 115;  CMO/1/116) 

\TranscriptionStart

[*centered:*] November 27. 1666

[…]

‌ Ordered , that the Secretary attend M^r^. Henry Howard of ~ ~\
Norfolk at Arundel- house, and acquaint him with the ſense\
they haue of his great civilities and respects to the R . Society\
which also they intend to acknowledge publickly , when he\
shall honour them with a visit , at a Meeting of the Society.

[…]

\TranscriptionStop

**(CMO/1 p. 115):** “Order for M^r^: Henry Howard of Norfolk to be complemented.” [later donated a library]  (CMO/1)


### 4 December 1666 (CMO/1 pp. 116-117;  CMO/1/117) 

\TranscriptionStart

[**Page 116:**]

[*centered:*] December 4 . 1666 .

[…]

M^r^. Henry Howard was sworne .

‌ D^r^. Wilkins moved , that M^r^. Hook might be considered , as\
to the payment of some money , he thought due to him from —\
the Society : but the orders concerning that busineſse not —\
yet being extracted out of the Journals , it was referred\
to the next meeting of the Council .

‌ S^r^. Robert Moray moved , that the Council would take care\
of supplying the defects in the Charter of the Society .  The\
Motion being approved of ,

‌ It was ordered , that the President , Earle of Northampton ,\
M^r^. Howard of Norfolk ,  S^r^. Robert Moray , M^r^. Aerskin , M^r^. ~\
Palmer , D^r^. Wilkins , D^r^. Goddard , and M^r^. Hoskyns , or any three\
or more of them be a Committee to consider , both of the ~\
particulars wherein the said Charter may be defectiue , and\
of the remedies thereof : And that they meet for that purpose\
at S^r^. Anthony Morgans lodgings , on Thursdays in the after⹀\
noon , about four of the clock ; and make report of their ~\
proceedigns from time to time to the Council .

‌ S^r^. Robert Moray proposed , that the Council would take\
into consideration , how the Experiments at the publick Meetings\
of the Society might be best carried on ; whether by a [**page 117:**]\
continued Series of Experiments , taking in collateral Ones ,\
as they are offered , or by going on in that promiscuous way\
that has obtain’d hitherto .

‌ This was left to further consideration , for the next meeting :\
as also whether the Experiments for propagating Motion , and the\
Magnetick ones, should‸^not^ be prosecuted by the Society , although\
Mons^r^. Huygens and M^r^. Ball haue engaged themselues  particu⹀\
larly , the one in those of Motion , the other in those of the\
Magnet : Mean time the Secretary should consult the Journal ,\
to see what had been ordered concerning this particular .

‌ As to the Experiments of Transfusion , there were suggested\
ſeveral : To try mutual Transfusion , between old and Young ,\
Sick and healthy , and that both of the same and of different\
Species .  In particular, it was suggested , that it should be\
tryed upon a Mangy and a Sound dog , A young and an old\
horse;  Item upon a diſeased horse and an Oxe or Cow , to\
bleed the Cow , to be killed , into such a horse .

[…]

\TranscriptionStop

**(CMO/1 , v. 1 p. 116:)** “Charter. Additional Articles considered.” (CMO/8A)


### 21 December 1666 (CMO/1 pp. 118-119;  CMO/1/118) 

\TranscriptionStart

[**Page 118:**]

[*centered:*] December 21 . [1666 .]{.underline}

[…]

‌ Ordered , that D^r^. Wilkins write a Letter from the Council\
to the Earle of Sandwich , giving him thanks for his respects\
to the Society , and his care of making Celestial Observations ;\
excusing also the omiſsion of corresponding with him —\
from hence in such Observations ; and annexing the ~ ~\
particulars of the late Solar Eclipse , observed at London\
Paris and Dantzick ; and some Experiments newly made\
in the Society.

‌ Ordered , that the Accounts concerning M^r^. Hook , be —\
Stated by the Treasurer ,  that it may appear what the said —\
M^r^. Hook hath already received , and what yet remains due\
to him , according to the several Orders formerly made by —\
the Council ; And that thereupon the Treasurer pay M^r^. Hook\
what shall thereby appear remaining due to him .

‌ Ordered , that the Summe of Forty pounds be by the Treasurer\
presented to M^r^. Oldenburg for the great pains he hath taken\
in behalf of the Society .

‌ Voted , that M^r^. Hook be desired to promise by his hand⹀\
writing, to obserue the Ends, for which the report from —\
S^r^. John Cutler, entered in the Journal-book of the Society ~\
November 9 . 1664 , affirmes the 50^~~li~~^. ⅌ annũ to be given him\
by the said S^r^. John Cutler .

‌ Ordered , that the President be desired to draw up a [**page 119:**]\
forme for such a Promise to be subscribed by M^r^. Hook .

\TranscriptionStop


**(CMO/1 p. 118: 82,118,162):** “Order concerning a Present to M^r^: Oldenburg.” (CMO/1)

**(CMO/1 p. 118):** “Oldenburg M^r^. S \| Appointed Secretary \| Presented with £40 for his great pains (3$\frac{1}{2}$ years)” [Oldenburg was the first editor of the Transactions] (CMO/8A)

**(CMO/1 p. 118,122):** “[Society Royal.] Declines to correspond with Lord Sanewich.” [TODO(confirm Sanewich in CMO/1)] (CMO/8A)


### 27 December 1666 (CMO/1 pp. 119;  CMO/1/119) 

\TranscriptionStart

[*centered:*] December 27 . [1666]{.underline}.

[…]

‌ Ordered , that the Operator Richard Shortgraue do henceforth ~ ~\
brings no Bill of Accounts , about his work for the Society , with⹀\
out some Avoucher or other , being a Curator of the respectiue\
Experiments , he shall haue been imployed about ; and that —\
without such Avoucher no account of the said Operator shall\
paſse in Council .

‌ The Operator was accordingly called in , and acquainted with\
this Order .

‌ Ordered , that S^r^. Paul Neile and M^r^. Oldenburg be added to the\
Committee appointed December 4^th^. last , for considering of the —\
Supplemental Charter ; and that the same Commmittee , upon the\
occasion of M^r^. Palmer's death , take into their consideration\
the Statute concerning the manner of Electing a New Member\
into the Council , in case of Vacancy in the intervals of the\
Anniversary Elections . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 119,150):** “[Operators] Order concerning his Bills.” (CMO/1)

**(CMO/1 , v. 1 p. 119:)** “Charter. Additional Articles considered.” (CMO/8A)


### 4 January 166$\frac{6}{7}$ (CMO/1 pp. 120;  CMO/1/120) 

\TranscriptionStart

[**Page 120:**]

[*centered:*] January 4^th^. [1666]{.underline}.


‌ Ordered , that the following Forme , to intimate the meeting\
to be henceforward at Arundel-house , be printed ;

Viz^l^. These are to giue notice , That the Weekly Meetings of the\
R. Society are appointed to be at Arundel-house , on Wednesday\
next , being the 9^th^. of this present January 1666 , and thencefor⹀\
ward, on the usual day and hour .

‌ Ordered, that M^r^. Hoskins , D^r^. Ball,  M^r^. Oldenburg and\
M^r^. Hook be a Committee for causing a Catalogue to be made\
of the Library of Arundel- house ; and that the Amanuensis\
and Operator, from time to time , attend this Committee , which\
[*margin:* ‘✗’] is to begin to meet on Thursday next in the afternoon , in\
the said Library .

‌ Ordered , that M^r^. Hook do attend D^r^. Wilkins , about ~\
reducing the Extracts of the Societies Journal-books into\
a Method , for M^r^. Spratt . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1, v. 1 p. 120):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

**(CMO/1 p. 120):** “Proposal concerning D^r^: Wallis Book. Le Metu.” (CMO/1)

**(CMO/1, v.1 p. 120):** “Journal Books. \| Extracts from, Ordered for Spratt’s History” (CMO/8A)

**(CMO/1 p. 120):** “Order concerning Societies Journal Books.” (CMO/1)


### 17 January 166$\frac{6}{7}$ (CMO/1 pp. 120-122;  CMO/1/121) 

\TranscriptionStart

[*centered:*] January 17 [. 1666 .]{.underline}

[…]

[**Page 121:**]

[…]

‌ Ordered , that the Forme, drawn up by the President , about the —\
promise to be made by M^r^. Hook , for observing the Ends for which\
(according to the Report of November 9 . 1664 ) the fifty poands —\
per Annum were given him by S^r^. John Cutler , be delivered to the\
said M^r^. Hook .  This was done accordingly : The Forme was\
as Followeth .

Whereas upon consideration , that Sir John Cutler Knight and\
Baronett hath setled upon me Fifty pounds per Annum during —\
my life , I haue promised and undertaken to read, in the ~\
Vacation times in Gresham College , or in such other place as\
the R. Society shall meet in , Sixteen Lectures per Annum ,\
in order to the Advancement of Art and Nature , the said —\
Society having been desired to direct the particular matter\
of the said Lectures , by reading one , each week , during the —\
so many weeks succeſsiuely , next after each of the four ~ —\
usual Termes in the year , as were weeks in the then last\
preceeding terme, upon such day of each week ,as the said\
Royal Society shall meet upon ; I do hereby renew the —\
said promiſe , and undertake to read the said Lectures ~\
upon such particular matters , as the said Society shall\
direct . In Testimony whereof , I haue hereunto set my\
hand and seale.

‌ Ordered , that a Copy of the said Report , as also of the\
Thanks that were to be returned to S^r^. John Cutler , be forth⹀\
with made , and delivered to S^r^. Robert Moray, or S^r^. Paul —\
Neile , to giue it to the B^p^. of Exeter , to show it to the said\
S^r^. John Cutler , that he may declare, whether it was really\
his intention , to entrust the Society with the managem^t^.\
of the 50^~~li~~^ given by him to M^r^. Hook .

‌ The letter to be sent from the Council to the Earle of [**page 122:**]\
Sandwich at Madrid , was signed by the President and ~\
Council .

‌ The Council apparoved of the‸^particulars\ for\ the^ Supplemental-Charter of\
the Society ; as also the Alteration in the Statute for —\
Electing a new Member into the Council , in case of —\
Vacancy , in the intervals of the Anniversary Elections . 

The Heads of particulars for the Charter , are these ;

‌ 1. That the power in the President ,  of substituting one\
Vice-president , may be inlarged to the substituting as\
[*margin:* ‘—’] many Vicepresidents at one and the same time , as to the\
President shall seem meet : And that all the clauses in\
the Charter , any way relating to the Vice-president , —\
may be made to relate to each of ſuch Vice-presidents\
respectiuely

‌ 2. That the several Powers which cannot be exercised\
but by the President and Council , or seven or more of them ,\
may be exercised by the President and Council , or Fiue or\
more of them .

‌ 3. That the Authority of meeting within London or ten\
miles of it , may be enlarged to all England .

‌ The Council licenced Numb. 21 . of the Philosophical —\
Transactions , consisting of 4 written sheets in folio , together\
with a Scheme ; M^r^. Martyn having undertaken again the\
printing thereof , as being somewhat resetled , after the\
late fire of London .

\TranscriptionStop


**(CMO/1 p. 121,124):** “[Mechanicall Lectures.] Form of a Subscription concerning the same.” (CMO/1)

**(CMO/1 p. 122: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 , v. 1 p. 122:)** “Charter. Additional Articles considered.” (CMO/8A)

**(CMO/1 p. 122: 118,122):** “[Society Royal.] Declines to correspond with Lord Sanewich” [TODO(confirm Sanewich in CMO/1)] (CMO/8A)


### 25 January 166$\frac{6}{7}$ (CMO/1 pp. 123-124;  CMO/1/122) 

\TranscriptionStart

[**Page 123:**]

[*centered:*] January 25 . [1666.]{.underline}

[…]

‌ The Council having considered the greater Conveniency[*over:* ‘inconveniency’] in\
the Societie’s meeting on Thursday, than Wednẽsdays ,

‌ Voted , that the fourth Chapter of their Laws , which appoints\
the ordinary weekly meetings of the Society to be on Wednesdays,\
shall be repealed .

‌ Voted also , that at the next meeting of the Council , the\
following draught , for the Societies Meeting on Thursdays ~ ~\
hereafter , be presented ; Viz^l^.

‌ The ordinary Meetings of the Society , shall be weekly upon\
Thursday , beginning about three of the clock in the afternoon,\
and continuing untill Six , unleſse the major part of the —\
Fellows present , shall for that time , resolue to rise sooner\
or sit longer ; And no Fellow shall depart without giving ~\
notice to the President. [*canceled:* ‘~~according to Statute .~~’

‌ Ordered , that the Treasurer do pay to the Operator ~ —\
( according to statute ) the yearly Salary of ten pounds , from\
the time , that the payment made to the said Operator, of\
twenty shillings per week has ceased .

‌ Ordered that the Treasurer do pay to Ellen Collet , her Arrears\
and adde to her yearly allowance , fiue shillings a quarter\
beginning from Christmas 1666 .

‌ M^r^. Hook delivered a Paper into the Council , Signed [**page 124:**]\
and Sealed by him , containing[*canceled, dittography:* ‘contain~~in~~ing’] a renewal of his promise\
and undertaking of reading 16 Lectures a year , upon —\
Such particular matters , as the said Society shall direct .\
This Paper was committed to the care of the Secretary.

\TranscriptionStop


**(CMO/1 p. 123):** “A Draught for the ordinary Meetings of the Society” (CMO/1)

**(CMO/1, v.1 p. 123,124,161,275):** “[Library.] Books to be bought for the Society” (CMO/8A)

**(CMO/1 p. 124: 121,124):** “[Mechanicall Lectures.] Form of a Subscription concerning the same.” (CMO/1)

**(CMO/1, v.1 p. 124: 123,124,161,275):** “[Library.] Books to be bought for the Society” (CMO/8A)

**(CMO/1 p. 124):** “Order concerning S^r^: Ken: Digbys Library at Paris.” (CMO/1)

**(CMO/1 , v. 1 p. 124:)** “Digby S^r^. Kenelm. Account of his Library desired” (CMO/8A) \index{Digby, Sir Kenelm+Account of library}

### 1 February 166$\frac{6}{7}$ (CMO/1 p. 124;  CMO/1/122a)

\TranscriptionStart

[*centered:*] February 1. 1666.

[…]

‌ Voted , that the Draught , which January 25 , was voted to\
be presented at the next meeting of the Council , paſse into\
a Law .

‌ Ordered , that S^r^. Samuel Turke be written to by the —\
Secretary , and desired to send the Council an Account of\
S^r^. Kenelme Digby’s Library at Paris ; what kind and number\
of books it consists of , and what they are rated at .

‌ Ordered , that M^r^. Hook do prepare himself to read before\
the Society in Arundel-house , at their next Meeting-day\
after this present Terme .

\TranscriptionStop

### 5 February 166$\frac{6}{7}$ (CMO/1 pp. 125-127;  CMO/1/123) 

\TranscriptionStart

[**Page 125:**]

[*centered:*] February 5 . [1666]{.underline} .

[…]

‌ Voted , that the Statute for meeting on Wednesdays be repealed .

‌ Voted, that the following Draught for a Statute now agreed\
upon , about the ſupplying of one or two vacant places of the —\
Council , be read at another meeting of the Council ; Viz^l^.

‌ The Eleventh Article of the Eighth Chapter of the Statutes of\
the *R.* Society , concerning the Supplying Vacancies of places , ~\
which happen in the interval of Anniversary Elections , shall\
haue place onely , where the number of persons to be Elected,\
by the Fellows of the said Society , into the Council or any Office,\
is three or more , and not other-wise .  But when there are\
but one or two to be elected , upon credible notice given to the\
President or his Deputy for the time being , that any Member\
or Members of the Council , or Officer or Officers , who ought\
to be chosen by the Fellows of the *R.* Society , is or are dead\
or otherwise removed , and his or their place or places , ~\
Office or Offices , thereby become Void ,  He the said President\
or his Deputy as aforesaid , shall , at the weekly meeting of\
the *R.* Society , which shall be next after such notice , or\
so soon as conveniently it may be done , declare to all —\
then and there present , that such Place or Places , Office\
or Offices , [*canceled:* ‘~~thereby~~‸^~~is or are~~^ ~~become void thereby~~’]‸ ^is\ or\ are^ become void and [**page 126:**]\
that at the weekly meeting then next ensuing , there shall\
be other or others elected , to supply the said vacant place\
or places , office or offices ; and at the next weekly meeting\
after , where there shall One and twenty or more be present ,\
Scrutators shall be chosen , as a Anniversary Elections ,\
and every Fellow then present , shall deliver to the [*canceled:* ‘~~Scrutators~~’]\
Secretary , a Scrowle or paper folded up, having in it\
written the name or names of such person or person , as\
he who delivers the said Scrowle shall think most fit to\
supply the said vacant place or places , Office or Offices ; —\
and when all the Scrowles are delivered in , they shall be opened\
read and counted , and he or they , that is or are named by —\
the greatest number , and such a number , as by the Charter\
is required , shall be declared elected to such Place or\
Places , Office or Offices : And if it happen , that no one —\
person be elected , to succeed in one or both of the said\
Vacant places or Offices , by a competent number of Votes ,\
as by the Charter is required , In such case , the President\
or his Deputy as aforesaid shall declare , what persons haue\
been named in the said Scrowles ; and by how many each man\
has been named , and shall then require the Fellows then\
present , to repeat the Election in manner afforesaid,‸^and^ that\
so often as there shall remain any one of the said places\
or offices unsupplyed , for want of a competent number\
of Votes , after the giving in , reading and conting the Scrowles\
as aforesaid ; unleſse the President or his Deputy and the\
Fellows of the *R.* Society , or the major part of them then ~ ~\
present , shall think fit to adjourne the Election to some other\
time , and then they shall proceed in manner abouesaid .

[…]

\TranscriptionStop


### 14 February 166$\frac{6}{7}$ (CMO/1 pp. 127-128;  CMO/1/124) Arundel House

\TranscriptionStart


[**Page 127:**]

[…]

[*centered:* ‘+’]

[*centered:*] February 14 . [1666]{.underline} .

[…]

‌ Voted , that the draught of the now agreed upon Statute , about\
Supplying one or two vacant places in the Council , do paſse —\
into a Law , as ‘tis found in the Journal of February 5 . 1666 .

[…]

[**Page 128:**]

[…]

‌ Item that S^r^. Anthony Morgan be put in mind , to draw up the\
Forme of the particulars to he added to the Charter , and to ~\
present them to the Council , when he is ready .

‌ Item , that the same S^r^. Anthony Morgan be desired to draw\
up a Deed of guift , concerning the Library presented by —\
M^r^. Howard to the Society . [*downward flourish*]

\TranscriptionStop

**(CMO/1 , v. 1 p. 128:)** “Charter. Additional Articles considered.” (CMO/8A)

**(CMO/1 , v. 1 p. 128:)** “[Arundel Library.] Given to the R.S. by L^d^. Howard” (CMO/8A)  “[Howard. Hon: Henry] Presents the Arundel Library.” (CMO/8A)

**(CMO/1 p. 128,130,138,143,197):** “Memorandum & Order concerning the Library, presented by M^r^. \| Howard” (CMO/1)


### 14 March 166$\frac{6}{7}$ (CMO/1 pp. 128-129;  CMO/1/125) probably Arundel House 

\TranscriptionStart

[*centered:*] March 14 . 1666 .

[…]

‌ S^r^. Anthony Morgan gaue the Council an Account , that D^r^. Wilkinson\
had delivered up the Charter of Chelsey-college into his hands , and\
referred all to the discretion of the Society , without insisting —\
upon any capitulation .

‌ The Council hereupon desired S^r^. Anthony Morgan ,that he would\
acquaint D^r^. Wilkinson , how well they had taken this frankneſse of\
his , and how ready they were to shew him their respect and kindeſse ,\
as occasion should serue .

‌ S^r^. Anthony Morgan was also desired , to consider , whether it\
would be neceſsary , that D^r^. Wilkinson should make a formal — —\
Resignation and Surrender of the said Charter .

‌ The same , M^r^. Aerskine and S^r^. [*canceled:* ‘~~Anthony Morgan~~’] Robert Moray ,\
were desired to look into the Title of M^r^. Coale to Chelsey-college ,\
and having found it clear , to see a conveighance of it made to\
the Society , and to conclude with him for One hundred pounds\
for it .

‌ S^r^. Andrew King , by a Note under his hand , desired to be left\
out of the List of the Society .

‌ M^r^. Howard desired , that if any Papers concerning his —\
Family were met with in the Library , he had made a Donation\
of to the Society , they might be carefully preserved for him .

‌ The Council licenced Numƀ. 23 . of the Phil : Transactions . [*downward flourish*]

\TranscriptionStop


### 28 March 1667 (CMO/1 pp. 129-130;  CMO/1/126) probably Arundel House 

\TranscriptionStart

[*centered:*] March 28 . 1667

[…]

[**Page 130:**]

[…]

Voted , that the Treasurer pay thirteen pounds to M^r^. Hook ,\
for four Cabinets , to be used in the Societie's Repository .

‌ S^r^. Anthony Morgan gaue an Account of M^r^. Coale's Title to\
the Land belonging to Chelsey-college ; which not being found\
clear , 

‌ It was ordered , that S^r^ Anthony Morgan should be desired\
to speak with M^r^. Coale from the Council , to this Effect ,

‌ That if he did make a legal conveighance of his Lease of the\
Land to the Council of the Society , and procured a release\
from those that pretended to it , or did get any such pretender\
to joyne with him in the conveighance of his Lease , they would\
then pay him an hundred pounds .  And S^r^. Anthony Morgan was\
desired accordingly to do so .

‌ The same S^r^. Anthony was desired to consider of a deed of Gift\
for the Library , which M^r^. Howard hath presented the Society —\
with .

‌ D^r^. Wilkins was desired to consider of the Instance that\
may be fit to be inserted in the History of the Society , —\
and having done so , to present them to the Council . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 129: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 129: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1 p. 129: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 129):** “[Memorandum & Order concerning the Library, presented by M^r^. \| Howard] His desire.” (CMO/1)

**(CMO/1 , v. 1 p. 129:)** “[Howard. Hon: Henry] Desires to have the family papers found in the Arundel Library” (CMO/8A)

**(CMO/1 p. 129):** “S^r^: Andrew Kings desired to be left out of the List.” (CMO/1)


### 4 April 1667 (CMO/1 pp. 130-131;  CMO/1/127) probably Arundel House 

\TranscriptionStart

[*centered:*] April 4 .[1667 .]{.underline}

[…]

[**Page 131:**]

[…]

There was Licensed Numb . 24 . of the Philosophical Transactions ,\
consisting of four written sheets in folio , and of Eight figures ;\
peculiarly designed for Seamen to make Observations in their\
Sea-Voyages : for which purpose , one hundred Copies should —\
be taken off from the Printers , at the Societies charge, and —\
lodged with the Master of Trinity-house , to be by him diſposed\
to fit  Seamen .

‌ It was also diſcoursed of , that the Operator should consider\
what would be the charge for a whole ſet of the Instruments\
described in these Transactions .

‌ It was also mentioned , that M^r^. Sprat desired to know ,\
what he should do in the matter of inserting the Statutes into\
the History of the Society .  It was thereupon thought good ,\
that D^r^. Wilkins should be desired , to peruse the said Statutes ,\
and so to abreviate them , as that the most material and ~\
least alterable particulars thereof might be inserted in[*canceled:* ‘in~~to~~’] the\
History . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 130: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 130):** “Money to be paid concerning the Repository.” (CMO/1)

**(CMO/1 , v. 1 p. 130:)** “[Furniture.] Sundries.” (CMO/8A)

**(CMO/1 p. 130: 128,130,138,143,197):** “Memorandum & Order concerning the Library, presented by M^r^. \| Howard” (CMO/1)

**(CMO/1 p. 130,131,132,133,134):** “[Society Royal.] Minutes relating to its History” (CMO/8A)

**(CMO/1 p. 131: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 131: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 131):** “Instruments described in the Philosoph: Transactions \| what they cost to be bought.” (CMO/1)

**(CMO/1 p. 131):** “[Transactions] 100 copies of N^o^. 24 to be lodged with the Master \| of the Trinity House for Seamen’s use” (CMO/8A)

**(CMO/1 p. 131: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 131: 130,131,132,133,134):** “[Society Royal.] Minutes relating to its History” (CMO/8A)


### 25 April 1667 (CMO/1 p. 132;  CMO/1/128) probably Arundel House 

\TranscriptionStart

[**Page 132:**]

[*centered:*] April 25[ . 1667 ]{.underline}.

[…]

Voted , That in the Petition to his Maj^ty^. about the Enlarg⹀\
ment of the Society's Charter , and the granting of Chelsey\
College , the Clause concerning[*over:* ‘containing’] a Recorder for the Society ,\
be omitted , and that there be inserted in this Petition ,\
a Power to be granted to the President-alone , to licence\
ſuch Books to be publish’d by any Fellow of the Society , and\
to employ other Printers besides the Printers of the Society .

‌ Voted , that the said Petition be presented to the King\
by the President and such others of the Council , as the said\
President shall take along with him .

‌ Voted , that the Secretary of State , the Lord Arlington ,\
be desired to prepare and haue ready , a Warrant concerning\
the Particulars of the aforesaid Petition , for His Maj^ty^. to\
ſigne .

‌ Voted , That the Committee for the Additionals to the ~\
Society's Charter , do meet on Monday next , about 6 . of\
the Clock , at the Presidents’ house , to consider of the Statutes\
of the Society to be inserted in their History .

‌ Number 24 . of the Philosophical Transactions licenced . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 132: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 132: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 132: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 132: 130,131,132,133,134):** “[Society Royal.] Minutes relating to its History” (CMO/8A)

**(CMO/1 , v. 1 p. 132:)** “[Charter.] A Petition to King Charles II. concerning.” (CMO/8A)


### 29 April 1667 (CMO/1 pp. 133;  CMO/1/129) probably Arundel House 

\TranscriptionStart

[**Page 133:**]

[*centered:*] April 29[ . 1667 .]{.underline}

[…]

D^r^. Wilkins was desired to be mindfull of selecting upon \<every\
Head of the matters hitherto done by the Society , one or two ~\
instances , to be offered to the Council for their approbation ,\
and then to be inserted into the History .

‌ Ordered , that M^r^. Hook do bring to D^r^. Wilkins the several\
heads , he hath drawn up for that purpose .

[…]

\TranscriptionStop



**(CMO/1 p. 133: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)


### 23 May 1667 (CMO/1 pp. 133-134;  CMO/1/130) probably Arundel House 

\TranscriptionStart

[*centered:*] May 23 . 1667 .

[…]

[**Pages 134:**]

[…]

‌ It being moved again , that such Instances , as are to\
be inserted in the Hiſtory of the Society , might be reflected\
upon , It was ordered , that it should be left to the —\
President and D^r^. Wilkins to agree upon such , as they —\
should judge fit for that  purpose .

‌ Numƀ^r^. 25 of the Phil. Tranſactions licenſed . [*downward flourish*] 

\TranscriptionStop


**(CMO/1 p. 134: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 134: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 134: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 134: 130,131,132,133,134):** “[Society Royal.] Minutes relating to its History” (CMO/8A)

**(CMO/1 p. 134: 130,131,132,133,134):** “[Society Royal.] Minutes relating to its History” (CMO/8A)


### 3 June 1667 (CMO/1 pp. 134-135;  CMO/1/131) probably Arundel House 

\TranscriptionStart

[*centered:*] June [3 . 1667 .]{.underline}

[…]

It was moved , that a fit person for another Curator\
to the Society might be thought upon ; and the Council —\
was desired accordingly , to take it into consideration .

[**Page 135:**]

‌ Mention was made by some , of D^r^. Walter Needham , by\
others , of D^r^. Richard Lower .  This matter was left to fur⹀\
ther consideration .

‌ It being moved that the Council would please to order\
something for the ſervants of Arundel-house , both those ,\
that had taken pains in fitting the room for the late ~\
Entertainment of the Dutcheſse of Newcastle ; and for\
fhose[those] that constantly , from time to time , do some service or\
other for the Society .

‌ It was Ordered , That a Crown be given to each of the two\
Maidservants employed on the late occasion of the said —\
Dutcheſse's presence , and an Angel to the Hous-keeper , upon\
the same account ; but to the Porter an Angel each quarter .

‌ Number 26. of the Phil : Tranſactions licenſed . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 135: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 135: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)


### 30 September 1667 (CMO/1 pp. 135-136;  CMO/1/132) probably Arundel House 

\TranscriptionStart

[*centered:*] September 30 [. 1667 .]{.underline}

The Meetings both of the Council and Society having been\
intermitted for some time , The Council met this day , for\
summoning of the Society to returne to their ordinary ~\
meetings , and for some other buſineſses .

[…]

[**Page 136:**]

[…]

‌ M^r^. Charles Howard, whose presence at the Council had\
been particularly desired [*canceled:* ‘~~,~~’] on this occasion , received the\
Thanks of the Council for his favour of giving them a —\
meeting , and was desired to take such Order pro tempore\
about the House of Chelsey-college , and particularly about\
getting it fitted for a hous-keeper, as he should think —\
fit .

‌ Mention being again made of another Curator , the\
consideration of it was referred to the next meeting\
of the Council .

‌ D^r^. Wilkins moved , that a Com̃ittee both of the Society —\
and Council might be considered of , for raiſing Contributions\
among the members of the Society in Order to build a College .

‌ It was ordered hereupon , that D^r^. Wilkins should be —\
desired to present to the Council at their nest meeting ,\
a List of such persons , as he should think proper for —\
that purpose ; Which he undertook to doe . [*downward flourish*]

\TranscriptionStop


### 5 November 1667 (CMO/1 pp. 137-140;  CMO/1/133) probably Arundel House 

\TranscriptionStart

[**Page 137:**]

[…]

‌ The Buſineſse of voluntary ſubscriptions for contri⹀\
buting towards the carrying on of the Ends of the Insti⹀\
tution of the R. Society being considerd of , it was after\
debate and mature deliberation unanimously agreed\
upon ,

‌ That it was now a Seasonable time for such Subscriptions\
and that they were to be made first by such of the Council , [**page 138:**]\
and of the Society , as were both willing and able ; and after⹀\
wards by such other wel-diſposed persons without the Society ,\
as should come in by the ſolicitation of a Com̃ittee , to be\
nominated by the Council , out of their own number , and\
out of the Fellows of the Society : which contributions ~\
should be employed in promoting the Ends of the Society ,\
and particularly to the Building of a College , as the most\
probable way of the Society's Establishment .

‌ In order to this , a Forme of Subscriptions was drawn up\
as followeth ;

‌ We , whose Names are underwritten , being satisffied of the ~\
great Usefulneſse of the Institution of the R . Society , and\
how requisite it is for attaining the Ends deseigned thereby ,\
to build a College for their Meetings , and to establish some\
Revenue for diſcharging the Expences neceſsary for Tryal of\
Experiments , Do heartily recommend it to the Bounty of —\
all Generous and Well-diſposed Persons , for their aſsistance\
to a work of such publick Usefullneſse : And we do each of\
us , for our selues , hereby promiſe to contribute to those —\
good Ends , the respectiue Summs , subscribed by each of us ,\
at four diſtinct quarterly payments , to be made to such ~\
persons , as shall be authorized under the Seal of the *R.*\
Society for the receipt thereof . The First payment to ~ —\
begin at \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ .

‌ This Forme was committed to Sir Anthony Morgan and\
M^r^. Hoskyns to make it obligatory in Law .

‌ The Same Persons were ordered to draw up a Deed of\
Gift of the Library presented to the *R.* Society by [*canceled:* ‘~~Henry~~’]\
Henry Howard of Norfolk .

‌ It was moved by M^r^. Colwall , that some effectual way [**page 139:**]\
might be taken by the Council , to oblige the Fellows of the ~ ~\
Society to pay their Arrears ; whereupon it was ordered ,\

‌ That M^r^. Colwall , as Treasurer , should write to the ſeveral\
Members of the Society , that are in arrear , and signify to\
them , that their positiue Answers concerning their payments\
are expected by the Council within one month from the time\
of the receipt of the respectiue letters , sent to them for that\
purpose , or from the time of their being left at their reſpectiue\
houses or lodgings ; And that in case of failure , the Council\
would think themselues obliged , after so long delays , to —\
proceed with them according to Statute , and so to leaue —\
them out of the Liste of the Society .

[…]

‌ The busineſse of Cataloguing[*over:* ‘Catag’] the Society's Library , being —\
spoken of , D^r^. Ball acquainted the Council , that now in a\
short time that Catalogue would be perfected .

[…]

‌ It being moved , that a Boy might be allowed to M^r^. Hook , fit\
to be employed by him , on such occasions , as concerne the\
Service of the Society , it was agreed upon by the Council , —\
That M^r^. Hook should find out such a Boy , and that fifteen —\
pounds per Annum should be allowed him towards the ~ —\
keeping of him .

[**Page 140:**]

[…]

‌ M^r^. Oldenburg read the Latin letter, he had drawn up , to\
be sent to Prince Leopold of Florence : which was approved\
of  , and ordered to be sent to the said Prince in the name\
of the Society , subscribed by himself , and adreſsed to S^r^. John\
Finch , accompanied also with an Exemplar of the ~ ~\
History of the Society , as a Present to the same from them .

‌ The same had leaue to send a Copy to M. Hevelius , another\
to M^r^. Winthorp , and a third to M. Auzout and M. Petit .

[…]

‌ There were nominated and appointed , for a Committee\
of the Council , to examine the Accounts of the Trear̃ .\
according to Statute , The President , both the Secretaries,\
M^r^. Hoskyns and D^r^. Ball ,  And the Treasurer was desired\
to state the Accounts as soon as he could . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 137):** “An Accompt given concerning the State of Chelsey Coll:” (CMO/1)

**(CMO/1 , v. 1 p. 137:)** “[College for the Royal Society.] Subscription resolved on, and the form thereof.” (CMO/8A)

**(CMO/1 p. 138,153):** “Form of the same [Order concerning Subscription]” (CMO/1)

**(CMO/1 p. 138: 128,130,138,143,197):** “Memorandum & Order concerning the Library, presented by M^r^. \| Howard” (CMO/1)

**(CMO/1 v. 1 p. 139):** “Library. Report concerning its catalogue” (CMO/8A)

**(CMO/1 p. 140: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 140):** “Letters ſent to ſome.” (CMO/1)

**(CMO/1 p. 140: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 140: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1, v. 1 p. 140):** “Bills of Sundries, Ordered to be paid.”
(CMO/8A) “[History of the Royal Society] Copies to be sent to Prince
Leopold, Havelius, Winthrop & Azout” (CMO/8A)


### 16 November 1667 (CMO/1 pp. 141-145;  CMO/1/134) probably Arundel House 

\TranscriptionStart

[**Page 141:**]

[*centered:*]  November 16 . [1667 .]{.underline}

[…]

‌ Vpon a motion made this day , it was agreed upon , That\
when the draught for subscribing Contributions , to carry on\
the work of the Society , shall be brought in by S^r^. Anthony\
Morgan and M^r^. Hoskyns , to whom it was formerly referred,\
it shall then be offered promiſcuously to able and willing\
Persons , as well without as within the Society .

‌ It was voted , that there shall be no Preface to the Bill ,\
to be subscribed for the purpose abouementioned : And ,\
that it shall be a single Obligatory Bill , to this purpose , [**page 142:**]\
‌ I [*A.B.*]{.smallcaps} acknowleg to owe to the *R.* Society the Summe\
of \_\_\_\_\_\_ to be paid &c .

[…]

‌ It being moved , that it might be considered , whether the\
Obligation , ſubscribed by the Fellows of the Society , as now\
it is , hath not a Legal validity in it ; S^r^. Anthony Morgan\
was desired to consider it accordingly , which he undertook\
to doe; and the Secretary was ordered to cause a Copy to\
be made of the said Obligation , and of the Statutes relating\
to it .

[…]

‌ Mention being made , that a Security might be provided\
for such Inventions or Notions , as Ingenious persons ~\
might haue , and desire to secure from Usurpation , or\
from being excluded from having a share in them , if they\
should be lighted on by others ; It was thought good , —\
that if any thing of that Nature should be brought in\
and desired to be lodged with the Society , that, if the [**page 143:**]\
Authors were not of their Body , they should be obliged to —\
shew it first to the President , and that then it should be\
ſealed up , both by the small ſeal of the Society , and ‸^by^[*canceled:* ‘~~of~~’] the\
Seal of the Proposer ; but if they were of the Society , that\
then they should not be obliged to show it first to the Preſident ,\
but onely to declare unto him , the general heads of the matter,\
and then it shold be sealed up , as mentioned before . 

‌ The busineſse of the Library of the Society , presented to\
them by Henry Howard of Norfolk , being diſcoursed of ,\
and particularly [*canceled:* ‘~~of~~’] the Donor's desire to haue it returne\
to his family , in case of failure of the Society , M^r^.\
Hoskyns suggested , That those Books , that remain unchang’d,\
and those that are changed for others , be delivered in two\
distinct Catalogues , and that such Catalogues being[*over:* ‘bee’] ~\
finisht , who-soever shall haue the custody of them and of\
the Library for the *R .* Society , be ordered to‸^be^ deliver’d[*over:* ‘deliver’] up\
to M^r^. Howard or his Aſsignes , in case the Society be at\
any time diſsolved .

[…]

[**Page 144:**]

‌ The Council Licenſed n^o^. 29 . of the Phil. Tranſactions .

‌ It was mentioned , that it was desired by the Society\
at their last meeting , that the Council would consider of —\
the Entring of the letters , that concern the Society , into\
their Letter - book , Viz^l^. That all such letters as are —\
written by the Society , or by any Member of the Society ~\
upon a Philoſophical account , and the answers to them ,\
read before the Society [*comma and parentheses inserted:*],(excepting those that shall be excepted\
upon their being read[*parentheses inserted:*]) are to be filed up , or put into a\
Book, and thence to be transcribed into a Letter-book\
appointed for that purpose ;  This was referred to —\
another meeting of the Council .

‌ The Committee reported to the Council , concerning the\
accounts of the Treasurer , as follows , Viz^l^.

[*three lines centered:*] At a Committee of the Council of the R. Society ,\
for Auditing the Treārs Accompts\
November 11 [. 1667 ]{.underline}.

Vpon Examination of M^r^. Colwalls Accompts , we find\
him Debtor ,

------------------------------------------------    ---------------------------
To the Arrears due to the Society for their\        992 : 18 : 6
‌ Quarterly payments to this eleventh\
‌ day of November 1667 — — — — —

To Money he hath received for Admiſsions —          23 : 10 : 6

To the Balances of his last acco^t^. (in money)\    73 : 2 : 4
‌ ending 5 November 1666 — — —

[Total:]                                             1089 : ii : 4
-------------------------------------------------   ---------------------------

‌  Signed,    Brouncker PRS.\
‌         John Wilkins . John Hoskyns.\
‌          Peter Balle .


[**Page 145:**]

‌ We alſo find him Creditor ,

---------------------------------------------------------------  ----------------------------
By Money he hath paid for the Use of the\                         203 : 11 : 11
‌ Society , as by Bills and Orders — — —

By Arrears of such persons as haue been\                          67 : 4 : —
‌ been omitted by Order of Council dated\
‌ October 29 . 1666 . Viz^l^.\
‌      Lord Lucas — — — — 9 : 2 . —\
‌      S^r^. John Denham — — — 14 : 15 : —\
‌      D^r^. Scarburgh — — — 11 : 18 : —\
‌      M^r^. Dryden — — — — 9 : 19 : —\
‌      M^r^. Vermuyden — — — 11 : 15 : —\
‌      Monſ^r^. Schroter — — — 9 : 15 : —

By Arrears owing by several Members ~\                            43 : 2 : —
‌ deceased , Viz^l^.\
‌      M^r^. Richard Boyle . — 7 : 7 : —\
‌      S^r^. Ken . Digby — — 5 : 17 : —\
‌      D^r^. Hoar — — — — 11 : 11 : —\
‌      Lord Maſsereen — — 14 : 9 : —\
‌      D^r^. Quatremain — 3 : 18 : —

By Arrears owing by the rest of the Fellows\                      697 : 18 : —
‌ yet unpaid — — — — — — — — —

By Ballances , resting in Cash now in his — —\                    77 : 15 : 5
‌ hand . Seventy seven pounds fifteen shillings\
‌ fiue pence. We say — — — — — —\
‌      [*inserted:*] Besides one hundred pounds in the Chest .

‌                                                                  1089 : 11 : 4 .
---------------------------------------------------------------  ----------------------------

Signed .

‌    Brouncker P.R.S.\
‌    John Wilkins\
‌    Peter Balle .\
‌    John Hoſkyns

\TranscriptionStop

**(CMO/1 p. 143: 128,130,138,143,197):** “Memorandum & Order concerning the Library, presented by M^r^. \| Howard” (CMO/1)

**(CMO/1 , v. 1 p. 143:)** “[Arundel Library.] To return to the Donors, if the R.S. should be dissolved” (CMO/8A)

**(CMO/1 p. 144: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 144):** “Desire concerning Letters of the Society.” (CMO/1)

**(CMO/1 p. 144: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

**(CMO/1 p. 144: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 144: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 , v. 1 p. 144:)** “[College for the Royal Society.] Subscription resolved on, and the form thereof.” (CMO/8A)

**(CMO/1 , v. 1 p. 144):** “Letter-Book, proposed, and its contents” (CMO/8A)


### 9 December 1667 (CMO/1 pp. 146;  CMO/1/135) probably Arundel House 

\TranscriptionStart

[**Page 146:**]

[*centered:*] December 9 [. 1667 .]{.underline}

[…]

‌ S^r^. Anthony Morgan and M^r^. Hoſkyns reported , concerning\
the legal validity of the Obligation , subſcribed by the Fellows\
of the Society, that the Statutes haue already specified the\
penalty for non-observance ,  Viz^l^. Expulsion , that thereby\
other penaltyes are precluded .

‌ The Council licenced n^o^. 30 . of the Ph. Tranſactions . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 146: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 146):** “Report concerning the validity of the same [Obligation]” (CMO/1)

**(CMO/1 p. 146: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)


### 2 January 166$\frac{7}{8}$ (CMO/1 pp. 147-148;  CMO/1/136) probably Arundel House 

\TranscriptionStart

[**Page 147:**]

[*centered:*] January 2 . 166$\frac{7}{8}$ .

[…]

‌ The buſineſse of voluntary Contributions for building a College\
being moved again , it was upon debate thought good ,

‌ That those, that had a mind to contribute , should not be ~\
obliged to subscribe their respectiue Summes , till the total of\
the subscriptions amounted to one thousand pounds : And —\
that those of the Council , that had most interest to engage [**page 148:**]\
others , both of the Society , and without it , should be desired\
to employ the same , in speaking to persons of both Sorts , and\
to learn the Summe of their intended Contributions , thereby\
to make an Estimate , what the toatall was like to amount\
to. [*downward flourish*]

\TranscriptionStop

**(CMO/1 , v. 1 p. 147:)** “[College for the Royal Society.] Subscription resolved on, and the form thereof.” (CMO/8A)


### 11 January 166$\frac{7}{8}$ (CMO/1 pp. 148-150;  CMO/1/137) probably Arundel House 

\TranscriptionStart

[*centered:*] January 11 . 166$\frac{7}{8}$ .

[…]

‌ M^r^. Hook was desired to bring in at the next Council ,\
a Draught for the building of the Societyes College

‌ The List of the Fellows of the Society being read over , —\
and the persons , that were thought both willing and able to\
contribute to the said Building , taken notice of , it[**page 149:**]\
was thought fit , that a Committee should be chose , to —\
solicite those persons , And there were named ,

[… *solicitors and who they will solicit listed*]

[**Page 150:**]

[…]

Ordered , That the four dayes work , formerly controverted in the\
Operators Bill of Novemb. 4 . 1667. be paid him by the Treasurer.

‌ Ordered that the Phil : Transactions n^o^. 31 be printed .

‌ There was read Col : Blounts letter to the President , deſiring\
his diſcharge from being any longer a Fellow of the Society.

\TranscriptionStop

**(CMO/1 p. 150: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 150: 119,150):** “[Operators] Order concerning his Bills.” (CMO/1)

**(CMO/1 p. 150: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 150):** “Letter concerning Discharge from being a Fellow.” (CMO/1)


### 25 January 166$\frac{7}{8}$ (CMO/1 pp. 151-152;  CMO/1/138) probably Arundel House 

\TranscriptionStart

[**Page 151:**]

[*centered:*] January 25 . 1[668 .]{.underline}

[…]

‌ There was prepared ( there being not a Quorum sufficient to make‸^it^\
effectual ) an Order for iſsuing twenty fiue pounds , towards the\
Expenses requisite for paſsing the Patent , concerning his Ma^ties^.\
Grant of Chelsey - college .

‌ There was likewise prepared an Order for providing a con⹀\
venient room for D^r^. Lower to make Anatomical Experiments in ,\
for the Society .

[…]

‌ The Letter formerly ordered to be drawn up by M^r^. Oldenburg\
for S^r^. Robert Moray , about soliciting Contributions in Scotland ,\
was read ; and it was thought fit , that something should be —\
added expreſsing M^r^. Howard's bounty in giving the Ground to —\
build the College upon .

[…]

\TranscriptionStop



**(CMO/1 p. 151):** “Preparacõn to an Order for providing a Room, to make Anatomical Experiments” (CMO/1)

**(CMO/1 p. 151):** “Preparation to an Order for the Expences of the Patent concerning Chelsey Coll:” (CMO/1)


### 30 January 166$\frac{7}{8}$ (CMO/1 pp. 152-154;  CMO/1/139) probably Arundel House 

\TranscriptionStart

[**Page 152:**]

[…]

[*centered:*] January 30 . 166$\frac{7}{8}$ .

[…]

‌ Ordered, That the Treãr do iſsue twenty fiue pounds ~ —\
toward the Expenses requisite to paſse the Patent concerning\
his Ma^ties^. Grant of Chelsey College , and deliver it to M^r^. Jepson —\
for that use upon account .

‌ Ordered , that the Operator do make ready as soon as he can\
two sets of Instruments for Tryals at Sea , according to\
the ſcheme annexed to Numb . 24 of the Phil: Tranſactions .

‌ He being asked what they would amount to , anſwered , to about\
forty shillings , excepting the Fans for trying the Strength\
of winds , that be~ing~ onely fit for use at Land .

[…]

‌ The Forme for subscribing Contributions to build a College\
for the Society , was agreed upon , as follows ;

‌ I \_\_\_\_\_\_\_\_ giue unto the President, Council and Fellows\
of the Royal Society of London for Improving Natural Knowledge ,\
towards the Building of an House or College for them , upon\
the ground near Arundel-house , given for that use by the\
Hon^ƀle^. Henry Howard of Norfolk , the Summe of \_\_\_\_\_\_\
And do hereby engage my self to pay the said \_\_\_\_\_\_\_\_\
within one year from the date hereof , upon the usual Feasts\
of Lady-day , Midsommer , Michaelmas and Christmas , by even\
and equal portions .  In witneſse whereaof I haue hereunto set\
my hand and ſeale this \_\_\_\_\_\_ day of \_\_\_\_\_\_\_\_\
‌ Sealed and delivered\
in the Presence of \_

‌ It was ordered , that two hundred Copies of this Forme —\
should be forthwith printed ; but so , that in one hundred of\
them should be left a blanck after the words , [Feasts of]{.underline} , unto\
the words, [by even]{.underline} : considering , that some Subscriptions are —\
like to be made after Lady-day shall be past .

‌ The President subscribed this day One hundred pounds —\
towards the said College .

‌ M^r^. James Hayes subscribed forty pounds for the same End.

‌ These two ſubscriptions were left with M^r^. Oldenburg .

[**Page 154:**]

‌ The Letter intended to be written by the Council to Sir\
Robert Moray by M^r^. Oldenburg , was read again this day ,\
with the Addition ordered the last day to be made unto\
it.  This Letter was approved of , and ſigned by the Preſident\
in the Name of the Council ; with this ſuperſcription ,

‌ [*paragraph serifed roman:*]For the Right Hon^ƀle^. Sir Robert Moray Kn^t^. One of his\
Ma^ties^. Commiſsioners for the Treaſury of Scotland , in Edinnburg [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 152):** “[Preparation to an Order for the Expences of the Patent concerning Chelsey Coll:] Order for the same.” (CMO/1)

**(CMO/1 p. 153: 138,153):** “Form of the same [Order concerning Subscription]” (CMO/1)

**(CMO/1 p. 153,170):** “[Preparacõn to an Order for providing a Room, to make Anatomical Experiments] Order for that purpose.” (CMO/1)

**(CMO/1 , v. 1 p. 153:)** “[College for the Royal Society] Form of subscription for and printing them.” (CMO/8A)


### 17 February 166$\frac{7}{8}$ (CMO/1 pp. 154-156;  CMO/1/140) probably Arundel House 

\TranscriptionStart

[*centered:*] February 17 . 166$\frac{7}{8}$

[…]

‌ D^r^. Wilkins asked the liberty to take up from the Stationer\
half a dozen Copies of the Hiſtory of the *R* Society , to ~ ~\
present to some persons , from whome he expects Contributions:\
And it was allowed him .

‌ M^r^. Oldenburg moved the Council to grant a Letter recom⹀\
mendatory to M^r^. Samuel Colepreſse , an ingenious and ſtudious\
person , ready to go to trauell , who had furnish’d the Society [**page 155:**]\
with several very good and Philosophical Accounts , touching\
Mines , Tydes and Agriculture .

‌ This motion was consented unto , and the Proposer having\
a Latin draught of such a Letter ready , it was read and\
approved of,as followeth ;

‌ [*paragraph serifed roman, ligatures:*] Cum præſentium Latour , Dn . SAMUEL COLEPRESSE , Vir\
probus et eruditus , reruḿ Naturalium perquam Curioſus ,\
præﬅitutum animo habeat , Oras exteras Studiorum gratiâ\
inviſere ibi’ Doctorum et Solertium Virorum conſuetudinem\
ambire , rogaverit’ Præſidem et Concilium [Societatis]{.smallcaps}\
[Regiæ]{.smallcaps} , á ſereniſsimo M. BRITANNIÆ Rege LONDJNJ\
ad Scientiam Naturalem augendam institutæ , ut Litteris ſuis\
Commendatitiis propoſitum equs ornare et promovere digna⹀\
rentur ; Prædictus Præſes et Conſilium , de ingenio et probitate\
Latoris satis ſuper’ perſuasi , id ipſi humanitatis oﬃcium ~ ~\
lubentiſſimé præſtare voluerunt ; proinde’ omnes Literarum\
et literatorum amantes enixe rogant , ut præmemorato Dn .\
SAMUELI COLEPRESSE , favore et consilio[*over:* ‘concilio’] ſuo adeſse , studia’\
et conatus equs pro viribus juvare ne graventur . Quam —\
gratiam uti Præſes et Concilium[*erased:* ‘Consilium’] dictæ [Societatis]{.smallcaps} pari\
oﬀiciorum genere ,[*over:* ‘gerere’] pro re nata , agnoſcere et redhoﬆire an⹀\
nitentur . In cujus rei Teſtimonium Præſentes haſce Sigillo ſuo\
munire[*over, uncertain:* ‘munixe’] voluerunt .  Script. LONDINI Anno Regni [Caroli]{.smallcaps}\
II. Auguſtiſsimi M. BRITANNIÆ, FRANCIÆ et HIBERNIÆ\
Regis VIGESIMO , Æræ autem Chriſtianæ MDCLXVIII
d. 17. Februarii

‌ Ordered, That this Letter be written fair on Parchment for\
the next Council , to haue the Society's Common Seal affixed to\
it .

[…]

[**Page 156:**]

‌ Ordered , that D^r^. Wilkins be desired to speak to D^r^. Croon\
to draw up a letter to be sent by the President to the\
Dutcheſse of Newcastle , to desire her contribution to\
the building of a College .

\TranscriptionStop



**(CMO/1 p. 154: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 154,155,157):** “A Letter recommendatory to M^r^: Colepresse.” (CMO/1)

**(CMO/1 p. 154):** “Letter written to S^r^: Robt Moray.” (CMO/1)

**(CMO/1 , v. 1 p. 154:)** “[History of the Royal Society] D^r^. Wilkins asks for 6, as presents, in hopes of contrib.” (CMO/8A)

**(CMO/1 p. 155: 154,155,157):** “A Letter recommendatory to M^r^: Colepresse.” (CMO/1)

**(CMO/1 p. 155,157):** “Recommendatory Letter, with the Society’s seal. \| Granted to M^r^. Colepreſs, in Latin” (CMO/8A)


### 24 February 166$\frac{7}{8}$ (CMO/1 pp. 156-157;  CMO/1/141) probably Arundel House 

\TranscriptionStart

[*centered:*] February 24 . 166$\frac{7}{8}$ .

[…]

[**Page 157:**]

‌ Ordered, That the Letter recommendatory for M^r^.\
Samuel Colepreſse , agreed upon at the last meeting of\
the Council , be sealed by the President , with the common\
Seal of the Society .

[…]

\TranscriptionStop


**(CMO/1 p. 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)


### 26 March 1668 (CMO/1 pp. 157;  CMO/1/142) probably Arundel House 

\TranscriptionStart

[*centered:*] March 26 . 1[668]{.underline}

[…]

‌ The Latin Letter, giving thanks to Prince Leopold of —\
Florence, for his Present of the Book of the Florentine Expe⹀\
riments,\* \[*margin:* (\*) Letter Books \| V[ol. 2./1.]{.underline} \| [177.]{.underline}\] was read , signed by the President and ſealed ;  And\
M^r^. Oldenburg ordered to deliver it to Sign^r^. Magalotti and\
Sign^r^. Falconieri, who presented the said Book to the Society\
from that[*over:* ‘the’] [*canceled:* ‘~~ſaid~~’] Prince.

‌ Ordered , that M^r^. Hook do at his leisure attend —\
M^r^. Charles Howard , to view the reparations made in Chelsey\
college , and make a report thereof to the Council . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 157):** “Letter giving Thanks to Prince Leopold of Florence.” (CMO/1)

**(CMO/1 p. 157):** “Answering Letter to Pince Leopold of Florence.” (CMO/1)

**(CMO/1 p. 157: 154,155,157):** “A Letter recommendatory to M^r^: Colepresse.” (CMO/1)

**(CMO/1 p. 157: 155,157):** “Recommendatory Letter, with the Society’s seal. \| Granted to M^r^. Colepreſs, in Latin” (CMO/8A)

**(CMO/1 , v. 1 p. 157):** “Letter. To Prince Leopold, of thanks for the book of the \| Florentine Experiment” [The *Saggi* (1667) TODO(confirm in CMO/1)] (CMO/8A)


### 13 April 1668 (CMO/1 pp. 158-160;  CMO/1/143) probably Arundel House 

\TranscriptionStart

[**Page 158:**]

[*centered:*] April 13 .[‌ 1668 .]{.underline}

[…]

‌ Ordered also , That the Amanuensis do audit against\
the next Council - day , the whole account of the said Bills\
concerning the Reparations of Chelsey-college .

‌ Ordered , That the President be desired to signify to\
the Society , that , considering the want of Experiments\
at their publick meetings , the Council haue thought good\
to appoint a Present of a Medal , of at least the value\
of twenty shillings , to be made to every Fellow , not\
Curator by Office , for every Experiment , which the —\
President or Vice-president shall haue approved of .[**page 159:**]\
And that the President be likewise desired to advise with\
M^r^. Slingsby about the Impreſse of such Medals .

[…]

‌ The Council licens’d D^r^. Wilkins his Book , entitled\
An[*over:* ‘an’] Eſſay towards a Real Character and Philosophical —\
Language .

‌ The Council licens’d also Numƀ^r^. 34 . of the Philosophical\
Transactions .

‌ Ordered, That the Amanuensis cause to be bound a Book\
in Folio , with vellum leaues , to contain the Names of the\
Benefactors to the Society , together with the particulars of\
their respectiue donations .

‌ Ordered also , that the Curator do compleate the printed\
List of the Collection , bestowed by M^r^. Daniel Colwall on the [**page 160:**]\
Society , and that this list be inserted in the next Edition\
of the Society's said History . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 158: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 158):** “Medal. Proposal to give for every approved Ep^t^. \| of the Fellows, a medal of 20 ct[?] value” [TODO(confirm CMO/1 ct too) Is a medal printed?  See what they think about it.] (CMO/8A)

**(CMO/1 p. 159: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 159,161):** “Order concerning the List of the Collection of Curiosities.” (CMO/1)

**(CMO/1 p. 159):** “D^r^: Wilkins book. Lycensed.” (CMO/1)

**(CMO/1 p. 159):** “[Wilkins John D.D.] His Eſsay on a Real Character. Licensed” (CMO/8A)

**(CMO/1 , v. 1 p. 159:)** “[Benefactors & Benefactions] A Book to be prepared for, by the Amanuensis” (CMO/8A)  “Catalogue, of the collection given by M^r^. Colwall, to be printed.” (CMO/8A)

**(CMO/1 v. 1 p. 159):** “[Library.] Orders concerning to M^r^. Hooke” (CMO/8A)

**(CMO/1 p. 159: 88,159,161,249):** “[The Benefactors to the Society to be registred in loose ~ \| vellum ſheets.] Resolution & Order concerning the same.” (CMO/1)

**(CMO/1 p. 160: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1, v. 1 p. 160):** “[Arrears of contributions] Form of notice to be sent” (CMO/8A)


### 20 April 1668 (CMO/1 pp. 160-161;  CMO/1/144) probably Arundel House 

\TranscriptionStart

[*centered:*] April 20 .[‌ 1668 .]{.underline}

[…]

The President moved , That , for the more effectual getting\
in of the weekely contributions , letters might be written\
to‸^all^ such , as are in arrear , not of the Nobility , desiring\
them to attend the Council , at certain dayes to be nomina⹀\
ted , and then[*over:* ‘there’] to declare their resolutions concerning the\
payment of such their Arrears .

‌ Ordered hereupon , That M^r^. Oldenburg draw up a Forme\
for such Letters against the next Council .

[…]

[**Page 161:**]

‌ It being suggested , that , D^r^. Louys du Moulin was willing\
to translate the History of the R. Society into Latin , and —\
that it was neceſsary to hasten this Translation , for fear it\
should be done in Holland , to the Prejudice of the Author , it\
was ordered , that M^r^. Oldenburg should be desired to speak to\
M^r^. Martyn , and to let him know , that the Council approved\
of the said D^r^. du Moulin , and that he should do well to agree\
with him about the recompence for his pains , and to pay\
the same .

‌ The President mentioned , that M^r^. Collins had received\
a Mathematical Book from M^r^. Gregory from Padua ,[*over:* ‘Pari’] by\
the way of the Post , whereof the postage cost him Eighteen —\
shillings .

‌ Ordered hereupon , that the Treasurer do pay to M^r^. Collins\
Eighteen shillings , for a Book sent by post from Padoa , —\
entitled [*line in serifed roman:*] Geometriæ pars univerſalis , Quantitatum —\
[*line to period in serifed roman:*] Curvarum tranſmutationi et menſuræ inſerviens . [*italic:*] Authore\
Iac . Gregorio , and that he deliver this Book to the Curator\
to be put into the Societies Library .

[*purchase inscription on GB-UkLoRS RCN: 41963:* ‘Liber Societatis \| Regiæ , emptus \| 18. solid. Anglic. \| d. 27 . April . 1668.’ in volume with two preceding works, the second presented by the author through Henry Howard in March 1667]

‌ The Amanuensis was commanded to hasten the Vellum ⹀\
book , for registring the Benefactors of the Society , and\
their several donations .

‌ M^r^. Hook was put in mind of compleating the list of the\
Arundelian Library ; as also of perfecting the list of M^r^.\
Colwalls collection of Curiosities .

‌ S^r^. Anthony Morgan to be minded by[*over:* ‘to’] the Secretary of\
drawing up a Deed of Conveyance , securing M^r^. Howards’\
Donation of the Ground for building a College . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 161: 86,87,130,131,132,133,134,140,154,160,161):** “Order & Report concerning the History of the Society.” (CMO/1)

**(CMO/1 p. 161):** “Mathematical Book received.” (CMO/1)

**(CMO/1 p. 161: 159,161):** “Order concerning the List of the Collection of Curiosities.” (CMO/1)

**(CMO/1 p. 161: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1 p. 161: 88,159,161,249):** “[The Benefactors to the Society to be registred in loose ~ \| vellum ſheets.] Resolution & Order concerning the same.” (CMO/1)

**(CMO/1 , v. 1 p. 161:)** “[Benefactors & Benefactions] A Book to be prepared for, by the Amanuensis” (CMO/8A) “[History of the Royal Society] To be translated into Latin by D^r^. De Moulin” (CMO/8A)

**(CMO/1, v.1 p. 161: 123,124,161,275):** “[Library.] Books to be bought for the Society” (CMO/8A)


### 27 April 1668 (CMO/1 pp. 162;  CMO/1/145) probably Arundel House 

\TranscriptionStart

[**Page 162:**]

[*centered:*] April 2[7 . 1668 .]{.underline}

[…]

‌ Ordered , That, there[*over:* ‘that’] shall be no standing sallary allowed\
to either of the Secretaries .

‌ Ordered , That a Present by made to M^r^.  Oldenburg of\
Fifty pounds .

‌ Ordered, That M^r^. Collins be desired to aſsist , in making\
a Catalogue of the Arundelian Library forthwith . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 162: 82,118,162):** “Order concerning a Present to M^r^: Oldenburg.” (CMO/1)

**(CMO/1 p. 162: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1, v. 1 p. 162):** “[Arrears of contributions] Form of notice to be sent” (CMO/8A); “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

**(CMO/1 p. 162):** “[Secretaries to the Society.] To have no standing salary” (CMO/8A)

**(CMO/1 p. 162,289, v.2 57,237,239,243,244,251,258,263):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)


### 4 May 1668 (CMO/1 pp. 163;  CMO/1/146) probably Arundel House 

\TranscriptionStart

[**Page 163:**]

[*centered:*] May 4 .[‌ 1668 .]{.underline}

[…]

‌ D^r^. Wilkins was desired to procure at the next meeting\
of the Council , D^r^. Wren's draught of the Building . [*downward flourish*]

\TranscriptionStop


### 11 May 1668 (CMO/1 p. 164;  CMO/1/[147]) 

\TranscriptionStart

[**Page 164:**]

[*centered:*] May 11 .[‌ 1668 .]{.underline}

[…]

‌ M^r^. Hoskyns was desired to conferre with S^r^. Anthony —\
Morgan , for drawing up the Reasons , whereby it may appear ,\
that the clause suggested by the Lord Privy-seal for restraining\
the Society from alienating Chelsey- college will not be good in\
Law , and that the inserting of such a Clause would onely put\
the said Society to new Charges , now the Additional Patent\
is already engroſsed :  And that this be drawn up against\
the meeting of the next Council , intended to be summoned\
for Monday next :  And that that paper be given to the Lord\
Ashley by the President and S^r^. Paul Neile to diſcourse the\
busineſse with the Lord Privy-seale .

‌ M^r^. Hook was desired to bring in his draught for the building\
of the College , and an Estimate of the Charges thereof on\
Monday next .

‌ S^r^. Paul Neile was desired , that in case he should see\
D^r^. Wren , between this time and munday next , he would\
endeavour to engage him to attend the Council at their\
next meeting , and to bring with him his draught for the —\
said Building . [*downward flourish*]

\TranscriptionStop


### 18 May 1668 (CMO/1 pp. 165;  CMO/1/148) probably Arundel House 

\TranscriptionStart

[**Page 165:**]

[*centered:*] May 18 .[‌ 1668 .]{.underline}

[…]

‌ M^r^. Hoskyns was desired to speake to S^r^. Anthony Morgan\
to make a Draught of the Conveyance of M^r^. Howard's ground\
for building , against the next Council .

‌ The same , to speak to the said S^r^. Anthony Morgan about an\
Atturney , who may appear for the defence of the Society's ~ —\
poſseſsion of Chelsey- college .

‌ M^r^. Hoskyns produced and delivered to the Preſident the\
Memorial for persuading the Lord Privy-seal of the— ~ —\
uneffectualneſse of adding a clause against alienating —\
Chelsey- college .

‌ M^r^. Hook was desired to bring in at the next Council , the\
number of the Books of the Arundel - library :  And to meet\
with D^r^. Balle on Saturday next , for the compleating of the\
Catalogue of the said Library .

[…]

\TranscriptionStop


**(CMO/1 p. 165: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1, v. 1 p. 165):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)


### 30 May 1668 (CMO/1 pp. 166–167;  CMO/1/149) probably Arundel House 

\TranscriptionStart

[**Page 166:**]

[*centered:*] May 30 .[‌ 1668 .]{.underline}

[…]

‌ The President gaue notice , that M^r^. Henry Howard had been\
pleased to set out the ground for building the Society's College\
upon, Viz^l^. an hundred foot one way , and forty foot the other .\
Hereupon , S^r^ Anthony Morgan was desired to draw up the\
conveyance of that ground , and to haue it ready for the\
next Council : which he promiſed to doe .

‌ The Lord Brereton and M^r^. Hoskyns were desired to\
speak with M^r^. Cheiny[*over, uncertain:* ‘Shein’] at Chelsey , to desire him to let them\
see his Conveyance of the Mannor of Chelsey ; which they\
promiſed to do .

[…]

[**Page 167:**]

‌ Ordered , That the Secretary do write a Letter to D^r^. Wren ,\
to desire him to attend M^r^. Henry Howard at Oxford , about\
the Draught of the Society's building .

[…]

‌ M^r^. Hook promised , that he would endeavour to diſpatch\
the Catalogue of the Library , by attending it two‸^whole\ [*canceled:* ‘~~together~~’]^ Afternoons

\TranscriptionStop

**(CMO/1 p. 166,168,169,171,172):** “Order about a draught of the Societys Building.” (CMO/1)

**(CMO/1 p. 167: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1, v. 1 p. 167):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)


### 19 June 1668 (CMO/1 pp. 168;  CMO/1/150) probably Arundel House 

\TranscriptionStart

[**Page 168:**]

[*centered:*] June [19 . 1668 ]{.underline}.

[…]

‌ M^r^. Hook promiſed to bring in a Complete Draught\
for the building of the College , on Monday next .

‌ Number 36. of the Phil: Transactions was licensed . [*downward flourish*]

\TranscriptionStop


### 22 June 1668 (CMO/1 pp. 168–170;  CMO/1/151) probably Arundel House 

\TranscriptionStart

[*centered:*] June 22 .[‌ 1668 .]{.underline}

[…]

[**Page 169:**]

[…]

‌ The Draught of the Building being examined and agreed upon ,\
M^r^. Hook was ordered to get a Model of it made with one door ,\
and to consider of the buying of the materials , and of con⹀\
tracting with workmen , to be paid by measure for so —\
much a Rod and Square : as also to find out a person , to be\
constantly present , and to see the workmen do their duty .

[…]

‌ Ordered , that the Treãr do pay to M^r^. Hook fourteen\
pounds ten shilings for fitting the place in Greſham —\
College for the Society's Repository , (according to M^r^.\
Hook's bill.)

‌ Ordered, That M^r^. Oldenburg do deliver the Obligations\
hitherto Subscribed by the Fellows of the Society for\
contributions to the Building of their College , to the\
Treasurer , taking from him a Receipt for the delivering\
of them .

[…]

[**Page 170:**]

[…]

‌ M^r^. Henry Howard hearing that the Society wanted —\
a room to mke Anatomical Experiments in , offered\
a room in Arundel-house for that purpoſe , which the\
Council accepted of , with hearty thanks for this ~\
respect and favour .

‌ Ordered, That M^r^. Hoskyns be desired to draw up\
against the next Council , a Draught of such Security ,\
as M^r^. Henry Howard offered to giue . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 168: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 168: 166,168,169,171,172):** “Order about a draught of the Societys Building.” (CMO/1)

**(CMO/1 p. 169):** “[M^r^: Hook appointed Keeper of the Repository of the Society] Order concerning payment for the same.” (CMO/1)

**(CMO/1 p. 169):** “Order concerning Payment for the Repository.” (CMO/1)

**(CMO/1 p. 169: 166,168,169,171,172):** “Order about a draught of the Societys Building.” (CMO/1)

**(CMO/1 p. 169,171):** “[Repository at Gresham College.] Hooke paid £14$\frac{1}{2}$ for fitting one up” (CMO/8A)


### 29 June 1668 (CMO/1 pp. 170–171;  CMO/1/152) probably Arundel House 

\TranscriptionStart

[*centered:*] June 29 .[‌ 1668 .]{.underline}

[…]

‌ M^r^. Hoskyns being call’d upon for the Draught of M^r^. Howard's\
Security , he said , he had delivered‸^it^ to M^r^. Howards Solicitor ,\
to be considered .

‌ M^r^. Hook was ordered to bring in at the next Council\
an Estimate both of the charge of the materials and[*canceled:* ‘and~~s~~’]\
workmanship of the Building .

[**Page 171:**]

[…]

‌ Ordered, That D^r^. Clark and D^r^. Lower be desired by M^r^.\
Oldenburg to make  a List of the particulars neceſsary for\
the making of Anatomical Experiments .

‌ Delivered to M^r^. Colwall the Order signed in Council, for\
paying the thirty pounds ten shillings for the reparations\
of Chelsey- college . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 170: 153,170):** “[Preparacõn to an Order for providing a Room, to make Anatomical Experiments] Order for that purpose.” (CMO/1)


### 6 July 1668 (CMO/1 pp. 171–172;  CMO/1/153) probably Arundel House 

\TranscriptionStart

[*centered:*] July 6[. 1668.]{.underline}

[…]

Ordered , That M^r^. Hook make a draught for the building of\
the College , representing the front thereof to the Thames ,\
and to draw it with‸^the^ windows , M^r^. Howard having declared,\
that it was indifferent to him , which way it stood , so it\
might be contrived to the conveniency of the Society .

‌ The same was again ordered, to prepare the workmen ,\
and to look after Materials , as also to make an Estimate [**page 172:**]\
of the charges , according to this last position of the Building .

[…]

\TranscriptionStop


**(CMO/1 p. 171: 166,168,169,171,172):** “Order about a draught of the Societys Building.” (CMO/1)

**(CMO/1 p. 171: 169,171):** “[Repository at Gresham College.] Hooke paid £14$\frac{1}{2}$ for fitting one up” (CMO/8A)


### 13 July 1668 (CMO/1 pp. 172;  CMO/1/154) probably Arundel House 

\TranscriptionStart

[*centered:*] July 13ſ. [1668 .]{.underline}

[…]

There were examined two Draughts for the building of the\
College , both fronting to the water ; one of M^r^. Henry Howard ,\
the other of M^r^.  Hook’s :  It was concluded upon that the\
determination of this matter , Viz^l^. which of these [*two words inserted:*] two draughts\
should be followed , be referred to the next council , [*canceled:* ‘~~against~~’] [*inserted:*] at\
which the said M^r^. Howard was desired to bring in his\
Deſseign of ordering the whole plot of ground .

[…]

\TranscriptionStop

**(CMO/1 p. 172: 166,168,169,171,172):** “Order about a draught of the Societys Building.” (CMO/1)

**(CMO/1 p. 172: 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)

**(CMO/1 , v. 1 p. 172:)** “[Charter.] To solicit L^d^. Privy seal about.” (CMO/8A)


### 10 August 1668 (CMO/1 p. 173;  CMO/1/155) probably Arundel House 

\TranscriptionStart

[**Page 173:**]

[*centered:*] August 10 [. 1668]{.underline} .

[…]

\TranscriptionStop

### 5 November 1668 (CMO/1 p. 173–174;  CMO/1/156) probably Arundel House 

\TranscriptionStart

[*centered:*] November 5 .[‌ 1668 .]{.underline}

[…]

There was nominated and appointed a Committee of the\
Council, to audit the Accompts of this year ; Viz^l^. the President,\
S^r^. George Ent , M^r^. Hayes , M^r^. Creed , M^r^. Oldenburg; and [**page 174:**]
three of them to be a Quorum . They agreed to meet next Monday\
next , the 9^th^. instant , at the Presidents house , at 6 . in the\
Evening .

[…]

‌ M^r^. Hoskyns was desired to speak with M^r^. Cole , and to —\
endeavour to get a ſight of the Writings , which M^r^. Cole\
saith to haue obtained from all those , that pretend[*over:* ‘pretent’] any\
Title to Chelsey-college : And also to speak to S^r^. Anthony\
Morgan's Clerk for the Papers of the Society , that con⹀\
cern the Graunt of the said College . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 173: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 173: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)


### 19 November 1668 (CMO/1 pp. 174–176;  CMO/1/157) probably Arundel House 

\TranscriptionStart

[*the whole report in a running italic hand like Oldenburg’s:*]

[*centered:*] November 19.[‌ 1668]{.underline}

[…]

The Committee of the Council for examining the Accounts\
of the Society for the last Yeare, made a report\
which was approved of, as Followeth;

[**Page 175:**]

[*three lines centered:*] At a Committee of the Council of the\
R. Society , for Auditing the Treãrs Accompts\
Novemb. 9 .[‌ 1668 .]{.underline}

Vpon Examination of M^r^. Colwal’s Accompts, we find him Debtor .

-----------------------------------------------------------------  ------------------------
‌ To the Arrears due to the R. Society , for\                        1102 : 6 : 6
‌ their Quarterly payments , this 9 Nov. 1668 —

‌  To Moneys he hath received for Admiſsions —                       29 : 10 : 6

‌ To the Ballance of his last Acco^t^. in money.                    77 : 15 : 5

‌ [Total:]                                                           1209 : 12 : 5 .
-----------------------------------------------------------------  ------------------------

[*centered:*] Wee also find him Credit^r^.

-----------------------------------------------------------------  ------------------------
By Moneys he hath paid for the use of the\                          264 : 5 : 5
‌ R. Society, By order of the Council - - - -

By Money in Arrear, resting unpaid by the\                          847 : 1 : 6
‌ Fellows of the Society - - - - - - - -

By Ballance resting in cash , now in his -\                         98 : 5 : 6
hands - - - - - - - - - - -

‌ [Total:]                                                          1209 : 12 : 5 .
-----------------------------------------------------------------  ------------------------

‌ And in the Cash chest of the R. Society\
‌ the summe of one hundred poundſ .

‌         Signed .\
‌            Brouncker P.R.S.\
‌     Ja: Hayes .\
‌                Henry Oldenburg . Secr.

[**Page 176:**]

M^r^ Oldenburg read a letter written from the Vice\
Chancellor of Oxford to M^r^ Boyle , importing that\
he would endeauor to procure an exchange of the MSS.\
now in possession[*canceled:* ‘possessi~~ti~~on’] of the Society, for [*canceled:* ‘~~which~~’]^such^ Books, as\
were proper for their purpose . The consideration hereof\
was referr’d to another Council , where Henry Howard\
of Norfolk might be present .

‌ Vpon the desire of S^r^ Peter Wyche , that the\
Society would licence their printers to print a —\
Translation[*over:* ‘Transaction’] Made by him at the desire of the Society\
of a MS. in the Portuguese Language concerning[*over:* ‘conserning’] the\
Nile[*over, uncertain:* ‘Nels’] the Vnicorne , the Red[*over:* ‘red’] Sea, Prester John and ~\
the variety of Palme[*over:* ‘palme’]-trees ; The Council granted him\
his desire , and ordered , that their Printers should haue\
leaue to print the same .

[*GB-UkLoRS RCN: 49947 with presentation inscription and penciled marks; Robert Southwell prepared an earlier extract in CLP.*]

‌ D^r^ Balle and M^r^ Colinſ were ordered together with\
M^r^ Hook to [*hand in typical cursive for the rest:*] expedite the Catalogue of the Arundelian —\
Library .

\TranscriptionStop

**(CMO/1 p. 174: 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)

**(CMO/1 p. 174: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

**(CMO/1 p. 175: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

**(CMO/1 p. 175: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 175: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 , v. 1 p. 174:)** “[Charter.] To solicit L^d^. Privy seal about.” (CMO/8A)


### 23 November 1668 (CMO/1 pp. 176–177;  CMO/1/158) probably Arundel House 

\TranscriptionStart

[*centered:*] November 23. [1668 .]{.underline}

[…]

[**Page 177:**]

‌ Ordered , That M^r^. Hoskyns be desired to prepare a draught\
for finishing the busineſse of the Society with M^r^. Cole .

[…]

‌ Ordered , That the President be desired to try by a Letter\
to M^rs^. More , whether she will deliver the Papers , left\
with S^r^. Anthony Morgan , concerning the affairs of the\
Society .

‌ Ordered , That the Treasurer do pay M^r^. Richards\
ſix pounds , fiue shillngs , and two pence for Books and\
Paper and pens bought of him for the use of the Society,\
according to his Bill form August 14 . 1667 . to Novemƀ .\
23. [1668 .]{.underline}

\TranscriptionStop

**(CMO/1 p. 176,184,185,188):** “Letter & Debates concerning Manuscripts.” (CMO/1)

**(CMO/1 p. 176: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1 p. 176):** “Order concerning Licence to Print S^r^: Peter Wyches book:” (CMO/1)

**(CMO/1 , v. 1 p. 176:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)

**(CMO/1 p. 176):** “[Printing of Papers & Books.] Sir Peter Myche’s translation of‸^a^ M.S. treating of Abyſsinia.” (CMO/8A)


### 1 February 166$\frac{8}{9}$ (CMO/1 pp. 177–179;  CMO/1/159) probably Arundel House 

\TranscriptionStart

[*centered:*] February 1[. 1668 .]{.underline}

[…]

[**Page 178:**]

[…]

‌ Ordered, That the Draught of the Contract of the Society\
with M^r^. Cole concerning Chelsey- College be engroſsed by the\
care of M^r^. Hoskyns against the next Council , then to be\
Sealed .

‌ M^r^. Colwall was desired to be present, to pay M^r^.\
Cole the one hundred pounds out of the Society's Chest .

‌ Ordered, That D^r^. Ball be desired by M^r^. Hoskyns to be\
on Wednesday next in the morning at  Arundel- house , there\
to meet M^r^. Walker in the Society's Library , and together\
with him to perfect the Catalogue of the Books , eſpecially\
the Manuscripts ; and that M^r^. Hooke be desired by\
the Amanuensis to deliver the Key of the Library to\
D^r^. Ball , if he cannot be there himself .

‌ Ordered, That the Catalogue of the said Manuscripts —\
being made , it should be delivered to M^r^. Collins , that\
he might informe himself ; what value to put upon them .

‌ Ordered, That the Treasurer pay to M^r^. Hook —\
the Arrears due to him according to the Allowance[*canceled:* ‘Allowance~~s~~’] —\
appointed for him , by a former order of the Council ,\
Viz^l^. of November 23[. 1664 .]{.underline}

‌ M^r^. James Gregories Reply to Mons^r^. Christian Huygens\
about the Book De vera Circuli et Hyperbolæ Quadratura ,\
was declared fit to be printed in the Transactions ; but\
withall , that care should be had of omitting all , what
might be offensiue .

‌ M^r^. Hoskyns brought the Papers , concerning the Society,\
that were left with S^r^. Anthony Morgan , to the Council , to [**page 179:**]\
the Number of thirty , great and small : which were delivered\
in to the Cuſtody and care of M^r^. Oldenburg .

‌ The Bish^p^. of Chester proposed a person whom he thought\
fit to translate the History of the R . Society into Latine ,\
if he might haue some Encouragement for doing it ; And —\
being asked, what recompence he would exſpect ,[*over, uncertain:* ‘ecſpect’] he said, he —\
doubted not but thirty pounds would content him .

‌ It was ordered thereupon , that he should be encouraged\
to undertake it , and that such a reward should be made\
good to him , one way or another ; ſo that, where the Printers\
of the Society should proue deficient in paying him such a Sum̃e,\
this Council would make it up .

‌ It was moved by M^r^. Oldenburg , that the Council would —\
please to think upon an effectual way of carrying on the\
busineſse of Experiments at the Meetings of the Society , and\
that in order thereunto , they would consider , whether it were\
not fit to constitute[*over:* ‘continue’] one or two Committees , made up both of\
Members of the Society and the Council , fit for directing —\
of Experiments , which Com̃ittees might meet for that purpose\
at least once a month , one on this , and the other on the\
other end of the Town .  The Proposal being approved of ,\
the President said , he would consider of the persons ~\
proper for such a work .

\TranscriptionStop


**(CMO/1 p. 177):** “[Money to be paid concerning the Repository.] And Books, Paper & Pens” (CMO/1)

**(CMO/1 p. 177: 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)

**(CMO/1 p. 177):** “Stationary wares. Bill paid” (CMO/8A)

**(CMO/1 , v. 1 p. 177:)** “[Charter.] To solicit L^d^. Privy seal about.” (CMO/8A)

**(CMO/1, v. 1 p. 177):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

**(CMO/1 p. 177, 178):** “[Papers.] To be obtained of the Executors of Sir Ant., Morgan” (CMO/8A)

**(CMO/1 p. 177,178):** “Letter Send to Mons^r^: Mors for Papers concerning the ~ \| Affairs of the Society.” (CMO/1)

**(CMO/1 p. 178):** “Declaration concerning a Book de vera Curiuli et Hiperbola Quadratura.” (CMO/1)

**(CMO/1 p. 178: 177,178):** “Letter Send to Mons^r^: Mors for Papers concerning the ~ \| Affairs of the Society.” (CMO/1)

**(CMO/1 p. 178: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1 p. 178):** “Order to make a Catalogue of Manuscripts.” (CMO/1)

**(CMO/1 p. 178):** “[Transactions] To have inserted. J. Gregory’s reply to Huygens.” (CMO/8A)

**(CMO/1 p. 178: 177, 178):** “[Papers.] To be obtained of the Executors of Sir Ant., Morgan” (CMO/8A)

**(CMO/1, v. 1 p. 178):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

**(CMO/1 p. 179):** “A Person recommended to translat the History in Latin” (CMO/1)

**(CMO/1 , v. 1 p. 179:)** “[History of the Royal Society] Proposal to translate it into Latin for £30” (CMO/8A)


### 8 February 166$\frac{8}{9}$ (CMO/1 pp. 180–181;  CMO/1/160) probably Arundel House 

\TranscriptionStart

[**Page 180:**]

[*centered:*] February . 8 . [1668 .]{.underline}

[…]

[*penciled line on left of paragraph:*]\
‌ Ordered, That the Lord Henry Howard of Norfolk , M^r^.\
Aerskine, D^r^. Goddard , D^r^. Ball M^r^. Hoskins , M^r^. Hook and\
M^r^. Collins , or any three or more of them , be a Committee\
to consider , which of the Manuscripts of the Library ,\
bestowed by the said Lord Howard upon the R . Society ,\
are proper to be kept , and which to be parted with to\
the Univesity of Oxford , together with the reasons\
for both .

‌ M^r^. Hoskyns was desired to attend the Lord Howard\
with this Order , and to learne of his Lordſ^p^. what time\
would be convenient to him for the meeting of this\
Committee .[*over:* ‘Council’]

‌ Memorandum , that the Catalogues of the Books were\
committed to M^r^. Hoskyns for the use of the said — —\
Committee .

‌ Memorandum, That this day there was read and seal’d\
an Aſsignment of M^r^. Cole's lease , concerning Chelsey-\
College ; and the same lease delivered with an Aſsignment\
endorsed, likewiſe executed .  All which was put into\
the Cash -chest of the Society at the house of the ~ —\
President : Further that M^r^. Cole received the one [**page 187:**]\
hundred pounds, that was in the said Chest , for the said — —\
Aſsignment .

‌ Ordered , That M^r^. Colwall do charge himself with the\
one hundred pound in the Cash-chest of the R. Society, and\
doe diſcharg himself of it to the same, by producing M^r^.\
Cole's acquittance .

[…]

\TranscriptionStop


**(CMO/1 , v. 1 p. 180:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)

**(CMO/1 p. 180,182,184):** “[Letter & Debates concerning Manuscripts.] Committee Ordered concerning the ſame.” (CMO/1)


### 22 February 166$\frac{8}{9}$ (CMO/1 pp. 181–182;  CMO/1/161) probably Arundel House 

\TranscriptionStart

[*centered:*] February 22 . 1668 .

[…]

‌ Ordered, That the History of the Silkworme written\
in Latine by Sign^r^. Marcello Malpighi , and dedicated to\
the R . Society , be printed forthwith by the Printers [**page 182:**]\
of the same , and that notice be given to the Author of\
this Order.

‌ The Forme of the Order :\
‌ Tractatus , cui titulus , Marcelli Malpighii Deſsertatio\
‌ Epistolica de Bombyce ; Soc. Regiæ dicta imprimatur\
‌ à Johanne Martyn et Jacobo Allestry , dictæ — —\
‌ Societatis Typographis .
‌                Brouncker PRS.

[*marginally penciled between horizontal lines:* ‘pub. 4^o^ \| same year’]

‌ S^r^. Robert Moray made a Proposition , which he said\
he had received from a Noble member of the Society ,\
concerning Chelsey-college , to this effect , Viz^l^. that that\
person was willing to undertake the management of\
the house and Land of the College , so as to employ\
it according to the deſigne of the Society , in planting\
the ground  with all sorts of choice Vegetables , exotick\
and domestick , and in repairing the Howse , all upon\
his own charges , the Society remaining allways pro⹀\
prietors and Masters[*canceled:* ‘Master~~’~~s’] thereof , with a full power of\
ordering and directing what particulars they would —\
haue observed and done in the managery of this affaire ;
the Proposer onely exſpecting to be perpetual Steward\
of that place .

‌ This was received as a noble proposition ; onely\
S^r^. Robert Moray was desired , that he would employ —\
his interest with the Proposer , to haue it put in writing ,\
for preventing any miſtakes .

‌ The Amanuensis was enjoyned to go to D^r^. Clark from\
the Council , and to desire him to deliver to him the Paper\
of Provisoes to be inserted in the Additional Patent ,\
and to carry the same to the Solicitor of the Society , to\
get it inserted accordingly . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 181):** “History of Silkworm to be printed.” (CMO/1)

**(CMO/1 p. 181):** “[*penciled:*] Malpighi’s Book de Bondyce [*Dissertatio epistolica de bombyce*] licensed” [Oldenburg wrote the introduction and reviewed it in the *Transactions*] (CMO/1)

**(CMO/1 p. 181,182):** “[Printing of Papers & Books.] Malpighi’s Hist. of the Silk-worm Ordered, & the form of the Order” (CMO/8A)

**(CMO/1 p. 182: 180,182,184):** “[Letter & Debates concerning Manuscripts.] Committee Ordered concerning the ſame.” (CMO/1)

**(CMO/1 p. 182: 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)

**(CMO/1 p. 182: 181,182):** “[Printing of Papers & Books.] Malpighi’s Hist. of the Silk-worm Ordered, & the form of the Order” (CMO/8A)

**(CMO/1 p. 182, 183, 186):** “Patent for Chelsea College. \| Minutes concerning” (CMO/8A)


### 1 March 166$\frac{8}{9}$ (CMO/1 p. 183;  CMO/1/162) probably Arundel House 

\TranscriptionStart

[**Page 183:**]

[…]

‌ Ordered, That notice be given to those of the Committee[*over:* ‘Council’]\
for considering the Manuscripts of the Societie's Library ,\
that they would please to attend the Council on Thursday\
next at two of the clock , at Arundel-house.

‌ Ordered, That M^r^. Hoskyns and M^r^. Oldenburg do speak —\
with M^r^. Williamson , and inquire of him , whether it be the\
practise , to adde any particulars , by way of endorsement or\
otherwise , to Patents already signed by the King ; and if\
so , then to goe , togethecr with Col : Tytus , to the Atturney Gen^~~ll~~^.\
to acquaint him, that ’tis the pleasure of the Lord Privy Seal ,\
to insert those two clauses concerning the Oath of Allegiance\
and Supremacy , to be given to the President‸ ,^and\ his\ Vice\ presidents,^ and that of —\
Non- alienation .

[…]

‌ Memorandum, That upon the motion of M^r^. Oldenburg of\
chosing Sign^r^. Malpighi an Honorary Member of the Society ,\
he be proposed as such , at the next meeting of the same . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 183: 156,172,174,177,182,183):** “[Chelsey College] Order concerning the Patent.” (CMO/1)

**(CMO/1 p. 183: 182, 183, 186):** “Patent for Chelsea College. \| Minutes concerning” (CMO/8A)

**(CMO/1 , v. 1 p. 183:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)

**(CMO/1 p. 183):** “Malpigi Marcellus, propounded as an hon^y^ Member” [On the way to having his book licensed CMO/1 210, 211] (CMO/8A)


### 4 March 166$\frac{8}{9}$ (CMO/1 p. 184–185;  CMO/1/163) Arundel House

\TranscriptionStart

[**Page 184:**]

[*centered:*] March 4 . 166$\frac{8}{9}$ .

[…]

‌ The Committee for considering the Manuſcripts in the Library\
bestowed by the Lord Henry Howard of Norfork on the R .\
Society , made their report ; whereupon the Council having\
debated it , and coming to no reſolution thereon ,[*uncertain, over:* ‘therein’] thought\
fit to order , that the Secretary should acquaint M^r^.\
Walker , that they are not yet come to a resolution in\
this busineſse , but , when they did , would take care ,\
that the Vice-chancellor of Oxford should haue notice\
of it .

[*centered:*] The Report followeth .

[*paragraph with penciled line on left:*]\
‌ In poursuance of an Order of the Council of the R . Society\
dated 8 . February last , Mee haue considered the Manuſcript\
books in the Library bestowed by the Hon^ble^. Henry Lord Howard\
of Norolk on the R. Society , and do find them cheifly —\
Valuable for their rarity , and the reputation , they carry\
along with them upon that account ; being unlikely to\
be otherwise of any very great advantage , either to\
the University of Oxford , or to the R. Society : Neither\
can we pitch upon any certain rule of putting a price\
upon them, they being Single each in its kind , and not [**page 185:**]\
poſsibly to be supplyed , if once parted with ; which besides\
can hardly be done , without seeming to slight the mumficence\
of the Giver , who has appeared willing to reserue and —\
continue to himself and Hon^ble^. family the Ownership in\
name of that Library , the use of which he was pleas’d to\
bestow on the R . Society .

‌ All which we humbly certify as our opinion , this\
first of March An^o^. Dom̃i . 166${8}{9}$ .   [*two lines:*] W^m^. Aerskine .  John Hoſkyns. \| Peter Balle .

‌ Ordered, That a Diſcourse presented to the R. Society , enti⹀\
tul’d Elemnts of Speech , an Eſsay of Enquiry into the Na⹀\
tural production of Letters , with an Appendix for persons deaf\
and dumb , by William Holder D.D . be printed by John Martin\
and James Allestry , Printers to the said Society . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 184: 180,182,184):** “[Letter & Debates concerning Manuscripts.] Committee Ordered concerning the ſame.” (CMO/1)

**(CMO/1 p. 184: 176,184,185,188):** “Letter & Debates concerning Manuscripts.” (CMO/1)

**(CMO/1 , v. 1 p. 184:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)


### 18 March 166$\frac{8}{9}$ (CMO/1 pp. 185–186;  CMO/1/164) probably Arundel House

\TranscriptionStart

[*centered:*] March 18 . 166$\frac{8}{9}$ .

[…]

‌ It was again considered , whether the Society should part —\
with any of the Books of the Library given to them by the\
Lord Henry Howard, and it being put to the Vote, was — —\
carried in the negatiue .

[**Page 186:**]

[…]

\TranscriptionStop


**(CMO/1 p. 185: 176,184,185,188):** “Letter & Debates concerning Manuscripts.” (CMO/1)

**(CMO/1 p. 185):** “M^r^. W^m^: Petty Book to be printed.” (CMO/1)

**(CMO/1 p. 185):** “D^r^: Holders Book to be printed.” (CMO/1)

**(CMO/1 p. 185):** “[Printing of Papers & Books.] Holder’s Elements of Speech. Ordered” (CMO/8A)

**(CMO/1 , v. 1 p. 185:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)


### 8 April 1669 (CMO/1 pp. 186–187;  CMO/1/165) probably Arundel House

\TranscriptionStart

[*centered:*] April 8. 1669 .

[…]

‌ Ordered , That the Treasurer do pay to M^r^. Gilbert Solicitor\
to the R . Society , or to his order , the Summe of thirty pounds\
twelue shillings ten pence , according to his Bill of April\
3. 1669.

‌ Ordered also that the Treãr do pay to the same M^r^. Gilbert\
or his order, such moneys , as by Warrant from the Preſident\
shall be judged neceſsary for having the Additional Patent\
paſse the Great Seal of England .

[**Page 187:**]

[…]

‌ Ordered, That D^r^. Merret be conferred with , against the\
next Council concerning Willizel , the Botanick Travellor ,\
to testify what he knows of his abilities in collecting Plants\
and other natural Curiosities , and that Willizel be ſummoned\
accordingly to attend the Council at their next meeting ,\
to receiue their resolution and Orders . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 186):** “Order to pay to the Sollicitor of the Society.” (CMO/1)

**(CMO/1 p. 186):** “Sollicitor to the Society \| M^r^. Gilbert” (CMO/8A)

**(CMO/1 p. 186: 182, 183, 186):** “Patent for Chelsea College. \| Minutes concerning” (CMO/8A)


### 20 May 1669 (CMO/1 pp. 187–189;  CMO/1/166) probably Arundel House

\TranscriptionStart

[*centered:*] May 20 . 1669 .

[…]

‌ The President took the Oaths of Allegiance and Supremacy\
according to the import of the Additional Charter .

‌ The ſame did nominate and constitute S^r^. Robert Moray —\
and D^r^. Goddard as two Vice-Presidents , by vertue of the —\
same Charter ,  giving power to the President , to appoint as [**page 188:**]\
[*marginal dash*] many Vice Presidents out of the Council as he shall think\
fit . These two also took the said Oaths of Allegiance —\
and Supremacy .

‌ Ordered. That the ten pounds, advanced by the Treasurer\
to Thomas Willizel , as part of the thirty poundſ appointed\
him by the Council for one year , be allowed him upon\
his account : [*end of line inserted:*] the Payment to begin the 25^th^. March laſt

[…]

‌ There was read a Forme of a Certificate from the Council\
of the R. Society for Thomas Willizel : whereupon it was\
ordered it should be reviewed by M^r^. Hoskins , and thereupon ,\
by the Presidents approbation , sent away to the said Thomas\
Willizel .

‌ S^r^. Theodore de Vaux renewing the motion formerly made\
concerning the Exchang of the manuscripts in the Arundelian\
Library , bestowed by the Lord Howard upon the R. Society ,[**page 189:**]\
the Council adhæred to their former vote , and the Lord Henry\
Howard declared , that as he had formerly left to the Council\
the full diſposal of that Library , so he did still , desiring\
onely that it might not be imputed to him , as if he were a —\
hinderer of that exchange , he being indifferent , either to the\
keeping them for the Society , or the exchanging them with the\
University of Oxford . 

‌ The Transactions of Number 47. were licensed . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 187: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 187,188):** “Order concerning the Botanick Traveller Willirell.” (CMO/1)

**(CMO/1 p. 187,188):** “Willirel M^r^. Thomas, the Botanic Traveller \| To attend the Council, & be employ’d at £30 a year” (CMO/8A)

**(CMO/1 p. 188: 176,184,185,188):** “Letter & Debates concerning Manuscripts.” (CMO/1)

**(CMO/1 p. 188: 187,188):** “Willirel M^r^. Thomas, the Botanic Traveller \| To attend the Council, & be employ’d at £30 a year” (CMO/8A)

**(CMO/1 p. 188: 187,188):** “Order concerning the Botanick Traveller Willirell.” (CMO/1)

**(CMO/1 , v. 1 p. 188:)** “[Arundel Library.] Minutes concerning the Oxford proposal to the R.S. to part with some of the Books” (CMO/8A)


### 3 June 1669 (CMO/1 p. 189;  CMO/1/167) probably Arundel House

\TranscriptionStart

[*centered:*] June 3. 1669 .

[…]

‌ The Latine Preface[*over:* ‘Presace’] made by M^r^. Oldenberg to Malpighi's\
Book was read and approved .

‌ Ordered , That a ſallery of forty poundſ per annum be —\
allowed to M^r^. Oldenburg , one of the Secretarieſ of this\
Society , form the time that the last present was ordered to\
him . [*downward flourish*]

[*two line note in lower right:*] June. 10\
\<ſee/1. 192.\>

\TranscriptionStop

**(CMO/1 p. 189: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 189):** “Order concerning M^r^: Oldenburghs Sallary” (CMO/1)

**(CMO/1 p. 189):** “Preface of M^r^: Malpigh Book.” (CMO/1)

**(CMO/1 p. 189: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 189):** “[Oldenburg M^r^. S \| Appointed Secretary]
Ordered a Salary of £40 ⅌ An [per year] since last present
(2$\frac{1}{2}$y)” [Oldenburg was the first editor of the
Transactions] (CMO/8A)


### 24 June 1669 (CMO/1 pp. 190–191;  CMO/1/168) probably Arundel House

\TranscriptionStart

[**Page 190:**]

[*centered:*] June 2[4. 1669.]{.underline}

[…]


‌ The President reported , that the Committee , appointed June^+^ 10 . [*margin:* ‘+ NB. The Notes of \| the Transactions in \| the Council , meeting \| june 10^th^, were by \| a mistake , postposed \| to these of june 24^th^.’]\
to consider of the way to carry on the Society's busineſse ,\
had agreed upon a letter , once more to be sent about for\
soliciting the Arrears ; and that such a letter was drawn\
up , and ready to be presented to the Council .

‌ It being read, it was , after some amendments , ordered ,\
that several copies should be made of it , directed first\
of all to such Fellows, as are in and about London , —\
and their delivery to be recommended to those , who had pro⹀\
posed such ; as were in Arrear to the Society .  The\
said Letter was in these words following ;

[**Page 197:**]

‌ S^r^. By Order of the Council of the R. Society I am to\
giue you notice , that you are in arrears of the Contri⹀\
bution , which by your ſubscription you engaged your\
self to pay , the summe[*over:* ‘samme’] of \_\_\_\_\_\_ : And to —\
offer to your consideratio , that the Society , being not yet\
endowed with any Revenue , is not in a capacity to\
bear the charges of the proper work thereof , and —\
maintain the neceſsary Officers , without the contri⹀\
bution of the Fellows ; So that if the Society fail therein,\
the Dishonour of it must light upon all ſuch as do not\
pay their Contribution : It is therefore hoped upon such —\
Considerations (which perhaps might not occurre to you\
before) that you will forthwith take order for your — —\
payment of all‸^your^ Arrears ; and duly pay the said Contribution\
for the future , as long as you shall think fit to —\
continue a Fellow of the Society. [*downward flourish*]

[*two lines inserted, to the right:*] \[   Octob.11.\
\<ſee/1. 194.\>

[*lower right:*] June 10.. by mistake

\TranscriptionStop


**(CMO/1 p. 191: 96,191):** “Form of Letters concerning y^e^ collecting of Arrears” (CMO/1)

**(CMO/1, v. 1 p. 191):** “[Arrears of contributions] Form of notice to be sent” (CMO/8A)


### 10 June 1669 (CMO/1 pp. 192–194;  CMO/1/169) probably Arundel House

\TranscriptionStart

[**Page 192:**]

[*centered:*] June[‌ 10 . 1669 .]{.underline}


[…]

‌ M^r^. Hoskyns made a Report of what was done by the\
Committee appointed May 20 , 1669 for improving Chelsey\
College , as Followeth ;

‌  At a Committee of the R . Society , for impriving —\
‌  Chelsey- college &c. at the Lord Breretons Lodgings\
‌  May 22. 1669. in poursuance of an Order of the —\
‌  Council of the 20^th^. past : Resolved to

‌ Quere the Charter , and what is by this last granted to the\
‌ Society of the Rights and Poſseſsions of the late Collodge ,\
‌ and how far grantable ?

‌  Item , Quære, The Map of the ſcite of the said College and\
‌ its Lands ; which Map is in the hands of S^r^. Anthony —\
‌ Morgan's Executors , ‸^or^ M^r^. Cole, [*canceled:* ‘~~and~~’] ‸^or^ M^r^. Chayny .[*over:* ‘Cheyny’]\

Ꝗ the Book of the said College printed, and Lately in Sir\
‌ Anthony Morgans custody ?

‌ It. Quære the Paper of bringing Uxbridge River to London ,\
‌  of M^r^. Packer ; and whether the liberty and power of\
‌  bringing that River were granted to the said College ,\
‌  and the Act of Parliament to that purpose ?

[**Page 193:**]

‌ Quære The Patent for making collection of Voluntary Contri⹀\
‌  butions throughout the Nation ,[*over:* ‘ſtation’] for advancing the said College ,\
‌  and what was collected , and in whose hands the same is ?

‌  Item, My Lord Brereton is desired to learne from Doct^r^.\
‌ Wilkinson , by means of M^r^. Cheyney , what Donations —\
‌ haue been made to the said College ; and what is become\
‌ of the Library ?

‌  Quære The Patents, that S^r^. Anthony Morgan mentions,\
‌ but could not find , made to the said College ?

[*marginally:* ‘See May 20^th^.’]\
‌ Lord Brereton reported , that M^r^. Cheyney was ready to treat —\
with the Society about a year hence , when it will be in his —\
power ; the Land, to be treated about , being now[*over:* ‘new’] leased out.

‌ Ordered , That the Council of the R. Society, or three or more\
of them be a Committee , to consider of the most convenient\
and effectual way to carry on the Buſineſse of the Society ;\
And that they meet the first time at the house of the Lord\
Brouncker , on Monday next at fiue of the clock in the\
evening

‌ The Certificate for Thomas Willisel , drawn up by M^r^. Hoskyns ,\
was read and [*canceled, dittography:* ‘and’] approved , as follows ;

‌ These are to certify all whom it may concerne, That the\
‌ Bearer hereof, Thomas Willisel, is at present employed by —\
‌ the President , Council and Fellows of the R. Society of\
London for improving Natural Knowledge , to go into —\
several parts of his Ma^ties^. Dominions for purposes suitable\
to their Institution , according to Authority unto them on this\
behalf given by his Sacred Ma^tie^. that now is ; And they\
earnestly[*over:* ‘errnestly’] recommend him to all [*canceled:* ‘~~ingenuous~~’] generous and\
ingenuous Spirits, [*canceled:* ‘~~that~~’] desiring that as occasion shall\
require , they will aſsist him in promoting a Work so [**page 194:**]\
generally beneficial to all Mankind .

‌ In Witneſse whereof, The said President Council and —\
‌ Fellows of the R. Society haue hereunto caused their\
‌ Common Seal to be affixed this      day of

[*centered, inserted:*]  \[ June 24. /1. 190 .\]

\TranscriptionStop

**(CMO/1 p. 193):** “[Willirel M^r^. Thomas, the Botanic Traveller] Letter of Recommendation: for collecting Art. in Nat history” (CMO/8A)


### 11 October 1669 (CMO/1 pp. 194–195;  CMO/1/170) probably Arundel House

\TranscriptionStart

[*centered:*] October 11[ . 1669 .]{.underline}

[…]

‌ 2. Ordered , that M^r^. Charles Howard , D^r^. Goddard , D^r^.\
Merret and M^r^. Hook , or any two or more of them  do —\
meet and direct Thomas Willisell in his Employment of\
further collecting , such Plants , Fowle , Fishes and Minerals ,\
and in such parts of his Ma^ties^. Kingdomes as they shall\
think best for the use of the R . Society and that the\
said Thomas Willisell at his returne do first of all attend\
the President , and receiue orders from him about the [**page 195:**]\
Collection , he shall then haue made .

‌ Order’d , That the President , S^r^. Robert Moray , S^r^. Theodore\
de Vaux , D^r^. Clark and M^r^. Oldenburg , or any three or more\
of them , be a Committee of the Council , for auditing the\
accompts of the Treasurer of the R . Society , and that they\
agree among themselves concerning the time and place of\
their meeting for that purpose .

— Order’d , That D^r^. Merret be desired to send the Collection ,\
made by Thomas Willisell in his first Voyage , to the R. Society\
at their next meeting in Arundel-house , Octob. 21 . 1669.

‌ The President declared, that he thought fit , that the — —\
Meetings of the Society should now begin again, and that\
therefore he intended to send out Tickets to Summon the\
Fellows to meet Octob. 21 . next : for which his Lordſ^p^. gaue\
this forme ;

‌ These are to giue notice , that the R . Society is to meet\
again on Thursday the 21^th^. of this present month of —\
October , in Arundel-house , at the usual hour , and —\
thenceforth to continue as formerly . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 194,195):** “[Voted for the Collection of Rarities.] Order for the same.” (CMO/1)

**(CMO/1 p. 194,195):** “[Willirel M^r^. Thomas, the Botanic Traveller] Com^[ee]{.underline}^. to give him instructions” (CMO/8A)

**(CMO/1 p. 195: 194,195):** “[Willirel M^r^. Thomas, the Botanic Traveller] Com^[ee]{.underline}^. to give him instructions” (CMO/8A)

**(CMO/1 p. 195: 194,195):** “[Voted for the Collection of Rarities.] Order for the same.” (CMO/1)

**(CMO/1 p. 195: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)


### 29 November 1669 (CMO/1 p. 196;  CMO/1/171) probably Arundel House

\TranscriptionStart

[**Page 196:**]

[*centered:*] November 29 . 1669 .

[…]

The Committee of the Council for auditing the Accompts ,\
made a Report , which was approved of by the Council ;\
and is as followeth ;

[*two lines centered:*] At a Committee of the Council of the R. Society\
for Auditing the Trẽars Accompt. Nov 10 1669

‌  Vpon Examination of M^r^. Daniel Colwall’s Accompts , Wee\
‌  find him Debtor

-------------------------------------------------------------------------------
                                                              ~~l~~i ſ d
------------------------------------------------------------  -----------------
To Arrears due to the R . Society for their Quarterly\         1283 : 4 : 6
‌ Payments this 10^th^. November 1669 - - - - -

To Money he hath received for Admiſsions - - - - - -           18 : — : —

To Money he received out of the Cask-chest - - - -             100 : — : —

To the Ballance of his last Accompt - - - - - - -              98 : 5 : 6

‌                     Summa .                                   1499 : 10 : 0
------------------------------------------------------------  -----------------

‌  That he is Creditor .

------------------------------------------------------------  -----------------
By Money he hath paid for the use of the Society — —           400 : 2 : 4

By money in arrear , resting unpaid by the Fellows —           1028 : 15 : 0

By Ballance resting in Cash now in his hands - - -             70 : 12 : 8

[Total:]                                                       1499 : 10 : 0
------------------------------------------------------------  -----------------

‌ Signed .    Brouncker PRS .\
‌  Theodore De Vaux\
‌               Tim: Clarke .\
‌      Henry Oldenburg .

\TranscriptionStop

**(CMO/1 p. 196: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)


### 20 December 1669 (CMO/1 pp. 197–198;  CMO/1/172) probably Arundel House

\TranscriptionStart

[**Page 197:**]

[*centered:*] December 20 . 1669 .

[…]

‌ D^r^. Ball and M^r^. Hook were desired to finish the Catalogue of\
the Society's library , within the Holy-dayes now approaching;\
which he promiſed to doe .

‌ M^r^. Oldenburg desired [*canceled:* ‘~~that~~’] the Council from M^r^. Henshaw ,\
that they would lend him for a few weeks , out of the Society's —\
library a Manuscript of Ireneus , for a friend of his upon\
security .

‌ It was ordered thereupon in general ,

‌ That all those , who shall haue granted to them by this Council\
the loane of any of the books belonging to the Arundelian Library\
bestowed on the R . Society by Henry Lord Howard of Norfork ,[**page 198:**]\
shall oblige themselves by a Bond of one hundred pounds Sterling\
(to be given to the President , Council and Fellows of the -\
R . Society ) to restore the same within the time to be\
respectively prefixt , entire , undefaced and unblotted .

‌ And thereupon in particular ,

‌ That M^r^. Hook do deliver to M^r^. Thomas Henshaw out\
of the Arundelian library , bestow’d on the R . Society\
by Henry Lord Howard of Norfolk , the Manuscript of\
Ireneus , upon the said Thomas Henshaw's sealing a Bond\
to the President Council and Fellows of the said Society,\
of one hundred pounds sterꝲ. and delivering the same\
to M^r^. Henry Oldenburg for their use , and thereby obliging\
himself to restore the said Book entire , undefaced —\
and unblotted within the ſpace of two months from the\
time of the Receipt thereof.

\TranscriptionStop


**(CMO/1 p. 197,198):** “Desire & Order for Lending a Manusrcipt.” (CMO/1)

**(CMO/1 p. 197, 232, 279):** “Order concerning the Loan of the books of the Library” (CMO/1)

**(CMO/1 p. 197: 128,130,138,143,197):** “Memorandum & Order concerning the Library, presented by M^r^. \| Howard” (CMO/1)

**(CMO/1, v. 1 p. 197):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)  “[Arundel Library.] No M.S.S. lent, under a Bond of £100”


### 27 April 1670 (CMO/1 pp. 198–199;  CMO/1/173) probably Arundel House

\TranscriptionStart

[*centered:*] April 27 . 1670 .

[…]

[**Page 199:**]

[…]


‌ Ordered, that the Philosophical Transactions n^o^. 58 be\
licensed . [*downward flourish*]

\TranscriptionStop

### 28 June 1670 (CMO/1 pp. 199–200;  CMO/1/174) probably Arundel House

\TranscriptionStart

[*centered:*] June 28[ .167]{.underline}0 .

[…]

‌ Ordered , That M^r^. Hook do find out a man fit to be —\
employed by him in the ſervice of the Society , and that such\
an one haue allowed him fiue pounds for a quarter of a\
year , to begin from the time , that M^r^. Hook shall declare\
to the President , that he hath taken such an one into\
ſervice.

‌ Agreed upon , that a Curator , if a fit one can be met [**page 200:**]\
with , be entertained by the Society for a quarter of a —\
year , to begin from Michaelmas next .

[…]

\TranscriptionStop


**(CMO/1 p. 199: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 199: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)


### 26 July 1670 (CMO/1 pp. 200–201;  CMO/1/175) probably Arundel House

\TranscriptionStart

[*centered:*] July 2[6 . 1670 .]{.underline}

[…]

‌ M^r^. Pitt , the Printer of D^r^. Wallis's two volums de Motu —\
having represented to the Council both the charges of printing\
those Books , and the length of time for selling of the Edition ,\
insisted upon the price of fifteen shillings six pence for\
both the Volums .  Whereupon the Council declared , that since\
he did not think fit to abate any thing of that price , and\
to take 14^s^. for both the Books , he was at liberty to\
sell them as he could , but that then the Subscribers\
who had agreed to pay such a rate as should be set by\
the Council , were also left at liberty to buy or not to buy\
at his rate . 


[**Page 201:**]

[…]

\TranscriptionStop


### 10 November 1670 (CMO/1 pp. 201–202;  CMO/1/176) probably Arundel House

\TranscriptionStart

[*centered:*] November 10 . 1670 .

[…]

‌ There were named fiue of the Council to be a Com̃itee for\
auditing the Treasurers Accompts , Viz^l^. The President , the\
Lord Henry Howard , the Lord B^p^. of Chester , D^r^. Smith and\
one of the Secretaries .

‌ Reſolved , that there be drawn up a legal forme , whereby\
every one , that desires to continue a Fellow of the R .\
Society , and shall signe the same , shall oblige himself\
to pay his arrears of fifty two shillings a year , and\
four pounds per annum for the future : Which Obligation\
shall yet bind none of the Subscribers , unleſse the number\
of them be such , whose Contributions the Council shall\
judge sufficient to defray the charges , requisite to carry on\
the busineſse of the Society .

‌ Resolved , that M^r^. Hoskyns be desired to draw up such a\
Forme against the next Council , to be held Novemb. 14 , at —\
the house of the President in Coven-garden .

‌ And M^r^. Hoskyns was desired accordingly , who also promiſed\
to do it .

[**Page 202:**]

‌ The Philosoph. Transactions of Numb . 65 were licensed . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 201: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 201: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)


### 14 November 1670 (CMO/1 pp. 202–203;  CMO/1/177) the President’s House (“Brouncker’s house in Covent Garden”)

\TranscriptionStart

[*centered:*]  November 14 [. 1670 .]{.underline}

[…]

‌ M^r^. Hoskyns produced the Legal Forme of Subscriptions\
which had been recommended to him at the last Council .

‌ It was read , and referr’d to another meeting to be —\
considered : It is as followeth:

‌ I \_\_\_\_ do covenant grant and agree to and with the\
President Council and Fellows of the R. Society &c. \_\_\_ to pay\
unto them the ſumme of \_\_\_ upon the \_\_ day of \_\_\_\_\
which shall be in the year of our Lord \_\_\_\_\_\ . And also\
the summe of four pounds yearly and every year , so lang as I\
shall continue Fellow of the said Society , by four even quarterly\
payments , the first to be on \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\
To the performance of which , I do hereby bind my self and my\
Heirs - Witneſse my hand and ſeal this \_\_\_ day of \_\_\_\_\_\
In the presence of\
[*to the right: a circle with three horizontal lines*]

[**Page 203:**]

[*centered:*] Or thus ,

Wee , whose names are underwritten , do every one for himself\
and not one for another covenant, promiſe, grant and agree to\
and with the President \_\_\_\_\_ &c . to pay all and every\
such ſumme and ſum̃s of money as each of us respectively\
owes and are due unto the said Society from us by virtue\
of any order or Statue of the said Society : And also the\
yearly summe of four pounds , by four equal [*canceled:* ‘~~partions~~’] quarterly\
payments , at the four usual dayes of payment in the year ,\
that is to say \_\_\_\_\_\_\_\_\_ . The said payment to con⹀\
tinue so long as each of us respectively do continue a -\
Member of the said Society .  To all which several payments\
wee do severally and respectively bind our selves , our several\
and respectiue Heirs .

[*to the right: three circles on atop the next, horizontal lines, dots, and vertical lines within*]\
‌ Sealed and delivered by\
\_\_\_ the \_\__ day of \_\_\_\
in the presence of

[*centered downward flourish*]

\TranscriptionStop


**(CMO/1 p. 202: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 202,203):** “[Resolution concerning an Obligaion of the Fellows for y^e^ carrying on the Societys businesse.] Forme of the Same.” (CMO/1)

**(CMO/1 p. 203: 202,203):** “[Resolution concerning an Obligaion of the Fellows for y^e^ carrying on the Societys businesse.] Forme of the Same.” (CMO/1)


### 24 November 1670 (CMO/1 pp. 203–204;  CMO/1/178) probably the President’s House

\TranscriptionStart

[*centered:*] November 24 . 1670 .

[…]

[**Page 204:**]

‌ The Committee of the Council for auditing the accompts ,\
made their Report , which the Council approved of , Viz^l^.

[*two lines centered:*] At a Committee of the Council of the R. Society\
for auditing the Treãrs Accompts

‌ Vpon Examination of M^r^. Daniel Colwall’s Accompt , Wee\
‌ find him Debtor ,

-----------------------------------------------------------------------
                                                          ~~l~~ d
--------------------------------------------------------  -------------
To the Arrears due to the said Society for\               1475 : 11 : 0
their quarterly payments this 10 . Nov . 1670

To Moneys he hath received for Admiſsions —               10 : 10 : 0

To the Ballance of his last accompt —                     70 : 12 : 8

£.                                                        1556 : 13 : 8
--------------------------------------------------------  -------------

[*centered:*] He is Creditor ,

-------------------------------------------------------------------------
                                                          ~~l~~ ſ d
--------------------------------------------------------  ---------------
By money he hath paid for y^e^. use of the Society —      220 : 15 : 10

By Arrears yet unpaid by the Fellows of the\              1267 : 2 : 0
‌ Society —

By Ballances resting in his hand, Sixty Eight\            68 : 15 : 10
‌ pounds fifteen shillings ten pence —

£.                                                        1556 : 13 : 8
--------------------------------------------------------  ---------------

[*centered downward flourish*]

\TranscriptionStop


**(CMO/1 p. 204: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)


### 11 May 1671 (CMO/1 pp. 205–206;  CMO/1/179) probably the President’s House

\TranscriptionStart

[**Page 205:**]

[…]

‌ There was read a Petition of Robert Thornhill Es to the\
R . Society , relating to Chelsey College &c . which being debated,\
it was order’d ,

— That M^r^. Charles Howard , S^r^. Robert Moray , S^r^. Paul Neile ,\
S^r^. John Lowther , S^r^. Peter Wyche , M^r^. Henshaw , or any two\
or more of them be a Committee: to consider of the Petition\
of Robert Thornhill Es. relating to Chelsey College &c. and\
to treat with him about the particulars contained therein ,\
and that they meet the first time on Monday next , being\
May 15^th^. at S^r^. Robert Moray's chamber at 10 a clock ;\
calling for their aſsistance , if they shall think fit , his\
Ma^ties^. Surveyor General D^r^. Christopher Wren, and M^r^. Hook .

[…]

‌ Ordered, that the Treasurer do pay fiue pounds , to be left\
in the hands of the President , and by his Lordſ^p^. to be diſposed of\
to Spencer Hickman , Stationer , for his encouragement of [**page 206:**]\
printing of M^r^. Horrex’s Manuscripts of Astronomy .

‌ Ordered , that the Treasurer do pay Eight shillings to ~ ~\
M^r^. John Collins for a transcript, by him caus’d to be made of\
some papers belonging to M^r^. Horrex's Astronomical\
Papers .

‌ There was licenced D^r^. Nathanael Grew's book , entitl’d \
The Anatomy of Vegetables begun, together with a\
General Account of Vegetation grounded thereon ,‸^and\ order’d^ to\
be printed by y^e^[*over:* ‘a’] Printer of the R .  Society . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 205):** “Letter written ot the Society concerning Chelsey Coll:” (CMO/1)

**(CMO/1, v. 1 p. 205):** “[Brounker L^d^:] To pay £5, of the Society [*previous word inserted above*] towards printing Horrox’s Astronomy” (CMO/8A)


### 9 November 1671 (CMO/1 p. 206;  CMO/1/180) probably the President’s House

\TranscriptionStart

[*centered:*] November 9 . 1671 .

[…]

‌ There was appointed the following Committee for auditing the\
Accounts ; 

[*centered:*] The President ,\

S^r^. Robert Moray    D^r^. Goddard .\
S^r^. Paul Neile    M^r^. Oldenburg .

There was sworne Spencer Hickman as one of the Printers\
of the R . Society , after he had been constituted for such by\
the President , according to the Power granted him by the\
Additional Charter .

‌ The Transactions of Numb .77. were licensed . [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 206):** “Spencer Hickman sworne as Printer of the Society.” (CMO/1)

**(CMO/1 p. 206: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 206):** “D^r^: Nathanael Grews Book licensed.” (CMO/1)

**(CMO/1 p. 206):** “[Printers to the Society. M^r^. Hickman sworn in] Reprimanded for printing a book called Lampas.” [is this Hickman right TODO(check CMO/1)] (CMO/8A)

**(CMO/1 p. 206: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 206: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 206: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 , v. 1 p. 206:)** “[Grew Nehemiah M.D.] Anatomy of Vegetables Licensed” (CMO/8A)

**(CMO/1 p. 206):** “[Printers to the Society.] M^r^. Hickman sworn in” (CMO/8A)


### 30 November 1671 (CMO/1 p. 207;  CMO/1/181) Probably the President’s House

\TranscriptionStart

[**Page 207:**]

[*centered:*] November 30 . 1671 .

[…]

‌ The Report of the Committee for Examining the Treãrs\
Accompts was made, and approved of , as followeth Viz^l^.

[*two lines centered:*] At a Committee of the Council of the R . Society\
for[*canceled:* ‘for~~e~~’] Auditing the Treãrs Accompts Nov. 21[*over:* ‘23’] . 1671 .

Vpon the Examination of M^r^. Daniel Colwal’s Acco^ts^. we find him\
Debitor ,

--------------------------------------------------------------------------------
                                                            ~~l~~
----------------------------------------------------------  --------------------
‌ To the Arrears due to the ſaid Society for their\          1696 : 0 : 0
Quarterly payments this 21 Novemb. 1671 - - - -

‌ To moneys he hath received for Admiſsion - - - - -         2 : 0 : 0

‌ To the Ballance of his last Accompt - - — — — -            68 : 15 : 10

[Total:]                                                     £. 1677 : 15 : 10
----------------------------------------------------------  --------------------

He is Creditor ,

----------------------------------------------------------  --------------------
‌ By Moneys he hath paid to the use of the Society—          202 : 1 : 4

‌ By Arrears yet unpaid by the Fellows of the Society - -    1554 : 4 : 0

‌ By Ballance resting in Cash in his hands - - - -           10 : 10 : 6

[Total:]                                                      £. 1766 : 15 : 10
----------------------------------------------------------  --------------------

‌    Signed

‌          Brouncker PRS .

‌           R Moray\[*five-pointed star*\]\[*overlapping:* ‘\rlap{R}{M}oray’\]

‌             J Goddard . [*overlapping:* ‘\rlap{J}{G}oddard’]

‌              H. Oldenburg S.


\TranscriptionStop


**(CMO/1 p. 207: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)


### 10 April 1672 (CMO/1 pp. 208–209;  CMO/1/182) Probably the President’s House

\TranscriptionStart

[**Page 208:**]

[*centered:*] April 10 . 1672 .

[…]

[*centered:*] M^r^. Oldenburg .

[*next two paragraphs with penciled vertical left*]\
‌ There was made a Proposal from M^r^. Evelyn by\
M^r^. Oldenburg , for letting out Chelsey- college to be\
a Prison -house during this warr; together with an —\
intimation , that M^r^. Evelyn hoped , he might get a rent\
of an hundred poundſ per’ Annum for it , besides some\
neceſsary repairs of the house .

‌ It was ordered hereupon , that the President of the\
R. Society , the Treasurer and the Secretary that —\
officiateth should have power to agree in the name\
of the Council with the said M^r^. Evelyn about the matter\
proposed , and conclude with him and his colleagues ,
if the abouementioned hundred poundſ per annum , together\
with good repairs could be obtained .  And that upon\
the Agreement concluded , the do make a report to the\
Council , for paſsing it under the Seal fo the Society .

[…]

[**Page 209:**]

[…]

‌ The Bish^p^. of Chester proposed D^r^. Grew to be a Curator\
to the R. Society for the Anatomy of Plants for a year ,
upon Subscriptions amounting to fifty poundſ , to[*over:* ‘m’] be made\
by such Members of the Society , as should be willing to\
contribute thereto .

‌ The Council approved of this proposal ,[*over:* ‘proposol’] and ordered that\
it should be signified to the Body of the R . Society at their\
next meeting , in order to actual Subscriptions .

[…]

\TranscriptionStop


### 12 June 1672 (CMO/1 p. 210;  CMO/1/183) probably the President’s House

\TranscriptionStart

[**Page 210:**]

[*centered:*] June 12 . 1672 .

[…]

‌ Ordered , That Mons^r^. Malpighi's book entituled Marcelli\
Malpighij Phil. et Medici Bonomiensis Desertatio Epistolica\
de Fomatione Pulli in Ovo , be printed by the Printer of\
the R . Society ; and the form of the licence to be as —\
followeth;

[*centered:*] Junij[*over:* ‘June’] 12 . 1672 .

‌ In Concilio [Regiæ Societatis]{.underline} Londini ad Scientiam\
Naturalem promovendam institutæ

‌ *Tractatus ,* cui Titulus , Marcelli Malpighii Philosophi\
‌ et Medici Bononiensis Diſsertatio Epistolica de — -\
‌ Formatione Pulli in Ovo *REGIÆ SOCIETATI* dicata ;\
‌ imprimatur à Johanne Martyn , dictæ Societatis\
‌ Typographo . [*downward flourish*]

[*GB-UkLoRS Tracts X12/8 with presentation inscription:* ‘Dissertatis hac exhibita fuit Regia Societati \| d. 30. Octob 1672 . /’ stabbed but now in modern pamphlet volume]

\TranscriptionStop


**(CMO/1 p. 210):** “[Preface of M^r^: Malpigh Book.] Licensed” (CMO/1)

**(CMO/1 p. 210: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 210,211):** “[Malpigi Marcellus,] his Book licensed. De formatione pulli in Ovo” (CMO/8A)


### 13 November 1672 (CMO/1 p. 211;  CMO/1/184) probably the President’s House

\TranscriptionStart

[**Page 211:**]

[*centered:*] November 13 . 1672 .

[…]

‌ A Committee of the Council , for auditing the Accompts of\
the Treasurer was appointed , Viz^l^.

[*centered:*] The President .

S^r^. Theodore de Vaux.    D^r^. Walter Needham\
D^r^. Goddard .      M^r^. Oldenburg .

‌ The Council allowed of S^r^. W^m^. Petty’s Accompt of Febr .\
29[*over:* ‘19’]. 167$\frac{1}{2}$ .

[…]

‌ Ordered, that what Copies of Sign^r^. Malpighi's book [de ]{.underline}\
[Formatione Pulli in Ovo]{.underline} the Printer will not furnish for\
the Author gratis , shall be paid for by the Society , who\
lately ordered thirty Copies of it to be sent to Bononia for\
the said Author . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 211):** “[M^r^. W^m^: Petty Book to be printed.] Order concerning Coppies of the same.” (CMO/1)

**(CMO/1 p. 211: 210,211):** “[Malpigi Marcellus,] his Book licensed. De formatione pulli in Ovo” (CMO/8A)


### 27 November 1672 (CMO/1 pp. 212–213;  CMO/1/185) probably the President’s House

\TranscriptionStart

[**Page 212:**]

[*centered:*] Novemb. 27. 1672 .

[…]

‌ The Committee of the Council , for Auditing the accompts\
of the Treãr. made a Report as follwes .

[*three lines centered:*] At a Committee of the Council of the R . Society ,\
for Auditing the Treãrs accompts\
Novemb. 23. 1672 .

Vpon Examination of M^r^. Daniel Colwall’s Acco^ts^. We find\
‌ He is Debtor,

----------------------------------------------------------  -----------------------
To the Arrears due to the said Society for\                  1957 : 12 : 0
‌ their Quarterly Paym^ts^. this 23 . Nov . 1672 - -

To Moneys he hath received for Admiſsions - -                20 : 15 : 0

To the Ballance of his last Accompt - - - -                  10 : 10 : 6

[Total:]                                                     ~~£ib~~ 1988 : 17 : 6
----------------------------------------------------------  -----------------------

Wee also find He is Creditor ,

----------------------------------------------------------  -----------------------
By Moneys he hath paid for the use of the\                   163 : 1 : 3
‌ Society , as by Vouchers doth appear - - -

By Arrears yet u‸^n^paid by the Fellows of the\               1818 : 7 : 0
‌ Society - - - — — - - — - - -

By Ballance resting in cash in his hand - - -                 7 : 9 : 3

[Total:]                                                     ~~£ib~~—1988 : 17 : 6
----------------------------------------------------------  -----------------------

‌ The Council finding the vast arrears of many fellows of [**page 213:**]\
this Society , order’d , that once again , a List should be drawn\
up by the Amanuensis of those , that are most behind in\
payment , and that thereupon , such persons should be so⹀\
licited to pay by those , that had proposed them for —\
Candidates ; and that this should be done against the\
next meeting of the Council .

\TranscriptionStop


**(CMO/1 p. 212: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 212: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)


### 18 December 1672 (CMO/1 pp. 213–215;  CMO/1/186) probably the President’s House

\TranscriptionStart

[*centered:*] December 18 . 1672 .

[…]

‌ The Secretary mentioning D^r^. Grew's desire to be informed,\
whether the Society would further imploy him in the Anatomy\
of Plants upon the former terms ; it was declared , that\
the Council and Society did well approve of what he had\
hitherto performed , and that the Council would further —\
recommend him to the Society , to continue him another\
year , if the Subscribers would please to continue  their\
contribution .

[…]

‌ S^r^. Paul Neile moved, anew , that considering the vast\
arrears due to the R. Society , the Fellows thereof might\
by a legal tye be obliged to payment : And it being resolved[*over:*‘resolv’d’]\
that as good council as could be had , should be advised\
with , whether the Obligation already subscribed by the\
Fellows did not amount to such a legal tye ? My\
Lord Stafford offer’d that he would undertake to\
enquire accordingly of the best lawyers he knew ,\
and thereupon satisfy this Council at their next —\
meeting :  His Lords^ps^. offer was accepted with thanks ,\
and the Secretary order’d to cause a Copy to be made\
of the said Obligation ; and also of the Statutes concerning\
the Payments in general termes included in that Obligation ,\
and to send that Copy to his Lors^p^.

‌ The Amanuensis was commanded to go to the ~\
Prerogatiue Court , and to copy out of the late Bish^p^. [**page 215:**]\
of Chester's last will what the legacy is , which he hath\
bequeathed to the Royal Society .

[…]

\TranscriptionStop

**(CMO/1 p. 213,285):** “Order for making a List concerning Debtors of Arrears” (CMO/1)

**(CMO/1 p. 214):** “Resolution for making an Obligacon towards the paym: of Arrears.” (CMO/1)


### 9 October 1673 (CMO/1 pp. 215–217;  CMO/1/187) President’s House

\TranscriptionStart

[*centered:*] October 9. 1673.

[*in a finer cursive ff:*]

[…]

‌ Whilst this was doeing came in S^r^: Theodore de\
Vaux , Sent by my Lo^d^: Marshall, to acquaint the Council\
that his Lo^p^: did wonder they were not met in Arundel\
house as formerly but yet hoped, they would hereafter\
still continue their Meetings there as formerly, which\
if they should remove to any other place he could not\
but take it very unkindly .

‌ Hereupon the President declared that for this —\
time he had caused the Council to be summoned in this place\
for his ⅌ticuler conveniency his present occassions not ~\
haueing ⅌mitted him to go afarr off: And the same —\
at the desire of the Council return’d their hearty thanks\
to my Lord Marshall for his Singular affection and —\
respect to the Society .

[…]

‌ Further Chelsey College being spoaken of & something\
mencon’d of pulling downe the house & selling the Materialls,\
it was thought fitt by the Council that M^r^. Hoskins or some\
other Lawyer should be consulted with, whether, notwith⹀\
standing the Clause in the Charter, of non-Alienation, the said [**page 217:**]\
house might be puld downe, & the Materialls sould .

‌ The Secretary produced the Report of the three —\
Physitian D^r^: Croon , D^r^: Needham , & D^r^: King , ~\
concerning the Anatomical controversies between D^r^: ~\
Swammerdan & D^r^: Graef, heretofore referr’d to the ~\
Judgement of the Society, and he desired to know, whether\
the said Report should be dispatched away to the persons\
concern’d or deferr’d till the Society meet againe .

‌ It was thought fitt , to send it away by the first\
Post , considering it had been very long deferr’d already .

‌ Numb: 97 . of the Phı~~l~~. Transactions was lycensed\
by the Council . /

\TranscriptionStop


**(CMO/1 p. 215):** “Amanuensis comanded concerning the Legacy bestowed upon the Society.” (CMO/1)

**(CMO/1 p. 216,225):** “[Amanuensis comanded concerning the Legacy bestowed upon the Society.] Order concerning the same.” (CMO/1)


### 22 October 1673 (CMO/1 pp. 217–219;  CMO/1/188) probably Arundel House

\TranscriptionStart

[*centered:*] Octob^r^: 22. 1673.

[…]

‌ The Council considering the necessity of securuing the —\
weekly payments for carrying on the work of the Society\
and hauing consulted the Treasurers Book concerning\
the persons that may be look’d upon as good Paymasters, they\
were found to be these,

[*1–57 in three columns:*]

1 . L^d^. Brouncker Presed^t^\
2 . Earl Marshal\
3 . Earl of Anglesey\
4 . Earl of Devonshire\
5 .Earl of Alisbury\
6 . Lo^d^. Vise^o^: Stafford\
7 . L^d^. B^r^. of Salisbury\
8 L^d^. B^p^. of Chester\
9 . L^d^. Berkley\
10. L^d^. Brereton\
11 L^d^. How^d^. of Castlerising\
12 M^r^. Boyle\
13 M^r^ Arskin\
14 D^r^. Arderne\
15 M^r^. Ashmole\
16. S^r^: Jn^o^. Banks . [*page 218:*]\
17. M^r^ Barrington\
18 M^r^. Brook.\
[…]\
57 M^r^. Wylde .

[…]

\TranscriptionStop

**(CMO/1 p. 217):** “List of good Paymasters concerning Weekly pam^ts^:” (CMO/1)

**(CMO/1 p. 217: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 217: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 218):** “Another List to be asked for Paym^ts^:” (CMO/1)


### 30 October 1673 (CMO/1 pp. 219–220;  CMO/1/189) probably Arundel House

\TranscriptionStart

[**Page 219:**]

[…]

[*centered:*] Octob^r^: 30^th^. 1673

[…]

‌ My Lord Marshall Reported that my L^d^. Yarmouth\
had declared to him he would clear his Arrears to the Society\
and for M^r^: Povey, he had desired that his Account with\
the Society , for whose Service in the busineſs of the Savoy\
he had expended about 20^~~li~~^. might be Stated, and that being\
done he was willing to pay what ſhould be due from him —\
aboue that Summe .[*flourish over period*]

‌ Hereupon the Secretary was Ordered to Search in\
the Council Book, what was formerly Ordered by the\
Council in this matter.

‌ Concerning M^r^. Ryeant My Lord Marshall offered,\
he would write to him to Smyrna about his Arrears, and he\
doubted not but they would be satisfied .

[…]

[**Page 220:**]

[…]

‌ It was also Ordered that the Fellowes that\
are in Arrears Shall be allowed , if they make no p^~~r~~^snt\
Payments of them to give a Bond to the Society, to\
pay within or at the end of . 6.  monthes. and that M^r^\
Hoskins draw up aform for such a Bond

‌ Memorandum That when the number of the —\
Fellows of this Sociiety thare are, or are to be, shall\
be agreed upon , the forme of a legall Tye be presente^d^\
to every one of them to Secure the weekly contribuions .

‌ It was mencōnd that as long as wee know not\
how many will remaine of those that are now of the\
Society , wee cannot fixe the numb^r^: of the ordinary\
Fellowes .

\TranscriptionStop


**(CMO/1 p. 219,220):** “Secretary Ordered to search in the Council Booke concerning the Savoy.” (CMO/1)

**(CMO/1 p. 219):** “Lord Marshalls Report & offering concerning Arrears.” (CMO/1)


### 6 November 1673 (CMO/1 pp. 220–223;  CMO/1/190) probably Arundel House

\TranscriptionStart

[*centered:*] Novb^r^. 6. 1673.

[…]

‌ The Secretary Reported to the Council what he had\
found upon the Council Book concerning M^r^. Povey's ~\
Busineſs, spoken of at the last meeting viz^t^. that May\
18^th^: 1664  There was an Order made that the Treaẽr\
of the Society should Reimburse M^r^: Povey of the —[**page 221:**]\
Expenses he then alledged to haue been at for the —\
Societys Service

‌ Whereupon it was Ordered That M^r^: Colwall\
ſhould be desired to State M^r^: Povey Arrears due to\
the Society and ſubduct from the ſumme by\
him demanded. and receive from him the Remainder\
of his Arrears .

‌ Ordered also That the Treaer̃ of the R. Socyety\
M^r^: Colwall doe ſend to ſuch Fellowes of the Society\
as are in Arrears and acquaint them with the new Regulacõn\
the Council is now makeing for a firme Establishment\
of the ſaid Society , And that therefore the Arrears\
due to the said Society are to be forthwith collected\
and also a legall Obligacon to be Subscribed by as many\
as shall desire to continue Fellowes , for the better\
Secureing the weekly Contribucõns for the future.\
And that therefore every Fellow being in Arrears\
ſhall be desired by the Treaer aforesaid either to ſend\
in between this and S^t^. Andrews day next ( being Novb^r^:\
30. 1673) all his Arrears , or at least to give a ſufficient\
Bond to pay the same within or at the end of . 6. monthes\
from the date of this Order and to declare withall ~\
whether he will continue a member of the Society —\
and comply with the aforesaid Subscripcõn

‌ After this my Lo^d^. Marhsall was by the Council\
acquainted with their thoughts of removing their\
weekly Aſsemblyes to Gresham Colledge, and of ~\
beginning to meet there againe upon the next[*over:* ‘nect’] anniversary\
Election-day; the Council being moved thereunto by ~\
considering the conveniency of makeing their Experim^ts^:\
in the place where their Curator dwells, and the apparatus\
is at hand, as also by the Solemne invitacon of the Citty\
of London & the Professors of Gresham Colledge; and [**page 222:**]\
likewise from the hopes (they find ground to entertaine) of\
meeting with some considerable Benefactors at that end\
of the Citty To w^ch^: was added that though the Society -\
should thus remove their Meetings: yet they were full\
of hopes that his Lo^p^: would be so farr from removeing\
his favours & kindneſses from them that he would ~\
preserve them in the same Degree, he had done all\
along, and especially dureing the many years he hath\
Entertain’d them under his roof , To all w^ch^: the Councils\
added this humble Request , that my Lord Marshall\
would be pleas’d to give the Council leaue, ‘still to meet —\
upon occasion in his Lo^ps^: house, there to enjoy the honor\
& advantage of his Council and directions , w^ch^: they had —\
alwayes found so affectionate & considerable to them

‌ Whereupon my Lord Marshall very obligeingly —\
and generously declared , That though he alwayes had\
esteemed and still did esteeme it a great honor to his —\
house , that the R. Society kept their Assemblies there,\
yet ~ understanding that the Council apprehended it really\
to be for the Service & good of the Society, to returne\
to Gresham Colledge, he could not but give up his Reason\
to the reason of the Council; adding further that he should\
continue the same respect and concerne for the Society —\
whereuer they mett , and be glad to receive the Council\
in his house upon any occasion of their meeting

‌ Which Declaracon of his Lo^p^: was soe deeply resented\
by the Council that they unaimously desired the L^d^. B^p^: of\
Salisbury in their name to give my Lord Marshall their\
very humble & hearty thanks for his extraordinary favour\
& bounty towards the Society , in receiving them so frankly\
& generously into his house , when upon the Sad Calamity\
of the Fire of London they were destitute of a place\
of Meeting , as also in entertaining them afterwards for\
so mayn years together with all the nobleneſs imagineable [**page 223:**]\
Superadding to all that his great Munificense of giveing\
them the Arundelian library, and heaping many other\
reall expreſsions of generosity upon them .  To which\
the Council added this further Order, that my Lord\
B^p^: of Salisbury should be desired to acquaint the Society\
it Selfe now ready to meet, with this whole matter, that\
they concurring with the Council in this affaire might\
present themselues in a body to my Lord Marshall, &\
make the like acknowledgements with the Council: which\
was done accordingly, as will appeare by the entry of\
the Societys Journall-book upon this very day .

\TranscriptionStop


**(CMO/1 p. 220):** “Desire to write out the List of 79 Fellowes w^ch^: do not pay.” (CMO/1)

**(CMO/1 p. 220,225):** “Form of a legal Tye for payments to be presented.” (CMO/1)

**(CMO/1 p. 220: 219,220):** “Secretary Ordered to search in the Council Booke concerning the Savoy.” (CMO/1)

**(CMO/1 , v. 1 p. 220:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)


### 13 November 1673 (CMO/1 pp. 223–225;  CMO/1/191) probably Arundel House

\TranscriptionStart

[*centered:*] Novƀ^r^: 13^th^. 1673 .

[…]

‌ Ordered, that the persons following or three of them\
whereof the President and Secretary Oldenburgh are to be\
two be a Committee for Auditing the Treasury Accounts\
and that they doe meet for that purpose on Munday next\
being the . 17^th^. of this moneth at the Presidents house\
about . 3 . aclock.

[*centered:*] Those persons being

‌   The President     M^r^. Hill\
‌   S^r^: Jn^o^. Lowther   M^r^: Oldenburg\
‌   D^r^. Goddard - - - - - -

[**Page 224:**]

Ordered also, That the following Bond be shewen[*over:* ‘shew’d’] by\
the Amanuensis to M^r^. le Hunt , and upon alteracōn\
if hee See cause Some copies be forthwith made and\
presented or Sent about by the Treasurer, to be signed\
by those that doe not presently pay their Arrears in\
ready money,  And that such of the Nobility as shall\
please to make use of this Bond, yet so as to Order their\
Stewards to Signe in their stead, shall be complyed\
with in this ⅌ticuler

[*centered:*] The Bond being this

‌ Noverint universi ⅌ p^r^ſentes me\
teneri et firmiter obligari Præsidi Concilio et Sodalibus\
Regalis Societatis Londini pro Scientia naturali-\
promovenda , et Successoribus suis in\
libris bonæ et legalis monetæ Angliæ Solvend : eidem —\
Præsidi Concilio et Sodalibus , aut Successoribus suis ~\
aut corum certo Attornato ; Ad quan quidem Solutionem\
bene et fideliter faciend Obligo me Harendes Executores\
et Admimistratores meos firmiter ⅌ p^r^sentes, Sigllo meo\
Sigillat .  Dat .\
Anno Regni Domi nr̃i Caroli Secundi Dei Gratia Angliæ\
Seotiæ, Franciæ et Hibernice Regis Fidei Defensor\
etc vigesimo tertio Anno Dini 1673 .

‌ The Condieon of this Obligacõn is such That if the\
aboue bounden          his Heirs Executo^rs^.\
Administrators or Aſsignes do well & truely pay or cause\
to be paid unto the aboue named President Council and\
Fellows of the R. Society or their Successors or ~\
Aſsignee or Aſsignees the full Summe of\
‌        of lawfull moeny of England , it being\
his Arrears due to the President Council & Fellowes\
of the R: Socioety aforesaid, according to his engagem^t^.\
upon his Admiſsion into the said Society on the [**page 225:**]\
‌       day of       next ensueing the date\
Fellows and their ſucceſsors, then this Obligacõn to\
be voyd or else to remaine in full force & vertue.

[*two lines partially marginal:*] Seal’d & delivered\
in the p^r^sence of

‌ Resolued that for want of time the execution of\
that part of the Order made at the late Council of\
November  . 6^th^: w^ch^: Relates to the legall tye to be —\
Subscribed for secureing the weekly Contributions for\
the future, be differred till after S^t^: Andrews day.

\TranscriptionStop


**(CMO/1 p. 223: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 223: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 224):** “[Fellows allowed to give a bond for payments] Forme of the same.” (CMO/1)

**(CMO/1 , v. 1 p. 224:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)


### 27 November 1673 (CMO/1 p. 225;  CMO/1/192) probably Arundel House

\TranscriptionStart

[*centered:*] Novemb^r^: 27^th^. 1673.

[…]

‌ The following Receipt for the Foure hundred pounds\
bequeathed by the late Lo^d^. B^p^: of Chester to the R. Society\
was Signed & Sealed in Council; and it was Ordered that\
this Receipt ſhould be delivered to the Dean of\
Canterbury D^r^: Tillotson and he M^r^: Colwall keep\
the money in his custody till further Order .

‌ The Committee of the Council for Auditing the\
Treasurers Acco^ts^: made their Report as followeth

[**Page 226:**]

[*blank page*]

\TranscriptionStop


**(CMO/1 p. 225: 220,225):** “Form of a legal Tye for payments to be presented.” (CMO/1)

**(CMO/1 p. 225: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)


### 22 December 1673 (CMO/1 pp. 227–229;  CMO/1/193) Arundel House

\TranscriptionStart

[**Page 227:**]

[*centered:*] Decemƀ^r^: 22^th^: 1673

[…]

‌ The Council takeing againe into serious consideracõn\
the necessity of Collecting the Arrears due to the R:\
Society,  voted , that the following Order should be\
delivered to such Members of the said Society as\
are in Arrears viz^t^.

‌ In pursuance of a former Order of this Council\
made the 6^th^. of Novemb^r^: 1673. It was this day ~\
Ordered , That the Earle of Dorset , the Earle of ~\
Alisbury, the Lo^d^ Visco: Stafford , S^r^ Jn^o^. Lowther\
S^r^: Wiꝉꝉi: Petty S^r^: Peter Wyche, S^r^: Cyril Wyche\
S^r^. Theodore de Vaux. S^r^: Christopher Wren , the\
Treasurer & the Secretarys of the R : Society, and\
M^r^. Hook, or any four or more of them be desired to\
apply themselues to such Members of the said Society\
ase are in Arrears, and to acquaint them, that this Council\
being now upon making a new Regulacõn for a firme\
Establishm^t^: of the said Society, in Order whereunto\
the Arrears due to the Same are to be forthwith\
collected, Every Fellow being thus in Arreare, is desired\
forthwith to pay to the Treasurer M^r^: Colwal, or his\
Deputy , su much thereof as was due at Michaelmas\
last , or at least to give a sufficient Bond for the\
doing thereof within the Space of six monthes from the\
date of the aforesaid Order .

[**Page 228:**]

‌ It was likewise then declared that such of the —\
Fellows as shall neither make present payment of theır\
Respective Arrears nor give Bond as aforesaid shall\
be accounted to haue deserted the Said Society, and be\
proceeded against according to the Statutes ; and that\
the Society will proceed to a legall Recovery of the\
said Arrears.

[*centered:*] This Order was signed by

‌  Brouncker P.RS   S^r^. W^m^: Petty\
‌  Seth Sarum      Daniel Colwall\
‌  S^r^Q Jn^o^Q Lowther    Aƀr Hill\
‌  S^r^Q Paul Neile     Hen.̃ Oldenburg

‌   D^r^: Needham went away before Signeing

‌ Ordered also , That copies be made of the Statute\
for Election , and of that for payment , As also of the\
Obligacõn Subscribed by every Fellow at his Admiſsion ;\
and that the Comm^Es^; named in the former Order, be ~\
furnisht with such copies, to be shewne uppon occasion , —\
to those, whome they shall make application to .

‌ Ordered likewise, that the said Comm^Es^: be desired\
to apply themselues by Letters to those that are in\
the Country, and others that they cannot haue an easıe\
acceſse unto

‌ Resolved That if any Fellow shall be ejected\
on this occasion , the cause of the Ejection shall be\
recorded.

‌ Ordered That M^r^: Boyle, S^r^: W^m^: Petty, S^r^.\
Christopher Wren , D^r^: Goddard, D^r^: Grew. & M^r^: Hook\
be desired to draw up a List of considerable Experim^ts^:\
to be tryed before the R: Society, and to prepare an —\
Apparatus  necessary for the exhibition of them upon all\
occasions —

‌ S^r^. W^m^. Petty was desired to take a ⅌ticular care of seeing\
this import of this Order put into effect .

[**Page 229:**]

‌ There being some Members of the Society, the time\
of whose admission did not appear upon the Journal, as\
the Earle of Anglesey, S^r^. J. Birkinhead, S^r^: Ro: Harley\
and M^r^. Thomas Harley, it was Ordered, that the —\
printed yearly Lists of the Society should be perused\
to see what year those Members were printed the first\
time, & from thence a measure taken of Stateing their\
Accompts.

‌ After this It was thought convenient, that as —\
many of this Council as conveniently could, should meet\
in some other place, to avoid disturbing the E: Marshall\
too long , and there make a distinction in Seuerall Columnes\
of the Fellowes of the Society, according as they ~\
constantly pay or not pay, and of such as are Honorary\
Absent , Excused , Doubtfull . 

‌ This was done accordingly by S^r^: Jn^o^. Lowther. S^r^.\
W^m^: Petty . M^r^: Colwall, M^r^: Hill, M^r^: Hook, Oldenburg\
who haueing found . 53 . Fellowes that pay well, and\
79 . that do not , and . 14. absent in the Country; resolued\
to apply themselues with all possible ſpeed to the 79 ;\
and in Order to it , desired that the copies of the aboue\
mencōned Statutes might be speedily made, and the\
said List of the . 79 . fairely written out , together\
with the Arrears due by every one of those ; as alsoe\
that a Letter might be drawne up by . H. O. to be\
ſent by the Comm^Es^: to the absent with all possible\
Safety , acquainting them with the Councils Order\
of Collecting the Arrears , and desireing those that\
are concern’d of giveing in their positive answer by\
the next Post to the said Secretary.

\TranscriptionStop

**(CMO/1 p. 228,229):** “Order to make Coppies of the Statute for Election.” (CMO/1)

**(CMO/1 p. 228):** “[Order to make Coppies of the Statute for Election.] and that of payments.” (CMO/1)

**(CMO/1 p. 228):** “Order of making Copies of the same Obligation [to be subscribed]” (CMO/1)

**(CMO/1 p. 228):** “Order of making a List of Experiments.” (CMO/1)

**(CMO/1 p. 229: 228,229):** “Order to make Coppies of the Statute for Election.” (CMO/1)

**(CMO/1 p. 229):** “Order to make a Catalogue for all Accompts.” (CMO/1)

**(CMO/1 p. 229):** “Yearly printed List of the Society to be perused.” (CMO/1)

**(CMO/1 p. 229):** “Order to distinguish the Fellows in Seuerall columnes.” (CMO/1)


### 5 January 167$\frac{3}{4}$ (CMO/1 pp. 230;  CMO/1/194) probably Gresham College

\TranscriptionStart

[**Page 230:**]

[*centered:*] January . 5^th^. 167$\frac{3}{4}$

[…]

‌ Ordered, That the Treaẽr pay into the Mercers .\
Company of London £. 400 . now in the Chest of the —\
Society , takeing their Bond for Repayment with ſuch.\
Interest as he can get for the Same, not leſs than . 5.\
⅌ Cent , and that the Bond soe taken be deposited —\
in the Said Chest

‌ Ordered That the Clerk give Lists of the persons\
in Arrears to the Seuerall persons Comm^rs^: that are to —\
demand the Arrears

‌ Ordered, That the Treãer give Deputaeõns to —\
each of the Said Comm^Eſ^: to receive any Summe in Arrear\
and to give a Sufficient discharge for the Same — 

[…]

\TranscriptionStop


### 12 February 167$\frac{3}{4}$ (CMO/1 pp. 230–231;  CMO/1/195) probably Gresham College

\TranscriptionStart

[*centered:*] Feƀr . 12^th .^. 167$\frac{3}{4}$

[…]


‌ Ordered That a Discourse of M^r^: Roƀt. Hook about the\
Starrs be printed by Jn^o^. Martin Printer of the R. Society . [Possibly either H2613 or H2611]

[**Page 231:**]

‌ M^r^: Colwall & M^r^: Hill acquainted the Council\
that those of the Mercers Company had declared, they\
had at present no occasion of taking any money, but the\
first they had they would take the. £. 400 . of the Societey\
and pay use for it

‌ Hereupon it was Ordered that the Members of\
this Council should be desired to enquire about a Speedy &\
safe way of disposing of the £. 400 . to the best advantage\
and to report to the Council concerning it at the next\
meeting thereof

‌ M^r^: Colwal produced a noate from D^r^: Jasper\
Needham to himselfe, importing that himself desired —\
henceforth to be excused from his Obligacōn to the Society

‌ This note was left With the Secretary.

\TranscriptionStop

**(CMO/1 p. 230):** “Order to give a List of those that are in Arrears to the same [Committee concerning Arrears]” (CMO/1)

**(CMO/1 p. 230):** “M^r^: Hooks Book about the Starrs licenced.” (CMO/1)


### 26 February 167$\frac{3}{4}$ (CMO/1 pp. 231–232;  CMO/1/196) probably Gresham College

\TranscriptionStart

[*centered:*] February. 26: 16$\frac{73}{4}$

[…]

[**Page 232:**]

[…]

‌ It was Ordered that the Council be Summoned againe\
to meet to morrow in the afternoone at . 4 a clock in\
Arundele house further to advise with my Lo^d^: Marshall\
about Solliciting the Arrears of the Lords that are\
Members of the Society ; as alsoe to consider about —\
putting into better Order & use the Library, w^ch^: his\
Lo^p^: hath been pleased to bestow upon the Society .

\TranscriptionStop


**(CMO/1 p. 231):** “Report of hauing recived Arrears” (CMO/1)

**(CMO/1 p. 231: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 231, v.2 p.25,47,108):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Appointed V.P.” (CMO/8A)


### 27 February 167$\frac{3}{4}$ (CMO/1 pp. 232–233;  CMO/1/197) Arundel House

\TranscriptionStart

[*centered:*] February. 27. 167$\frac{3}{4}$

[…]

‌ Ordered That M^r^. Bernhard Astronomy Professor\
at Oxford haueing desired by M^r^: Hook the Loane of\
a Diogenes Laertius and of a coptic Psalter out of\
the Library bestowed upon the R . Society by the Earle\
Marshall, be accommodated[*over:* ‘accod’] with the said Book for the\
Space of a month , he giveing a Bond to the said Society [**page 233:**]\
of an hundred pounds to restore those books at the end of\
the said time to be accounted from the date of this Order.

‌ Ordered also That M^r^. Hook take care of haueing\
the Catalogue of the Arundel Library compleated within\
a moneth, and to haue a Duplicate made thereof .

‌ My Lord Marshall was pleas’d to take a list of some\
of the  Noble men of the Society that are deep in Arrears\
as the D. of Buckingham, Marq^ſ^. of Dorchester, Earle\
of Dorsett , E : of Northampton, E. of Peterbrough\
E : of Carlisle , Lo^d^. Yarmouth , Lo^d^. Cavendish, M^r^.\
Edward Howard .

‌ The Same did upon occasion name M^r^. Thomson\
& M^r^. Nelthrop as very good men to put the £. 400 —\
Legacy to upon use. at . 6 ⅌ 100 . [possibly of Thompson and Company banking venture]

\TranscriptionStop


**(CMO/1 p. 232: 197, 232):** “Order concerning the Loan of the books of the Library” (CMO/1)

**(CMO/1 p. 232):** “Order concerning my Lord Marshal. Library.” (CMO/1)

**(CMO/1, v. 1 p. 232):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A) “[Arundel Library.] No M.S.S. lent, under a Bond of £100”


### 12 March 167$\frac{3}{4}$ (CMO/1 pp. 233–234;  CMO/1/198) probably Gresham College

\TranscriptionStart

[*centered:*] March: 12^th^. 167$\frac{3}{4}$

[…]

‌ There was read a Letter written by the E: Marshall\
to M^r^: Oldenburg, wherein his Lo^p^: proposeth a way of\
well disposing the . £ . 400 . Legacy to some considerable\
Cittizens , where his Lo^ps^: freinds haue Lodged considerable\
Sumē

‌ The Council haueing debated this matter, thought\
good to referr the Determinacōn thereof to another —\
meeting of the Council ordering in the meane time, that\
my Lord Marshall  should haue given him the hearty thanks [**page 234:**]\
of the Council for the favour of his care for the —\
concerne of the Society .

‌ M^r^: Hook was againe urged to compleate the\
Catalogue of the Arundelian Library .

\TranscriptionStop

**(CMO/1 p. 233: 120,129,161,162,165,167,176,178,233):** “A Committee Ordered for making a Catalogue of the Library” (CMO/1)

**(CMO/1 p. 233):** “A List taken of some Noblemen for Arrears.” (CMO/1)


### 18 June 1674 (CMO/1 p. 234;  CMO/1/199) probably Gresham College

\TranscriptionStart

[*centered:*] June. 18^th .^. 1674.

[…]

\TranscriptionStop


### 27 August 1674 (CMO/1 pp. 234–235;  CMO/1/200) probably Gresham College

\TranscriptionStart

[*centered:*] Aug^st^: 27. 1674

[…]

\TranscriptionStop

**(CMO/1 , v. 1 p. 234):** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

### 29 September 1674 (CMO/1 p. 235;  CMO/1/201) probably Gresham College

\TranscriptionStart

[**Page 235:**]

[…]

[*centered:*] Septƀ^r^: 29^th^. 1674

[…]

‌ Ordered , That there should be prepared a forme\
of a Legall subscripcōn for paying . 52. Shillings\
a yeare

[…]

\TranscriptionStop

### 7 September 1674 (CMO/1 p. 236;  CMO/1/202) probably Gresham College


\TranscriptionStart

[**Page 236:**]

[*margin:* ‘Octob .’]

[*centered:*] [Sept .]{.underline} 7^th^. 1674 .

[…]

‌ Ordered That such of the Fellowes as [*canceled:* ‘~~the~~’] loue\
the welfare of the Society shall be desired to oblige\
themselues to entertaine the Society, either ⅌ se or\
⅌ alios , once a year at least with a Philosophical\
discourse grounded upon Experiments made or to be ~\
made, and incase of failure to forfeit . 5^~~li~~^: And that\
S^r^: W^m^. Petty be likewise desired to draw up a forme\
for Such an Obligacon as may bind in Law

[…]

\TranscriptionStop

### 15 October 1674 (CMO/1 pp. 236–238;  CMO/1/203) probably Gresham College

\TranscriptionStart

[*centered:*] Octob^r^: 15. 1674

[…]

‌ The two draughts of the Declaration for restoring the Society [**page 237:**]\
brought in by S^r^: W^m^: Petty & D^r^: Goddard, were read, and\
the Substance of both reduced into one paper: which the —\
Amanuensis was Ordered to transcribe faire for further\
consideracōn at the next Meeting of the Council

‌ It being represented, that the ⅌mitting of such‸^as^ are not\
of the Society to be present at the meetings thereof, is\
both troublesome & prejudiciall to the same, it was voted,\
that the Repeal of that Statute, w^ch^: allows such an —\
admission, w^ch^: is the. 2^d^. of the. 4^th^. Chapter, containing the\
Statutes about the ordinary Meetings of the Society, shall\
be propounded at the next meeting of this Council .

‌ It being likewise represented, that the liberty of ~\
divulging what is brought into the Meetings of the Society,\
is also very prejudiciall to the same, and doth render divers\
of the Members thereof very Shy in presenting to them —\
what they haue discovered, invented, or contrived, it was —\
moved that a forme of a Statute might be prepared enjoyning\
Secrecy to the Members of the Society in such matters, as\
Shall be brought in , and by the President or Vice President\
declared to be kept secret at the Communicators desire.

[*centered:*] A Forme to this end was propos’d as follow’s

‌ Every Fellow of the R. Society shall make a Solemne\
promise before the same, not to discover directly or indirectly\
to any person, not being of the Society, Such Observations,\
Experim^ts^, or other Communications, as ſhall be brought —\
into the Meetings of the same, and thereby the Presid^t^:\
or one of the Vice Presidents declared to be kept Secret\
at the desire of the Communicator

‌ The time of the Summons of the Society, for —\
returning to their weekly Meetings being Spoaken off, it\
was resolved, that since the. 29^th^: of October. and the. 5^th^. of\
Novembr^r^. falling both upon a thursday, w^ch^: is the Societys\
Meeting day, would prooue inconvenient for their meetings [**page 238:**]\
the said Summons ſhould be made for the . 12^th^. of Novemb^r^:\
next ; and to the end, that the Fellows might haue ~\
Some notice of what late hath been considered and\
done by the Council in order to put life into those —\
Meetings , it was concluded upon, that the forme of\
those Summons ſhould be as followeth, viz^t^.

‌ ‌These are to give notice that the R Society ~\
intends to returne to their publick Meetings on ~\
Thursday the . 12 . of November instant 1674 . in Gresham\
Colledge , at 3. of the clock , at w^ch^: time the Company\
will be entertained with an Experimentall Exercize\
by their President the Lo^d^. Viscount Brouncker, or\
by D^r^. Wallis : The like will be ⅌formed the next —\
Meeting day , being the . 19^th^. of November, by the ~\
Hono^ble^: Robt Boyle ; and the . 26^th^. of the Same month\
by S^r^. W^m^: Petty , or in the absence of any of them , by\
M^r^: Robert Hook, their Curator by Office, in Order to\
a vigorous prosecution of the ends of their Institucōn\
Touching w^ch^: the Intentions of the Council of the said\
Society will be further declared on the day of their\
Anniversary Election , being the. 30^th^: of this instant\
November

[…]

\TranscriptionStop


**(CMO/1 p. 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 236):** “Desire to draw up a forme of the said Obligacon [Experimental Discourses to be had at Meeting]” (CMO/1)

**(CMO/1 p. 237: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 237):** “Desire for making a Statute enjoyning Secrecy. \| Form of the same.” (CMO/1)

**(CMO/1 p. 237: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 237: 14,15,237,239):** “A Draught to be brought in for a Statute concerning the Laws for Strangers in the Meetings.” (CMO/1)

**(CMO/1 p. 237):** “Secrecy to be observed. \| Form proposed” (CMO/8A)


### 19 October 1674 (CMO/1 pp. 238–239;  CMO/1/204) probably Gresham College

\TranscriptionStart

[*centered:*] Octob: 19 . 1674 .

[…]

‌ The Busineſs of engaging the Members of this Society\
to enter into a legall Obligacōn, in reference to their weekly\
payments; and the Declaracon drawne up by S^r^: W^m^: Petty\
and D^r^: Goddard and the Form also for Summoning\
the Society to returne to their weekly Meetings, being ~ [**page 239:**]\
againe considered of , as the maine things to be determined\
with all possible Speed , it was Ordered , first that S^r^\
Robert Southwell should be desired to apply himselfe\
to M^r^: Attorney Generall; and to desire his advice in\
drawing up such a forme , as might be binding in law .

‌ Secondly That the Declaracōn & the forbme focr Summons\
be likewise read againe & considered of at the next Council ,

[…]

‌ Voted that the 2^d^. Statute of the : 4^th^. Chapter of\
the Book of Statutes of the R: Society, be repealed, and\
it was repealed and made voyd accordingly .

\TranscriptionStop


**(CMO/1 p. 238,239,240,243):** “[Society to be ſummoned to Meet] Forme of ſummons for the same.” (CMO/1)

**(CMO/1 p. 239: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 239: 238,239,240,243):** “[Society to be ſummoned to Meet] Forme of ſummons for the same.” (CMO/1)

**(CMO/1 p. 239: 14,15,237,239):** “A Draught to be brought in for a Statute concerning the Laws for Strangers in the Meetings.” (CMO/1)


### 30 October 1674 (CMO/1 pp. 240–241;  CMO/1/205) probably Gresham College

\TranscriptionStart

[**Page 240:**]

[*centered:*] Octob^r^: 20^th^: 1674

[…]

‌ The forme of the Summons for returning to the\
weekly Meetings being read againe, it was thougt fitt\
to omit the names of the persons, that are to entertaine\
the Society , and to lett it runn as followes .

‌ These are to give notice that the R : Society intends\
to returne to their publick Meetings on Thursday being\
the. 12^th^. of this instant Novemb^r^: 1674. in Gresham\
Colledge, at . 3 . of the clock , at w^ch^: time and the following\
dayes of their Meetings the Company will be entertain’d\
with experimentall exercises to be ⅌formed by Seuerall\
Eminent members of the same, in order to a more vigorous\
prosecucōn of the ends of their institution : Touching w^ch^:\
the Intencōns of the Council of the said Society will\
be further declared on the day of theri Anniversary\
Election, being the. 30^th^. of this instant November.

‌ This was ordered to be forthwith committed\
to the Preſse .

‌ The forme for a new Subscription , drawn up\
by M^r^: Attorney Generall, was read , and approved of ,\
and Ordered that S^r^: R. Southwell be thanked\
for his care in procureing it , and withall desired to\
acquaint M^r^: Attorney Generall with the acknowledge
⹀ments of the Council for this his favour.

‌ The Declaracōn formerly drawne up by S^r^: W^m^: Petty\
& D^r^: Goddard was read againe: whereupon it was ~ ~\
mencon’d , that S^r^: R : Southwell had taken a coppie of\
it , in Order to Shew it to my Lord Keeper.

‌ Mention was againe made of two Propositions; one\
made by S^r^. W^m^: Petty, relateing to the putting out of the [**page 241:**]\
£. 400 ; the other by S^r^: Jonas More concerning the letting\
out of Chelsey Colledge

‌ Both being well accepted of , it was thought necessary,\
that both the proposers should be desired to put their ~ ~\
respective propositions in writing, that so both y^e^ busineſs\
to w^ch^: they relate might be with the more certainty and\
vigour be put in execution

‌ A Memorandum for busineſs of the next Meeting;

‌  1. About printing the forme of new Subscription

‌  2. Sending out one to call in money

‌  3. Renewing the Order for the Treãrs Deputacõn

‌  4. What to doe with the Declaracon

‌  5. To think on more Entertainers of the Society

\TranscriptionStop


**(CMO/1 p. 240: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 240: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 240: 238,239,240,243):** “[Society to be ſummoned to Meet] Forme of ſummons for the same.” (CMO/1)

**(CMO/1 p. 240,241,242,244):** “[Subscription of the Fellows] New Form of, for weekly payments” (CMO/8A)


### 9 November 1674 (CMO/1 pp. 241–244;  CMO/1/206) probably Gresham College

\TranscriptionStart

[*centered:*] Novƀ^r^: 9^th^. 1674

[…]

‌ The forme of the new Subscription was agreed upon\
‌    as followes :

‌ I A. B:                  do Grant & agree to\
and with the President Council & Fellow’s of the R. Society\
of London for improueing Natural Knowledge that,\
so long as I shall continue a Fellow of the said Society,\
I will pay to the Treasurer of the same for the time\
being , or to his Deputy, the Sum̃e of ffifty two shilꝲ\
⅌ annum , by foure equal quarterly payments, at the —\
foure usuall dayes of payment , that is to say, the Feast [**page 242:**]\
Feast of the Nativity of our Lord , the Feast of\
the Annunciation of the blessed Virgin Mary . the\
Feast of S^t^. Jn^o^. Baptist , and the Feast of S^t^\
Michael the ’Archangel , the First payment to be\
made upon the\
next ensueing the date of these presents , and I will pay\
in proportion , viz^t^ One Shilling ⅌ week, for any —\
leſser time , after any of the said dayes of payments,\
that I ſhall continue Fellow of the said Society,\
For the true payment whereof , I bind my self,\
and my heirs in the penal summe of Twenty poundſ\
In wittnesse whereof I haue hereunto put my hand &\
Seale this        day of          in the\
‌         year

[*two lines part marginal:*] Sealed & delivered\
in the presence of

‌ M^r^. Hooks Discourse concerning Animadversions\
upon Hovely his Machina Cælestis was licensed .

‌ The Transactions of N^o^. 107 . were licensed .

‌ S^r^: Roƀt Southwell redelivered to the Council the\
Declaracōn formerly drawne up for the new regulateing\
the Society , after he had read it to the Lord Keeper,\
who, hee said, did well approue thereof, and withall —\
expreſsed his readineſs of serving the Society, and ⅌ticularly\
in doing them good Offices about his Ma^tie^

‌ He was thanked by the Council both for this care\
and that other of procuring for them from M^r^: Atturney\
Generall that legall forme of Subscripcon aboue inserted ;\
and he was alsoe desired , in the name of the Council to —\
assure the Lord Keeper & M^r^: Attorney Generall -\
of the Councels deep sence they had of their favour to\
the Society and their regard to the welfare of the same.

‌ S^r^: W^m^: Petty proposed in writeing severall wayes of\
disposeing of [*canceled:* ‘~~the disposeing of~~’] the £. 400 Legacy: One\
was, The Inheritance of about . £ . 80 . ⅌ annu[*flourish*] , Rent\
in ground & Houses in Hog lane near ſong alley in —\
Morrefeilds, and about . £ . 8 ⅌ annu[*flourish*] at Erith in Kent;\
w^ch^: is now under Mortgage for. £ . 350 . with about . 15.^~~l~~^ Arrears\
of Interest . there is a Naime of a Dower on the p^r^miſses\
w^ch^: may be had under. £ . 150 .

‌ The other way is , A House. viz^t^. the Seaven starrs\
in Birchin Lane, in Lease to Richd Hutson, who\
paid . £ . 95 . fine & £ . 50 . ⅌ Annu[*flourish*]. of w^ch^: Lease. 19^$\frac{1}{2}$^\
years are yet unexpired . Which house is a Lease of\
38. year yet to come; out of w^ch^: a ground Rent of —\
£ . 14 . ⅌ annu[*flourish*] is to be paid

‌ M^r^: Hooke was desired against the next Meeting\
to view teh place in Hoy-Lane, whether the houses\
be in good Repaire , and likely to be tenanted .

‌ S^r^. W^m^. Petty was desired to Engage M^r^: Barrow\
the Picture drawer to collect the Arrears due to the R:\
Society , and that the Said S^r^: W^m^: & M^r^: Hoskins doe\
Joine in takeing good Security from the said M^r^: Barrow;\
and that being done direct him to M^r^: Dan^l^: Colwal to\
receive from him Such Instructions, as shall be requisite\
to render this busineſs effectuall , and ⅌ticularly to furnish\
him under his hand with power of Collecting the Arrears\
in his name , according to a former Order of the Council\
bearing date January. 5. 167$\frac{3}{4}$ , w^ch^: Order was this\
day renewed to the said M^r^: Colwall

[…]

‌ Memorandum that those intentions of the Council\
mencōnd in the Sumōns of the Society to returne to\
their Meetings , are cheifly, that there is a legal [**page 244:**]\
Subscription , & a declaraacõn , both Subscribed by the\
Council , and Some of the other Members of the —\
Society

‌ Ordered that . 250. Copies to forthwith —\
Printed of the new forme of Subscription , and\
that if poſsible, against Thursday next .

\TranscriptionStop


**(CMO/1 p. 241: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 241: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 241):** “A Memorandum for businesse of the next Meeting.” (CMO/1)

**(CMO/1 p. 241: 240,241,242,244):** “[Subscription of the Fellows] New Form of, for weekly payments” (CMO/8A)

**(CMO/1 p. 242: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 242: 240,241,242,244):** “[Subscription of the Fellows] New Form of, for weekly payments” (CMO/8A)

**(CMO/1 , v. 1 p. 242):** “[Hook M^r^. Robert.] His Discourse Licensed, on Hevelius’s Machina Cœlestis.” (CMO/8A)

**(CMO/1 p. 243: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 243: 238,239,240,243):** “[Society to be ſummoned to Meet] Forme of ſummons for the same.” (CMO/1)


### 12 November 1674 (CMO/1 pp. 244–245;  CMO/1/207) probably Arundel House

\TranscriptionStart

[*centered:*] Novƀer. 12 . 1674 .

[…]

‌ There was appointed a Comittee of the Council ~\
for Auditing the Accompts viz^t^. the President . ~\
the two Secretaryes D^r^: Goddard & M^r^: Hoskins

‌ It was left to them to meet at such time & place\
as should Seem most convenient to themselues .

‌ The new forme of a legall Subscription for the —\
weekly payments being againe Spoaken of, it was thought\
necessary to make a Statute concerning the same to this\
effect ;

‌ Every person , continuing , or to be hereafter ~\
admitted Fellow of this Society, Shall Signe , Seale, &\
as his Act & Deed deliver an Obligacōn in the ~\
following words , vizt

‌ I                 do grant and agree\
to & with the President , Council, & Fellows of the Roy^l^. [**page 245:**]\
Society, of London for improueing Natural knowledge.\
That so long as I shall continue a Fellow of the said\
Society , I will pay to the Treasurer of the said —\
Society for the time being , or to his Deputy, the Sum̃e\
of ffiftie two Shillings ⅌ annu[*flourish*]; by foure equal —\
quarterly payments , at the foure usuall dayes of\
Payment , that is to Say, the Feast of the Nativity\
of our Lond, the Annunciacōn of the bleſsed Virgin\
Mary, the Feast of S^t^: Jn^o^. Baptist , and the Feast\
of S^t^: Michael the Archangel ; the first payment to\
be made upon the\
next ensueing the date of these Presents ; and I will pay\
in proporcōn viz. One Shilling ⅌ week, for any lesser —\
time, after any the said dayes of payment, that I shall\
continue Fellow of the said Society.  For the true\
payment wehreof I bind my Self & my Heirs in the\
penal Sumẽ of Twenty pounds . In wittnesse whereof\
I haue hereunto Set my hand & Seal this\
day of\

[*two lines slightly marginal:*] Sealed & delivered\
in the presence of

‌ It being put to the Vote by the President , whether\
this Draught now agreed upon by the Council ſhould be\
read at another Meeting : It was voted in the affirma⹀\
tive nemine contradictente .

[…]

\TranscriptionStop


**(CMO/1 p. 244: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 244,246,247):** “Form, & Copies, & Order of new Subscription” (CMO/1)

**(CMO/1 p. 244: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 244: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 244: 240,241,242,244):** “[Subscription of the Fellows] New Form of, for weekly payments” (CMO/8A)


### 19 November 1674 (CMO/1 pp. 246–247;  CMO/1/208) probably Gresham College

\TranscriptionStart

[**Page 246:**]

[*centered:*] Novƀr . 19^th^ : 1674 .

[…]

‌ The Declaracōn was read, and Ordered to be read\
againe on Monday next, for w^ch^: time a Council is to\
be Summoned againe

‌ It being put to the vote, whether the draught of the\
Bond, at the last Council voted to be read at another\
Meeting should passe into a Law, it was unaimously\
voted in the affirmative .

[…]

‌ D^r^: Plots designe of makeing a Survey of all England\
for compiling a History of Nature & Art , in reference to\
that Kingdome was well approu’d of , and the Secretary\
Ordered , to aſsist him in directing him to such of his — -\
Correspondents up and downe the Country, as may be likely\
to direct & Instruct him in this undertakeing .

[**Page 247:**]

‌ Ordered also , that the observacõns & Experiments\
registered be Sorted , and reduced to Seu^r^all Lectures , to\
be read at the Society upon occasion .

\TranscriptionStop


**(CMO/1 p. 246: 244,246,247):** “Form, & Copies, & Order of new Subscription” (CMO/1)

**(CMO/1 p. 246: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 246):** “Plott D^r^. Designs to survey all England.” (CMO/8A)


### 23 November 1674 (CMO/1 pp. 247–248;  CMO/1/209) probably Gresham College

\TranscriptionStart

[*centered:*] Novƀer. 23^th^ . 1674

[…]

‌ Ordered That the Declaracōn with the Forme\
of the new Bond annext to it , be put to the Presse.[*flourish*] so\
as haue ready a proof of it for the next Council .

‌ Ordered, that M^r^: Barlow be taken for a Collector\
of the Arrears , after he hath given a Bond of his owne\
of one hundred pounds for ſecurity; and that he be ~\
allowed twelve pence in the pounds.  And that this be —\
declared to him by the President in the name of the\
Council .

‌    W^ch^: was done accordingly

‌ Those that were at the Council Seald the new Bond,

[…]

\TranscriptionStop


**(CMO/1 p. 247: 236,237,239,240,241,244,247):** “[Order to advance a years Weekly Contribution.] Form of Obligacon to be drawne up to the same end.” (CMO/1)

**(CMO/1 p. 247: 244,246,247):** “Form, & Copies, & Order of new Subscription” (CMO/1)

**(CMO/1 p. 247):** “[Declaration ordered to be drawn up for restoring the Society —] Is to be printed.” (CMO/1)

**(CMO/1 p. 247):** “Order about the Observaions & Experiments registered.” (CMO/1)


### 26 November 1674 (CMO/1 p. 248;  CMO/1/210) probably Gresham College

\TranscriptionStart

[**Page 248:**]

[…]

[*centered:*] Novƀer . 26. 1674

[…]

‌ The Committee of the Council for Auditing the\
Accompts made their report w^ch^: was approved of .

[…]

‌ A proof of the Declaracõn being ready it was —\
Read againe and after some alteracōns , Ordered to be\
printed off, to the number of. 225. copies to be comitted\
to the custody of the President .

\TranscriptionStop


**(CMO/1 p. 248: 236,237,240,241,242,243,246,248):** “Declaration ordered to be drawn up for restoring the Society —” (CMO/1)

**(CMO/1 p. 248: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

**(CMO/1 p. 249):** “Order for a Catalogue to be made of all the Presents.” (CMO/1)

**(CMO/1 p. 249):** “Order to make a Catalogue of all Discourse Letters.” (CMO/1)

**(CMO/1 p. 249):** “Order for making a Catalogue for all Instruments.” (CMO/1)

**(CMO/1 p. 249):** “Order concerning Discourses entered into the Societys Register Book” (CMO/1)

**(CMO/1 p. 249: 88,159,161,249):** “[The Benefactors to the Society to be registred in loose ~ \| vellum ſheets.] Resolution & Order concerning the same.” (CMO/1)

**(CMO/1 , v. 1 p. 249):** “[Benefactors & Benefactions]  Lists, or Tables, of them ordered” (CMO/8A)  “[Catalogue,]Of Books, Discoveries, Letters, &c. to be made & preserved; and Mess^rs^. Aubrey & Collins to assist.”

**(CMO/1, v.1 p. 249):** “[Instruments.] Of the Society’s, to be collected & catalogued” (CMO/8A)

**(CMO/1 p. 242):** “M^r^: Hooks. Discourse concerning Animadversions upon Hevely Machinæ colesti Licensed.” (CMO/1)

**(CMO/1 p. 242: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 245,246,284):** “D^r^: Plots proposal to be menconed.” (CMO/1)

**(CMO/1 p. 246: 245,246,284):** “D^r^: Plots proposal to be menconed.” (CMO/1)

**(CMO/1 p. 249, 303):** “Presents to the Society \| Minutes” (CMO/8A)

### 3 December 1674 (CMO/1 pp. 248–249;  CMO/1/211) probably Gresham College

\TranscriptionStart

[*centered:*] Decƀr. 3^d^. 1674

[…]

[**Page 249:**]

[…]

‌ Ordered, that M^r^: Oldenburg be desired to offer the\
new legall Obligacōn for paying . 52 . Shilꝲ: a year for the\
use of the R Society, to as many Members of the Same\
to signe & Seale as conveniently he can; and do alsoe —\
shew them the Statute made by this Council to engage\
every Fellow of the said Society to such a Subscripcõn

‌ Ordered, that there be forthwith made a Catalogue\
of all the Presents made by Seuerall persons to this —\
Society , together with the Donors names ; and that\
Duplicates thereof be made , the one to be kept by the —\
keeper of the Repository , and the other by the Treasurer\
⅌ tempore.

‌ Ordered also that a Catalogue be made of all y^e^. Instrum^ts^\
or other Apparatis of the Society, paid for out of their publick\
Treasury , and that the Instruments be lookt out and kept\
together in the Repository for Instruments

‌ Ordered that a Table & Catalogue be made of all the —\
Books, Discourses Letters, and Acco^ts^: brought into the —\
Society, together with the Authors names : And that all\
the Said Bookes Discourses, Letters & Accompts be kept\
in convenient Presses under Locks & Keyes, and that the\
President & Secretaries pro tempore haue the keeping of\
the said Keyes

‌ Ordered further that M^r^: Aubrey & M^r^. Collins be\
desired to be aſsisting in this Business & to make proposalls at\
the next meeting of the Council, this day Sennight, what they\
will expect for their Aſsistance

‌ Memorand̃ That it was propounded by S^r^: W^m^: Petty\
that all the Discourses enter’d in the Societys Regist^r^: Books\
ſhould be divided into ſe^r^all Sections, & Chapters; and that this\
should be taken into consideracõn the next Meeting .

\TranscriptionStop

### 10 December 1674 (CMO/1 pp. 250;  CMO/1/212) probably Gresham College

\TranscriptionStart

[**Page 250:**]

[*centered:*] Decemb^r^: 10^th^ : 1674

[…]

‌ Ordered That the Society haueing desired S^r^\
W^m^: Petty to print his Discourse made before them\
the . 26. Noveƀ^r^: last , be printed by the Printer to\
the said Society, viz^t^: in forme foƚƚ [*absent*]

‌ The Council haueing formerly charged themselues\
to provide each of them an Experimental Discourse, for\
the Society at some one meeting within the year , ~\
Resolued that a Letter should be written by the —\
Secretary & Signed by the President , to the Fellows\
of the Society hereafter named, to desire them to —\
provide the like Discourses, and to name the day after\
the 14^th^: of January next , when to bring it in .

[*…list of Fellows*]

The Council nominated also those Persons to whom Applicacõn\
‌ ſhould be made for Signeing the new Bond viz^t^

[*…list of persons*]

\TranscriptionStop

**(CMO/1 , v. 1 p. 250:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)

**(CMO/1 p. 250):** “Printing. S^r^ W^m^. Petty’s discourse ordered” (CMO/8A)


### 17 December 1674 (CMO/1 pp. 251–252;  CMO/1/213) probably Gresham College

\TranscriptionStart

[**Page 251:**]

[*centered:*] Decemƀ^r^: 17^th^: 1674 .

[…]

‌ The President S^r^: R: Southwell & M^r^: Pepys were\
desired to make application to his Highnes Prince Rupert\
concerning the mischeif w^ch^: his Glassehouse does to Chelsey\
Colledge; and to Suggest to the Prince , that his Highnes\
may ⅌haps put it and the Land to some good uses, if he —\
pleases to take it to himself, and to consider the Society\
for it .

‌ S^r^ Jonas Moor was desired to write a Letter to the\
Prince [*over:* ‘Pri       ’] and to acquaint him that the House & Land of\
Chelsey] might haue been well disposed of for the benefit of\
the Society if it had not been for the annoyance of the\
Neighbouring Glassehouse

‌ The forme of the letter drawne up by the Secretary\
to write divers members of the Society to provide Discourses\
for the publick Meetings was reported by the said Secretary\
to haue been viewed & altered by the President , and by his\
Lo^p^: Ordered to be thus issued;

[**Page 252:**]

[*margin:*] S^r^:

‌ The Council of the R : Society considering with\
themselues the great importance of haueing the publick\
Meetings of the Said Society constantly provided with\
Entertainements Sutable to the designe of their —\
Institucõn haue thought fitt to undertake, to contribute\
each of them one ; not doubting but that many of the\
Fellows of the Society will joyn with them in carrying\
on such an undertaeking .  And being well ⅌suaded of\
yo^r^: approbacōn of this their purpose, so much tending\
to the reputacon & Support of the Society; they desire\
that you would be pleas’d to undertake for one, and to\
name any Thursday after the . 14^th^. of January next ,\
such as ſhall be most convenient for you, when you\
will present the Society at once of their Said publick\
Meetings by yo^r^: Self or some other of the Fellows. —\
for you with such a Discourse grounded upon or Leading\
to Philosophical Experim^ts^: on a Subject of yo^r^: owne —\
Choice. In doing of w^ch^: you will benefit the Society &\
Oblige\
‌      S^r^:\
‌          Yo^r^: humble Servant\
‌           Brouncker. P.R:S

[…]

\TranscriptionStop

**(CMO/1 p. 251):** “Form of Letter to that purpose. [Experimental Discourses to be had at Meeting]” (CMO/1)


### 14 January 167$\frac{4}{5}$ (CMO/1 pp. 253–254;  CMO/1/214)  probably Gresham College

\TranscriptionStart

[**Page 253:**]

[*centered:*] January. 14^th^ 167$\frac{4}{5}$

[…]

\TranscriptionStop

### 21 January 167$\frac{4}{5}$ (CMO/1 pp. 254–255;  CMO/1/215) probably Gresham College

\TranscriptionStart

[**Page 254:**]

[…]
[*centered:*] Jan^ry^: 21^th^. 167$\frac{4}{5}$

[…]

‌ A Report being made that about . 60. Fellows had\
Signed & Seald the new Obligacõn, w^ch^: is the number\
that was thought fitt to be assured of before the\
Sending about the printed Declaracõn ;) it was resolued [**page 255:**]\
upon, that a Letter should be drawne up by the ~\
Secretary against the next Council to accompany the\
Seuerall coppies of the said Declaracõn ; to be ſent\
to all Such as are not excepted by the Same, The Substance\
of that Letter should be to desire the respective Fellows\
to consider the Contents of that Declaracõn, and to ~\
returne an answer thereunto within such time as —\
Should be limited by the President, who is to signe both\
the letter & the Declaracon 

[…]

‌ It was Ordered that a Discourse made before the\
R : Society Decemb^r^: 10. 1674 . by D^r^: Nehemiah Grew\
concerning the nature, Causes & Power of Mixture ,\
be printed by the Printer of the R : Society .

\TranscriptionStop

### 28 January 167$\frac{4}{5}$ (CMO/1 pp. 255–257;  CMO/1/216) probably Gresham College

\TranscriptionStart

[*centered:*] Jan.̃ 28: 16$\frac{74}{5}$

[…]

[**Page 256:**]

[…]

‌ There was read a draught of a Letter, to be\
ſent to those that haue not Signed the new Bond\
whether in London or absent from it; together —\
with a coppie of the printed Declaracōn , The —\
time for an answer limited to those that are in —\
London , was the . 11^th^: of February next; the time —\
for the absent from a month from the date of the\
respective Letters to them .

‌ The Letter was agreed upon as followeth;

[*blank*]

‌ Ordered that the Amanuensis doe make faire\
coppies of this Letter first of all for the persons following

[*…three column list of 45 people*]

[**Page 257:**]

[…]

‌ M^r^. Oldenburg desired the Councils Lycense\
for the printing D^r^: Wallises Discourse, made before y^e^\
Society on the 12^th^. of Novemb^r^. 1674 .  Whereupon it was\

‌ Ordered that a Discourse made before the R Society\
the . 12^th^ . of Novemb^r^: 1674 . by D^r^. John Wallis, concerning\
Gravity and Gravitation grounded on Experimentall\
Observacōns be printed by the Printer of the said\
Society .

[*GB-uklors Tracts CI/1 in pamphlet volume with presentation inscription on license page verso:* ‘Presented to ye R . Society from the Author \| April 22^th^, 1675 .’]

[…]

\TranscriptionStop


**(CMO/1 , v. 1 p. 255):** “[Grew Nehemiah M.D.] Discourse of Mixture, to be printed” (CMO/8A)

**(CMO/1 p. 255):** “Order for D^r^. Nehemiah Grews Discourse to be printed.” (CMO/1)

**(CMO/1 , v. 1 p. 254:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)

**(CMO/1 p. 256):** “Letter sent to some concerning the same. \| and Copies made.” (CMO/1)

**(CMO/1 , v. 1 p. 256:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)


### 25 February 167$\frac{4}{5}$ (CMO/1 pp. 257–259;  CMO/1/217) probably Gresham College

\TranscriptionStart

[*centered:*] Febru[*flourish*] 25 . 16$\frac{74}{5}$

[…]

‌ The President inquireing what answers were come in\
to the printed Declaracōn.  M^r^: Hook delivered a Letter\
of M^r^. Oudard very civilly excusing & alledging Reasons [**page 258:**]\
for his late omiſsion , and promising complyance to\
the Import of the said Declaracōn, as ſoon as he\
ſhould be able .

[*… Oldenburg gives similar accounts*]

‌ At was Ordered that M^r^: Wicks & M^r^: Shortgrane\
ſhould carry Coppies of the printed Bond to as many in\
& about Towne, as had not yet Signed, and to desire\
their positive Resolucõn for Signing or not Signing

‌ M^r^. Hook read before the Council M^r^: Hoskins\
Letter of Jan : 26 : 1674 : importing that he had made\
the Conveyance from S^r^. Jn^o^. Banks , such as he judged\
Safe & Sufficient, of the Fee-farme Rents at —\
Lewes in [*word inserted:*] Sussex; adding that S^r^: Jn^o^. Banks & his\
Trustees must Seale, and he or any one of them —\
acknowledge it before a Master in Chancery, that\
it may be inrolled ;  Moreouer, that M^r^. Lilly had\
since promised him to haue it ready ingroſs’d for —\
Thursday & to bring it [*over:* ‘or’] to M^r^: Hook an authentique\
copie of those Records that make out what is due\
& out of what Lands, that so if occasion be, the [**page 259:**]\
Society may be able to proue their title : Besides that\
the same has promised to make knowne to the Council\
[*margin:* ‘M^r^: Henshaw to be \| found in Cliffords Inn’] M^r^: Thomas Henshaw, who is the person that returns\
the Rent up from Lewes : Lastly that S^r^: Jn^o^. Banks\
has promised to lend the Council his Conveyances,[*over:* ‘conveyances’] if they\
need them .

[…]

‌ Ordered , that M^r^: Hook doe assoon as he can ~\
remove the Societys Repository and library to the\
North Gallery of Gresham Collegde; and that done\
to ⅌fect the Catalogue of both, according to aformer.

‌ M^r^: Hook mentioning that he had an Invention\
for finding the Longitude to a minute of time, or. 15. m\
in the heavens, w^ch^: he would make out , and render practi⹀\
⹀cable, if a due compensacon were to be had for it;\
S^r^: James Shaen promised, that he would procure for\
him either a thousand pound Sterꝲ in a lump, or an hundred\
& ffiftie pound ⅌ annu[*flourish*] .

‌ M^r^: Hook declaring , that he would rather chuse\
the latter, the Council preſsed him to draw up Articles\
accordingly, and to put the thing into act .

\TranscriptionStop


**(CMO/1 p. 257):** “Order for D^r^: Jn^o^. Wallis discourse to be printed.” (CMO/1)

**(CMO/1 p. 257):** “Wallis John D.D. \| His discourse on Gravity to be printed” (CMO/8A)

**(CMO/1 p. 258,260):** “[Letter sent to some concerning the same. \| and Copies made.] and Sent.” (CMO/1)

**(CMO/1 , v. 1 p. 258:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)


### 25 March 1675 (CMO/1 pp. 259–260;  CMO/1/218) probably Gresham College

\TranscriptionStart

[*centered:*] March. 25 . 1675

[…]

‌ D^r^: Whistler mencōn’d a proposal to be made to this — [**page 260:**]\
Council for disposing of Chelsey Colledge

‌ He was desired to receive it in writing for the —\
next Meeting of the Council

‌ It was Ordered , that the Amanuensis and the\
Operator should be urged to greater dilligence and care\
in offering the printed Bonds to those as haue not —\
yet Signed , between this & the next Council; and\
that they Should desire every one of those they —\
offer the bond to, to give their positive answer, —\
whether they would Signe or not .

\TranscriptionStop


**(CMO/1 p. 259: 72,259):** “[M^r^: Hook appointed Keeper of the Repository of the Society] Order to the same.” (CMO/1)

**(CMO/1 p. 259):** “Concerning removing & making a Catalogue.” (CMO/1)

**(CMO/1 p. 259):** “Order concerning removing & Making a Catalogue.” (CMO/1)


### 8 April 1675 (CMO/1 pp. 260–261;  CMO/1/219) probably Gresham College

\TranscriptionStart

[*centered:*] April . 8. 1675

[…]

[**Page 261:**]

[…]

‌ S^r^: Jn^o^. Banks delivered the Indenture —\
concerning the Fee-farme Rents of £. 24. Rent\
⅌ annu[*flourish*] in Lewis in Sussex : which was delivered to\
M^r^. Colwall to keep .

\TranscriptionStop


**(CMO/1 p. 260: 258,260):** “[Letter sent to some concerning the same. \| and Copies made.] and Sent.” (CMO/1)

**(CMO/1 , v. 1 p. 260:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)


### 30 April 1675 (CMO/1 p. 261;  CMO/1/220) 

\TranscriptionStart

[*centered:*] April. 30^th^.

‌ The Council was Summoned but met not

\TranscriptionStop


### 17 June 1675 (CMO/1 pp. 261–262;  CMO/1/221) probably Gresham College and ff.

\TranscriptionStart

[*centered:*] June. 17^th^: 1675

[…]

‌ Ordered, That a Treatise, entituled , Francisci\
Willughbeji de Midleton Armigeri, quondam e Societate\
Regia Ornithologia, be printed by Jn^o^. Martin, Printer\
to the said Society [ESTC R471002 and R27077 GB-UkLoRS copy?]

‌ Ordered That a Discource, made before the Royal\
Society the , 29^th^. of April & 13^th^ : May last , by Jn^o^ Evelyn [**page 262:**]\
Esq^r^ , concerning the improuem^t^ : of Earth for Vegetacõn\
be printed by J Martyn Printer to the Said Society [ESTC R21425, GB-UkLoRS copy?]

‌ Ordered That a Treatise intituled , Marscelli\
Malpighij Philosophi et Medici    , O\
Regia Anatome Plantarum, [*over:* ‘Plantareui’] eui Subjungitur appendix ,\
repetitas \~ \~ \~ ab codem Authore de Formatione\
Pulli in Ovo Observationes continens : be printed by\
Jn^o^. Martin Printer to the Said Society . [ESTC R2466, GB-UkLoRS copy?]

\TranscriptionStop


**(CMO/1 p. 261,262):** “Francisci Willughbeÿ Treatise to be printed:” (CMO/1)

**(CMO/1 p. 261,262):** “A Discourse made by Jn^o^. Evelyn Esq^[r]{.underline}^ to be printed.” (CMO/1)

**(CMO/1 p. 261):** “[Printing of Papers & Books.] Willoughby’s Ornithologia. Ordered” (CMO/8A)


### 24 June 1675 (CMO/1 p. 262;  CMO/1/222) 

\TranscriptionStart

[*centered:*] June . 24 : 1675

[…]

‌ M^r^: Willugbys Ornithologii,  M^r^: Evelyns late ~\
Discourse of Agriculture, and Sign^r^: Malpighi's Anatome[*over:* ‘Ant’]\
Planatarum were Lycensed

[…]

‌ S^r^: J : Banks reported, that the Comittee of —\
the East India Company had given Order to empty the\
North-gallery in Gresham College , and to deliver it —\
againe to the Mercers from whom they had receiud it .

\TranscriptionStop

**(CMO/1 p. 262):** “Report concerning North gallery in Gresham Coll.” (CMO/1)

**(CMO/1 p. 262: 261,262):** “Francisci Willughbeÿ Treatise to be printed:” (CMO/1)

**(CMO/1 p. 262):** “Marscelly Malpighij Treatise to be printed.” (CMO/1)

**(CMO/1 p. 262: 261,262):** “A Discourse made by Jn^o^. Evelyn Esq^[r]{.underline}^ to be printed.” (CMO/1)

**(CMO/1 p. 262):** “[Printing of Papers & Books.] Evelyn’s On Vegetation, & Malpighi Anat. plantarum. Ordered” (CMO/8A)


### 28 June 1675 (CMO/1 p. 263;  CMO/1/223) 

\TranscriptionStart

[**Page 263:**]

[*centered:*] June . 28^th^ : 1675.

[…]

‌ Inquiry being made, whether the [*canceled:* ‘~~North~~’]‸^West^ Gallery\
in Gresham College was emptied, and found that\
it was not yet ; M^r^. Hook was Ordered to call upon\
the Officers of the East India Company , to remove\
their Goods according to the Order of the Committee\
of the said Company

‌ Ordered That the Amanuensis do from this —\
Council attend M^r^. Helthrop, and demand the Arrears\
of his quarterly payments due to the said Society at\
Midsum^r^: last past , amounting to . £. 14 . 19. shiꝉꝉ: ~\
acquainting him with the Order, that whosoever of the\
said Society shall refuse to pay their Arrear due by\
their Subscriptions at their Admiſsion into the said Society ,\
shall be proceeded against according to Statute

\TranscriptionStop


### 21 October 1675 (CMO/1 pp. 263–264;  CMO/1/224) 

\TranscriptionStart

[*centered:*] Octob^r^: 21^th .^ : 1675

[…]

‌ Ordered That the following persons be Sent unto\
to know their positive Answer, whether they will Signe the Bond\
or no . viz^t^.

[**Page 264:**]

[*…four columns of twenty names*]

[…]

‌ Ordered That Mich[*canceled:* ‘Mich^~~l~~^’] Wicks doe attend S^r^: Jn^o^: Banks\
to morrow at the East India house , to receive the Order\
for clearing the Gallery .

‌ Ordered That D^r^: Grews Book, \[ entitled, The\
Comparative Anotamy of Trunks, together with an acco^t^:\
of the Vegetacõn of Trunks grounded thereupon ; in\
two parts : the former read before the R: Society . ~\
Feƀry . 25 . 167$\frac{4}{5}$ ; the latter June . 17 . 1675 . The\
whole explicated by ſeu^r^all Figures in . 19. Copperplates,\
presented to the R: Society in the years. 1673 & 1674\]\
be printed for Water Kebleby by the Aſsigne of John\
Martin Printer to the R: Society . [ESTC R218849, GB-UkLoRS copy?]

\TranscriptionStop


**(CMO/1 p. 263):** “Amanuensis Ordered to demand the Arrears from M^r^: Nelthrop.” (CMO/1)

**(CMO/1 , v. 1 p. 263:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)

**(CMO/1 p. 264):** “D^r^. Grews Book of the comparative Anatomy to be printed.” (CMO/1)

**(CMO/1 p. 264):** “[Printing of Papers & Books.] Grew’s Comparative Anatomy. Ordered” (CMO/8A)


### 11 November 1675 (CMO/1 p. 265;  CMO/1/225) 

\TranscriptionStart

[**Page 265:**]

[*centered:*] Novemb^r^: 11. 1675

[…]

There was appointed a Comittee for Auditing the\
‌ Accompts. viz^t^

‌ President     M^r^: Hoskins\
‌ D^r^. Whistler\
‌ M^r^. Hill       Oldenburg

Three of these to be a Quorum,

[…]

\TranscriptionStop

### 18 November 1675 (CMO/1 p. 265–266;  CMO/1/226) 

\TranscriptionStart

[*centered:*] Novemb^r^: 18 . 1675

[…]

‌ Ordered , That the following persons be left out\
of the List, now to be printed for the approaching —\
Election Day , viz^t^

[*… two columns of eight people*]

[**Page 266:**]

‌ The reason o omitting them was their not ⅌forming.\
their Obligacōn to the Society

[…]

\TranscriptionStop


**(CMO/1 p. 265):** “Some Persons to be left out of the List.” (CMO/1)

**(CMO/1 p. 265: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 265: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)


### 25 November 1675 (CMO/1 pp. 266–267;  CMO/1/227) 

\TranscriptionStart

[*centered:*] November. 25^th^. 1675 .

[…]

‌ Ordered , That the President or his Deputy\
be desired to intimate to the Society on their approaching\
Anniversary Election day , that they could not but take\
notice of some persons left out of the List , that this\
wwas done, because they were found not to haue ⅌formed\
their Obligacõn to the Society , and that therefore d\
the Council intended to proceed against them according\
to Statute. And that the names of these persons\
should be then publickely read . viz^t^. 

[*… two columns of eight names*]

[**Page 267:**]

[…]

\TranscriptionStop

### 29 November 1675 (CMO/1 pp. 267–268;  CMO/1/228) 

\TranscriptionStart

[*centered:*] Novemb^r^: 29^th^. 1675

[…]

[*margin:* ‘\* The Original report\
of this order was hap⹀\
⹀pily recoverd by the,\
Ps James Earl of Mor⹀\
⹀ton in January 1766\
Andis now preserved\
in the Society’s Guard\
Book of 1675.\
by Order of the, S^d^—\
President [*canceled:* ‘~~James~~’]\
[*inserted:*] & copied [*canceled:* ‘~~this report~~’] in\
the Chasm over leaf.\
[*canceled:* ‘~~Emanuel Mendes~~’]\
[*canceled:* ‘~~de Costa librarian~~’]\
[*canceled:* ‘~~&c R.S 19 January~~’]\
[*canceled:* ‘~~1766.~~’]\
’]

\* Ordered , That S^r^: John Banks , S^r^. Jonas\
Moore D^r^. Croon , M^r^. Colwall, M^r^: Hill, and\
M^r^: Hook , or any two or more of them ( whereof\
M^r^. Colwal to be one) doe take care to gett the possession\
of the white Gallery in Gresham College, to fitt it\
for a Repository, and to remove thither with all ~\*\
possible speed what is in the Repository of the Society.

[…]

‌ The Committee of the Council for Auditing the\
Accompts made their Report to the Council, as followeth,

[**Page 268:**]

[*blank*]

[*rest of entry in Emanuel Mendes de Corta’s hand:*]

Copy of the Original report concerning the white Gallery at\
Gresham College which is now (1766) laid up in the Guard Book\
of 1675. viz (Copy Verbatim & Litteratim)

At a Meeting of the Committe of the R. Society for contriu⹀\
⹀ing the white Gallery of Gresham College for the Library and\
the Repository of the Royal Society

‌  December 9. [1675]{.underline}

There being present the Coittee ; viz^t^.\
S^r^ Jonas More. M^r^ Colwall. D^r^ Croon. M^r^ Hill. M^r^ Hook.

‌ In pursuance of an Order of the Council, we haue viewed the\
White Gallery, and having considered the same, we conceive, if fit, that the\
whole Gallery be forthwith repaired, as to the whiting of the walls mending the\
Glaſse, making the Doors, Partitions and Shutters of the Windows: And that the\
whole Gallery be divided into two parts; the South-part of which, containing seven\
Windows, we conceive fit to be made the Repository, and the North-part or remainder\
thereof to be made the Library. [*gap around text*] And further, that the\
Stairs and Doors leading [*gap around text*] thereunto at the\
South-end thereof should\
be repaired and Amended:\
And that the Work⹀\
⹀men of the College\
should be treated\
withall, to Understand\
the time and Charge\
of the said Repair.\
In order thereunto\
we have summoned\
the said Workmen\
to meet Us here at\
Gresham College, to\
Morrow at 5. of Clock\
in the Afternoon 

Dan: Colwall\
A Hill\
Robert Hook

endorsed “Report of the Committee”\
“for taking care of the Repository ”

\TranscriptionStop

**(CMO/1 p. 267: 113,144,174,175,196,204,207,212,225,248,267):** “Reports to the Council” (CMO/1)

### 6 March 1676 (CMO/1 pp. 268–271;  CMO/1/229) 

\TranscriptionStart

[*centered:*] March. 6. 16$\frac{75}{6}$

[…]

‌ The President moved , that it might be considered, how [**page 269:**]\
to provide for the weekly Meetings of the Society\
a Sufficient number of Eperiments, to be made from\
time to time, and to pitch upon such ⅌sons as might\
be depended upon for the exhibiting of them .


[*centered:*] After some debate it was Order’d

‌ That S^r^. John Banks , S^r^: Cyril Wych, S^r^: Jonas\
More, M^r^. Colwal, D^r^. Croon, D^r^: Grew, M^r^: Hill\
or any three or more of them ( whereof the Treaẽr\
M^r^. Colwal to be one) and as many more of the Council\
as shall please to Joyne themselues with them, be a\
Committee for considering of persons members of the R:\
Society , fit to entertaine the said Society at their ~\
weekly meetings from time to time w^th^: Experim^ts^, & discourses\
upon them ; to be left in writeing in order to be registered\
and haueing found such persons as are able and will engage\
in this worke to offer unto them for euery such ex⅌tal\
Exercise  ⅌formed before the said Society & delivered\
in writeing ~ ~ ~ not exceeding the value of £. 4.\
besides the charges requisite to make the respective —\
Experiments  And this Committee to meet in y^e^ Society's\
Repository on Thursdays at . 3 . a clock precisely, and to\
make a Report of their progresse in this matter to the\
Council

[…]

‌ This done there was read a Proposal for encourageing\
the Presse of Oxford, recomended by M^r^. Secretary —\
Williamson to the Council, viz^t^. to pitch upon some good\
book or books to be printed there at a reasonable rate,\
at such time as . 500 . Subscribers should haue bee obtained\

‌ The Council declared, That they would of some —\
books to be printed accordingly and thereupon offer and\
recommend the paper produced to ⅌ticular persons —\
of their number, for Subscriptions , and to doe the —\
like to the body of the Society

‌ After this , the Committee for manageing the\
businesse of the Repository, made a Report to the\
Council of what they had done in that affair , viz^t^: —\
that they had removed the ⅌ticulers thereof out\
of the rooms where they had hitherto been, into the\
Gallery at the west end of Gresham College , and there\
ranged them in order, and that now it remained for y^e^\
Council to order an Inventory or Catalogue to be made\
both of those Curiosities and the Books , and to appoint\
⅌sons to haue the custody of the Same .

[**Page 271:**]

‌ Whereupon it was Ordered

‌  That S^r^: John Banks , S^r^: Cyril Wyche, S^r^: Jonas\
Moor, M^r^. Milles , M^r^: Daniel Colwall, D^r^: Croon\
D^r^. Grew, M^r^: Hill, or any   or more of them ~\
(whereof M^r^: Colwal to be one) be a Committee for ~\
considering of persons that may be fitt to make an —\
Inventory or Catalogue not onely of the naturall —\
Curiosities and Books of the R. Society, but also of\
all the goods & Chattels belonging to the same, and to\
cause all those ⅌ticulars to be entred into a booke —\
Farther to pitch upon a trusty ⅌son fitt to be keeper\
of all the foremenconed things and to represent the result\
of all to the Council

‌  The Treasurer moving that henceforth he might not\
pay any Sallary to the respective Officers of the Society\
without an Order from the President , it was

‌  Ordered That henceforth the Treasurer do not pay\
any Sallary to any of the Officers of ſd Society without\
an Order from the President

‌  M^r^: Henshaw desired to borrow for the use of some\
learned friends of his out of the Arundelian library given\
to the Society , the MS. of S^t^. Cyprian, for the restoring\
of w^ch^: he would engage himself by a note under his hand

‌  Ordered

‌  That M^r^: Hook do deliver to Thomas Henshaw Esq^r^\
as soon as conveniently he can, the MS of S^t^: Cyprians —\
works, one or more of them, to be found in the R Society’s\
Library now standing in Arundel house , the Said M^r^. Henshaw\
giveing to M^r^. Oldenburg a note under his hand to restore\
Such Book or Books , safe & undamnified within the space\
of Six monthes from the date hereof

‌  This Order to be Signed by the President.

‌ The Philosoph : Transactions of N^o^. 122. were Lycensed . [*flourish*]

\TranscriptionStop

**(CMO/1 , v. 1 p. 269):** “[Experiments. To consider of, to entertain the King.] For weekly meetings considered; and £4 Offered for each & a written account thereof” (CMO/8A)

**(CMO/1 p. 270):** “Proposal read concerning the Oxford Presse.” (CMO/1)

**(CMO/1 p. 270):** “[Order to Mich: Wicks concerning the Gallery.] Report concerning that businesse—” (CMO/1)

**(CMO/1 p. 270):** “Oxford Preſs. Proposal to encourage” (CMO/8A)

**(CMO/1 p. 271: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 271):** “Order for a lending a Manuscript out of the Arundelian Library” (CMO/1)

**(CMO/1 p. 271):** “Order to make a Table & Catalogue of Books & Curiosities &c” (CMO/1)

**(CMO/1 p. 271):** “[Order to make a Table & Catalogue of Books & Curiosities &c] And to pitch upon a person fitt to be keeper ouer all the said things” (CMO/1)

**(CMO/1 p. 271: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 , v. 1 p. 271):** “Arundel Library. Books borrowed from it.” (CMO/8A)

**(CMO/1/, v.1 p. 271):** “[Inventory.] Com^ee^. approved to consider of” (CMO/8A)

**(CMO/1 , v. 1 p. 271:)** “[Librarian’s] Minutes about appointing one” (CMO/8A)


### 18 July 1676 (CMO/1 p. 272;  CMO/1/230) 

\TranscriptionStart

[**Page 272:**]

[*centered:*] July. 18^th^ . 1676.

[…]

‌ Ordered , That the Phil: Transactions , being\
Numb: 127 . together with two Tables of Cutts , be —\
printed by the Printer to the R . Society

\TranscriptionStop

### 3 October 1676 (CMO/1 p. 272;  CMO/1/231) 

\TranscriptionStart

[*centered:*] Octob^r^: 3 . 1676

[…]

‌ Ordered , That the Phil: Transactions, being\
Numbered 128. with two Tables of Cutts, be printed\
by the Printer to y^e^. R. Society

[…]

\TranscriptionStop

### 12 October 1676 (CMO/1 pp. 272–273;  CMO/1/232) 

\TranscriptionStart

[*centered:*] Octob^r^: 12 . 1676

[…]

‌ Ordered That the Printer to the R: Society , M [**page 273:**]\
M^r^: Martin , be required to give notice in the Phiꝲ ~\
Transactions. next to be printed , of what this Council is ~\
inform’d he hath declared, viz^t^ That the Tract called\
Lampas, made by R: Hook F. of the R. Society, and\
lately printed for John Martin Printer to the said\
Society, to w^ch^: is annexed a Postscript , reflecting on the\
said Publisher of the Transactions, was printed without\
the leaue & knowledge of the Council of the R: Society ~\
and that the said Printer had ſeen nothing of the Postscript\
thereof before it was printed off, nor knew any ground for\
the aspersions contain’d therein . And incase the said Printer\
doe refuse to obey this Order, that then the new Printer,\
w^ch^: the President hath power to constitute in his roome ,\
be required to signifie to the publick in Print, that M^r^\
Martin was remov’d for disobeying this Order of the —\
Council .

‌ Ordered, That M^r^: Colwall and the Secretary\
doe prepare for the next Council a List of those that haue\
not yet Sealed the Bond , nor pay their weekly contribucõns 

\TranscriptionStop

**(CMO/1 p. 272: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 272,296,302):** “[Transactions] Orders about printing and publishing” (CMO/8A)

**(CMO/1 p. 272,275,277):** “[Transactions] Orders about printing N^os^. 127. 128. 129. 130” (CMO/8A)


### 2 November 1676 (CMO/1 pp. 273–274;  CMO/1/233) 

\TranscriptionStart

[*centered:*] Novemb^r^: 2 . 1676

[…]

‌ Vpon the debate concerning a Scandalous Poscript\
annexed to a Book called Lampas, it was Ordere’d, that\
it be referred to D^r^: Croon and M^r^: Hill, to present\
a draught to the Council of what they conceive may be [**page 274:**]\
fitt for the Council to publish in the next Transaction\
in the behalfe of M^r^: Oldenburgs integrity and —\
faithfullnesse to the R. Society

‌ At the same time Leaue was given to M^r^: Oldenburg\
to print that part of Mons^r^: Heugens ꝉre written to\
him the 20^th^. Feƀry 1675 . w^ch^: devolus on the R: Society\
or him his right of desireing in England a Patent\
for his Watches. /

[*new handwriting going forward:*]

‌ M^r^: Henry Hunt being propos’d to Succeed in —\
M^r^: Shortgraues place, the Council hauing heard the\
ſeuerall good Testimonyes given him of his ability\
and honesty, receiued him , to be Operator to the R :\
Society , quamdiu se bene gesserit; and he was sworne\
at the same time

\TranscriptionStop

**(CMO/1 p. 273):** “List to be made of those, that haue not paid.” (CMO/1)

**(CMO/1 p. 273):** “Order for a List to be made of those, that haue now Sealed the same.” (CMO/1)

**(CMO/1 p. 273):** “Order given to the Printer.” (CMO/1)

**(CMO/1, v.1 p. 273,275):** “Lampas, a book so called \| Com^ee^. appointed to consider of it.” (CMO/8A)

**(CMO/1 p. 273,275):** “Order concerning a Scandalous Postscript annexed ot a Book called Lampas.” (CMO/1)


### 20 November 1676 (CMO/1 pp. 274–275;  CMO/1/234) 

\TranscriptionStart

[*centered:*] Novemb^r^: 20^th^: 1676

[…]

‌ There was appointed a Comittee of the Council for\
Auditing the Treārs Accompts viz^t^: The President\
D^r^: Croon , M^r^. Mills , M^r^. Hill , M^r^. Oldenburg

‌ Vupon a Debate how to proceed with those Members\
of the Society that haue not yet Sealed the Bond, nor\
do pay, It was Ordered

‌ That the Bond for payment of the weekly ~\
contributions to the R. Society, be offer’d by M^r^: Wicks [**page 275:**]\
their Clerk to every member that hath not yet Sealed it,\
and that incase any ⅌son shall refuse to seale the said\
Society or not .

‌ Ordered , That D^r^: [*after M name inserted:*] Meibomius his two Books\
de [*two words inserted:*] Turemium fabrica ,  and de Proportionibus, be bought\
for the library of the R : Society . [These are in the library’s collection with the fore-edge inked ‘10’, the later with ‘A.42.’ on the front, free endpaper]

‌ D^r^: Croon and M^r^. Hill brought in their Report\
concerning the Postscript, annexed to Lampas, viz^t^

‌ Whereas the Publisher of the Phil: Transactions hath\
made complaint to the Council of the R: Society of some\
passages in a late book or M^r^. Hooks intitul’d Lampas , and\
printed by the Printer of the said Society, reflecting on\
the integrity & faithfullnes of the said Publisher in his ~\
management of the intelligence of the said Society, The\
Council hath thought fitt to declare in behalfe of the —\
Publisher aforesaid , That they knew nothing of the —\
publication of the said Book, and further, that the sd\
Publisher hath carried himself faithfully & honestly in\
the management of the intelligence of the R. Society\
and given no just cause for any such reflexions .

‌ This report was approu’d off, and Ordered to be ~\
printed in the next Transactions .

\TranscriptionStop


**(CMO/1 p. 274):** “M^r^: Hunt sworne as Operator” (CMO/1)

**(CMO/1 p. 274):** “Leave given to print a part of Mons. Heu gen: Cre.” (CMO/1)

**(CMO/1 p. 274: 66,79,101,112,140,173,175,201,206,210,223,244,265,274):** “A Committee of the Council ordered, to examine ~ \| the Treaer̃s Accompts.” (CMO/1)

**(CMO/1 p. 274: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 274):** “[Oldenburg M^r^. S \| Appointed Secretary] Has leave to print Huggens’s letter, concerning \| a patent for His Watches” (CMO/8A)


### 23 November 1676 (CMO/1 pp. 275–276;  CMO/1/235) 

\TranscriptionStart

[*centered:*] Novemb^r^: 23. 1676

‌ Ordered, That number 129. of the Phiꝲ Transactions [**page 276:**]\
[*margin:* ‘6’] for the month’s of Octob^r^: & Novemb^r^: 167[8]{.underline} . together\
with a Declaracon of this Council at their last meeting ,\
Ordered to be printed in the next Transactions, be\
printed accordingly . /

\TranscriptionStop

**(CMO/1 p. 275: 273,275):** “Order concerning a Scandalous Postscript annexed ot a Book called Lampas.” (CMO/1)

**(CMO/1 p. 275: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 275):** “Order to buy D^r^: Meibomy Books de Trivemium Fabrica” (CMO/1)

**(CMO/1 p. 275: 272,275,277):** “[Transactions] Orders about printing N^os^. 127. 128. 129. 130” (CMO/8A)

**(CMO/1, v.1 p. 275: 123,124,161,275):** “[Library.] Books to be bought for the Society” (CMO/8A)

**(CMO/1 , v. 1 p. 275:)** “[Clerk.] Orders to him about Bonds.” (CMO/8A)


### 14 December 1676 (CMO/1 pp. 276–277;  CMO/1/236) 

\TranscriptionStart

[*centered:*] Decƀer. 14^th^. 1676.

[…]

‌ The Secretary read a Letter written to him\
the . 1^th^ . of Xƀer . 1676. from Amsterdam by a Merchant\
called Elias Sandra junior, & desired to be communicated\
to the R : Society containing an Offer made by the said\
Merchant of discovering to this Society such places , —\
where great plenty of Ambregreis , is to be found; w^ch^:\
discovery he would make upon certaine conditions and\
articles accompanying this offer

‌ The Councel upon the debate of the whole resolued,\
that the Secretary should returne an answer to this effect,\
viz^t^: That the Articles appearing to this Councel to be\
such, as required ⅌sonal conferences between the parties\
contracting for the better understanding of one anothers\
minds, this Council would treat with him if he thought fitt\
to come over. And further that they were of Opinion, in\
case they could agree with him in the rest , they should [**page 277:**]\
not differ from him as to his demanded third, nor the\
import of the first and .8 . Article,[*over:*] ‘Articl;’] provided first, That,\
when he hath discovered the thing , the same be not found\
a thing already knowne, and then that the places for\
finding the proposed plenty of Ambergris be not already\
Subject to some jurisdiction or other, of either the English\
or Dutch East India Company, or any other that might\
justly oppose or hinder the execution of this Dissein

‌ Ordered, That the number. 130 . of the Ph :\
Transact . consisting of three Sheets be printed by the\
Printer of the R: Society .

\TranscriptionStop


**(CMO/1 p. 276: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)


### 25 January 167$\frac{6}{7}$ (CMO/1 pp. 277–279;  CMO/1/237) Gresham College

\TranscriptionStart

[*centered:*] Jan^[ry]{.underline}^ . 25 : 16$\frac{76}{7}$

[…]

M^r^. Henshaw was sworne Vice President of the R: Society

‌ M^r^. Oldenburg acquainted the Council from the —\
Earle Marshall that his Lo^p^: was desirous, that the\
Library at Arundel house, given by him to the R :\
Society, might be better look’d after ; as also that he—\
should be glad to haue those books of that Library\
delivered to him w^ch^: he reserved out of it to himself\
at the time of the donation thereof , viz^t^. Books of —\
Heraldry & Genealogy

[**Page 278:**]

‌ The Council hereupon Ordered, that S^r^: John\
Hoskins ; M^r^. Evelyn , M^r^: Hill, and M^r^: Oldenburg ,\
or any two‸^or\ more^ of them, should be a Com^tee^ : to attend my\
Lord Marshall, & to deliver to him Lo^p^: such Books\
as he hath reserved for himself out of the sđ Library ,\
as also to secure the same from Damage.

‌ Ordered likewise that the Apothecarys bill\
concerning the last Sicknes of the late M^r^: Shortgraue,\
amounting to about five pounds, be paid by the Trēar\
of the R : Society, his widdow hauing first delivered\
up to M^r^: Hunt all the Instruments, Utensils etc.\
belonging to the R: Society , So that satisfaction be\
given in this matter to the Comittee appointed for —\
taking care of the Repository

‌ The Phil: Transactions for January, consisting\
of three Sheets were Lycensed

‌ M^r^: Hunt was ordered to take a coppy of the\
picture of the late Lord B^p^: of Chester, D^r^: Wilkins .

‌ Ordered, that the Astronomical Instruments —\
belonging to the R : Society, & being in their Repository\
at Gresham College, be lent to the Observatory at —\
Greenwich, for making Astronom^l^: Observacons, and\
that M^r^: Hooks new Quadrant be forthwith finished\
at the charges of the Society.

‌ M^r^: Oldenburg read a letter of Elias Sandra —\
Junior, Merchant at Amsterdam, dated the. 22^th^. of ~\
Jan^ry^. 1671 being a Returne to the Answer‸^sent^ to him ~\
from this Council upon his first ꝉre concerning the\
proposition of discovering a great plenty of Ambergris :\
The Substance of w^ch^: letter being, that he desired to\
know the thoughts of this Councel touching the rest\
of the Articles formerly proposed, The Council caused [**page 279:**]\
those Articles to be read, and ordered thereupon, that\
as to the Articles for Secresy, they cannot be kept secret\
from those, w^ch^: the Council shall make use of in this matter:\
As to the . 5 . Article, it will be the concern & interest\
of the Council, to oblige that shall goe out upon this ~\
desein , to follow his directions relating to the place,\
where the said plenty of Ambergris is to be found.\
As to the. 6. Article, the Discoverer & the . 3^d^ ; part\
shall be brought free and without any of his expense\
into England, As to the . 8 . Article, the Council —\
will not be tyed to secrecy of a generall but a ~\
⅌ticular discovery .

\TranscriptionStop

**(CMO/1 p. 277: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 277: 272,275,277):** “[Transactions] Orders about printing N^os^. 127. 128. 129. 130” (CMO/8A)

**(CMO/1 , v. 1 p. 277:)** “[Arundel Library] Minutes concerning the Books of Heraldry and Genealogies” (CMO/8A) “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A)

**(CMO/1 p. 277):** “The Earle Marshalls desire concerning the Arundelian Library” (CMO/1)

**(CMO/1 p. 278):** “[The Earle Marshalls desire concerning the Arundelian Library] Thereupon Ordered a Committee to deliver to his Lo^p^: the reserued books.” (CMO/1)

**(CMO/1 p. 278):** “Order to take a coppie of the Picture of the late Lord B^r^ of Chester D^r^: Wilkins.” (CMO/1)

**(CMO/1 p. 278: 90,91,92,93,95,96,98,100,101,106,108,109,122,129,131,132,134,135,144,146,150,159,168,189,199,202,206,217,271,272,275,277,278):** “Order concerning Philosophical Transactions.” (CMO/1)

**(CMO/1 p. 278: 122,129,131,132,134,135,144,146,150,158,189,199,206,217,242,271,278):** “Transactions \| N^os^. Licensed” (CMO/8A)

**(CMO/1 p. 278):** “[Wilkins John D.D.] Copy of his picture to be taken by M^r^. Hunt” (CMO/8A)

**(CMO/1 p. 278):** “[Pictures or Portraits.] \| Of Bishop Wilkins, M^r^. Hunt to take a Copy.” [possibly an engraving] (CMO/8A)


### 13 September 1677 (CMO/1 pp. 279–280;  CMO/1/238) 

\TranscriptionStart

[*centered:*] Thursday Sept . 13^th^ : 1677

[…]

Resolued That the next meeting of this Councel, the\
Statute of by Law now proposed for takeing votes by\
a ballotting boxe shall be read,

‌ For the more free & private giving of Votes at the\
Council of this Society. Bee it Ordered & it is Ordered,\
that henceforth all votes in passing questions at Meetings\
of the Council shall be taken by a ballotting boxe.

‌ Ordered that M^r^. Bernard have two Arabicke\
Psalters out Arundell Library giving Security such as\
M^r^. Hooke shall approue for returning them within Six [**page 280:**]\
months Provided the E: Marshall consent

‌ Ordered That M^r^: Boyle S^r^: John Hoskins &\
M^r^: Hill or any two of them doe at ten of the clock\
to morrow morning goe to the widow of [*canceled:* ‘~~M^r^~~’] Henry ~\
Oldenburg Esq^r^: late Secretary of this Society & demand\
receive or take order for secureing for the use of this Society\
all such Goods bookes & Writeings belonging to this ~\
Society as are or haue been in the possession of her, or ~\
of her late husband

‌ That M^r^: Hooke agree with M^r^: Forster for\
making the Catalogue of the Arundel Library, and\
that what he agrees for be paid for by the Treãer.

\TranscriptionStop

**(CMO/1 p. 279: 197, 232, 279):** “Order concerning the Loan of the books of the Library” (CMO/1)

**(CMO/1 p. 279,281):** “Resolution concerning a By-Law for taking Votes by a Ballotting Boxe.” (CMO/1)

**(CMO/1 , v. 1 p. 279:)** “Arundel Library. Books borrowed from it.” (CMO/8A)


### 24 September 1677 (CMO/1 pp. 280–281;  CMO/1/239) 

\TranscriptionStart

[*centered:*] Munday. 24^th^; Sept . 1677

[…]

‌ This Councel met by Order of M^r^ Henshaw who\
21 . instant in absence of the President ordered M^r^: ~\
Hunt to Sumõn a Meeting of the Council on this day

‌ Upon Reading the Case of M^r^: Oldenburgs children\
& S^r^: Ricħd. lLoyds opinion thereupon .  Ordered that\
Michael Wicks & Henry Hunt be named Comm^rs^: on —\
behalfe of the Society & that M^r^: Hunt give notice\
hereof to M^r^. Boyle . and that the persons in pos̃s̃ion of\
the house be desired to keep pos̃s̃ion unlesse demanded with\
M^r^. Boyles consent .

‌ Ordered that when any ꝉers concerning the ~\
Society be directed to the Secretary w^t^: concerns the said\
Society shall be read at the Meeting of the Society next -\
after the receipt of Such Letters.

[**Page 281:**]

‌ That all papers & books concerning the Society\
be kept in the repository or Library of the said Society\
and that if any thing be to be transcribed it be done there.

‌ That the officiating Sec^ry^: taking[*over:* ‘take’] short notes of all\
that passes at the Society or Council before the rising read\
the said notes to see they be rightly taken .

‌ That the notes so taken be fairely entred by the next\
meeting day respectively

‌ The Stat for taking votes by Ballott propounded last\
Meeting was passed as a Stat .

[…]
\TranscriptionStop

**(CMO/1 p. 280):** “Order concerning a Catalogue to be made of the Arundelian Library” (CMO/1)

**(CMO/1 p. 280):** “Committee Ordered concerning M^r^: Oldenburghs Children.” (CMO/1)

**(CMO/1 p. 280):** “Order concerning Letters directed to M^r^ Oldenburg” (CMO/1)

**(CMO/1 , v. 1 p. 280:)** “Arundel Library. Minutes and Orders about making its Catalogue” (CMO/8A) “[Books & Papers of the Royal Society] To be recovered from Oldenburg’s Executors.” (CMO/8A)

**(CMO/1 p. [304]):** “Committee for Recovery of letters papers &c out of M^r^. Oldenburg ([*uncertain:*] ‘Addtnill’) hand.” (CMO/1)

**(CMO/1 p. 280,282,283,284):** “[Oldenburg M^r^. S \| Appointed Secretary] On his death, a Com^ee^. to recover the Society’s papers” (CMO/8A)

**(CMO/1 p. 280,282,283,284):** “Ordered to go to M^r^: Oldenburg’s (late Secretary) Widow concerning \| Books & Writings to be restored.” (CMO/1)


### 27 September 1677 (CMO/1 pp. 281–282;  CMO/1/240) 

\TranscriptionStart

[*centered:*] Thursday. 27 . Sept . 1677

[…]

[**Page 282:**]

[*bracketed left:*]
‌ M^r^: Henshaw\
‌ S^r^: Jonas More
‌ M^r^. Hooke\
‌ S^r^: John Hoskins\
‌ D^r^: Grew . . . [*bracketed right:*]\
are named a Comittee by the\
Council to audit M^r^: Colwalls\
accounts for the year past .

[*bracketed left:*]
‌ Ordered that S^r^: J: Hoskins\
‌     M^r^. Henshaw\
M^r^. Boyle\
M^r^: Hill - -\
M^r^: Hooke [*bracketed right:*]\
or any three of them\
be desired to go to the\
Administratrix of M^r^\
Oldenburg & make demand\
of the books & papers -[*full lines:*]\
belonging to the R : Society now in her custody especially—\
those wh^ch^: are already laid aside & Sealed up in a Trunck\
as such, And they are hereby impowered to‸^re^pay such , —\
reasonable charges as the said Administratix hath ~ ~\
disbursed upon this Societys Account and that they doe\
also give her a discharge for what they shall so Receive . 

\TranscriptionStop

**(CMO/1 p. 281,283):** “Order that the Secretary take short Notes of all that passeth at the Soc: or Council” (CMO/1)

**(CMO/1 p. 281):** “Papers & Books to be kept in the Repository or Library.” (CMO/1)

**(CMO/1 p. 281: 279,281):** “Resolution concerning a By-Law for taking Votes by a Ballotting Boxe.” (CMO/1)

**(CMO/1 p. 281):** “Papers & Books to be kept in the Repository or Library.” (CMO/1)

**(CMO/1 p. 281,283):** “[Secretaries to the Society.] The Letters they receive as Officers. Orders about.” (CMO/8A)

**(CMO/1 p. 281,283, v.2 p. 30):** “[Secretaries to the Society.] To take Notes or Minutes, for fair copying” (CMO/8A)


### 19 December 1677 (CMO/1 pp. 282–283;  CMO/1/241) 

\TranscriptionStart

[*centered:*] Decemb^r^: 19^th^: 1677 .

[…]

‌ Ordered that what Exp^ts^: shall be undertaken by the\
Curators, shall be propounded a fortnight before the shewing [**page 283:**]\
thereof , that objections answers or confirmacōns may be —\
timely thought of

‌ Ordered also that the Curators or any other person ~\
shewing an Experim^t^: to the Society shall explaine the —\
Same & shew the designe & usefullnes thereof .

‌ A Debate about makeing Collections out of the Regist^r^\
or Jornall Books in Order to print them , to be resumed, ~\
when the papers & books are recovered by the Society

‌ Both the Secretaries to be employed in takeing the —\
Minutes at the meeting but to be drawen up fair for the\
amanuensis to enter by one who is also to read them .

‌ Ordered That a comon letter be drawne up by the\
Secretarys aga^st^: the next Councell

‌ That all letters to the Secretarys be henceforth ~\
inclosed in a paper to the President S^r^: Joseph Williamson\
his Ma^ts^: Principall Secretary of State

‌ That all letters recēd shall be pasted into a booke as\
they are received & that such of them as shall be thought\
fitt shall be fairely coppied out into the letter book for —\
that year

\TranscriptionStop


**(CMO/1 p. 282,284):** “Order that a comõn Letter be drawne by the Secretarys.” (CMO/1)

**(CMO/1 p. 282: 280,282,283,284):** “Ordered to go to M^r^: Oldenburg’s (late Secretary) Widow concerning \| Books & Writings to be restored.” (CMO/1)

**(CMO/1 p. 282):** “Order about making collection out of the Register or Journal Books.” (CMO/1)

**(CMO/1 p. 282: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 p. 282: 280,282,283,284):** “[Oldenburg M^r^. S \| Appointed Secretary] On his death, a Com^ee^. to recover the Society’s papers” (CMO/8A)

**(CMO/1 p. 282, v.2 p. 26):** “[Secretaries to the Society.] To have the care of the Society’s Books & Papers” (CMO/8A)

**(CMO/1 , v. 1 p. 282,283,284):** “[Books & Papers of the Royal Society] To be recovered from Oldenburg’s Executors.” (CMO/8A)

### 2 January 167$\frac{7}{8}$ (CMO/1 pp. 283–285;  CMO/1/242) 

\TranscriptionStart

[*centered:*] Jan^ry^ : 2 . 167$\frac{7}{8}$

[…]

‌ Ordered That the former Com^tee^ : be desired to visit M^r^\
Boyle & to desire his assistance in Recovering the books\
& papers of the Society yet remaining in M^rs^: Lardens hands 

[**Page 284:**]

‌ Then that care be taken to haue the Oathes of D^r^: Pell\
& the Administratrix made in Chancery that all the papers\
belonging to the Society haue been delivered & that they\
know of none else

‌ The coon Letter to be sent to all the correspond^ts^\
was read & allowed onely somwhat of returne for incouragem^t^.\
of the correspondents should be added . 

‌ The Correspondents named were Malpighius, Hugenius\
Justell, Carcanie , Slusius ,[*over:* ‘Slasius’] Hevelius,[*over:* ‘Havelius’] P Lana , Bullialdus\
Auzout ,[*over:* ‘Aazoat’] Leibnitz , S^r^: W^m^: Petty, M^r^: Lister, M^r^: Newton.

‌ Ordered That all letters recēd by the Sēc^rys^: shall be\
produced at the next weekly meeting and( if the Society\
shall think fitt) be read & that the Secretaries take the ~\
directions of the Society for their Speedy answering at\
least so far as to the acknowledging the Receipt

‌ Ordered That D^r^: Roƀt Plott In consideraion\
of the promise he hath made the Society of accomodateing\
them with Naturall curiositys, and accounts of such other\
⅌ticulars as will be ⅌tinent to their designe w^ch^: he may\
meet with in his Survey of England , be excused from\
his weekly payments

‌ M^r^: Henry Hunt was appointed as M^r^: Hills Deputy\
to receive such Arrears of the members of the Society\
as he shall receive directions from  M^r^: Hill soe to doe\
from time to time .

‌ Ordered That all Acts of the Councell shall be —\
Entered fairely into the Councell book . 

‌ Ordered also that there be prepared once a year a\
Collection of all such matters as haue been handled that\
year concerning . 4 . 5 . or more Subjects w^ch^: haue been well\
prosecuted & compleated w^ch^: may be printed in the name\
of the Society, against S^r^: Andrews day.

‌ Ordered also that the Register books of the Society\
be ⅌used and that what shall be thought fitt by the Council [**page 285:**]\
to be publisht be drawne out of it & printed accordingly.

‌ That Lists of the seuerall persons in Arrears to the\
Society & their respective Arrears recomended to seuerall\
of this Councell be made & delivered to them as a ~ // ~\
Memorandum of which they haue undertaken .

\TranscriptionStop

**(CMO/1 p. 283: 281,283):** “Order that the Secretary take short Notes of all that passeth at the Soc: or Council” (CMO/1)

**(CMO/1 p. 283):** “That all Letters to the Secretaryes be inclosed in a paper to the President.” (CMO/1)

**(CMO/1 p. 283):** “About Letters, received.” (CMO/1)

**(CMO/1 p. 283,284):** “Order for recovering Books & Papers yet remaining in M^r^: Lardens hand.” (CMO/1)

**(CMO/1 p. 283: 280,282,283,284):** “Ordered to go to M^r^: Oldenburg’s (late Secretary) Widow concerning \| Books & Writings to be restored.” (CMO/1)

**(CMO/1 p. 283: 281,283, v.2 p. 30):** “[Secretaries to the Society.] To take Notes or Minutes, for fair copying” (CMO/8A)

**(CMO/1 p. 283: 281,283):** “[Secretaries to the Society.] The Letters they receive as Officers. Orders about.” (CMO/8A)

**(CMO/1 p. 283):** “[Presidents of the Royal Society] Sir Joseph Williamson. Sec^r^. of State — 1677” (CMO/8A)

**(CMO/1 , v. 1 p. 283: 282,283,284):** “[Books & Papers of the Royal Society] To be recovered from Oldenburg’s Executors.” (CMO/8A)

**(CMO/1 p. 283: 280,282,283,284):** “[Oldenburg M^r^. S \| Appointed Secretary] On his death, a Com^ee^. to recover the Society’s papers” (CMO/8A)

**(CMO/1 , v. 1 p. 283):** “[Letter.] To be pasted into a book” (CMO/8A)

**(CMO/1 , v. 1 p. 283,284):** “[Letter.] A common one to be sent to Correspondents” (CMO/8A)

**(CMO/1 , v. 1 p. 283,284):** “[Letter.] To the Secretaries, to be sent to the President, and produced \| at the next meeting” (CMO/8A)

**(CMO/1 p. 283,284):** “[Printing of Papers & Books.] Of Articles from the Registers & Journals, proposed” (CMO/8A)

**(CMO/1 p. 284: 282,284):** “Order that a comõn Letter be drawne by the Secretarys.” (CMO/1)

**(CMO/1 p. 284):** “Order concerning Register Books to be ꝑ[per]used for printing the fittest things out of it.” (CMO/1)

**(CMO/1 p. 284: 245,246,284):** “D^r^: Plots proposal to be menconed.” (CMO/1)

**(CMO/1 p. 284):** “Order concerning Answers for received Letters.” (CMO/1)

**(CMO/1 p. 284):** “Order concerning all Acts of Council.” [TODO(check): should they be printed, Does this change orders to print] (CMO/1)

**(CMO/1 p. 284):** “Correspondents concerning the Society, named.” (CMO/1)

**(CMO/1 p. 284):** “Order to prepare Collection of Matters handled by the Society.” (CMO/1)

**(CMO/1 p. 284):** “Order for hauing the Oathes of D^r^: Pelle & the Administra \| concerning the same Books.” (CMO/1)

**(CMO/1 p. 284: 283,284):** “Order for recovering Books & Papers yet remaining in M^r^: Lardens hand.” (CMO/1)

**(CMO/1 p. 284: 280,282,283,284):** “Ordered to go to M^r^: Oldenburg’s (late Secretary) Widow concerning \| Books & Writings to be restored.” (CMO/1)

**(CMO/1 p. 284):** “M^r^. Hunt Ordered concerning Arrears.” (CMO/1)

**(CMO/1 p. 284: 283,284):** “[Printing of Papers & Books.] Of Articles from the Registers & Journals, proposed” (CMO/8A)

**(CMO/1 p. 284: 280,282,283,284):** “[Oldenburg M^r^. S \| Appointed Secretary] On his death, a Com^ee^. to recover the Society’s papers” (CMO/8A)

**(CMO/1 p. 284):** “[Printing of Papers & Books.] Of 4 or 5 of the Subjects best treated on in the year. \| against S^t^. Andrews day. Ordered” (CMO/8A)

**(CMO/1 , v. 1 p. 284: 283,284):** “[Letter.] To the Secretaries, to be sent to the President, and produced \| at the next meeting” (CMO/8A)

**(CMO/1 , v. 1 p. 284: 283,284):** “[Letter.] A common one to be sent to Correspondents” (CMO/8A)

**(CMO/1 , v. 1 p. 284: 282,283,284):** “[Books & Papers of the Royal Society] To be recovered from Oldenburg’s Executors.” (CMO/8A)

**(CMO/1 p. 284, v.2 p.216):** “[Treasurer to the Royal Society.] M^r^. Hunt (the clerk) appointed the Treas^rs^ Deputy” (CMO/8A)


### 4 May 1678 (CMO/1 p. 285;  CMO/1/243) Christopher Wren’s House

\TranscriptionStart

[*two lines centered:*] At a Councell held at S^r^: Christopher Wrens\
May. 4^th^ . 1678.

[…]

Ordered That the Iron Chest in the Gallery of Gresham\
Colledge be Opened upon Thursday next in the presence of a —\
Vice-president & any two of the Councel & and an Inventory\
be taken of what is conteined therein and that M^r^: Hunt do\
forthwith provide convenient padlocks for the same.

Ordered That M^r^: Hooke doe treat w^th^: M^r^: Lem ~\
concerning Chelsey Colledge & give an account of his proceedings\
at the next meeting of the Councel .

‌ Ordered That an Index be made of all the materiall\
things conteined in the Councel booke by the advice of y^e^ Secretarys.
& that a reasonable Allowance be made to a fit ⅌son to doe y^e^ Same.

‌ Ordered That M^r^: Henshaw M^r^: Hill & M^r^: Hooke\
be desired to goe to Chelsey Colledge & to get a Survey thereof\
Sometimes before Thursday come Seaven night .

\TranscriptionStop


**(CMO/1 p. 285: 213,285):** “Order for making a List concerning Debtors of Arrears” (CMO/1)

**(CMO/1/, v.1 p. 285:)** “Index to the Council Books. \| Orders to make one” (CMO/8A)

**(CMO/1, v.1 p. 285):** “[Iron Chest.] Inventory of its contents, ordered.” (CMO/8A)


### 30 May 1678 (CMO/1 p. 286;  CMO/1/244) Gresham College

\TranscriptionStart

[**Page 286:**]

[*two lines centered:*] At a Councel held at Gresham Colledge\
May. 30^th^ : 1678 .

[…]

\TranscriptionStop

### 6 June 1678 (CMO/1 pp. 286–287;  CMO/1/245) Gresham College

\TranscriptionStart

[*two lines centered:*] At a Councel held at Gresham Coledge\
Thursday. June. y^e^. 6^th^: 1678.

[…]

‌ Ordered That M^r^: Treasurer to pay unto M^r^: [**page 287:**]\
Robert Hook the Summe of Fortie five pounds for his\
Allowance as Curator of the Experiments of this Society\
for one year & an half ending the twenty fourth of this\
instant June.

‌ It being moved by M^r^: Hooke that M^r^: Joseph\
Lane Controuler of the chamber of London, who was lately\
Elected a Member of this Society , desired to be excused\
from the usuall payments thereof , upon his allegation that\
he would otherwise be ready to promote the designe & good\
of the Society , & to be assistant to them in matters of\
[*four line penciled vertical*] Law,  It was thought fitt & Ordered that the said M^r^\
Lane, should be excused from the said payments upon his —\
Subscribing the ingagement to ⅌forme the other Dutyes ~\
incumbent on a Fellow of this Society .

‌ Ordered That M^r^: Hooke be desired to go to the\
Library at Arundel house there to meet S^r^: W^m^: Dugdale\
Garter King at Armes And to receive his proposalls —\
concerning the bookes of that Library w^ch^: concerne ~\
Herauldry & Genealogy or the History of the Family\
of Norfolk w^ch^ : his Grace was pleased to reserved when\
he formerly granted the rest to the Society, and that\
S^r^: John Hoskins & M^r^: Hill & M^r^: Hall be alsoe —\
desired to accompany him upon Friday next at. 3. in the\
afternoone.

\TranscriptionStop


**(CMO/1 p. 287):** “[Norfolk library] M^r^. Hook to meet S^r^. W^m^. Dugdale ab^t^. Books reserved.” (CMO/1)

**(CMO/1 v. 1 p. 287):** “[Library.] Norfolk. M^r^ Hooke to meet D^r.^ W^m^. Dugdale at Arundel House \| about the books of Heraldry &c” (CMO/8A)

**(CMO/1 , v. 1 p. 287:)** “[Arundel Library] Minutes concerning the Books of Heraldry and Genealogies” (CMO/8A)

**(CMO/1 v. 1 p. 287):** “[Library. Norfolk.] Request to the Duke that his Library, given to the \| Society, may be removed to Gresham College” (CMO/8A)


### 13 June 1678 (CMO/1 p. 288;  CMO/1/246) probably Gresham College

\TranscriptionStart

[**Page 288:**]

[*Two lines centered:*] At a Council of the R : Society\
Thursday . 13. Jun[e. 1678 ]{.underline}

[…]

[*paragraph left penciled vertical*]\
‌ Ordered, That M^r^: Hall & M^r^: Hooke do attend\
his Grace the Duke of Norfolk , and intreat him from the\
Royal Society , that seing his Grace is now pulling downe\
his House , he will be pleased to Suffer his Library he —\
bestowed on this Society to be removed, to Gresham ~\
Colledge , where is room on purpose provided for the same :\
and that they also then deliver to his Grace the Catalogue\
of the said Library

[…]

\TranscriptionStop

### 26 December 1678 (CMO/1 pp. 288–290;  CMO/1/247) Gresham College

\TranscriptionStart

[*two lines centered:*] At a Councel of the R : Society held at\
Gresham College. Decemb^r^: 26 : [1678 .]{.underline}

[…]

[**Page 289:**]

‌ D^r^: Grew was put in minde to procure the Museums for\
the Library of the Society according to the desire & direction\
of the President .

‌ Ordered That the Treasurer do give to D^r^: Grew\
ten pounds as a gratuity for his Service to the Society as a\
Secretary voted by Ballot

‌ Ordered That the Treasurer do pay M^r^: Wicks one\
years Sallary ending at X^t^mas. 1677. voted by Ballott

‌ Ordered That D^r^: Grew may haue liberty to Borrow\
such of the Naturall Raritys of the Repository, as he shall\
haue occasion to describe and that he leaue a Catalogue of y^e^\
same with M^r^: Hunt till he returne them w^ch^: is to be w^th^: in\
one week. voted by the Ballot .

‌ Ordered That M^r^. Hooke be desired for the future\
to keep the Correspondence of the Society , and that the —\
same shall be continued by the help of a small Journall of\
some particulars Read in the Society. That the said ~\
Jornals shall not be sent or Sold to any one but such as -\
are members of the Society and such as correspond with the\
said M^r^: Hooke by the Societys directions & make con⹀\
siderable returnes to him for the Societys use, all which\
Returnes shall be constantly brought into the Society &\
read before them at the very next meeting after the receipt\
thereof .  It is further desired that M^r^: Hooke doe —\
draw up a Specimen of the said Jornall propounded by him\
against the next meeting of the Councel . voted by Ballot .

‌ A paper brought in by D^r^: Gale conteining Rules for\
the keeping of the Library were Read & approued being\
as followeth .

[**Page 290:**]

[*two lines centered:*] Orders concerning the Government of the\
Bibliotheca Norfolciana

1. That the long Gallery in Gresham college be the place for\
‌  the Library, if it may be procured .

2. That an Inscription in Letters of Gold be sett up in some\
‌   convenient place in honor of the Benefactor

3 . That there be an exact Catalogue of all the Bookes of the\
‌   Bibliotheca Norfolciana made apart, and also of all other\
‌   Bookes w^ch^ ; ſhall accrew.

4. That for securing the bookes & to hinder their being ~\
‌   imbezelled noe booke shall be Lent out of the Library\
‌   to any ⅌son whatever

5 That Such ⅌son or ⅌sons as shall desire to use any booke[*canceled:* ‘booke~~s~~’]\
‌   in the Library shall returne it into the hands of the\
‌   Library keeper intire & unhurt .

6. That the Library shall be surveyed once in the yeare\
‌   by a Committee chosen by the Council to the number of Six\
‌   any three of w^ch^ : to be a Coram .

[…]

\TranscriptionStop


**(CMO/1 p. 288):** “[Norfolk library] To this Duk: about removing the Library to Gresham Coll.” (CMO/1)

**(CMO/1 , v. 1 p. 288:)** “[Arundel Library] To be removed to Gresham College” (CMO/8A)

**(CMO/1 p. 289,295):** “Correspondence to be kept by M^r^. Hook and how.” (CMO/1)

**(CMO/1 p. 289: 162,289, v.2 57,237,239,243,244,251,258,263):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/1 p. 289):** “[Repository at Gresham College.] D^r^ Grew to have such things he wants to describe” (CMO/8A)

**(CMO/1 , v. 1 p. 289:)** “Correspondenc. M^r^. Hooke to keep it” (CMO/8A)

**(CMO/1 p. 289):** “Muſæum. D^r^. Grew to procure the Gresham Muſæum \| for a Library for the Royal Society” (CMO/8A)

**(CMO/1 p. 290):** “[Norfolk library] Orders concerning the Govermen^t^ of the Biblioth. Norfolciana” (CMO/1)

**(CMO/1 v. 1 p. 290):** “[Library. Norfolk.] Orders concerning” (CMO/8A)

**(CMO/1 v. 1 p. 290,292,314,315,319, v.2 p. 33,95,96,99,100):** “[Library.] Orders concerning” (CMO/8A)


### 23 January 1679 (CMO/1 p. 291;  CMO/1/248) Gresham College

\TranscriptionStart

[**Page 291:**]

[*two lines centered:*] At a Councel at Gresham College\
Jan^[ry]{.underline}^ ; 23 . 167$\frac{8}{9}$

[…]

Ordered That the Papers brought in by S^r^: Jn^o^: Hoskins\
& here ⅌used and amended be forthwith fairly ingrossed[*over:* ‘ingros’ed’]\
in Parchment , and the instrument Sealed, and carryed\
to the President . /

\TranscriptionStop


**(CMO/1 p. 292):** “[Library] Library Keeper his Duty & Obligation” (CMO/1)

**(CMO/1 v. 1 p. 292: 290,292,314,315,319, v.2 p. 33,95,96,99,100):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/1 , v. 1 p. 292:)** “[Hook M^r^. Robert.] Offer to him of Library keeper” (CMO/8A)

**(CMO/1 , v. 1 p. 292:)** “Librarian’s Office and Obligation” (CMO/8A)

### 27 February $\frac{8}{9}$ (CMO/1 pp. 292–293;  CMO/1/249) Gresham College

\TranscriptionStart

[**Page 292:**]

[*centered:*] Feƀr^ry^: 27. [167$\frac{8}{9}$]{.underline}

[*centered:*] The Duty & Obligacõn of the Library Keeper

1. The Library Keeper shall attend two dayes in y^e^ week.\
‌    On Tuesday & Thursday .\
‌    On Tuesday in the Morning from . 9 . to : 12.\
‌    On Thursday from . 9 . to : 12 . And on the ~\
‌     Afternoon from . 2. till y^e^ President take the Chaire

2. The Library-Keeper shall not Lend out any Book ,\
‌   without an Order of the Councel ; Or the President\
‌   or Vice President in the Chaire .

3. That the Library-Keeper shall make a ⅌fect ~\
‌  Catalogue of the Printed & Manuscripts Books, after\
‌  the most usuall method .

4. The Library Keeper shall use no fire nor Candle\
‌  in the Library .

5 He shall be provided alwaies of Pen, inck, & paper

6. He shall give such Security to the Society for the\
‌   keeping of the books as the Councel shall accept off .

Ordered That M^r^: Hooke haue time till Thursday\
next to consider of these Proposalls , in order to returne his\
final Answer.

‌ And upon his Refuseall, That M^r^: Perry haue the -\
Offer of the keeping the Library

[**Page 293:**]

[*two lines centered:*] Feƀr^ry^: 27 :^th^ 167$\frac{8}{9}$\
At a Councel at Gresham College

[…]

\TranscriptionStop

### 6 March 1679 (CMO/1 p. 293;  CMO/1/250) Gresham College

\TranscriptionStart

[*centered:*] March. 6^th^. 167$\frac{8}{9}$

[*two lines centered:*] At a Meeting of the Council\
at Gresham Colledge.\

‌ Ordered That M^r^: Hooke shall Employ\
Such person as he has proposed for the writing Letters\
as he Shall think fitt .

\TranscriptionStop

**(CMO/1 p. 293):** “[Letters.] That M^r^. Hook imploy a person for writing Letters” (CMO/1)

### 3 July 1679 (CMO/1 p. 294;  CMO/1/251) Gresham College

\TranscriptionStart

[**Page 294:**]

[*two lines centered:*] July. 3^d^. [1679]{.underline}\
At a Counell at Gresham Colledge

[…]

Ordered That M^r^: Hook shall haue power to\
Employ M^r^: Pappin for the writing of all Such letters\
as shall be Ordered to the Correspondents of the Society,\
And that all Such letter shall be written into a ~\
Letter book to be kept by the Sec^ry^: of the Society\
and that all the said Letters when faire written\
shall be shewne to such person of the Society as the\
Councell shall appoint from time to time for viewing\
of them before they be Sent to the Correspondents and\
after such ⅌usall shall be sent by the Secretary as -\
directed .  And that for so doing the Said  M^r^. Pappin\
shall receive from the Treasurer of the Society the\
Sumẽ of . 18.^d^ ⅌ letter unless[*flourish*] the letter shall exceed\
two sides of a quarter of a sheet of paper, for every of ~\
w^ch^: he shall receive two Shillngs, he produceing his bill of\
    the number of Such letters signed & attested by the Secret^[ry]{.underline}^:

‌ Ordered that the Letters being approued off\
by any two of the Councell shall be Sealed & Sent as aboue .

[…]

‌ Ordered and Desired That M^r^: Hooke may Publish ~\
(as he hath now declared he is ready to doe) a ſheet or two every fortnight\
of such Phylosophicall matters as he shall meet with from his Correspond^ts^:\
not making use of any thing conteind in the Register Bookes without\
the leaue of the Councel &[*over:* ‘or’] Author. -

\TranscriptionStop


**(CMO/1 p. 294):** “[Printers & Impressions] That M^r^. Hook may print every fortnight not making use of the Register books.” (CMO/1)

**(CMO/1 p. 294):** “[Letters. That M^r^. Hook imploy a person for writing Letters] M^r^. Pappin named, price aſſigned, to be entered, to be approued 🙰” (CMO/1)

**(CMO/1 , v. 1 p. 294:)** “[Hook M^r^. Robert.] To employ Mon^r^ Papin to write Letters to correspondints” (CMO/8A)  “[Hook M^r^. Robert.] May publish once a fortnight his correspondence” (CMO/8A)

**(CMO/1 , v. 1 p. 294):** “[Letter.] M^r^. Papin to write them” (CMO/8A)

**(CMO/1 , v. 1 p. 294,295):** “[Letter.] To Correspondents. Order concerning” (CMO/8A)


### 7 August 1679 (CMO/1 p. 295;  CMO/1/252) Gresham College

\TranscriptionStart

[*two lines centered:*] Aug^st^: 7 .^th^ [1679]{.underline}\
At a Councel at Gresham Colledge

[…]

‌ It is Ordered & desired That M^r^. Hook doe as soon as\
may be print a Relation of all the Experiments Observācons\
& Relations made & brought into the Society by himself\
since his first coming into this Society and that he haue —\
leaue to take his owne method in the doing thereof .

‌ It as also left to M^r^. Hooke to print the transactions he\
Designes to publish either once a moneth or once in a fortnight or oftener.

‌ It is Ordered that M^r^. Hook shall proceed with the corres⹀\
pondence & send away such letters as are already written & likewise\
such others as shall be written for the future & that M^r^. Hill shall\
take care to defray the charge of Postage both outwards & homewards.

‌ Ordered that S^r^: Jn^o^; Hoskins S^r^. Jonas Moore M^r^. Colwall\
M^r^. Hill D^r^. Gale D^r^. Brown D^r^. [*inserted:*] Grew D^r^: Mapletost or any\
three of Councell do meet together & go to M^r^: Cheaney to Discourse\
with him concerning Chelsey Colledge .  and that in the mean time S^r^.\
John Hoskins be desired to informe himself as fully as may be —\
concerning the title of this Societey to Chelsey Colledge and in —\
Order thereunto gett coppies of such Records as he conceives —\
necessary & are wanting .  And they they do meet at M^r^: Colwals\
house on Tower Hill on wensday next at . 3. in y^e^ afternoon precisely.

‌ Ordered That all papers that for the future shall be brought\
into the Society to be read may be returned to the said persons provided\
he desireth the same.

[*centered:*] Ordered That the following Bills be paid

----------------------------------------------------------------------  ------------
A Bill of D^r^. Grews for .7 . Lectures — .                             9 : 0 . 6

A Bill of M^r^. Martin for Lists Tickets &c —                           5 . 4 . 0

A Bill of M^r^: Wicks for Expences ending Xmas.1678—                    5 . 5 . 0

A Bill of M^r^. Wicks ending May. 29 . 1679 for writing &c              16 : 0 . 0

M^r^: Wicks Sallary for . 1$\frac{1}{2}$. year ending Midsum̃er. 1679—   15 : 0 . 0

The Station^rs^: Bill to July. 4^th^. 1679 — — — — .                    6 : 16 : 0 .
----------------------------------------------------------------------  ------------

\TranscriptionStop


**(CMO/1 p. 295):** “[Printers & Impressions M^r^. Hook] to print his own Exp^ts^. Observations & Relations after his own method.” (CMO/1)

**(CMO/1 p. 295):** “[Letters. That M^r^. Hook imploy a person for writing Letters M^r^. Pappin named, price aſſigned, to be entered, to be approued 🙰] that M^r^. Hook send him away.” (CMO/1)

**(CMO/1 p. 295: 289,295):** “Correspondence to be kept by M^r^. Hook and how.” (CMO/1)

**(CMO/1 p. 295, v.3 p.314):** “[Papers.] Read to the Society, may be returned, if asked” (CMO/8A)

**(CMO/1 , v. 1 p. 295: 294,295):** “[Letter.] To Correspondents. Order concerning” (CMO/8A)

**(CMO/1, v.1 p.295):** “Library. Of Geo. Ent. Esq^r^. presented to the Society” (CMO/8A)

**(CMO/1, v. 1 p. 295):** “Bills of Sundries, Ordered to be paid.” (CMO/8A)  “[Hook M^r^. Robert.] Ordered, that he publish all himself hath done” (CMO/8A) “[Hook M^r^. Robert. Ordered,] that he print the Transactions once a month” (CMO/8A) “[Hook M^r^. Robert.] That he proceed with his correspondence and be allowed postage &c.” (CMO/8A)

**(CMO/1 p. 295):** “Papers brought in to the Society to be read ſhall be returned if desired.” (CMO/1)


### 22 September 1679 (CMO/1 pp. 296–297;  CMO/1/253) Gresham College

\TranscriptionStart

[*two lines centered:*] Munday . Sept . 2[2^d^ . 1679. ]{.underline}\
At a Councel at Gresham Colledge.

[…]

‌ Upon Debate about the Printing the next transaction\
It was thought fitt that if M^r^. Martin should refuse to\
print the same as usually, the Councell should be acquainted\
with it at the next meeting to consider of some other means\
of doing it

‌ M^r^. Hook Relating that Mo^r^: Pappin was suddenly going for\
Paris and therefore mou’d them that in consideracon of the time he had\
Spent about entertaining the Society at their Meeting, & in writing\
certaine ꝉers he might be considered;  It was Ordered that the ~\
Treasurer should present. 5 . guineas and that if M^r^: Hill should\
not returne before his departure M^r^. Hook was desired to deliver\
it to him & receive them from M^r^: Hill againe .

‌ It was also further Ordered that M^r^: Hooke should —\
propose to him in the name of the Society 20^ꝉ^. a year certaine,\
and if threre can be a convenience found in Gresham Colledge -\
that he should haue a lodging also gratis and that the Society\
would further study to assist him

[…]

‌ D^r^: Gale may haue the use of the Manuscript of the life of\
Thomas of Becket for. 3 . weeks he giving a note for the Secure\
Returne thereof according to the Orders of Councell made in that\
behalfe .

[**Page 297:**]

[…]

\TranscriptionStop

**(CMO/1 p. 296):** “[Printers & Impressions] About printing the next tranſaction by M^r^ Martin” (CMO/1)

**(CMO/1 p. 296: 272,296,302):** “[Transactions] Orders about printing and publishing” (CMO/8A)


### 29 September 1679 (CMO/1 p. 297;  CMO/1/254) Christophen Wren’s House

\TranscriptionStart

[*centered:*] Sept 29^d^ . [1679 .]{.underline}

[…]

\TranscriptionStop


### 30 September 1679 (CMO/1 p. 298;  CMO/1/255) Gresham College

\TranscriptionStart

[**Page 298:**]

[*two lines centered:*] Tuesday. Sept . 30^th^ : [1679]{.underline}\
At a Councel held at Gresham Colledge

[…]

‌ It is Ordered & desired that M^r^: Perry —\
should forthwith Speak with M^r^: Everard ( the Execut^r^\
of George Ent Esq^r^. late Deceased) concerning the books\
left by the said M^r^: Ent for the Royall Society, and to\
take care to haue the said Bookes remoued to Gresham Colleg[*blot*]\
with all convenient Speed.  And that his Receipt shall be\
a sufficient Discharge .

‌ M^r^: Hook [*canceled:* ‘~~also~~’] Reporting that upon his making —\
the proposall of the Councel of the 22^th^ . last past to M^r^:\
Pappin of £.20 . ⅌ annu[*flourish*] certain for writing all letters for\
the Society, he had accepted the same , It was well —\
approued off.  But it was further thought fitt that —\
Articles should be drawne up to expresse the conditions\
expected by the Society to be ⅌formed by him, to w^ch^: he was\
to Subscribe. —

\TranscriptionStop

**(CMO/1 p. 298,303,310,323):** “Books promiſid to the Society’s Library” (CMO/1)

### 10 October 1679 (CMO/1 pp. 298–299; CMO/1/256) probably Gresham College

\TranscriptionStart

[*centered:*] October. 10^th^. [1679 .]{.underline}

‌Upon Debating the Propositions made by M^r^: Fobert\
concerning Chelsey Colledge w^ch^: were as followeth ,

[**Page 299:**]

‌ It was thought fitt & Ordered That the House &\
the five Acres whereon Chelsey Colledge doth Stand shall\
be Lett by Lease for . 41 . years to such persons as shall\
be ready & Ingage to deposite the Sumẽ of money required\
at the Annuall Rent of Thirty pounds, And It was further\
Ordered that M^r^. Fobert should be acquainted therewith.

‌ M^r^. Fobert being in the next room was call’d in, and\
acquainted with these Proposalls who readily concurred with\
them  And it was desired that care should be forthwith taken\
to make this Conveyance from the Society and that S^r^: John\
Hoskins would be pleasd to consult with\
Councell to that purpose

‌ At was further Ordered That a letter should be\
Sent to the President to acquaint him with this proceeding\
& to desire his concurrence & Assistance.

[…]

\TranscriptionStop

### 11 November 1679 (CMO/1 p. CMO/299;  CMO/1/257) Gresham College

\TranscriptionStart

[*two lines centered:*] Novemb^r^: 11. [1679]{.underline}\
At a Councel ot Gresham Colledge.

[…]

‌ M^r^. Hook to gett a paper fair drawne of M^r^: Foberts designe

‌ M^r^. Evelyn is desired to draw up a Letter to be sent to\
Such ⅌sons as are much in Arrear to the Society .

\TranscriptionStop


### 20 November 1679 (CMO/1 p. 300;  CMO/1/258) Gresham College

\TranscriptionStart

[**Page 300:**]

[*two lines centered:*] Novemb^r^: 20^th^: [1679]{.underline}\
At a Councell at Gresham Colledge

[…]

‌ S^r^: Chr. Wren read a letter drawn up by himself to\
be Sent to Such person as shall be agreed upon by the —\
Councells

‌ M^r^: Hooke was desired to enquire after M^r^: Rob: Reynols

S^r^. Chr: Wren . M^r^: Henshaw S^r^. J. Hoskins D^r^: Croone\
M^r^. Hooke were nominated a Comittee to Audit the Acco^t^: of\
the Treasurer .

[…]

\TranscriptionStop


**(CMO/1 p. 300):** “[Norfolk library] Catalogue of the Norfolck Library ordered to be printed” (CMO/1)

**(CMO/1 p. 300):** “[Arrears] to send letters.” (CMO/1)

**(CMO/1 p. 300,323):** “Accounts of the Treaſurer Com̄ittee ordered.” (CMO/1)

**(CMO/1 p. 300: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)

**(CMO/1 v. 1 p. 300):** “[Library. Norfolk.] Catalogue taken by M^r^. Perry, to be printed” (CMO/8A)

### 27 November 1679 (CMO/1 p. 300;  CMO/1/[259])

\TranscriptionStart

[*centered:*] Novemb^r^ . 27^th.^ . [1679.]{.underline}

‌ That S^r^. Chr. Wren be desired to ⅌fect the Letter\
he had drawn up to Send to the Fellows of the Society much\
in Arrear and that then Coppys of the Said letter be made to be\
Sent .

‌ That M^r^. Perry be desired to print his Catalogue\
of the Norfolk Library with an Epistle to the Society making\
mention of the Bounty of the Duke of Norfolke. / [ESTC R14407 GB-UkLoRS copies?]

\TranscriptionStop

### 8 December 1679 (CMO/1 pp. 301–302;  CMO/1/260) Joseph Williamson House

\TranscriptionStart

[**Page 301:**]

[*two lines centered:*] At a Councel of the Royall Society At y^e^ Presidents\
House.  Decemb^r^: 8^th^ : [1679]{.underline}

[…]

‌ Resolued That there shall be some one Subject pitched\
upon for the Society to proceed upon for the ensueing time\
as their main work til they are Satisfied concerning that —\
Subject

‌ And that within some reasonable time as a year, or soe —\
soone as they shall be satisfied it is brought to perfection , some\
thing concerning their Progresse therein shallbe published .

‌ That in pursuance of this Designe some one Experim^t^:\
shall be appointed by the Society at every meeting , to be shewn\
at the next meeting in prosecuon of that Subject so made —\
choice of .

‌ And That the seuerall Members present when the ~\
Experim^t^: is appointed be desired ag^st^ : the next day to consult —\
Such Authors as haue treated of the said Experim^t^: or the ~\
Subject in Debate, and to deliver in what they shall meet —\
with concerning it and also to Speak their owne Opinion of\
it

‌ That the first thing done at every meeting ſhall be —\
the Reading ouer of the notes of what was done the preceding\
meeting

‌ That a ⅌ticular & distinct Acco^t^ : and narrative of\
the Exper^t^: made the prceceding day shall be brought in —\
fair written by the Curator at the next meeting and be there\
read before the Society in order to its being Entred in the\
Register in case the Society think fitt so to direct

‌ That whereas it is found that seuerall Experiments\
made before the Society these late years haue been Entred [**page 302:**]\
onely in the Jornal Book & not in the Register, It is Ordered\
that all such Exp^ts^. shall be forthwith transcribed out of the\
Jornall Booke , and being first made ⅌fect & full by the\
Curator, be then fairly Entred in the Register

‌ That whoeuer is in the Chair the last Thursday of\
the month, do call to the Secretary & the Amanuensis ,\
And see , if aswell the Jornall , as the ⅌ticular Exp^ts^:\
‌    &c appointed to be Entred in the Regist^r^:\
be Entred accordingly and finding them to be So, that —\
under the paper or Jornal Entred he write .   Entred.

‌ And also at the same time that he view the Letter\
Books and See that the letters appointed to be Entred —\
be Entred accordingly.

‌ That the Secretarys take care to haue a Small acco^t^:\
of Phylosophicall matters, such as were the transactions by\
M^r^. Oldenburgh and under the same title published once\
a Quarter at least . That it be recommended to them to\
do it monthly if it may wellbe, but at least that it be\
done quarterly

‌ M^r^: Hooke being asked concerning the undertaking\
of this matter, Answered that he would See what he could\
do in it , but could not as yet undertake it absolutely.

\TranscriptionStop

### 10 December 1679 (CMO/1 pp. 302–304;  CMO/1/261) Joseph Williamson House

\TranscriptionStart

‌  December. 10^th^: [1679]{.underline}\
‌  A Councell of y^e^ Royall Society at y^e^ Præs^ts^: house

[…]

‌ It was Ordered That Mo^r^. Pappin should be discharged\
but that he should be allowed for the time till he be discharged and —\
paid accordingly

[**Page 303:**]

‌ M^r^. Evelyn Gaue an account that he had Spoken to my\
Lord Sunderland about sending the Societys Letters in the\
Secretarys pacquet,  And that his Lo^pp^. had readily consented\
thereunto .  M^r^. Evelyn was thanked by the Councel and was\
desired also to returne the Thanks of this Councel to the\
Lord Sunderland for this Grant . /

‌ M^r^: Hook was desired to gett Seuerall Exp^ts^. ready against the\
next Meeting by reason some Strangers would be present and M^r^.\
Hunt was to sumõn Seuerall of the Members to be present

‌ D^r^. Gale was desired to Send a ꝉre to the Seuerall —\
⅌sons beyond Seas who had formerly corresponded with the —\
Society , And ⅌ticularly to M^r^. Pullein concerning Sign^r^.\
[*after M inserted:*] Malpighi   , And to assure them of a constant correspondence\
for the future .

‌ D^r^. Gale . D^r^. Croone , D^r^. Allin M^r^: Hill M^r^. Hooke\
were appointed a Com^tee^. to see to haue a partition put up in the\
long Gallery of Gresham Colledge.

‌ D^r^. Grew menconed that M^r^. Mannering was going to\
the Indies and undertook to haue Barometers sent to the\
Indies & the Barbados with full directions for the use of them\
& to procure an Acc^t^: from thence of what observacons Should be\
made

‌ D^r^: Gale promised to give to the Society for their Library\
all the works of Scotus. [Dun Scotus?]

‌ S^r^: Robert Southwell promised to give Weckerus\
Discretis . [possibly Wecker, Johann Jacob?]

[*partially marginal:*] Ordered

‌ That a Paper booke be forthwith provided in which\
Shall be Entred all the transactions of the Councel

‌ That a perticuler booke be provided for Entring all\
Such Letters as are written by the Secretarys to the ~\
Seuerall Correspondents at home & abroad , and another for —\
coppying all Such answers & Returnes as come to the Society\
or Secretarys from abroad or from any part of England Scotland\
or Ireland .

‌ That a Com^tee^ : be appointed to view the former letter\
Books to See how ⅌fect the Entryes are , and to take an [**page 304:**]\
account of all loose letters recouered from the Administratix\
of M^r^. Oldenburgh & of those that haue come to the Sec^[rys]{.underline}^.\
since, and to take care that those that are not yet entred, be\
Entred forthwith & the Originals to be with all care gott\
together in order to their being preserved in such way as the\
Society shall direct .  M^r^: Hill, S^r^: John Hoskins D^r^: Gale\
D^r^. Croone or any two are desired to take this care as a Com^tee^:=
for that purpose.

‌ That a ⅌ticular Presse be provided to Sett in the\
Gallery appointed for the Library for the keeping of all\
the Letters papers & Books of Entries Jornalls Registers\
& all other written Books of the Society under the custody\
of the Secretarys . And that a note or List be made -\
of all such as at present are or hereafter shall be in their\
keeping for the future to be interchangeably Signed between\
the Præs^t^: and the Secretarys , And those Lists or Catalogues\
to be examined once a year, as at S^t^. Andrews day .

‌ That some of the best & most Entertaining Exp^ts^: produced\
before the Society , be Set apart & a List taken of them , to\
be at hand for the Entertainm^t^: of any ⅌son of quality &c that\
shall visit the Society .

‌ That a Com^tee^: of Exp^ts^: be appointed every year, to\
make a List of what Authors haue written of Physico ~\
Mathematicale matter  2^đly^ That they charge themselus —\
with one or more of the said Authors, & that it be recomended\
to others of the Society to take part of the Rest of Such —\
Authors for the reading them ouer & extracting out of them —\
the principall Exp^ts^: therein mencõned . 3^ly^. That all Such\
Experim^ts^. Soe Extracted be brought into the Com^tee^: for Exp^ts^:\
and by them considered of in Order to the presenting to the\
Society Such of them as they ſhall think fitt to offer to the\
Society to proceed upon .

\TranscriptionStop


**(CMO/1 p. 302,303,325):** “[Society Royal.] Minutes concerning their Books & Papers.” (CMO/8A)

**(CMO/1 p. 302: 272,296,302):** “[Transactions] Orders about printing and publishing” (CMO/8A)

**(CMO/1 p. 303):** “[Letters.] Forreighn Letters to be sent by \| the Secretarys Packit. D^r^. Gale to write to Forreighn correspondences.” (CMO/1)

**(CMO/1 p. 303: 298,303,310,323):** “Books promiſid to the Society’s Library” (CMO/1)

**(CMO/1 p. 303,304,309,318,319):** “Orders for the ſeveral conveniences to be made in Greſham Colledge for the Library 🙰” (CMO/1)

**(CMO/1 p. 303: 249, 303):** “Presents to the Society \| Minutes” (CMO/8A)

**(CMO/1 , v. 1 p. 303:)** “[Correspondence.] Resolutions concerning” (CMO/8A)

**(CMO/1 p. 303):** “[Society Royal.] Letters sent abroad in the Secretary’s packet.” (CMO/8A)

**(CMO/1, v. 1 p. 303):** “[Library.] Books promised to be given” (CMO/8A)

**(CMO/1 p. 304):** “Preſs ordered for keeping Papers, Books, of Entries Journals 🙰.” (CMO/1)

**(CMO/1 p. 304:** “[Letters.] Letters 🙰 in M^r^ Oldinburgs hands (lately dead) to be retrieved.” (CMO/1)

**(CMO/1 p. 304: 303,304,309,318,319):** “Orders for the ſeveral conveniences to be made in Greſham Colledge for the Library 🙰” (CMO/1)

**(CMO/1 , v. 1 p. 304:)** “[Books & Papers of the Royal Society] Committee appointed to examine them.” (CMO/8A)


### 17 December 1679 (CMO/1 p. 305;  CMO/1/262) Joseph Williamson House

\TranscriptionStart

[**Page 305:**]

[*centered:*] Decemb^r^: 17^o^ . [1679]{.underline}

[*two lines centered:*] A Meeting of the Councel of the Royall Society\
at the Præsidents House

[…]

‌ M^r^. Hill, D^r^: Gale, D^r^: Crone & S^r^. J : Hoskins\
are appointed a Com^tee^ : to meet at M^r^: Hookes Lodging on\
Munday next to see the Letters in his custody

‌ That a List be made by the Sec^[rys]{.underline}^. of all Lẽrs that\
haue come to their hands during the whole year, and at the end\
of each year to deliver in such a List to the President

‌ M^r^: Hooke was desired to continue the Phylosophicall\
Collections

‌ D^r^. Gale was desired to undertake all fforreigne ~\
correspondence w^ch^. he accordingly did . as also that he would\
forthwith write to Malpighius Borrelli & M^r^. Pullein .\
and that he would Apologize with the Correspondents for the\
Defect of Returnes w^ch^: Should haue been made from hence .

[…]

\TranscriptionStop

**(CMO/1 p. 305):** “[Letters.] A list of the whole years letters to be delivered to the Pres^t^.” (CMO/1)

**(CMO/1 p. 305):** “[Correspondence to be kept by M^r^. Hook and how.] Forreighn correspondences by D^r^ Gale & with whom” (CMO/1)

**(CMO/1 , v. 1 p. 305:)** “[Hook M^r^. Robert.] Desired to continue the Philosophical Collections” (CMO/8A) “[Books & Papers of the Royal Society] Committee appointed to examine them.” (CMO/8A) “[Correspondence.] Resolutions concerning” (CMO/8A)


### 14 January 1680 (CMO/1 p. 306;  CMO/1/263) Joseph Williamson House

\TranscriptionStart

[**Page 306:**]

[*two lines centered:*] Jan^ry^: 14^th^ . [1679]{.underline}\
A Councel at the Præsidents

[…]

‌ M^r^: Hill & D^r^. Gale hauing Spoken with\
M^r^. Pouey concerning Mons^r^ : Fauberts treaty about\
Chelsey Colledge, Reported that M^r^. Pouey was yet in\
hope that M^r^. Faubert would go on with his Designe\
& that he would Speak with M^r^. Faubert & Returne his\
positive Resolucõn upon that concerne.

[…]

‌ M^r^: Hunt was call’d in & demanded what he would expect\
for employing all his time in the Service of the Society , ~\
whereupon he said he would Referr himself for his Reward\
to the Councel, but said he had been Informed that M^r^. —\
Shortgraue had Sometimes been allowed . £ . 50 . ⅌ annum\
Whereupon. £. 40. ⅌ annu[*flourish*] being proposed to him in full for\
all businesse done for the Society , the ten pounds already —\
allowed him being part thereof, he readily accepted thereof .

\TranscriptionStop

### 21 January 1680 (CMO/1 p. 307;  CMO/1/264)  Joseph Williamson House

\TranscriptionStart

[**Page 307:**]

[*two lines centered:*] Jan . 21.^th^ 16[79]{.underline}\
A Councell at the Presidents

[…]

[*a new hand ff. with diagonal slashes ever few lines:*]\
‌ The business of the new [*inserted:*] Atlas  undertaken by M^r.^\
Pitts was debated, And it was resolved that M^r^ Pitts should\
be desired to give a Meeting with these Members of the\
Society who are concerned and named in that designe and\
to take an Account of the present State and progreſs of\
the worke ~

‌ D^r^ Crone made a proposal as from M^r^ Roſsig̃ton[*over:* ‘Reſsig̃ton’]\
concerning Chelsey Colledg as followeth Viz that he had\
treated with M^r^ Roſsington about the said Colledge and\
received this proposall that he was willing to take a lease\
of the Colledg and Ground thereunto belonging being about\
6 Acres for 61 years and had proferred 30^~~l~~^ ⅌ And and\
to be Obliged to lay out in substantiall building upon the\
premiſses the sume of [*blank*]

[*… further negotiations*]

‌ D^r^ Crone proposing from M^r^ Collins that the said\
M^r^. Collins was ready to print Two Volumes of Algebra\
written by D^r^ Wallis M^r^. Baker M^r^ Newton &c provided [**page 308:**]\
the Society would engage to take off 60 Copies after\
the rate of 1^l^ ⅌ sheet  It was Ordered that M^r^\
Collins should be desired to make his proposall in\
writing and that then the Society would further con⹀\
⹀sider of in incourageing his undertakeing —

\TranscriptionStop

**(CMO/1 p. 307):** “Algebra printed by M^r^. Collins” (CMO/1)

**(CMO/1 p. 307):** “Atlas by M^r^ Pitt” (CMO/1)

**(CMO/1 p. 307):** “[Wallis John D.D.] Proposal about his History of Algebra” (CMO/8A)

**(CMO/1 , v. 1 p. 307:)** “Collins Mr^r^. John. Proposed to print some books of Algebra” (CMO/8A)  “Geography. Resolutions about Pett’s Atlas.” (CMO/8A)


### 23 February 1680 (CMO/1 pp. 308–309;  CMO/1/265) Gresham College

\TranscriptionStart

[*two lines centered:*] Feb. 23^d^. 16[79]{.underline}\
A Councell at Greſham Colledge

[…]

‌ Ordered That M^r^ Hunt doe prepare a silver\
box for M^r^ Lewenhooks Diploma.

‌ Dr Gale desired to gett a Table fairly written\
with the Inscription he now produced acknowledging\
the noble bounty of the Duke of Norfolk in bestowing\
the Norfolcian Library on this Society to be hung up\
in the Gallary where the said Library is now planed

‌ It was moved That the Duke of Norfolk should\
be solicited that hee would please bestow bestowe his picture\
to be preserved in the Library —

‌ M^r^ Colwall was at the same time desired by the\
Commitee to bestowe his picture upon the Society to be\
kept constantly in the Repository which he though\
with much modest reluctancy promised to doe —

[**Page 309:**]

‌ Ordered That there shall be provisiõ made for\
all the Manuscripts of the Norfolcian Library

‌ \[*scraped:* ‘M^r^ Hooke’\] \[*canceled:* ‘~~D^r^ Grew~~’\] read a Proposall of his about pro⹀\
⹀curing Subscriptions for incouragemt of his under⹀\
⹀taking to print a Catalogue of the Naturall and\
Artificiall Curioſsities of the Society He was\
encouraged to bring in his Proposalls on Thursday\
next for Subscriptions —

‌ The matter of the new Diurnall to be printed\
in halfe a sheet of Paper was debated

‌ To meet upon this matter on Satturday morn⹀\
⹀ing at 10 in the Morning —

‌ M^r^ Bates Bill for the particõn in the Gallery\
was past

\TranscriptionStop

**(CMO/1 p. 308):** “The Pictures of the Duke of Norfolk & M^r^. Colwal desired by y Soc.” (CMO/1)

**(CMO/1 p. 308):** “Duke of Norfolk to be desired for his Picture” (CMO/1)

**(CMO/1 p. 308):** “[Pictures or Portraits.] \| Of the D. of Norfolk & M^r^ Colwall requested” (CMO/8A)

**(CMO/1 v. 1 p. 308):** “[Library. Norfolk.] Order for the Donation to be written in a Table” (CMO/8A)

**(CMO/1 p. 308):** “A table inſcribed acknowledging his Bounty in bestowing the Library on the Society” (CMO/1)


### 28 February 1680 (CMO/1 p. 309;  CMO/1/266) probably Gresham College

\TranscriptionStart

[*two lines centered:*] Feb:28^th^: 1679\
A meeting of a Comittee of the Councill

[…]

‌ The heads of the Philosophicall Gazett wer\
discoursed and some heads sett downe 

‌ M^r^ Hooke was desired to make a Tryall of one

\TranscriptionStop


**(CMO/1 p. 309):** “D^r^. Grew about printing his Book of the Natural & artificial Curioſities of the R. Society & subſcriptions.” (CMO/1)

**(CMO/1 p. 309: 303,304,309,318,319):** “Orders for the ſeveral conveniences to be made in Greſham Colledge for the Library 🙰” (CMO/1)

**(CMO/1 p. 309):** “Philosophical Gazett Discoursed about.” (CMO/8A)

**(CMO/1 p. 309):** “Duirnal debated” (CMO/1)

**(CMO/1 p. 309):** “Philosophical Gazett discoursed & some hiadsil down for tryal.” (CMO/1)

**(CMO/1 , v. 1 p. 309:)** “[Grew Nehemiah M.D.] Proposal for printing the curiosities of R.S.” (CMO/8A)


### 23 November 1680 (CMO/1 pp. 310–311;  CMO/1/267) probably Gresham College

\TranscriptionStart

[**Page 310:**]

[*three lines centered:*] November 23 : 1[680 .]{.underline}\
A meeting of y^e^ Councell of the Royall\
Society .

[…]

‌ Ord^r~~d~~^ That a Paper brought in by M^r^. Hill con⹀\
⹀cerning the Books of M^r^ Ent delivered to the Society\
by M^r^ Everard being as followeth viz^l^

‌ Whereas Geo: Ent of the Middle Temple Esq^r^\
deceđ did give his BOoks in the Catalogue hereunto\
anexed to the Ro^ll^ Society of London and made M^r^ W^m^\
Everard of Miđđx gent the Excuto^r^. of his laſt Will &\
Testam^t^ These are to acknowledg the receipt of the s^d^\
Books by order of the Roy^ll^ Society And the s^d^ Royall\
Society doth hereby promise that if any contreversy ſhall\
arise in Law which may alter the poſsion of the afors^đ^\
Bookes That then they will returne the ſ^d^ bookes to the\
said M^r^ W^m^ Everard as he hath now delivered them

[*partially marginal:*] Dated the    day\
‌ of      168

‌ It is agreed unto by this Councell and ord^đ^ that a Copie\
thereof be delivered to M^r^ Everard to be signed by the [*uncertain:*] same

[**Page 311:**]

[…]

\TranscriptionStop

**(CMO/1 , v. 1 p. 310:)** “Books presented to the Society.” (CMO/8A)

**(CMO/1 p. 310: 298,303,310,323):** “Books promiſid to the Society’s Library” (CMO/1)

### 8 December 1680 (CMO/1 p. 311;  CMO/1/268) probably Gresham College

\TranscriptionStart

[*centered:*] December 8 . 1680

[…]

Ordered , That a Book entitled [The Digister , or the]{.underline}\
[description of an Engine for ſoftening of Bone]{.underline}s written\
by Denys Pappin D^r^. of Physick and Fellow of this\
Society , be printed and published .

[…]

‌ That the Secretary do send for M^r^. Newton in\
answer to his letter. That the Society giue their\
consent for the Italian to dedicate his book &c.

‌ That D^r^. Gale do write a letter to S^r^. Joseph\
Williamson in answer to his letter to S^r^. John\
Lowther. [*downward flourish*] 

\TranscriptionStop


**(CMO/1 p. 311):** “[Printers & Impressions] the Digester M^r^. Pappins book to be printed” (CMO/1)

**(CMO/1 p. 311):** “[Printing of Papers & Books.] Papin’s Digestor. Ordered” (CMO/8A)

**(CMO/1 , v. 1 p. 311):** “[Letter.] D^r^. Gale to write to D^r^. Joseph Williamson” (CMO/8A)

**(CMO/1 , v. 1 p. 311):** “[Letter.] The Secretary to write to M^r^. Newton” (CMO/8A)


### 12 January 1681 (CMO/1 p. 312;  CMO/1/269) probably Gresham College

\TranscriptionStart

[**Page 312:**]

[*centered:*] January 12 . 1680 .

[…]

S^r^. Christopher Wren sworne President .

The same deputed Sir John Hoskins Vice-President for\
the year ensuing.

‌ Si^r^. John Hoskins sworne Vice-President [*downward flourish*]

\TranscriptionStop


### 19 January 1681 (CMO/1 p. 312;  CMO/1/270) probably Gresham College

\TranscriptionStart

[*centered:*] Jan : 19 : [168$\frac{0}{1}$]{.underline}

[…]

‌ The Presid^t^ mov’d that there might be An\
Anatomicall Comittee

‌ D^r^ Crone Objected the Colledge of Phisicians —

[*margin:*] 2ly a Georgicall Comittee—

[*margin:*] 3 A Cosmographicall Comittee to Register all notable\
things concerning any thing that shall be remarkable

‌ Resolved That there shall be these Three Com̃ittees\
nominated and appointed and that the presid^t^ shall\
take care to think of fitt persons to be on the said\
Comittees and to draw up some dirccõns for what\
they are to consider of . —

\TranscriptionStop


**(CMO/1 p. 312,321):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Is President” (CMO/8A)

**(CMO/1 p. 312: 14,108,113,114,187,231,276,312):** “[Vice Presidents] Appointed” (CMO/8A)

**(CMO/1 p. 312,321):** “[Presidents of the Royal Society] Sir Christopher Wren — 1680” (CMO/8A)


### 23 March 1681 (CMO/1 pp. 313–314;  CMO/1/271) probably Gresham College

\TranscriptionStart

[**Page 313:**]

March. 23^d^. 1[680 :]{.underline}

[*centered:*] Att a Councill of the Royall Society

[…]

‌ M^r^ Hooke & M^r^ Aston are desired to repaire to M^r^ Lane\
and give him his Fee to peruse all our Writings and to make\
an orderly Abstract especially of these relating to our Title to\
Chelsy Colledg and Land and that when they be orderly placed\
according to the Statutes in our Chest together with our [**page 314:**]\
other Writing now ni M^r^ Hills hand concerning our\
Fee farmes and that M^r^ Lane be asked in any of the\
writings be neceſsary to be Enrolled and that it be ac⹀\
⹀cordingoy done if he advise it .

‌ M^r^ Hunt is ordered to make a good and ⅌fect\
Survey and Mapp in Vellum of our Lands in Chelsy.

‌ That in regard the Catalogue of our Bookes is\
now finished it is requisite to overlooke all our\
Orders concerning the Library and Register than to\
the end a Statute may be formed out of them to enter\
into the Statute Booke and M^r^ Perry is desired to take\
M^r^ Hunt to him and direct him and imploy him from\
day to day to sort the Bookes and M^r^ Weekes to worke\
out Tables upon Card pastboards to affix to the\
Outside of the preſses —

‌ The Question being putt whether we ſhould\
choose a Printer to the Society, and divers Objecting\
that it was rather prejudiciall then otherwise to the\
Society it was carried in the Affirmative but soe as\
the Debate, may be resumed, if new reasons offer to\
the contrary , The consideracon of the person is ~\
deferred to the next Councill

\TranscriptionStop

**(CMO/1 , v. 1 p. 313:)** “Deeds &c. of the Society. Abstracts of them Ordered” (CMO/8A) \index{Orders to write+Deeds etc.}


### 13 April 1681 (CMO/1 pp. 314–315;  CMO/1/272) probably Gresham College

\TranscriptionStart

[*centered:*] Arpill 13^th^ [1681]{.underline}

[…]

[**Page 315:**]

[…]

‌ Ord:^d^ That the small Charter bookes of the —\
Society's Statutes and Charter be carried to M^r^\
Lane, as also some Queries As 1^st^ Whether wee may\
Demolish any part of the building now ſtanding ~\

‌ Secondly Whether wee may sell dispose and\
any waies Alienate any part of the said materialle

‌ Ord^d^ That M^r^ Wirks makes a Copie of\
all the Orders that have been made concerning\
the Library to be delivered to the next Councill

‌ M^r^ Perry Presented the Councill with [*canceled:* ‘~~13~~’] 13\
Printed Catalogues of the Library of this Society\
for which and his care and great paines in composing —\
the same hee had the thanks of the Councill ~

‌ Doct^r^. Gale is desired to speak to S^r^ William\
Dugdale in order to procure the Copie of the Doomes\
⹀day Booke now in the Heralds Office —

‌ Vupon resuming the Debate whither a Printer\
should be chosen for the Society  It was Resolued\
in the Affirmative and M^r^ Richard Chiswell was\
unanimously chosen

‌ M^r^ Colwall desired That the Drawers of the —\
Repoſitory might be placed in the way of Shelves\
which was agreed to —

‌ Ord^~~d~~^. That M^r^. Chiswells Charter be made by\
M^r^ Weeks against next Meeting .

‌ Ord^d^. That some Catalogues well bound at the charge of\
the Society be provided to be presented as followeth .\
‌   To my Lord Duke of Norfolk\
‌   My Lord Arundell\
‌   My Lord Thomas\
‌   M^r^ Charles Howard\
‌   M^r^ Edw^d^ Howard .

‌ The said 5 Bookes to be left with S^r^ Theodore De Vaue [i.e. Vaux]

\TranscriptionStop

**(CMO/1 p. 314,315):** “Orders concerning the Library” (CMO/1)

**(CMO/1 p. 314):** “[Printers & Impressions] Question whither a printer should be chose carried in the Affirm.” (CMO/1)

**(CMO/1 p. 314):** “[Library] Orders concerning the Library to be registred” (CMO/1)

**(CMO/1 v. 1 p. 314: 290,292,314,315,319, v.2 p. 33,95,96,99,100):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/1 p. 314,315,316,321):** “[Printers to the Society.] Considerations about, & appointment” [TODO(check CMO/1) is this Hickman probably not, but if so put that back] (CMO/8A)

**(CMO/1 p. 315):** “Repository to have drawers placed in the way of shelves.” (CMO/1)

**(CMO/1 p. 315):** “[Printers & Impressions] Resolved & M^r^. Rich. \| Chiswell chosen.” (CMO/1)

**(CMO/1 p. 315):** “A Printed Catalogue to be presented to the Duke 🙰 of the ſame.” (CMO/1)

**(CMO/1 p. 315):** “[Library] Catalogue of the Library preſented to y^e^ Society” (CMO/1)

**(CMO/1 p. 315: 314,315,316,321):** “[Printers to the Society.] Considerations about, & appointment” [TODO(check CMO/1) is this Hickman probably not, but if so put that back] (CMO/8A)

**(CMO/1 v. 1 p. 315: 290,292,314,315,319, v.2 p. 33,95,96,99,100):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/1 v. 1 p. 315):** “[Library. Norfolk.] Printed Catalogue. 13 Copies presented to Society” (CMO/8A)

**(CMO/1 v. 1 p. 315):** “[Library. Norfolk. Printed Catalogue.] 5 Copies Ordered to be presented to the Norfolk Family” (CMO/8A)


### 27 April 1681 (CMO/1 pp. 316–317;  CMO/1/273) probably Gresham College

\TranscriptionStart

[**Page 316:**]

[*centered:*] April 27 . 1681 .

[…]

‌ S^r^. John Hoskyns is desired to peruse the Statute\
relating to the Printer and to draw up another\
Patent for M^r^. Chiswick in which the powers given\
the Printer may be leſse diſadvantageous to the Society\
then ( as by experience we haue found ) the Patent given\
formerly to Martin and Allestree appeared to be; a\
copy of which Charter M.Wicks is to bring S^r^. John Hoskyns.

‌ A Proposition being made by one M^r^. Thomas Hutton\
to become Tenant for Chelsey-college , in order to erect\
there a new Manufacturer of Paper ; it is referred to a\
Committee , S^r^. John Hoskyns , M^r^. Hill , M^r^ Hook with Sir\
Christopher Wren , to meet this evening .

‌ S^r^. John Hoskyns is added to the Com̃ittee appointed March [**page 317:**]\
the 23^th^ . to discourse M^r^. Lane about our Writings , and\
also to enquire of him or any learned in the Law\
concerning the right of Copies , upon D^r^. Gales motion\
that M^r^. Chiswell would reprint the Hiſtory of the Royal\
Society and other things , if he knew where the Title —\
were .

‌ D^r^. Gale having informed the Council , that there was\
an excellent Library of the sort , which was one Smith’s\
and he beleived the Widow would be willing to deposite\
them with us , or at least upon easy termes : D^r^. Gale\
is desired to proceed in his inquiry according to his\
direction and make further report .

‌ D^r^. Gale aſserts My Lord Berkely's books probably were\
not yet diſposed of ; He is desired to enquire further\
and acquaint the President .

‌ M^r^. President desired M^r^. Hill to giue in a paper to the\
President of the state of our income , how the salaries\
are diſposed , and what the other expenses haue been of\
late years ; and also a Medium of the Payments how\
the haue amounted per annum the last 5 . or 7 years ,\
in order the better to state and proportion our Expences. [*downward flourish*]

\TranscriptionStop

**(CMO/1 p. 316):** “[Printers & Impressions] printed Catalogue of the Library presented & Patent to be drawn for M^r^. Chiſwell with limitations” (CMO/1)

**(CMO/1 p. 316: 314,315,316,321):** “[Printers to the Society.] Considerations about, & appointment” [TODO(check CMO/1) is this Hickman probably not, but if so put that back] (CMO/8A)


### 4 May 1681 (CMO/1 pp. 317–318;  CMO/1/274) probably Gresham College

\TranscriptionStart

[*centered:*] May 4 . 1681 .

[…]

M^r^. Hill reports from the Dean of Canterbury, that the\
Books which wee hoped might haue been obtained\
from my Lord Berkly were certainly diſposed of [**page 318:**]\
and legally settled ; and that he had ſpoken to My Lord of\
Berkly who seemed ſorry he had not been sooner put\
in mind of it , before he had been engaged .

‌ M^r^. Hill brought in an Account in general of our\
Income and Charges for some late years , as he was ordered ,\
to the end the Council might consider of new modelling\
the affairs and expences of the Society . After several\
diſcourses , the debate was adjourned till the next Council ,\
and the President is desired to bring in some Propositions\
in writing .

‌ M^r^. Hunt is ordered without further delay to surveigh\
the Land at Chelsey College ( as he was formerly ordered\
Novemb . 23 . last ) to the end the Tenants may haue their\
leases sealed accordingly : And S^r^. John Hoskyns, M^r^. Hinshaw\
and M^r^. Hook are appointed a Committee to cauſe draughts\
to be made of the lease to Thomas Franklyn of the Arable\
and meadow , according as M^r^. Henshaw had formerly treated\
with him , viz^l^. in Novemb . last ; and that they do truly\
expreſse the names and boundaries , according to our\
original papers , in which (after the surveigh made)\
they are desired to inspect the words of the Grant and\
other papers neceſsary .  And the said Com̃ittee are to haue\
Copies of this Order , and make report thereupon at the\
next Meeting . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 317):** “[Printers & Impressions] Q. about the Right of Copies in order to reprinting Hist. R. Soc. &c” (CMO/1)

**(CMO/1 , v. 1 p. 317:)** “Deeds &c. of the Society. Abstracts of them Ordered” (CMO/8A) \index{Orders to write+Deeds etc.}

**(CMO/1, v.1 p. 317,318,319, v. 2 p. 212, v. 3 p. 336):** “[Library.] Deliberations about the sale of some Books, Duplicates &c.” (CMO/8A)


### 18 May 1681 (CMO/1 pp. 318–319;  CMO/1/275) 

\TranscriptionStart

[*centered:*] May 18. 1681 .

[…]

‌ Ordered , that seven pair of preſses more be made to fill up [**page 319:**]\
the Southside of the Library , and M^r^. Perry is desired to\
contiriue (which is computed may be done ) that the whole\
Arundel-library may be contained in that side , and M^r^. Ent's\
books to be placed between the windows , in order to commence\
the Library of Philosophical books on the window-side.

‌ Ordered , that M^r^. Hunt do provide a ſtamp according to\
direction for the new books and such as are ordered to be\
bound .

‌ Ordered , that M^r^. Hill M^r^. Aston and M^r^. Perry be a Com^te^.\
to enquire what books are lent abroad and to send for\
them and diſpose them in the Library , and M^r^. Perry is\
to add them to his Catalogue. [*over:* ‘Catalogs’]

‌ Ordered , that Obligations be printed and boung together\
in a book , one sort with the penalty of ten pounds for\
printed books , another sort of Fifty poundſ for Manuscripts\
which obligations are to remain in the Librarian's hands\
and to be ſigned and ſealed by the Borrower and cancelle ,\
(when the book is returned) by the Librarian

‌ Ordered, That Si^r^. John Hoskyns , S^r^. John Lowther , M^r^. Henshaw,\
M^r^. Hill , D^r^. King and M^r^. Hook be a Com̃ittee to meet at\
Man's Coffee-house on Friday next , half an hour after\
three , in order to go by water to Chelsey-college to discourse\
the Neighbourhood there in relation to our interest in the\
Publique way

‌ M^r^. Hunt brought in the Map , which is ordered to be kept\
with the Charter and other writings.  And M^r^. Hunt is to\
attend the Committe at Chelsey on Friday next with the\
said Map .

‌ Ordered , that M^r^ Lane be added to the Com̃ittee appointed\
May 4 . for Sealing the writings , and M^r^. Hook is desired to\
act in this affair to get a meeting of M^r^. Lane with Sir\
John Hoskyns out of hand .

[…]

\TranscriptionStop


**(CMO/1 p. 318: 303,304,309,318,319):** “Orders for the ſeveral conveniences to be made in Greſham Colledge for the Library 🙰” (CMO/1)

**(CMO/1, v.1 p. 318: 317,318,319, v. 2 p. 212, v. 3 p. 336):** “[Library.] Deliberations about the sale of some Books, Duplicates &c.” (CMO/8A)

**(CMO/1 p. 319):** “Obligations for books borrowed out of the Library.” (CMO/1)

**(CMO/1 p. 319):** “[Library] Obligations to be taken for Books lent & how” (CMO/1)

**(CMO/1 p. 319: 303,304,309,318,319):** “Orders for the ſeveral conveniences to be made in Greſham Colledge for the Library 🙰” (CMO/1)

**(CMO/1, v.1 p. 319: 317,318,319, v. 2 p. 212, v. 3 p. 336):** “[Library.] Deliberations about the sale of some Books, Duplicates &c.” (CMO/8A)

**(CMO/1 v. 1 p. 319: 290,292,314,315,319, v.2 p. 33,95,96,99,100):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/1, v.1 p. 319):** “[Library.] Stamp for the Books Ordered” (CMO/8A)


### 1 June 1681 (CMO/1 p. 320;  CMO/1/276) 

\TranscriptionStart

[**Page 320:**]

[*centered:*] June 1 . 1681 .

[…]

\TranscriptionStop

### 22 June 1681 (CMO/1 pp. 320–321;  CMO/1/277) 

\TranscriptionStart

[*centered:*] June 22 . 1681 .

[…]

[**Page 321:**]

[…]

‌ Ordered, That all Orders made by the President during\
his Presidentſhip shall continue till Christmas following\
S^t^. Andrews day .

‌ In consideration of Propositions made by M^r^. Hook for\
a more sedulous prosecution of the Experiments for the\
ſervice of the Society, and particularly the drawing up\
into Treatises several Excellent things which he had\
formerly promised the world ; The Council as Encourage⹀\
ment , according to the poor abilities of the Society , haue\
agreed to adde forty poundſ for this year ending at\
Christmas to M^r^. Hook’s ſalary. [*downward flourish*]

\TranscriptionStop

### 29 June 1681 (CMO/1 pp. 321–322;  CMO/1/278) probably at Gresham College

\TranscriptionStart

[*centered:*] June 29 . 1681 .

[…]

M^r^. Chiswell's Patent to be Printer to the Society being fairly\
engroſsed , was read and approved .

[…]

\TranscriptionStop

**(CMO/1 p. 321):** “[Printers & Impressions] Chiswells Pattent read & approved” (CMO/1)

**(CMO/1 p. 321: 312,321):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Is President” (CMO/8A)

**(CMO/1 p. 321: 314,315,316,321):** “[Printers to the Society.] Considerations about, & appointment” (CMO/8A)


### 27 July 1681 (CMO/1 p. 322;  CMO/1/279) probably Gresham College

\TranscriptionStart

[**Page 322:**]

[…]

[*centered:* July 23 [. 1681 .]{.underline}

[…]

‌ Resolved , that a legal course shall forthwith be —\
taken , for recovering the Arrears of the Society ; and\
that the President be desired forthwith to speak with\
M^r^. Ballard in order to haue it forthwith diſpatched . [*downward flourish*]

\TranscriptionStop

### 5 October 1681 (CMO/1 pp. 322–323;  CMO/1/280) probably Gresham College

\TranscriptionStart

[*centered:*] October 5[ . 1681 .]{.underline}

[…]

\TranscriptionStop

### 16 November 1681 (CMO/1 pp. 323–324;  CMO/1/281) probably Gresham College

\TranscriptionStart

[**Page 323:**]

[…]

[*centered:*] November 16 . 1681 .

[…]

Agreed and ordered , that S^r^. Christopher Wren , S^r^. John\
Lowther , M^r^. Colwall , M^r^. Aston and M^r^. Hook be a Com̃ittee\
in prosecution of the Statute , in that case made , to\
examine and audite the Treasurer's Accomyts : And\
they are desired to meet and performe the same, before\
the next Anniversary-Election .

‌ The president diſcoursing concerning the Library of\
the Society , promised to giue fiue poundſ to the Society ,\
to be expended in books of Geometry ; and M^r^. Hook was\
desired to find out such books as he should find were\
fit , and not already in the Library .  And the Council [**page 324:**]\
taking into consideration the improuement of the Library ,\
thought fit to order , that there should be annually\
expended in purchasing Philosophical Books the sum\
of Ten pounds . [*downward flourish*]

\TranscriptionStop


**(CMO/1 p. 323: 298,303,310,323):** “Books promiſid to the Society’s Library” (CMO/1)

**(CMO/1 p. 323: 300,323):** “Accounts of the Treaſurer Com̄ittee ordered.” (CMO/1)

**(CMO/1 p. 323: 66,79,101,112,113,140,144,173,175,195,201,206,212,223,244,265,274,282,300,323):** “Treasurer’s Accounts Audited \| Auditors chosen.” (CMO/8A)


### 23 November 1681 (CMO/1 p. 324;  CMO/1/282) probably Gresham College

\TranscriptionStart

[*centered:*] Novemb . 23[ . 1681 .]{.underline}

[…]

M^r^. Hill brought in the Conveyance from S^r^. John Banks\
for a Fee farme - rent of twenty four pounds per annum ;\
which was ordered to be put into the Chest .

[…]

\TranscriptionStop


### 7 December 1681 (CMO/1 pp. 324–325;  CMO/1/283) 

\TranscriptionStart

[*two lines centered:*] December y^e^ 7^th^ [1681 ]{.underline}\
A Councell held at Gresham Colledg.

[…]

M^r^ Aerskine and M^r^ Pepys were sworne of the Councell ,\
M^r^ Aston was sworne Secretary . 

The President desired that the Statutes concerning the paym^t^\
of the Members of the Society & alsoe concerning the causes of\
Ejection might be transcribed into a paper for the next Coun⹀\
cell to consider and Debate. ~

[…]

‌ The Order of November the 23^d^. wherein S^r^. John Hoskins .\
M^r^ Henshaw . and M^r^ Hooke. are desired to be of the Comitte to\
treat with M^r^. Francklin, about his Lease is Revived and they\
are desired out of hand to Employ a Scrivener[*over:* ‘Scribr’] to draw up a\
Booke of the said Lease to be offered next Councell . M^r^\
Henshaw is desired to give such instructions as are necess⹀\
ary  according to the agreement he made, & because there\
are pretences still on foot relating to the way . The Councill\
thinks fitt that the number of Acres be expreſs’d, that our\
right be not prejudiced , & wheras M^r^ Henshaw reports\
that he agreed after the rate of 32 ſhillings the acre,\
the rent is to be expreſs’d in the groſs Sum̃e by Com⹀\
putacõn w^th^ a reasonable abatement for the prejudice\
which that pretence may doe to the Tenant . ~

[…]

‌ The papers that were formerly left in the hands of\
M^r^ Lane to consider Digest and make an abstract of the\
title were brought home and left in the Custody of M^r^\
Hooke. ~

‌ It was Order’d that M^r^ Hooke should take care that all\
orders of the Councell should be transcribed into the\
Journall Booke. ~

‌ The President & M^r^ Packer vndertooke to peruse y^e^\
writings of the Society & to make an abstract of them . ~

\TranscriptionStop


**(CMO/1 p. 324):** “[Library] Ten pounds ordered to be expended yearly in Books” (CMO/1)

### 14 December 1681 (CMO/1 p. 325;  CMO/1/284) probably Gresham College

\TranscriptionStart

[*centered:*] December . y^e^ 14 [1681 ]{.underline}

[…]

A Draught of‸^a\ lease\ of^ the Meadow and Arable ground of Chelsey Colleg\
to be lett to M^r^ Franklin for 21 years from X^t^mas last was\
read & approved, and M^r^ Franklin being called called in was acquainted\
w^th^ y^e^ Condicõns to w^ch^ he agreed but desir’d to have y^e^ same to shew\
w^ch^ w^n^ he had done he was to returne it y^t^ It might be ingroſsd & Sealed~

\TranscriptionStop


**(CMO/1 p. 325):** “Orders of the Council to be tranſcribed in the Journal Book:” [this is where the journal book becomes more important!!] (CMO/1)

**(CMO/1 p. 325):** “Abstract of the writings of the Society to be made.” (CMO/1)


### 11 January 1682 (CMO/1 p. 326;  CMO/1/285) probably Gresham College

\TranscriptionStart

[**Page 326:**]

[*centered:*] January . 11.^th^ [168$\frac{1}{2}$]{.underline}

‌ The President haveing been mpowered by former Orders\
to dispose by sale of Chelsey Colledg with the appurtenances ,\
Reports that he hath sold Chelsey Colledge with all the land[*inserted:*]s\
appertaining to S^r^ Stephen Fox for His Maj^ties^. use in case the\
Councell ratifie the same for 1300^£^ ready money to be pđ\
by S^r^ Stephen Fox at one payment at the Sealing the Con⹀\
veighances .

‌ The Councell hereupon , Judge that M^r^ Pesident hath done\
service to the Society, and approue of the sđ sale at the\
rate of 1300^£^ ready money and Returne M^r^ President\
theire thanks accordingly, And they Order that the Papers\
relating to the Title, be lodged in Councellō[r]\[*over:* ‘Councell’] Bailys Hand in\
order to give Satisfaction, to y^e^ Attourney Gen^ll^. ~

[…]

‌ M^r^ Hill haveing treated with D^r^. Hornecke[*over:* ‘Harnecke’] concerning\
his arrears, being 28^£^ pounds 19^s^ & the same haveing propos’d\
to give his bond for 20^£^ to pay within halfe a yeare, with a\
desire to withdraw from y^e^ Society for the future, It was Orde’d^r^\
and desired that the treasurer should agree with him according⹀\
ly and take his bond to himselfe for the twenty pounds and\
give him a full discharge for the time past. ~

[…]

\TranscriptionStop

### 18 January 1682 (CMO/1 pp. 326–327;  CMO/1/286) Gresham Colledge

\TranscriptionStart

[*centered:*] Jan .18 . 168$\frac{1}{2}$

[…]

[**Page 327:**]

[…]

‌ The president gaue an account to the Councell of his proceed⹀\
ings concerning the disposall of Chelsey Colledge, for w^ch^ they Return’d\
him theire thanks , and theire Earnest desire to Compleat and finish\
the said undertaking . ~

‌ M^r^. Hill was desired to speake with M^r^ Edwin about the\
Depositing the money when it shall be Recd in the East India\
Company .

([D^r^ Wood]{.underline})

‌ The Treãr Read over a list of the names of such Members\
of the Society as doe not pay at all or are much in  Arrears .\
Whereupon the Treār and two Secretarys were desired to\
meet the president and S^r^ John Hoskins at the presidents\
house every Munday night, and they were appointed a Committee\
to Consider of all Matters touching the arrears, and of what\
Expedients were fitt to be made use of for the Recovery of y^e^\
same, and to Report[*inserted:*]s their Proceedings and Opinions there⹀\
in to the Councell. ~

[M^r^ Henshaw]{.underline}) D^r^ Wood reported his proceedings with M^r^\
Sheridon concerning his arrears, namely that he had proposd\
to pay downe 10^£^ . provided he might continue of the Society\
and be excus’d from any further payments for the future\
which proposall being debated was rejected ; ~

‌ [M^r^ Colwall]{.underline} reported the severall answers of M^r^ Hoar &\
D^r^ Mills to his demand of theire arrears , which were left\
to be considered by the Comitte

‌ Whereupon the President moved that the answers made by\
severall Persons vpon such demands should be Registred in\
a booke to be produced when the Councell shall Consider of\
that affaire

‌ An Order was past for the payment of M^r^ Wicks his Salery\
for one yeare ending at Christmas last , And it was further\
Ordered that a stop be put to the Salary of the Clerk and\
Opperator for the future till the Councill have further Con⹀\
sider’d and setled that affaire. ~

\TranscriptionStop


### 25 January 1682 (CMO/1 p. 327–329;  CMO/1/287) probably at Gresham College

\TranscriptionStart

[*centered:*] January 25 . [168$\frac{1}{2}$]{.underline}

[…]

[**Page 328:**]

[*centered:*] January : 25 168$\frac{1}{2}$

[…]

‌  Voted that noe Person w^t^soever that is a Forreiner shall\
be admitted fellow of the Society w^th^out a Daplama , sent to\
him from [*uncertain:*]ther Society . ~

[…]

‌ And whereas Notice has been taken in the Councill that\
ſeverall persons haue been of late admitted Members of the\
Society w^th^out Subscribing [*over:* ‘Subscribed’]‸^[y^e^]{}^ bond & paying theire admiſsion money,\
to Prevent which for the future It is propos’d that after any\
person has passed the Ballot & been declared Chosen by the Pres^t^.\
the person who propounded him shall take Care to bring him\
to the Treãr to pay his admiſsion money and signe his bond &\
Receive an acquittance for the‸^same^ from the Trẽar , w^ch^ acguittance\
being showne to the Secretary the person shall be admitted\
to write his name in the Statute Booke and by the Secretary\
be presented to the President for his admission . ~

‌ And these two last orders are againe to be debated at\
the next Councell In order to be passed[*over:* ‘past’] into a Statute . ~

‌ Ordered that a booke of Blanke bonds be forthwith pro⹀\
vided for the treasurer for such persons soe to be admitted. ~

‌ Ordered that the Secretarys may make tryall of some\
persons for writing between this and the next Councell and\
give an account to the next meeting . ~

‌ D^r^ Grews Order brought in by himselfe . [*two words inserted:*] was signed . 

[*inserted:*]Adjou^r^: To meet at the Presidents . Tuesday night night. ~

\TranscriptionStop

### 8 February 1682 (CMO/1 pp. 329–330;  CMO/1/288) the President’s House

\TranscriptionStart

[*two lines centered:*] Att A Meeting of the Councell of the\
Royall Society . ~

‌ D^r^ Grew haueing Read ſeverall Lectures before the said\
Society, of the Anatomy of Plants ; some whereof have ‸^already^ been\
printed at diverse times, & some are not printed , together w^th^\
some others , of theire Colours ; Tasts ; & salts , alsoe of the Solution\
of Salts ; of the Waters of this Citty of London ; & of Mixture, all of\
them to the good likeing & Approbacõn of the said Society : It is\
therefore Ordered ;

‌  That he be desired, to cause them to be printed to⹀\
gather in one volume, And in regard of the great Number\
of FIgures belonging to them, to take vpon himselfe a more\
particular care of the Impreſsion. ~

[*centered:*] Febr : 8 : 168$\frac{1}{2}$

[…]

‌ M^r^. Wiꝉꝉ Bayly & M^r^ Nicholas Johnson appearing from the\
Attourney Gener^ll^ in order to see the Conveyance of Chelsey Colledge\
and the appurtenances, to the Kings most Excellent Ma^tie^. Sealed\
with the Com̃on Seale of the Society . the Councell being mett the\
Deed of Sale was Read over and the Comon seale was sett to\
the same the whole Councell consenting thereto . And at the\
same tyme the said M^r^ Johnson did produce an acquittance\
to be signed by M^r^ Hill vpon the Receipt of the money A .

‌ Ordered that the 1300^£^ now Rēcd by Abraham Hill Esq^r^\
Treãr of this Society from M^r^ Johnson be by him Depoſsited\
in the East India Company , and that the Obligacõn from the\
sd Company for the same be Depoſsited in the Iron Chest\
and that the Keyes of the same be in the Custody of the persons\
mencõned in the Statutes . & that‸^at^ the same tyme the 3 Charters\
& the Com̃on Seale be put in alsoe.~

S^r^ John Hoskins is desired to call againe on M^r^ Bayly &\
obteine a coppie of the Conveyance to the King of Chelsey\
Colledge and to peruse the papers now in‸^[y^e^]{}^ hands of M^r^ Bayly\
to see if any of them be of further use to the Socjety and[**page 330:**]\
Not belonging to Chelsey Colledge and to bring back such with him\
and to leave the rest in M^r^ Baylys hands . ~

‌ M^r^ Hill Returned & brought in a Note from Cook & Kary\
for the payment of 1300^£^ to the East India Company & he was\
desired to procure the bond of the East India‸^Company\ .^ for the same vpon\
Friday next and to bring in the same that it may be put\
in the Iron Chest as aforesaid . ~

\TranscriptionStop


**(CMO/1 , v. 1 p. 328:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)

**(CMO/1 , v. 1 p. 328:)** “[Grew Nehemiah M.D.] Anatomy of Plants &c to be printed” (CMO/8A)

**(CMO/1 p. 328):** “Caution about Admiſsion, paying Admiſsion money, taking a Recipt \| signing Bond, before writing his Name in the Statute Book e[t]c” (CMO/1)

**(CMO/1 p. 329):** “[Printers & Impressions] D^r^. Grews letters to be printed” (CMO/1)

**(CMO/1 , v. 1 p. 329:)** “[Grew Nehemiah M.D.] Anatomy of Plants &c to be printed” (CMO/8A)

## Volume 2

**(CMO/2 , v. 2 p. 1:)** “Bonds. E.J. for £1305.^s^6.^d^8 put into the Iron chest.” (CMO/8A)  “[Copying.] John Wilkinson employed” (CMO/8A)

**(CMO/2 , v. 2 p. 9:)** “[Fellows.] Names omitted in the List” (CMO/8A)

**(CMO/2 , v. 2 p. 10:)** “[Benefactors & Benefactions]  Lists, or Tables, of them ordered” (CMO/8A) “Books bought and sold by the Society.” (CMO/8a)

**(CMO/2 , v. 2 p. 11:)** “Bonds. E.J. for £1305.^s^6.^d^8 put into the Iron chest.” (CMO/8A)

**(CMO/2 p. 12):** “Wood Robert, L.L.D. \| Appointed V.P.” (CMO/8A)

**(CMO/2 , v. 2 p. 13:)** “Collins Mr^r^. John. Proposed to print some books of Algebra” (CMO/8A)

**(CMO/2, v.2 p. 15,23):** “[Library.] Books presented” [probably not Of Geo. Ent. Esq^r^., but look back at CMO/8A if so] (CMO/8A)

**(CMO/2 , v. 2 p. 17:)** “[Fellows.] Names omitted in the List” (CMO/8A)

**(CMO/2 , v. 2 p. 19:)** “[Fellows.] Names omitted in the List” (CMO/8A)

**(CMO/2, v.2 p. 20,29):** “[Journal Books.] \| Gaps in them from 1677 to be filled up” (CMO/8A)

**(CMO/2 , v. 2 p. 21:)** “[Fellows.] Names omitted in the List” (CMO/8A)

**(CMO/2 , v. 2 p. 21,44,46,168:)** “Books bought and sold by the Society.” (CMO/8A)

**(CMO/2 , v. 2 p. 22:)** “Collins Mr^r^. John. Proposed to print some books of Algebra” (CMO/8A)

**(CMO/2 , v. 2 p. 23:)** “[Hook M^r^. Robert. Delivers] the keys & Council books to M^r^ Aston & D^r^ Plott. Sec^r^” (CMO/8A)

**(CMO/2, v.2 p. 23: 15,23):** “[Library.] Books presented” [probably not Of Geo. Ent. Esq^r^., but look back at CMO/8A if so] (CMO/8A)

**(CMO/2 p. 24,67):** “[Transactions] Society to subscribe for 60, annually ordered” (CMO/8A)

**(CMO/2, v. 2 p. 24):** “[Arrears of contributions] The defaulter’s names to be left out of the list” (CMO/8A)

**(CMO/2 p. 24):** “[Repository at Gresham College.] D^r^. Grew made Keeper \| Catalogue to be taken of its Rarities. \| Benefactions to be entered in a book \| Rarities to be preserved, Augmented & Described” (CMO/8A)

**(CMO/2 p. 24):** “[Printing of Papers & Books.] Of Blank Bonds & Council Summons, ordered” (CMO/8A)

**(CMO/2 p. 25):** “Wyche Sir Cyril Bart \| Appointed V.P.” (CMO/8A)

**(CMO/2 p. 25,47,108, v.2 p. 231):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Appointed V.P.” (CMO/8A)

**(CMO/2 p. 25):** “[Presidents of the Royal Society] Sir John Hoskins Bar^t^ — 1682” (CMO/8A)

**(CMO/2 p. 26, v. 1 p. 282):** “[Secretaries to the Society.] To have the care of the Society’s Books & Papers” (CMO/8A)

**(CMO/2 , v. 2 p. 26:)** “[Books & Papers of the Royal Society] L^d^. Anglesey, who bought Oldenburg’s Books &c, to be asked for those of the Society, if any” (CMO/8A)

**(CMO/2 , v. 2 p. 28:)** “[Aston Francis Esq. Secretary] Minute that he buy books for teh Society &c” (CMO/8A)

**(CMO/2, v. 2 p. 28,33):** “[Instruments.] Order about their arrangement &c” (CMO/8A)

**(CMO/2 p. 28):** “[Secretaries to the Royal Society] Have leave to buy books for the Library” (CMO/8A)

**(CMO/2, v.2 p. 29: 20,29):** “[Journal Books.] \| Gaps in them from 1677 to be filled up” (CMO/8A)

**(CMO/2 p. 30, v.1 p. 281,283, v.2 p. 30):** “[Secretaries to the Society.] To take Notes or Minutes, for fair copying” (CMO/8A)

**(CMO/2, v.2 p. 30,69,71):** “[Journal Books.] \| To be inspected monthly about the entries” (CMO/8A)

**(CMO/2, v.2 p. 31):** “[Journal Books.] \| To be in a box & present at the Meetings” (CMO/8A)

**(CMO/2, v.2 p. 31):** “[Journal Books.] \| To be inspected by D^r^. Hooke” (CMO/8A)

**(CMO/2, v.2 p. 32):** “[Journal Books.] \| Gaps in them, offered to be filled up by the President.” (CMO/8A)

**(CMO/2, v. 2 p. 33: 28,33):** “[Instruments.] Order about their arrangement &c” (CMO/8A)

**(CMO/2, v.2 p. 33,95,96,99,100, v. 1 p. 290,292,314,315,319):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/2 , v. 2 p. 35:)** “Clerk. Order to seek one who may assist the Secretaries” (CMO/8A)

**(CMO/2 p. 36):** “[Printing of Papers & Books.] Waller’s transl. of the Florentine Exp^t^. Licensed” (CMO/8A)

**(CMO/2 p. 36):** “Weeks M^r^. \| Clerk to the Society \| Dismiſsed by a Council, & M^r^. Cramer chose in his place,” (CMO/8A)

**(CMO/2 , v. 2 p. 36:)** “[Books & Papers of the Royal Society] Minutes concerning” (CMO/8A) “[Experiments. To consider of, to entertain the King.] Historical account of, to be published by M^r^. Hooke.” (CMO/8A)

**(CMO/2 p. 37):** “[Wyche Sir Cyril Bart] Is President” (CMO/8A)

**(CMO/2 p. 37):** “[Weeks M^r^. \| Clerk to the Society \| Dismiſsed by a Council, & M^r^. Cramer chose in his place,] This Act of Council declared to be void, as illegal” (CMO/8A)

**(CMO/2 p. 37):** “[Presidents of the Royal Society] Sir Cyril Wyche Bart — 1683” (CMO/8A)

**(CMO/2 , v. 2 p. 37:)** “[Clerk.] Appointed by the Council, illegal.” (CMO/8A)

**(CMO/2 p. 39,40):** “[Repository at Gresham College.] M^r^. Lister to arrange the Minerals in Drawers” (CMO/8A)

**(CMO/2 p. 39):** “Tillotson, D^r^. \| Proposes to give Books, for the giving up his bond, accepted” (CMO/8A)

**(CMO/2 p. 40):** “[Repository at Gresham College.] Report of Grew’s Index” (CMO/8A)

**(CMO/2 , v. 2 p. 40):** “Index to the Journal &c books \| Resolved to have a very particular one made” (CMO/8A)

**(CMO/2, v.2 p. 40):** “[Journal Books.] \| Orders for their Duplicates, for security.” (CMO/8A)

**(CMO/2/ , v.2 p. 44):**

**(CMO/2 , v. 2 p. 21,44,46,168:)** “Books bought and sold by the Society.” (CMO/8A)

**(CMO/2 , v. 2 p. 44:)** “Collins Mr^r^. John. Proposed to print some books of Algebra” (CMO/8A)

**(CMO/2 , v. 2 p. 44:)** “[Books & Papers of the Royal Society] List of those in custody of the Secretary.” (CMO/8A)

**(CMO/2, v. 2 p. 45):** “Baker’s Algebra, Society take 60 copies, at £15” (CMO/8A)

**(CMO/2 , v. 2 p. 21,44,46,168:)** “Books bought and sold by the Society.” (CMO/8A)

**(CMO/2 , v. 2 p. 46,79,84,86,87,115:)** “[Books & Papers of the Royal Society] Minutes concerning” (CMO/8A)

**(CMO/2 p. 47: 25,47,108, v.2 p. 231):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Appointed V.P.” (CMO/8A)

**(CMO/2 , v. 2 p. 47:)** “Flamsteed. M^r^. Complains of M^r^ Hook for reflections in the minutes” (CMO/8A)

**(CMO/2 p. 48):** “[Presidents of the Royal Society] Samuel Pepys Esq. — 1684.” (CMO/8A)

**(CMO/2 p. 49,50,77,247,249):** “Library. Minutes relative to the Catalogue” (CMO/8A)

**(CMO/2 p. 50: 49,50,77,247,249):** “Library. Minutes relative to the Catalogue” (CMO/8A)

**(CMO/2 p. 50):** “Plates Engraved. \| To Hist. Piſc. (Willoughby) Society pays for.” (CMO/8A)

**(CMO/2 p. 53,60,61,75,83):** “[Repository at Gresham College.] Minutes concerning a List or Catalogue” (CMO/8A)

**(CMO/2 p. 56):** “[Printing of Papers & Books.] Ray’s Hist. Plantarum  Licensed” (CMO/8A)

**(CMO/2 p. 52, 57):** “[Plates Engraved.] Given by some Members” (CMO/8A)

**(CMO/2 , v. 2 p. 52,53,84,90,144,147:)** “Copying. Minutes thereon. & Sums paid” (CMO/8A)

**(CMO/2/, v. 2 p. 52):** “[Index to the Journal &c books] \| Orders about to the Secretary” (CMO/8A)

**(CMO/2/, v. 2 p. 52):** “[Index to the Journal &c books] \| to 2 Minute books desired of M^r^. Waller & M^r^. Haak” (CMO/8A)

**(CMO/2 v.2 p.54,279, v.3 p.147, v.1 p. 109):** “Presents from the Society \| Minutes” (CMO/8A)

**(CMO/2, v. 2 p. 55):** “[Arrears of contributions] The defaulter’s names to be left out of the list” (CMO/8A)

**(CMO/2 p. 56):** “[Printers to the Society.] Henry Fairthorn sworn” (CMO/8A)

**(CMO/2 p. 57):** “[Secretaries to the Royal Society] Order for Summons, to elect new ones” (CMO/8A)

**(CMO/2 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 , v. 2 p. 57:)** “[Book. Hist. Nat. Piscium, Willughby.] Appendia to the book Licensed” (CMO/8A)  “Clerk. Order to seek one who may assist the Secretaries” (CMO/8A)

**(CMO/2 p. 57: 52, 57):** “[Plates Engraved.] Given by some Members” (CMO/8A)

**(CMO/2 , v. 2 p. 58:)** “[Clerk.] No limit to their number by the Charters” (CMO/8A)

**(CMO/2 , v. 2 p. 58,71,72,113,125,127,130,134,216,234,246,247,231:)** “[Clerk.] Salaries and allowances.” (CMO/8A)

**(CMO/2 , v. 2 p. 59,267):** “[Clerk.] Qualifications \| Business \| Statutes concerning” (CMO/8A)

**(CMO/2 , v. 2 p. 60:)** “[Clerk.] D^r^. Halley appointed” (CMO/8A)  “[Clerk.] The choice in the Society” (CMO/8A)

**(CMO/2/, v.2 p. 60):** “[Inventory.] Of Papers, D^r^, Halley to enquire after” (CMO/8A)

**(CMO/2 p. 60: 53,60,61,75,83):** “[Repository at Gresham College.] Minutes concerning a List or Catalogue” (CMO/8A)

**(CMO/2 p. 61: 53,60,61,75,83):** “[Repository at Gresham College.] Minutes concerning a List or Catalogue” (CMO/8A)

**(CMO/2 , v. 2 p. 61,62,63,64,65:)** “[Committee.] To consider of the Book of Fishes.” (CMO/8A)

**(CMO/2, v.2 p. 61,62):** “[Journal Books.] \| Papers wanting in them to be recovered.” (CMO/8A)

**(CMO/2 p. 62):** “[Transactions] Halley to draw them up, Secretaries to approve.” (CMO/8A)

**(CMO/2, v.2 p. 62: 61,62):** “[Journal Books.] \| Papers wanting in them to be recovered.” (CMO/8A)

**(CMO/2 p. 63):** “Navigation \| Journals & Registers to be consulted for” (CMO/8A)

**(CMO/2 p. 64):** “[Transactions] Halley paid £13$\frac{1}{2}$ for 60 copies” (CMO/8A)

**(CMO/2 , v. 2 p. 66:)** “[Halley, D^r^. Edmund.] Prints S^r^ I. Newton’s book at his own charge.” (CMO/8A)  “[Committee.] To consider of the Book of Fishes.” (CMO/8A) “[Halley, D^r^. Edmund.] his salary as Clerk to be farther considered” (CMO/8A)

**(CMO/2 p. 66,67):** “[Printing of Papers & Books.] Of M^r^ Newton’s Book, under Halley’s care, Ordered” (CMO/8A)

**(CMO/2 p. 67: 24,67):** “[Transactions] Society to subscribe for 60, annually ordered” (CMO/8A)

**(CMO/2 p. 67: 66,67):** “[Printing of Papers & Books.] Of M^r^ Newton’s Book, under Halley’s care, Ordered” (CMO/8A)

**(CMO/2 , v. 2 p. 67:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)  “[Committee.] To consider of the Book of Fishes.” (CMO/8A) \index{Book of Fishes} \index{Bills of sundries}

**(CMO/2 p. 68):** “[Printing of Papers & Books.] Pittfield’s transl. Hist. Naturelle des Animaux. Licensed” (CMO/8A)

**(CMO/2 p. 68):** “[Printing of Papers & Books.] Flamsteeds Tide table for 1687. Ordered” (CMO/8A)

**(CMO/2 p. 68):** “Pittfield Alexander Esq. Prints a transl. Hist. Nat. des Animaux” [translation of a french work] (CMO/8A)

**(CMO/2 , v. 2 p. 68:)** “Fishes, Book of; Orders concerning” (CMO/8A) “[Hunt M^r^.] Paid for graving, of Plates, for the book of fishes” (CMO/8A) \index{Book of fishes}

**(CMO/2 p. 69,179):** “Waller, Rich^d^. Esq. \| Is Secretary” (CMO/8A)

**(CMO/2 p. 69):** “[Printing of Papers & Books.] Papin’s Improvement of the Air-pump. Licensed” (CMO/8A)

**(CMO/2, v.2 p. 69: 30,69,71):** “[Journal Books.] \| To be inspected monthly about the entries” (CMO/8A)

**(CMO/2 p. 70):** “Vincent Nath^l^. D.D. Desires to Withdraw.” (CMO/8A)

**(CMO/2, v.2 p. 71: 30,69,71):** “[Journal Books.] \| To be inspected monthly about the entries” (CMO/8A)

**(CMO/2 , v. 2 p. 71:)** “[Book. Hist. Nat. Piscium, Willughby.] Proposal to sell of the book 400, at 25 each.” (CMO/8A)

**(CMO/2 , v. 2 p. 72:)** “[Book. Hist. Nat. Piscium, Willughby.] To be exchanged for the Hortus Malabaricus” (CMO/8A)

**(CMO/2 p. 73):** “[Presidents of the Royal Society] John Earl of Carbury — 1686” (CMO/8A)

**(CMO/2 , v. 2 p. 73:)** “[Halley, D^r^. Edmund.] To have fifty books of Fishes instead of £50” (CMO/8A)

**(CMO/2 p. 73):** “[Halley, D^r^. Edmund.] To have 20 books more de Piſcibus for services last year” (CMO/8A)

**(CMO/2 , v. 2 p. 74:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 75: 53,60,61,75,83):** “[Repository at Gresham College.] Minutes concerning a List or Catalogue” (CMO/8A)

**(CMO/2 p. 75):** “[Papin M^r^. Denis] Has a Testimonial of Service, under the Society’s seal.” (CMO/8A)

**(CMO/2 p. 76):** “[Printing of Papers & Books.] Grew’s Index to the Society’s Muſæum Licensed, & they take 60.” (CMO/8A)

**(CMO/2 , v. 2 p. 76,77:)** “Books, Ordered to be bound.” (CMO/8A)

**(CMO/2 p. 76,84):** “[Papers.] Com^ee^. to direct their Registering” (CMO/8A)

**(CMO/2 p. 77: 49,50,77,247,249):** “Library. Minutes relative to the Catalogue” (CMO/8A)

**(CMO/2 p. 78,79,114):** “[Printing of Papers & Books.] List of Society” (CMO/8A)

**(CMO/2 p. 79: 78,79,114):** “[Printing of Papers & Books.] List of Society” (CMO/8A)

**(CMO/2 , v. 2 p. 79,80,209:)** “[Book. Hist. Nat. Piscium, Willughby.] Orders about the books and the sale, &c.” (CMO/8A)

**(CMO/2 p. 79):** “[Printing of Papers & Books.] Of M^r^. Hook’s Phil. Collections, the Society will take 60” (CMO/8A)

**(CMO/2 , v. 2 p. 80:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 81):** “Postage. D^r^. Hook allowed it conditionally” (CMO/8A)

**(CMO/2 p. 82):** “[Presidents of the Royal Society] Thomas Earl of Pembroke — 1689. \| Sir Robert Southwell — 1690” (CMO/8A)

**(CMO/2 p. 82):** “Plagiary’s Proposal to discover” (CMO/8A)

**(CMO/2 p. 83):** “[Transactions] Halley to be aſsisted: M^r^. Boyle requested \| to put his pieces in” (CMO/8A)

**(CMO/2 p. 83: 53,60,61,75,83):** “[Repository at Gresham College.] Minutes concerning a List or Catalogue” (CMO/8A)

**(CMO/2 p. 84: 76,84):** “[Papers.] Com^ee^. to direct their Registering” (CMO/8A)

**(CMO/2 p. 86):** “[Pictures or Portraits.] \| Of R. Boyle. by his Executors” (CMO/8A)

**(CMO/2 p. 86,101,102,103,108):** “[Library. Books lent from, their return to be secured] Books lent, not be be out longer than 6 weeks” (CMO/8A)

**(CMO/2 p. 86):** “[Library. Books lent from, their return to be secured] Books long out, to be asked for.” (CMO/8A)

**(CMO/2 , v. 2 p. 87:)** “[Halley, D^r^. Edmund.] Offers to provide matter for $\frac{1}{4}$ of a Phil: Trans.” (CMO/8A)

**(CMO/2 p. 87, v. 3 p. 201):** “[Library. Books lent from, their return to be secured] MSS. to be lent on Bonds” (CMO/8A)

**(CMO/2 p. 87):** “[Transactions] Materials for, to be searched among the papers” (CMO/8A)

**(CMO/2 p. 88):** “[Transactions] D^r^. Plott. to print them, be allowed 60 Books &c” (CMO/8A)

**(CMO/2 , v. 2 p. 89,117,154,212,291:)** “Books bought for the Society’s Library” (CMO/8A)

**(CMO/2 p. 89):** “[Printing of Papers & Books.] Grew’s tract de Salis cathartici &c Licensed” (CMO/8A)

**(CMO/2 p. 89):** “[Vincent Nath^l^. D.D.] Readmitted signing a new bond” (CMO/8A)

**(CMO/2 p. 90,93,95,96):** “Printing of Papers & Books. Malpighi’s Posthum. Works. Orders about” (CMO/8A)

**(CMO/2 p. 93: 90,93,95,96):** “Printing of Papers & Books. Malpighi’s Posthum. Works. Orders about” (CMO/8A)

**(CMO/2 p. 93):** “[Printing of Papers & Books.] License refused to a Theological book” (CMO/8A)

**(CMO/2 p. 93):** “[Library. Books lent from, their return to be secured] Books lent, to keep account of.” (CMO/8A)

**(CMO/2 p. 95: 90,93,95,96):** “Printing of Papers & Books. Malpighi’s Posthum. Works. Orders about” (CMO/8A)

**(CMO/2 , v. 1 p. 95):** “[Librarian’s] Hunt chosen” (CMO/8A)

**(CMO/2, v.2 p. 95: 33,95,96,99,100, v. 1 p. 290,292,314,315,319):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/2 p. 96: 90,93,95,96):** “Printing of Papers & Books. Malpighi’s Posthum. Works. Orders about” (CMO/8A)

**(CMO/2 p. 96):** “[Presidents of the Royal Society] Charles Montagu Esq. (Early of Hallifax) — 1695” (CMO/8A)

**(CMO/2 p. 96):** “[Papers.] To be entered in the Letter book, altho’ in the Trans.” (CMO/8A)

**(CMO/2, v.2 p. 96: 33,95,96,99,100, v. 1 p. 290,292,314,315,319):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/2 p. 97):** “[Printing of Papers & Books.] A Book of Anatomy  Licensed” (CMO/8A)

**(CMO/2, v.2 p. 99: 33,95,96,99,100, v. 1 p. 290,292,314,315,319):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/2, v.2 p. 100: 33,95,96,99,100, v. 1 p. 290,292,314,315,319):** “[Library.] Orders concerning” (CMO/8A)

**(CMO/2, v. 2 p. 100):** “[Arrears of contributions] The defaulter’s names to be left out of the list” (CMO/8A)

**(CMO/2 p. 101: 86,101,102,103,108):** “[Library. Books lent from, their return to be secured] Books lent, not be be out longer than 6 weeks” (CMO/8A)

**(CMO/2 p. 102: 86,101,102,103,108):** “[Library. Books lent from, their return to be secured] Books lent, not be be out longer than 6 weeks” (CMO/8A)

**(CMO/2 p. 101,105):** “Vernon M^r^. (Francis) \| Society encourages his Voyage to the Canaries” [TODO(check) perhaps asks for a report] (CMO/8A)

**(CMO/2 v. 2 p. 102,103):** “[Library.] That D^r^. Pope make good the books he has lost” (CMO/8A)

**(CMO/2 p. 103: 86,101,102,103,108):** “[Library. Books lent from, their return to be secured] Books lent, not be be out longer than 6 weeks” (CMO/8A)

**(CMO/2 v. 2 p. 103: 102,103):** “[Library.] That D^r^. Pope make good the books he has lost” (CMO/8A)

**(CMO/2 , v. 2 p. 103:)** “L^d^. Chancellor, Somers President [*to sign the charter book, confirming the society’s right to print*]” (CMO/8A)

**(CMO/2/, v. 2 p. 103):** “[Index to the Journal &c books] \| D^r^. Sloane to write to M^r^. Waller for” (CMO/8A)

**(CMO/2 p. 104):** “[Secretaries to the Society.] To see that their Minutes are fairly entered” (CMO/8A)

**(CMO/2 , v. 2 p. 105:)** “[Halley, D^r^. Edmund.] Appoints D^r^. Arbuthnot his deputy as Clerk” (CMO/8A)

**(CMO/2 p. 105: 101,105):** “Vernon M^r^. (Francis) \| Society encourages his Voyage to the Canaries” [TODO(check) perhaps asks for a report] (CMO/8A)

**(CMO/2 p. 108: 25,47,108, v.2 p. 231):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Appointed V.P.” (CMO/8A)

**(CMO/2 p. 108):** “[Presidents of the Royal Society] John Lord Somers (L^d^. Chancellor) — 1698” (CMO/8A)

**(CMO/2 p. 108: 86,101,102,103,108):** “[Library. Books lent from, their return to be secured] Books lent, not be be out longer than 6 weeks” (CMO/8A)

**(CMO/2 , v. 2 p. 109,110:)** “Defamation [*in the satirical pamphlet* Transactioneer], D^rs^. Woodward & Harris, clear of.” (CMO/8A) \index{Defamation}

**(CMO/2 p. 109,170):** “Sloane, Hans M.D. \| Appointed Secretary \| Thanked for his managment of the Societys affairs” (CMO/8A)

**(CMO/2 p. 110):** “[Transactions] Abridgement of, disowned by the Society \| and should be by leave of the Council” (CMO/8A)

**(CMO/2 p. 110):** “[Sloane, Hans M.D. \| Appointed Secretary] Charge against by Flamsteed, to be delivered in writing.” (CMO/8A)

**(CMO/2 , v. 2 p. 113:)** “[Clerk.] Jonas, J. [appointed]” (CMO/8A)

**(CMO/2 p. 114: 78,79,114):** “[Printing of Papers & Books.] List of Society” (CMO/8A)

**(CMO/2 , v. 2 p. 117:)** “[Clerk.] Wanley, H. [appointed]” (CMO/8A)

**(CMO/2 p. 117):** “[Pittfield Alexander Esq.] To write concerning the Fee Farm rents in Suſsex” (CMO/8A)

**(CMO/2 p. 119,121,156,163):** “Repairs of Buildings. \| Of the Library & Repository in Gresham Col.” (CMO/8A)

**(CMO/2 p. 120):** “[Wren Christopher L.L.D. afterwards S^r^. Christ^r^.] Desired to view & consider of the design proposed by \| the Gresham Com^[ee]{.underline}^. for accomodating the Society” (CMO/8A)

**(CMO/2 p. 121: 119,121,156,163):** “Repairs of Buildings. \| Of the Library & Repository in Gresham Col.” (CMO/8A)

**(CMO/2 p. 121):** “[Presidents of the Royal Society] Sir Isaac Newton — 1703” (CMO/8A)

**(CMO/2 p. 122):** “Register Book \| D^r^. Sloane recovers one, many years miſsing” (CMO/8A)

**21 April 1703 (CMO/2/151, v. 2 p. 122):**

\TranscriptionStart

[…]

The Councell being informed by Dr Sloane that a Register book of the
Society having been missing many years he had found it being offered
him to sale, and that he had stopped it. The Councell desired him to
take care to procure it for the Society.

[…]

\TranscriptionStop

**5 May 1703 (CMO/2/152, v. 2 p. 123):**

“[Hook M^r^. Robert.] His MS. papers found in his books to be shown to the Council” (CMO/8A)


“At a Meeting of the Royal Society […] Mr. Lowthorp Presented a Proposal for Printing and Abridgment of the Philosophical Transacitons.  This Design was Approv’d by the Society, and He was Desir’d to Proceed therein.”  (Lowthorp, *Phil. Trans. and Coll. Abrig’d*. London, 1705, t.p. - 1)

**(CMO/2 p. 125):** “Ray John Esq \| His Books of Fishes, to be disposed of” (CMO/8A)

**(CMO/2 p. 127):** “[Secretaries to the Society.] To write Letters of thanks” (CMO/8A)

**(CMO/2 p. 127):** “Prince George of Denmark. To be waited on, to sign his name as Fellow” (CMO/8A)

**(CMO/2 , v. 2 p. 127,252:)** “Books presented to the Society.” (CMO/8A)

**(CMO/2 p. 130):** “[Library.] Leave for consult books &c” (CMO/8A)

**(CMO/2 , v. 2 p. 132):** “Irregularity. Orders concerning.” (CMO/8A)

**(CMO/2 , v. 2 p. 137,138,141,142:)** “Bonds for payment of contributions; Minutes about their establishment.” (CMO/8A)

**(CMO/2 p. 139,140,142):** “[Statutes] To pay the Fee & sign Bond before Admiſsion” (CMO/8A)

**(CMO/2 p. 139,140,142):** “Statutes. Members to sign bond, who have neglected it.” (CMO/8A)

**(CMO/2 p. 140: 139,140,142):** “[Statutes] To pay the Fee & sign Bond before Admiſsion” (CMO/8A)

**(CMO/2 p. 140: 139,140,142):** “Statutes. Members to sign bond, who have neglected it.” (CMO/8A)

**(CMO/2 p. 142: 139,140,142):** “[Statutes] To pay the Fee & sign Bond before Admiſsion” (CMO/8A)

**(CMO/2 p. 142: 139,140,142):** “Statutes. Members to sign bond, who have neglected it.” (CMO/8A)

**(CMO/2 p. 145):** “Omitted in the List of Fellows. \| Order to leave out M^r^ Bridgman, M^r^. Fryer and \| D^r^. Robinson in the next yearly list” (CMO/8A)

**(CMO/2 , v. 2 p. 146:)** “[L^d^. Chancellor,] Trevor, to be waited on to sign the Charter book [*confirming the society’s right to print*]” (CMO/8A)

**(CMO/2 p. 148,149,212):** “Thorpe D^r^. John \| His proposals to the Society, accepted. (1707)” [TODO(check): printing related] (CMO/8A)

**(CMO/2 , v. 2 p. 148–9:)** “[Clerk.] Thorpe, J. [appointed]” (CMO/8A)

**(CMO/2 p. 149: 148,149,212):** “Thorpe D^r^. John \| His proposals to the Society, accepted. (1707)” [TODO(check): printing related] (CMO/8A)

**(CMO/2 , v. 2 p. 149):** “[Librarian’s] Not to let any person be in the Library but in his presence.” (CMO/8A)

**(CMO/2 p. 156: 119,121,156,163):** “Repairs of Buildings. \| Of the Library & Repository in Gresham Col.” (CMO/8A)

**(CMO/2, v. 2 p. 159):** “[Arrears of contributions] The defaulter’s names to be left out of the list” (CMO/8A)

**(CMO/2 p. 163: 119,121,156,163):** “Repairs of Buildings. \| Of the Library & Repository in Gresham Col.” (CMO/8A)

**(CMO/2 , v. 2 p. 163:)** “[Flamsteed M^r^.] His name ordered to be left out of the list for one year.” (CMO/8A)

**(CMO/2 , v. 2 p. 165:)** “[Hook M^r^. Robert.] His Posthumous works, Waller is desired to publish” (CMO/8A)

**(CMO/2 p. 166):** “Reflecting Words. Fixed on D^r^. Woodward. \| D^r^. Sloane did not offend” [TODO(check): what does this mean] (CMO/8A)

**(CMO/2 p. 166):** “[Reflecting Words.] spoken at a Meeting, subjects the spear to be ordered \| to withdraw; and be absent during the Council’s pleasure” [TODO(check): what does this mean] (CMO/8A)

**(CMO/2 p. 168):** “[Statues] Wrote on Vellum, and the copy kept by the Librarian” (CMO/8A)

**(CMO/2 p. 168):** “[Statues] To be reported to the Society, and to be entered” (CMO/8A)

**(CMO/2 , v. 2 p. 168: 21,44,46,168):** “Books bought and sold by the Society.” (CMO/8A)

**(CMO/2 p. 169):** “[Sloane, Hans M.D. \| Appointed Secretary] His declaration concerning D^r^. Woodward, voted sufficient” (CMO/8A)

**(CMO/2 p. 170,235):** “[Library.] Repairs Ordered” (CMO/8A)

**(CMO/2 p. 170: 109,170):** “Sloane, Hans M.D. \| Appointed Secretary \| Thanked for his managment of the Societys affairs” (CMO/8A)

**(CMO/2 p. 171):** “Mandamus. Minutes concerning” [TODO(check) What is this, is it in scope] (CMO/8A)

**(CMO/2 p. 179):** “Strong Box \| Deed & Insurance put in. Crane Court Estate” (CMO/8A)

**(CMO/2 p. 179: 69,179):** “Waller, Rich^d^. Esq. \| Is Secretary” (CMO/8A)

**(CMO/2 , v. 2 p. 183,184:)** “[Fees. For affixing the Society’s seal.] For the Queen’s letter, ordered to be paid” (CMO/8A)

**(CMO/2 , v. 2 p. 184: 183,184:)** “[Fees. For affixing the Society’s seal.] For the Queen’s letter, ordered to be paid” (CMO/8A)

**(CMO/2 p. 184):** “[Orders of Council. \| Relating to the Meetings] Minutes of the papers read to be entered in the Journal.” (CMO/8A)

**(CMO/2 p. 184):** “[Orders of Council. \| Relating to the Meetings] All papers, to be read in English.” (CMO/8A)

**(CMO/2 p. 185):** “Repository in Crane Court. Com^[e]{.underline}^. to consider where to place it” (CMO/8A)

**(CMO/2 p.185,229,271):** “[Library. Books presented] Committee to regulate” (CMO/8A)

**(CMO/2 p. 188):** “[Society’s House, or College.] Repository to be built” (CMO/8A)

**(CMO/2 p. 188):** “[Society’s House, or College.] Library to dispose” (CMO/8A)

**(CMO/2 p. 189):** “[Pictures or Portraits.] \| Of M^r^. Waller, presented by himself” (CMO/8A)

**(CMO/2 , v. 2 p. 191):** “Turin D^r^. Moves for Periodical books to be bought.” (CMO/8A)

**(CMO/2 p. 191):** “[Society’s House, or College.] List of Members wrote to for subscriptions. (N^o^.64)” (CMO/8A)

**(CMO/2 p. 192):** “[Library.] L^d^. Sunderland wants certain books in exchange \| refused” (CMO/8A)

**(CMO/2 p. 192):** “[Repository in Crane Court.] Undertaken to be built by Se.c Waller for £200” (CMO/8A)

**(CMO/2 p. 201):** “[Repository in Crane Court.] Cost £400; Society paid £300. Waller £100” (CMO/8A)

**(CMO/2 p. 201,206):** “[Repository in Crane Court.] Minutes about.” (CMO/8A)

**(CMO/2 p. 201):** “[Waller, Rich^d^. Esq.] Undertakes to build the Muſæum in Crane Court. \| Is paid by the Society for the Muſæum £300” (CMO/8A)

**(CMO/2 p. 201):** “[Waller, Rich^d^. Esq. Undertakes to build the Muſæum in Crane Court.] Is a benefactor toward the Muſæum £100” (CMO/8A)

**(CMO/2 , v. 2 p. 204:)** “[Gratuity.] Offered for information, whereby natural knowledge may be improved” (CMO/8A)

**(CMO/2 p. 206: 201,206):** “[Repository in Crane Court.] Minutes about.” (CMO/8A)

**(CMO/2 p. 206):** “[Library. Books lent from, their return to be secured] Order for some books to be lent to the E. of Sunderland” (CMO/8A)

**(CMO/2 p. 206,210):** “[Printing of Papers & Books.] Commercum Epistolicum” (CMO/8A)

**(CMO/3 p. 208):** “[Recommendatory Letter, with the Society’s seal.] Granted to M^r^. Molyneaux” (CMO/8A)

**(CMO/2 , v. 2 p. 208:)** “[Commitee.] For Philosophical Books & Treatises” (CMO/8A)

**(CMO/2 p. 212: 148,149,212):** “Thorpe D^r^. John \| His proposals to the Society, accepted. (1707)” [TODO(check): printing related] (CMO/8A)

**(CMO/2, v. 2 p. 212, v.1 p. 317,318,319, v. 3 p. 336):** “[Library.] Deliberations about the sale of some Books, Duplicates &c.” (CMO/8A)

**(CMO/2 , v. 2 p. 212:)** “[Clerk] Petitions for the employments.” (CMO/8A)

**(CMO/2 , v. 2 p. 215):** “Inspectors of the Library & Repository \| To be appointed by the Council” (CMO/8A)

**(CMO/2 , v. 2 p. 214:)** “[Clerk.] M^r^. Alban Thomas appointed” (CMO/8A)

**(CMO/2 , v. 2 p. 215:)** “[Halley, D^r^. Edmund.] is Secretary” (CMO/8A)

**(CMO/2 , v. 2 p. 216,269:)** “Clerk & House-keeper &c. Minutes concerning.” (CMO/8A)

**(CMO/2 p. 216: v.1 p. 284):** “[Treasurer to the Royal Society.] M^r^. Hunt (the clerk) appointed the Treas^rs^ Deputy” (CMO/8A)

**(CMO/2 p. 217):** “Thomas, M^r^. Alban. \| Chosen Clerk, &c” (CMO/8A)

**(CMO/2, v. 2 p. 225):** “Leibnitz. Notice that he shall ask a favour” [TODO(check): Is this something about printing and writing] (CMO/8A)

**(CMO/2 p. 227):** “[Papers.] Of the Com^m^. Epist^m^. [*Commercium Epistolicum*]  To be put in the Iron chest.” (CMO/8A)

**(CMO/2 p.229: 185,229,271):** “[Library. Books presented] Committee to regulate” (CMO/8A)

**(CMO/2 p. 229):** “[Library. Books lent from, their return to be secured] Books not to be lent out, but by leave of the Society” (CMO/8A)

**(CMO/2 , v. 2 p. 232:)** “[Aston Francis Esq. Secretary] Minutes concerning his Legacy and Books.” (CMO/8A) “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 233, v.4 p.4):** “[Pictures or Portraits.] \| Orders to be cleaned & inscribed” (CMO/8A)

**(CMO/2 , v. 2 p. 234:)** “[Committee.] For the care of the House, Pictures & Library” (CMO/8A)

**(CMO/2 p. 235: 170,235):** “[Library.] Repairs Ordered” (CMO/8A)

**(CMO/2 , v. 2 p. 236:)** “[Foreign Members] Some names left out” (CMO/8A)

**(CMO/2 237: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 , v. 2 p. 237,238:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 239: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 p. 239,249,299,&c):** “[Medal.] Minutes concerning the disposal” [TODO(think) Is a medal printing or not]  (CMO/8A)

**(CMO/2 p. 239,240):** “[Secretaries to the Society.] Proposed, by S^r^. I. Newton, to have salaries, arising \| out of M^r^. Aston’s Legacy” (CMO/8A)

**(CMO/2 p. 240: 239,240):** “[Secretaries to the Society.] Proposed, by S^r^. I. Newton, to have salaries, arising \| out of M^r^. Aston’s Legacy” (CMO/8A)

**(CMO/2 243: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 244: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 p. 247):** “[Thomas, M^r^. Alban.] Is allowed £20 a year for a Seru^t^.[servant] (1719)” (CMO/8A)

**(CMO/2 p. 247: 49,50,77,247,249):** “Library. Minutes relative to the Catalogue” (CMO/8A)

**(CMO/2 p. 247,248):** “[Repository in Crane Court.] Ordered a Fire place, and Airing” (CMO/8A)

**(CMO/2 p. 248: 247,248):** “[Repository in Crane Court.] Ordered a Fire place, and Airing” (CMO/8A)

**(CMO/2 p. 247,249,271):** “[Repository in Crane Court.] Orders concerning Catalogue” (CMO/8A)

**(CMO/2 , v. 2 p. 248,249,273:)** “[Clerk] To give Security.” (CMO/8A)

**(CMO/2 p. 249: 247,249,271):** “[Repository in Crane Court.] Orders concerning Catalogue” (CMO/8A)

**(CMO/2 p. 249: 49,50,77,247,249):** “Library. Minutes relative to the Catalogue” (CMO/8A)

**(CMO/2 p. 249: 239,249,299,&c):** “[Medal.] Minutes concerning the disposal” [TODO(check): Is a medal printing or not]  (CMO/8A)

**(CMO/2 , v. 2 p. 250,254,262:)** “[Foreign Correspondence.] Robert Keck Esq. His Legacy to support it” (CMO/8A)

**(CMO/2 251: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 p. 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

**(CMO/2 258: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 263: 57,237,239,243,244,251,258,263, v.1 162,289):** “[Secretaries to the Society.] Presents, or Gratuities, Ordered” (CMO/8A)

**(CMO/2 p. 264, v. 3 p. 182):** “[Papers.] Originals, To be preserved in Guard Books” (CMO/8A)

**(CMO/2 p. 265,267):** “[Thomas, M^r^. Alban.] Leaves the Society’s service (precipitately)” (CMO/8A)

**(CMO/2 , v. 2 p. 266):** “Brawn M^r^. Requests the R.”s care of a box of Deeds relating to Houses in Crane Court” (CMO/8A)  “[Crane Court.] Minutes about the Writings of M^r^. Braund” (CMO/8A)

**(CMO/2 , v. 2 p. 267):** “[Clerk.] Qualifications” (CMO/8A) “[Clerk] Petitions for the employments.” (CMO/8A)

**(CMO/2 , v. 2 p. 268:)** “[Clerk] Petitions for the employments.)” (CMO/8A)

**(CMO/2 , v. 2 p. 269:)** “[Clerk.] Modes of Election” (CMO/8A)

**(CMO/2 p. 270):** “[Secretary of the foreign correspondence] Philip Henry Zollman appointed  (£20)~1723~” (CMO/8A)

**(CMO/2 , v. 2 p. 271:)** “[Clerk.] M^r^. Francis Hauksbee chosen” (CMO/8A)  “[Hauksbee M^r^. Francis Jun^r^.] Chosen Housekeeper &c” (CMO/8A)

**(CMO/2 p. 271: 247,249,271):** “[Repository in Crane Court.] Orders concerning Catalogue” (CMO/8A)

**(CMO/2 p. 271: 185,229,271):** “[Library. Books presented] Committee to regulate” (CMO/8A)

**(CMO/2 p. 272):** “[Library. Books presented \| Committee to regulate] Their Report by M^r^. Folkes” (CMO/8A)

**(CMO/2, v. 2 p. 272,280, v.3 p. 13,97):** [Library.] Orders about binding some Books. (CMO/8A)

**(CMO/2 p. 274):** “Malefactor for Diſsection \| Order about securing the Society’s right, to have” (CMO/8A)

**(CMO/2 p. 275,276,277):** “[Thomas, M^r^. Alban.] Minutes concerning his Acc^ts^. with the Society.” (CMO/8A)

**(CMO/2 p. 275: 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

**(CMO/2 , v. 2 p. 275,280,281:)** “[Crane Court.] Minutes about the Writings of M^r^. Braund” (CMO/8A)

**(CMO/2 p. 276: 275,276,277):** “[Thomas, M^r^. Alban.] Minutes concerning his Acc^ts^. with the Society.” (CMO/8A)

**(CMO/2 p. 277: 275,276,277):** “[Thomas, M^r^. Alban.] Minutes concerning his Acc^ts^. with the Society.” (CMO/8A)

**(CMO/2 v.2 p. 279: 54,279, v.3 p.147, v.1 p. 109):** “Presents from the Society \| Minutes” (CMO/8A)

**(CMO/2 , v. 2 p. 282,283,285:)** “[Books & Papers of the Royal Society] Committee appointed to examine them.” (CMO/8A)

**(CMO/2 , v. 2 p. 283:)** “[Aston Francis Esq. Secretary] Minutes concerning his Legacy and Books.” (CMO/8A) “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 284: 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

**(CMO/2 , v. 2 p. 285:)** “[Aston Francis Esq. Secretary] Minutes concerning his Legacy and Books.” (CMO/8A) “Bills of Sundries, Ordered to be paid.” (CMO/8A) “[Desguliers D^r^.] Borrows Lowthorp’s paper on the figure of the Earth” (CMO/8A) \index{Bills of sundries paid} \index{Aston Francis Esq. Secretary+Legacy and books} \index{Desguliers, Doctor+Borrows Lowthorp’s paper}

**(CMO/2 , v. 2 p. 286:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 287: 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

**(CMO/2 , v. 2 p. 287:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 , v. 2 p. 296:)** “Bills of Sundries, Ordered to be paid.” (CMO/8A)

**(CMO/2 p. 298: 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

**(CMO/2 p. 299: 239,249,299,&c):** “[Medal.] Minutes concerning the disposal” [TODO(check): Is a medal printing or not]  (CMO/8A)

**(CMO/2 p. 302):** “[Sloane, Hans M.D. \| Appointed Secretary] Chosen President” (CMO/8A)

**(CMO/2 p. 302):** “[Presidents of the Royal Society] Sir Hans Sloane Bart. — 1727” (CMO/8A)

**(CMO/2 p. 303):** “[Orders of Council.] Copies of Journals &c, to be lodged with the President.” (CMO/8A)

**(CMO/2 p. 303, v.3 p. 12):** “[President. Minutes concerning the Office] Is to have lodg’ed with him, the Duplicates of Journals” (CMO/8A)

**(CMO/2 p. 303):** “[President. Minutes concerning the Office \| Is to have lodg’ed with him, the Duplicates of Journals] Is to giv an Obligation for the return of them” (CMO/8A)

**(CMO/2,  v. 2 p. 304):** “[Keck. Robert Esq^r^.] Orders about a copy of the Will” (CMO/8A)

**(CMO/2 p.305,306,307,310):** “King George II. \| To be waited on with the Charter Book” (CMO/8A)

**(CMO/2 p. 306: 305,306,307,310):** “King George II. \| To be waited on with the Charter Book” (CMO/8A)

**(CMO/2 p. 307: 305,306,307,310):** “King George II. \| To be waited on with the Charter Book” (CMO/8A)

**(CMO/2 p. 310: 305,306,307,310):** “King George II. \| To be waited on with the Charter Book” (CMO/8A)

**(CMO/2 p. 312: 254,275,284,287,298,312):** “[Secretaries to the Society.] Paid £50 a year, each.” (CMO/8A)

## MS/703

Transcription of items related to printing, the production of
manuscript separates, or external compilations.  Papers were routinely
transcribed into the Record books as manuscript compilations published
for the members of the society, the indexing of which is its own
project, repeatedly undertaken by librarians at the society.


### Papers read 1660

“[Merrit,]{.underline} Christ^R^. history of [Refining]{.underline} — [R. 1660] January 9 — [GB] 9 — [RBO] 1:08 — Tr: 142.” (MS/703, sp. 2)

“[Evelyn]{.underline}, John: history of [Graving]{.underline} &
[Etching]{.underline} \rlap{-}{(}[Chalcography.]{.underline}) — [1660]
July 3 — [Pr.] Lond^n^: 1662.” (MS/703, sp. 2)

“[Brouncher, Vic^t^.]{.underline} Acc^t^ of Exp.^ts^ of [Recoyling]{.underline} — ſeen [1660] July 10 & Jan.29. — [R. 1660] February 13 — [RBO] 1:143 — [Pr.] hist R.ſ.p.233.” (MS/703, sp. 2)

“Merret Dr. Appendix to hist. of [refining]{.underline} — [R. 1660] February 20 — [GB] 9 — [RBO] 1:08 — Tr: 142.” (MS/703, sp. 2)

# Index

\placeindex
