---
title: List of the contents of bundles, 17c
author: J. P. Ascher
date: 2 Nov 2023
citation-style: chicago-fullnote-no-bibliography.csl
bibliography: /Users/jascher/org/research.bib
...
\input{standard-style.tex.include}
\input{custom-style.tex.include}

# TODO

- [x] finish rough transcription
- [ ] proof rough transcription against item
- [ ] check the archives for each item
- [ ] tweak the formatting to include number of items in headers, etc.
- [ ] write a summary of each bundle or unit
- [ ] my “Grew” throughout might be “Grub[enol]”, check!
- [ ] decide between ½ and $\frac{1}{2}$ etc.
- [ ] begining might always be beginnig, check them out!
- [ ] Sheet or ſheet, throughout!

# Queries

- [ ] is it all of Oldenburg’s work?  Did he store some somewhere else?
- [ ] how physically big would this have been?
- [ ] where was it reorganized?
- [ ] was it really conducted orally?
- [x] could every “Mart” be “Marh”, no look at aut Patent, etc. 

# Observations

- ^E^9 has “your” annotations on a drawing, Hunt?
- Gεofrey Palmer crosses the gutter, so written open here
- Report to the R. Society Dec 22 1673 cancelled(??) “Rεport to y^ε^ R. Society Dεc. 22 . 73 . cancεlld .”

# Description of item

Within the miscellaneous manuscripts, volume sixteen, is an item
described as “List of bundles of manuscripts pertaining to the Royal
Society” (GB-uklors MM/16/20).  It consists of seven sheets in folio
and one half sheet, all in two columns, listing the contents of
various bundles and boxes reclaimed from Oldenburg’s family after his
death.

Volume sixteen must have been bound together from loose papers between
1985 and 1995.  University Publications of America filmed the
miscellaneous manuscript collection as it existed in 1985.  The
finding aid for these microfilms describe the collection at that point
as “a fourteen-volume collection of more than 1,800 letters and
private papers”^[@UPI85:RSarchives [vii]].  Keith Moore and Mary
Sampson’s finding aid of 1995 describes the miscellaneous manuscript
collection as “2,550 documents, largely manuscript letters, in 22
volumes, nos. 18, 20–22 not bound.”^[@moore95:guide [37]].  Thus
volume sixteen may have been bound in the interval between 1985 and
1995.

Keith Moore himself, however, contradicts this chronology.  He does
not recall these being bound between 1985 and 1995 and thinks that the
bindings look older than that.  All are produced by Matby’s of Oxford.
The volumes one through thirteen appear to have been bound in one
batch, with fourteen and fifteen nearly the same, but with a lettering
piece at the foot.  Volume sixteen, seventeen, and nineteen are
significantly taller, have different binders tools, and the vellum is
of a different quality.  Additionally, the inventory at the front of
these volumes is a carbon-copy from a typescript whereas the inventory
for the preceding volumes is spirit duplication.  Keith Moore also
notes that the penciled item numbers in volume sixteen are on
particular items, rather than the guards of the volume, which means
that they had been numbered prior to being bound in this volume.  The
volume sixteen, thus, dates to the era where carbon copies replaced
spirit duplication as the technique used in the library, but it’s not
certain when this is.

The bindings have other evidence.  Volumes through nine are two
numbered volumes in one physical codex, excepting number seven, and
have a paper label at the foot or the shadow of one, excepting five
and six.  Based on physical appearance alone, it would seem volumes
one through nine have been rebound from some previous volumes, with
ten through thirteen a second set from the same era.  Volumes fourteen
and fifteen have the same tooling on their lower lettering piece and
volumes.  Volumes sixteen, seventeen and nineteen have the same
tooling on their lower lettering piece.

Another possibility is that volumes through fourteen had returned by
the time of the microfilming and fifteen shortly after.  Volumes
sixteen and higher may have been in progress or not fully accessioned.

Lastly, the finding aid at the front of volume sixteen explains that
items 69 through 100 were presented in 1957, 103 through 106 in 1957,
108 in 1957, and 109 in 1958.

The paper of the list is on the same stock of evenly formed, writing
paper watermarked with a post horn and countermarked “LV” where the
whole sheet measures a trimmed 296/300 × 376/380 [12" × 15",
cf. Pott].  The Society kept a stock of paper ready for the
amanuenses, so this seems likely to have been from that stock.

Each sheet has been folded once to form a folio and then a second time
to make a sort of oblong quarto.  The second set of folds form the
boundaries between the columns.  The writing occasionally crosses the
column, but in several places continues on the next line with a
bracket rather than crossing to the column on the same page or across
pages.  The writer must have worked in narrow, oblong quarter sheets
of 296/300 × 93/100.  The widths vary between columns and sheets, so
these would not have been stored folded together.  Each has browning
on the uppermost page indicating storage in a uneven stack of folios.
The recto of the first leaf of the first bifolium is browned [ΔE
10.35] to typically grayish yellow brown [66.56% 17.63 80.77° 45:0]
compared with the typical pale orange yellow [80.29% 15.17 83.29°
45:0] of the recto of the second leaf.  A few ink stains occur on the
edge of leaves suggesting that these were trimmed to size before they
were used in the seventeenth century to make the list.

Presumably, the seventeenth-century bundles were unpacked and each
item, one after another, inventoried in these columns.  The writer
could have unfolded a letter to the left and had their list as an
oblong quarter sheet to the right so they could write their list.
After finishing a bifolium, these were stored as folios.  Once the
inventory was complete, these were stored in a pile or bundle for the
next few centuries close to the order they are currently in, with the
recto of the top darkening the most and the edges of subsequent
bifolia darkening as well.  The half sheet continues the preceding
bifolium and the half blank bifolium continues the bifolium that
preceeds it.

The handwriting and order suggest…

\BiblHangingIndentsStart

(Incipit: The Bundle inscribed A): 2^o^: [A]^2^; 2 leaves, cols. \[^A^1–8\]

(Incipit: A Bundle inscribed Undertaken for arts of Agriculture): 2^o^: [B]^2^; 2 leaves, cols. \[^B^1–8\]

\SubordinateStart
(Completing the previous, incipit: “de Letterah”): 2^o^: [C]1; 1 leaf, cols. \[^C^1–4\]
\SubordinateStop

(Incipit: A Bundle of Mathematical papers Inscribed the Problem answered): 2^o^: [D]^2^; 1 leaves, cols. \[^D^1–8\]

(Incipit: A deal Box containing): 2^o^: [E]^2^; 2 leaves, cols. \[^E^1–8\]

(Incipit: A Bundle departed from the Bundle inscribed the Bullet): 2^o^: [F]^2^; 2 leaves, cols. \[^F^1–8\]

(Incipit: A Bundle taken out of another): 2^o^: [G]^2^; 2 leaves, cols. \[^G^1–8\]

\SubordinateStart
(Completing the previous, incipit: “moved through in 1 page”): 2^o^: [H]^2^; 2 leaves, cols. \[^H^1–2\], pp. \[1–3\]\(blank\)
\SubordinateStop

\BiblHangingIndentsStop

# Transcription


## Bundle A (cols. ^A^1–3; A1^r^–A1^v^)

\TranscriptionStart

[**column ^A^1, A1^r^:**]

[*two lines centered:*] The Bundle inscribed\
A .

Letter to D^r^ Wallis .\
Flavel to Old . Nov. 4. 70.\
Vernon to Old . Mart . 8. 72 .\
Caſsini to Old. with y^e^ Titles\
‌ of 2 Books of Boccone .\
Wallis to Old . Jun . 10. 71 .
Old . to P. Pardies . Aug. 10 . 71 .\
Pardies to Old . July 18 . 71 .\
Letter of Mr. Blondel Feb. 2 . 70.\
Malpighius to Old . Feb. 10. 71 .\
Doddingto . to Old  Feb. 13. 71[*over:* ‘70’] .\
Temiſon to Old . Mart . 4. 70 .\
Laubellius to Grew . Mar. 9. 71.\
Hevel . to Old. Mart . 4. 71 .\
Marhindal to Old .Mart . 2 . 70 .\
Vernon to Old . Mart . 18 . 71 .\
Sachs[*over:* ‘Sash’] to Old . Mart . 20 . 71 . pa-\
‌ pers 2 .
Malpigh. to Old . Jul . 1 . 71 .\
Dodington to Old . Jun . 5 . 71 .\
Sylvius to Old .\
Kiſner to Old . May 12. 71 .\
Sylvius to Old . May 23 . 76.\
Old . to Marhindal[?] .\
Flamſtead to Old . Jan . 28 . 70 .\
Clerk to Suers Feb. 28. 68.\
Bεll to Old . 3^d^ . of y^ε^ 4^th^ M .\
Old . to Hεvεlius May 27. 64 .\
Old . to Mayor . Dεc . 10 . 65 .\
Bεrnard to Old . Nov. 24. 70.\
Payεn to Old . Jan . 9 . 70 .\
Fogelius to Old . Jan . 8 . 70 .\
Wallis to Old . Feb. 15 . 71 .\
Flamstead to Old . Feb . 10 . 7$\frac{1}{2}$[*over:* ‘72’] ·\
Oiks to Old . Mart . 24 . 70 . [*bracketing previous line*]\
Old . to Tornard Feb . 15 . $\frac{71}{2}$ .\
Sachs[*over:* ‘Sash’] to Old . Jul . 1675 . [**column ^A^2, A1^r^:**]\
Sa^c^hs[*over:* ‘Sash’] to Old . Oct . 29 . 71 .\
Flamſtead to Old . Aug. 12 . 72 .\
Flamſt . to Old . Aug : 28 . 72 .\
Syvers to Old . Oct . 25 . 72 .\
Flamſt . to Old . Apr . 11 . 70 .\
Newton to Old . Sεpt . 21 . 72 .\
Townly to Old. Aug . 15 . 72 .\
Newton to Old. May . 21. 71 .\
Lεttεr of Mr Old . July 10. 72 .\
Entr of a Lεtter to M^r^ New -\
‌ ton July 16 . 72 .\
Newton to Old . July 13 . 72 .\
Dodington to Old . Apr . 22[*over:* ‘23’] . 72.\
Vεrnon to Old . Apr. 20. 70.\
Newton to Old . July 30. 72 .\
Ignace to Grew. Apr. 9 . 72 .\
Pardies Lεttεr Apr . 13 . 72 .\
Flamſtead to Old . May 30. 72 .\
Pardieſ to Grew. Jun . 18 . 72 .\
Boalliau to Old . May 4 . 72 .\
Kirkby to Old . Apr . 27. 72 .\
Newton to Old . Mart . 16. 71 .\
Th . Cornelio to Old . Mart . 5 . 72.\
Flamſtead to Collins Mart . 18 . 71 .\
Flamſt . to Old . May 8 . $\frac{71 .}{2}$\
Extr . of Flamſteads Lεtter to\
‌ Collins in 2 papers .\
Wallis to Old . Nov . 27 . 71.\
Dodington to Old . Dεc : 5 . 70.\
Böineburg to Old . Sεpt . 14 . 70.\
‌    May . 26. 70 .\
Olden . to Böineburg . Aug. 10 .1 70.\
Dεnis to Old . Dec : 29 .\
Newburg to Old . Jan . 25 . $\frac{70.}{1}$\
Aſtronomical paper $\frac{1}{4}$ ſheet .\
Hεlmfield[?] to Grew. Aug . 17 . 70.\
‌    Dεc : 13 . 70 .\
Liſter to [*canceled:* ‘~~Martin~~’] Old . Aug.9.70\
Willughby to Old . Jun . 7.\
A. D. to Glanvile . Jan . 17. 70 .\
Fogεl to Old . July 28 . 70 .[**column ^A^3, A1^v^:**]\
Winthrop to Old . Oct . 4 . 69 .\
‌  Answer $\frac{1}{2}$ ſheet & cover .\
Wallis to Old . Mart . 24 . $\frac{69}{70}$\
Paiſεa to Old .\
Powle to Old .\
Dodington to Old . Feb . 5 . 71 .\
Old . to Otte Jan . 15 .[*over:* ‘1ſ’] 72 .\
T‸^h^εvenot to Old . Feb . 7 . 71.\
Old. to Sax .[Sachs] Dεc : 22 . 71 .\
Fogεlius to Old . Jan : 20[*over:* ‘29’]. 72.\
Hεum^c^man[?] to y^e^ R. S .\
Dodington to Old . Feb . 26 . 72.\
Flamſtead to Old . Jan . 3. 70.\
Walliſ to Old . Jan . 16. $\frac{71}{2}$\
Flamſtead to Old . May 30. 70 .\
Fεrmat to Old July 14.\
Flamſtead to Wilſon July 3 . 71 .

------

\TranscriptionStop

## Letters to be omitted (cols. ^A^3–8; A1^v^–A2^v^)

\TranscriptionStart

[*two lines centered:*] A Bundle inſcribed,\
Letters to be omitted .

Rëāy to Moray . Mart . 6. 64 .\
Major to Old . 13 Dec : 64 .\
Moncunyſ’s[*over:* ‘Monconyes’s’] lεtter 12 Dec : 64\
Beal to Old . 6 half ſheets\
21 Dεc : 62\
Moncunys[*over:* ‘Moncunyeſ’] 1 Aug : 64 to Moray.\
2 half ſheets .\
Pope’s Letter Apr . 5 . 64 .\
Copy of [*canceled, uncertain:* ‘M^r^’] Old . Lεtter to\
‌ Brown .
Bεch to Old . Aug : 15 .[*over:* ‘1ſ’] 68 .\
Old . to Bonleuſ[?] 11 . Nov . 67 .\
Bouleus[?] to Old . May. 26. 68.\
Fairfax to Old . 26. Nov . 67 .\
Bonleus[?] to Grew . 3 Dεc : 67 .\
Old. to Dεniſ[*over:* ‘Deny’] 23 Dεc : 67 .\
Burton to Old . Dεc : 9 . 67 . [**column ^A^4, A1^v^:**]\
Sauſon to King 9 Dεc ; 65 .\
Anzout to Old . 3 Oct . 65 .\
‌    14 . Apr . 66 .\
Burton to Old . Fεb . 17 . 67 .\
Travagini to Digby . Col . Jan .\
‌ 1666 .
Lubieniεrzki[*over:* ‘Lubieniεrski’] to Old . 26 Jan .67.\
Backman to Old . Nov. 20 . 67 .\
Sylvius to Old . 12 Apr. 68 .\
Lubieuierzki[*over:* ‘Lubieuierski’] to Old . 28 Apr . 67.\
‌    14 Nov . 66 .\
‌    10 July 66 .\
Walliſ to Old . Apr. 3 . 66 .\
Old . to Lubieuierzki July 26\
‌ 1666 .
Auzout’s Lεttεr 11 July. 65 .\
Excεrpt . ex lib .[?] Nor . Apr. 67.\
Old . to Denis . Mart . 9. 66 .\
[*canceled, uncertain:* ‘Margεsy to M’]
Rεlation of [*uncertain:*]raining of Land .\
Old to Powle Sεpt . 12 . 66 .\
Eclipſis Obſεrv .^o^ July 2 . 66 .\
Dε’Launay[*over, uncertain:* ‘Dεlannay’] to Old . Mart . 10. 68.\
Walſh to de Vaux 20 Jan . $\frac{67}{8}$\
Wallis to Old . 12 Fεb. 66 .\
Old . to Sybius 25[*over:* ‘26’] Feb . $\frac{67}{8}$\
Hεvεlius to Old . 29 Sεpr. 68 .\
Old . to Glanvile 3 Oct. 68 .\
J. G. to Old. Sεpt . 18. 68 .\
Lubienierzki to Old . 11 Oct . 67.\
Nelſon to Old . Aug : 22 . 68 .\
Wallis to Old . Sεpr . 3. 68 .\
Pεtti to Old . May 28 . 67.\
Draught of a letter to Lubieni\
-εrzki[*over:* ‘-εrski’] . 12 Feb . 66.\
De Graſſe to Old . 20 Jul. 68[*over, uncertain:* ‘69’] .\
Lεtter to Old . 17 . M . 68 .\
Justel to Grew . Oct . 68 .\
Lεtter to Old . 26 May 66 . [**column ^A^5, A2^r^:**]\
Juſtel to G^r^ew . Aug. ult[*canceled:* ‘s’] 1668 .\
‌    Feb . 18 . 68 .\
[*canceled:* ‘~~Colpreſs~~’] Mart . 21. 68 .\
‌    A Paper with out date\
‌    Mart . 28 . 68 .\
‌    Feb . 25 . 68 .\
‌    Nov . 28 . 68 .\
Old . to Travagini 6 Apr. 67.\
Old to Bεckman 30 Mart . 68.\
Copy[*over:* ‘Copp’] of a lεtter from Old . to Pε\
‌ tit . Apr . 5 . 67.\
Extr . of Auzouts lεtter\
‌ 28 Dεc : 66 .\
Paiſεn to Old . 12 Cal . Jan .\
‌ 1669 . [*bracketing to line below*]\
Auzout to Old . 2 Jul . 65 .‸^3\ half\ Sh.^\
Old . to Finch Jan . 14 . 68 .\
Jackſon to Glauvil . Feb. 19. 68.\
Pεtty to L^d^ Brouncker 4 Feb. 62 .\
Extr . of Pεttyſ lεtter .\
Extract . of Sr W . Pεttyſ letter .\
1 Sheet of y^e^ double bottomed Shipp.\
Skippon to Old . Fεb. 16 . 68 .\
Curtius to Old . Mart . 13. 69[*over, uncertain:* ‘68’] .\
Pεtty to L^d^ Brouncker 2 ſheets.\
Dε Graff to Old . 22 Fεb . 69 .\
Old . to dε Graff 8 May 69 .\
Ext . of Sr W . Pεtty’s Boat\
‌ 1 Nov . 62 .\
Hεvεlius to Old . 21 Mart . 67.\
Skippon to Old . 15 . Mart . 68.\
Pεtis to Old . 23. Oct. 60 .\
Obſervatio magnetica .\
Griſley to Old . Mart . 15. 69.\
Sorbier[*over:* ‘Sorbser’] to Moray.6.Cal. Sεpt.\
‌ 1661 .\
Ext . of lεtter from Prince\
‌ Lεopoldo .\
Huεt to Old . 24 . July 69 .\
‌    3 . Cal . Apr . 68 .\
Browne to Old . 31 May . 69 . [**column ^A^6, A2^r^:**]\
Dε Cocherεt to Old . Apr. 22.69\
Curtius to Old . Jan . 9 . 69 .\
Dε la Coſte to Old . 30. Jan . 69\
‌ togεther with hiſ printed proposion[*bar over ‘osi’*]\
Pεrbaner to Old . 2 Feb. 69 .\
‌      16 Mart . 69 .\
1 ſheet & $\frac{1}{2}$ of de La Coſte .\
Paiſεn to old . 1 Cal .[*over:* ‘Jan’] Jan . 69.\
Maven to Grew .\
Walliſ to Old . Jun. 15 . 69 .\
[*canceled:* ‘~~Hυet to Old . 24 . Ju~~’]\
Sluſius to Old . 4 Oct . 68.\
Copy of Blundels lεtter. Oct .\
‌  6 . 68 .\
Walliſ to Old . Nov. 19 . 68 .\
‌    Nov . 27. 68 . 2 ſheets\
‌    Dεc : 9 . 68 .\
Clerk to Old . Dεc : 20 . 68 .\
Bourdelot to Old . 2 Mart . 68.\
J . H . to [*uncertain:*]Port Fεrran . 9 Dec.68\
Old[*over:* ‘F’] . to Fogεlius Apr . 4 . 68 .\
Walliſ to Old . Dec . 9 . 68 .\
Colepreſs to Old . Jun 30. 69 .\
‌  with a paper of draughtſ .\
Fεrmat to Old . 15 July . 69 .\
‌     18 Dεc : 68.\
Meulzεbiy to Old . 1 Jan . 69 .\
Walliſ to Old . 12 Jun . $\frac{68}{9}$
Gorniey to Old .\
‌  [*uncertain:*]Jd . ad Sunden[*over, uncertain:* ‘Jd’] 11 July 69 .\
Nazarus to Old . Oct . 69 .\
Willughby to Old. 23 July .\
Richard to old . 10 July 69 .\
‌  3 papers .\
Oldenb. to Meulzεlius 12 Feb .\
‌  1669 .
Copy of a lεtter of Old. to\
‌ Malpighius . 4 Aug . 69 .\
Brown to Old. 28 July 69 . [**column ^A^7, A2^v^:**]\
Martel to Old . 21 Apr. 69 .\
‌ 1 ſheet & $\frac{1}{2}$ .\
Cotton’s lεtter 17 Aug . 69 .\
[*uncertain:*]Downſ to Shippon 9 Feb . 68 .[*over:* ‘67’]\
Townly to Old . 11 Fεb . 68 .\
Walliſ to Old . 11 Feb . 68 .\
Old . to Newburg 11 Sεpr . 69 .[*over:* ‘68’]\
Wallis to Old . Jan . 2 . $\frac{68}{9}$\
‌   Apr . 24 . 69 .\
Cotton to Grew . 7 Sεpr . 69 .\
de Martel to Old . 4 Dεc : 69 .\
‌  2 ſheets .
Newburg to old. Oct . 30 . 69 .\
Fogεlius to Old . 11 . July .68 .\
Magalotti[*over:* ‘Magalotty’] to Grew . 9 May. 68 .\
‌    21 Nov . 68 .\
Burton to Martin 29 Sεpt . 67 .\
‌   2 half ſheets\
Magalotti to Grew. 26 Jun 68 .\
‌   28 May 68 .\
Jackſon to Old . Dεc . 11 . 69 .\
‌    18 Dεc : 69 .\
Paiſεn to Old . 21 Aug . 69 .\
Sharas to Old . 22 Feb . 70 .\
Glanvil to old. 15 . Aug  69 .\
‌  with a ſheet of Proposils for\
‌  corrεſpondencſ .\
Reed[*over, uncertain:* ‘Realand’] to Old . Feb. 14 . 69 .\
Cotton’s lεtter with 2 littlε\
‌ papers folded , & a printed half ſheet\
‌ 31 Aug : 69 .\
Newburg to Old . 21 Sεpt . 69 .\
Copy of Frericle to Digby\
‌ Aug Q ult . 61 .\
Vεrnon to Old . Jan . 29[*over:* ‘26’] . 70 .\
‌    Oct . 3 . 67 .\
‌    Apr . 9 . 69 .\
‌    Apr . 5. 69 .\
‌    Aug . 3 . 69 . 4 papers .\
pind togεther .
Farifax to , Old. 28 Sεpr. 67\
‌  with [*canceled:* ‘a’] $\frac{1}{2}$ ſheets of draughtſ .[**column ^A^8, A2^v^:**]\
Fairfax to Old 28 . Mart . 67\
Colepreſse[*over:* ‘Colepreſ’] to Old . 26 July. 67\
[*four lines bracketed right:*]Remoquietano’s[Remoquietanus] Obſεrvaionſ\
‌ of Mercury in y^e^ Sun , by\
‌ therwith extr . of Hεvεliuſ’s\
‌ Lεttεr to Old . 1 June . 65.\
Sheets paper concεrning y^e^\
‌ ſame .\
Extr . of Auzoutſ lεtter in\
‌ Engliſh .\
[*canceled:* ‘~~Charles~~’] Auzout to Charles\
‌  30 Oct . 64 .\
2 ſheets in Frεnch 20 Oct . 64\
Old. to Hεveliuſ 2 Aug . 69\
‌  2 papers .\
Figurεſ of y^ε^ Chaiſεroulat[*possibly:* chaise roulant]\
‌  in 2 paperſ .\
Hugεnſ to Moray 15 July 61\
Old . to Hugεnſ 2 Jun . 69\
Hugεnſ to Moray 30 . Mart . 69\
Copy of Sr R . Morayſ lεtter\
‌ to Hugεnſ 1669 .\
Morayſ lεtter to Hugεns ex-\
‌ tracted 22 July 65 .[*over:* ‘6\rlap{\bf 5}{5}’]\
Hugεnſ to Moray 6 Fεb. 65 .\
‌     27 Apr . 64 .\
‌     12 June 64 .\
‌     17 . June 64 .\
Extr . of Morayſ letter to Hu\
‌ geεus 18 Sεpt . 65 .\
Cocheral to Old 15 Oct 69\
‌   with a paper .\
Hεvεliuſ to Old . 10. Mart . 76\
‌  with an Obſεrvation 1675

\TranscriptionStop

## Undertaken for are^ts^ of Agriculture (cols. ^B^1–2; B1^r^)

\TranscriptionStart

[**column ^B^1, B1^r^:**]

[*three lines centered:*] A Bundle inſcribd\
Undertaken for are^ts^\
of Agriculture .

Undεrtaken for Agriculturε .\
a Gεoigical acc^t^ of Dεvon-\
ſhire & Cornwall . 2 ſheets\
6 Feb . 68 .\
Martindale’ſ lεtter 2 Dεc . 70 .\
Account of Enginſ out of Ker -\
‌ cher by M . Hill .\
Boccone to Old . 8 Apr 74 .\
Tranſactionſ of y^ε^ Mεchanical\
‌ Coittee . 2 paperſ  .\
Undertaken for y^ε^ buſineſſ of y^ε^\
R . S . in 6 paperſ .\
Bεal to Old . 19 Apr . 65 .\
Bεal to Old. 23 . May 64 .\
‌      20 Jan . 64.. 2 Sheetſ\
‌      27. Jan . 64 . 3 Sheetſ\
‌      1 July 64 . 2 Sheetſ\
‌      9 July 64 . 1 Sheet\
‌      20 June 64 1 Sheet\
‌      1 Apr 64 . 2 Sheets .\
Watkinſons paper of of Agriculturε\
Sr  . Knatchbullſ[*over:* ‘No’] Anſwεr .\
Paper of Agricult . in York ſhire .\
D^r^ [*uncertain:*]Smithy acc^t^ of Huſband in Glo\
‌ cεſter ſhire .
Extr . of D^r^ Bealſ paperſ con -\
‌ cεrning y^ε^ conduct of Agricult .\
Headſ of Huſband. to bε conſiderd.\
Sr R. Morayſ headſ for breeding\
‌ of Horſes .
a Rεport from a Coittee\
[*canceled:* ‘Gar’] Enquiryſ concεrning yε\
kitchen Gardεn &c 3 Sheetſ\
Knatchbull of flooding uſed\
on Kεnt . 1 Sheet .\
Of Agriculture 1 1 Sheet\
Gεorgicall εnquiryſ of  Kεnt .\
Anſwεr to yε Gεorgicl[*over:* ‘Gεorgica’] εnquiryſ\
from Rippon Yorkſhirε\
‌ Another paper of y^ε^ ſame . [**column ^B^2, B1^r^:**]\
M^r^ Howardſ acc^t^ of Karkſhirε\
Tεniſon to Old . 7 Nov. 71 .\
Sεymer[*over:* ‘Sεrmer’] to Beal 18 . Apr . 65 .\
Hotham of Agricuel . 2 Sheetſ\
Farifax to Old . 5 . Dεc . 67..\
W . C. to Bεal . Oct . 13 . 64 .\
‌  [*uncertain:*]Anther Sheet .\
Exts . of Bεalſ lεtter .\
Tranſactionſ of y^ε^ Georgicall\
‌ Coittee . 2 Sheets\
Rεport of y^e^ Georgical Coittee\
Notεſ of Coitteeſ . $\frac{1}{2}$ Sheet .\
Propoſallſ about Potatoſ . $\frac{1}{2}$ Sheet .\

------

\TranscriptionStop

## Letter case full of originals and memoirs (cols. ^B^2–3; B1^r^–B1^v^)

\TranscriptionStart

[*two lines centered:*] A Lεtter Caſe full of\
Originalſ & Mεmoirεſ

Flamſtead to Old . Dεc . 3 . 73 .[*over:* ‘73’]\
Walliſ to Old. 15 Oct. 74 .\
Draught of a Stable .\
2 papers of y^ε^ Lawſ of y^ε^ Sociε\
‌ - ty .\
A-lεtter & [*uncertain:*]Memoirεſ in 1 paper\
a Paper bεgining , Sir y^ε^ Council .\
Liſter to Old Jan . 28 . 75 .\
Draught of an Order .\
Sir J. Williamſon 3 . Oct . 75 .\
Forme of makeing a Vice-Prεſi-\
‌ dεnt .\
Paper endorſed , Council .
A Liſt of ſεvεrall Mεmberſ of\
‌ yε R. S.\
Lister to Old . 29 . May 74 .\
Paper produc’d by L^d^ Stafford .\
‌ 27 . Nov. 73 .\
Obsεrv^ſ^. of Windſ .\
Caſsini to Old . 8 Nov. 73 .[*over:* ‘73’]\
‌    22 Sεpt . 73 .[*over:* ‘73’]\
Liſter’s paper 26 July 75 .\
Hεvεlius to Old . 21 Aug. 74 .\
Caſſini to Old . 11 Aug . 73 .\
\[*canceled* ‘Flamſtead to Caſſini to F .’\]\[**col. ^B^3, B1^v^:**]\
Caſſini to Flamſtead , a Copy\
‌ 11 Aug. 73[*over:* ‘73’] .\
‌  Another 8 Nov. 73 .\
2 papers of y^ε^ Subtεrran . fire .\
Flamſtead to Caſſini , a Copy[*over:* ‘Copp’]\
‌  5 Sεpt. 73 .\
Experimentſ rεcoended to Mr\
‌ Hook .\
Notε dated 1 Dεc . 73 .\
Hεvelius to Old . 23 . Aug . 73 .\
Paper bεgining, Subscriptionſ .\
Liſt of Engagεmtſ &c .
Noteſ of a Council Nov. 10 . 73.\
Part of yε Charter bεgining ,\
— εt uetεrius volumenſ .
Grεgory to — 31[*over:* ‘31’] Dεc . 73 .\
Paper bεgninig , [*uncertain:*]Cu primiſ .
Liſt of Names &c\
Superſcription of a Lεtter

------

\TranscriptionStop

## Robert Southwells Papers (cols. ^B^3–4; B1^v^)

\TranscriptionStart

[*three lines centered:*] A Bundle inſcribd\
Sr R . Southwellſ\
Paperſ .

Foure curiouſ poyntſ diſcourſ\
ed by Father Hiεronmus Lobo\
‌  pag 28 . A .\
Modo dε Farar ε[*canceled* ‘e~~t~~’] Prεparar\
&c 1 Sheet .\
Letter to Cap^n^. Shanon . Dεc :\
‌ 10 . 67 . D .\
Lεtter to Coꝉ^v^ Dεmpſay .\
‌ Dεc : 10 . 67 . E . .\
Crake to Sr. R. Southw. Janu\
‌ 3 . 68 . G.\
Of Planting & dreſsing of Vineſ\
‌ 1 Sheet .\
Lεtter to Smith Dec . 9 . 67 .\
Read’s Lεtter Jan . 19 . 68[?] . F.\
[*uncertain:*]E^xta^mos[*canceled, uncertain:* ‘E~~atc~~mus’] Earth &c a paper be\
‌ gining with theſe wordſ . [**col. ^B^4, B1^v^:**]\
Of a Diſεaſε in y^ε^ Southerne\
parts called Bico . J .\
Read’ſ letter July 13.67 . K .\
Lobo’s Diſcourse of Palme\
‌ Treeſ . ffεb . 15 . 68 . B .
[*canceled:* ‘~~Letter~~’]

------

\TranscriptionStop

## Copy of Mr Neils Papers (cols. ^B^4–5; B1^v^–B2^r^)

\TranscriptionStart

[*three lines centered:*] A Bundle inſcribed\
Copy of Mr Neilſ\
Papers .

—\
Hiſtory of Philoſophy\
1 Sheet & $\frac{1}{2}$ .

Propoſals concεrning εxpe\
rimentſ . 1 Sheet .

Diſquiſition of Vinouſ Liquorſ.

Mr Nεil to Old. 18. Dεc .

Neile 28 . Dεc .\
‌   2 Jan .\
‌   26 . Jan .\
‌   22 Jan .\
‌   12 Jan .\
‌   5 . Dεc : 67 .\
‌   15 Dεc : 67 .

Note begining , When dεcus-\
ſant &c[*over:* ‘&s’]

Nεile to Old . 30 . Oct .\
‌     1 June . 69 .\
Walliſ to Old . 29 May 69 .\
Nεile May 7. 69 .\
Walliſ to Old . June 19 .\
Neile June 23 .

Paper begining , Space is .

Sheet begining , Vinouſ liquorſ .

Walliſ to Old . 7 June . 67 .\
‌     17 May 69 .\
Neile May 20 . 69 .\
Walliſ May 10. 69 .\
Nεile May 13 . 69 .\

Queryſ of Motion .[**col. ^B^5, B2^r^:**]

Letter begining , Sir I dont find .\

Paper begining , Philoſophicall\
‌ appearances .

[*canceled:* ‘~~Letter~~’] Neil to Old . 21[*over, uncertain:* ‘11’] Jan .

Lεtter of Mr Neile . 3 Sheetſ\
‌ 15 . June 69 .\

Neile of Motion . 1 Sheet .

Neileſ Principles of Philo-\
‌ ſophy . 1 Sheet

Hiſtory of Philoſophy. 3 Sheets

Experiments propoſd , a paper .

Proposals offerd . 2 Sheets .

Diſquiſition of Vinous liquors.\
‌  2 Sheets .

Diſcours bεgining , By Atomeſ\
‌ I meane

------

## [Letters and books] (col. ^B^5; B2^r^)

Prints of Lεttεrs . 20 ſheets .

—

A Journall Book of y^ε^ Council\
‌ of y^ε^ R. S . of 214 pagεſ.

------

\TranscriptionStop

## Letters concerning the Royal Society, the originals Mr Sprat has (cols. ^B^5–6; B2^r^)

\TranscriptionStart

[*three lines centered, fourth left:*] A Bundle inſcribed\
Lεttεrs concεrning y^e^ R .\
S . , y^ε^ Originalſ M^r^ Sprat\
has

—

Dε la Piεrce ’ſ lettεr\
‌ 14 Dεc . 67 .

Hugεniuſ ’ſ lεtter . 1 Jun. 63 .

Stεno’s lεttεr . 2[*over:* ‘3’, *canceled:* ‘~~half~~’] ſheetſ .

Paper about a chaiεr be-\
gining , La matierε

Hugeniuſ’ſ lεttεr 20 . Dεc . 62.

Hugeniuſ’s letter. 64 . to Moray.\
‌   Sεpt . 18[*over:* ‘11’] . 65 .\
‌   Sεpt . 1 . 62 .\
‌   Aug . 29 . 64 .

[*canceled:* ‘~~Moray~~’] Lεtter to Hugεniſ 14 Jan\
‌ 1666 .

Lεttεr to Hugεniuſ from Pariſ\
‌ 20 Aug . 64 . with a paper\
‌ bεgining , Lεsquelles .

Hugεnſ to Moray 11 Nov. 63.

Old . to Van Dam 23 Jan . 63 . [**col. ^B^6, B2^r^:**]

Lεttεr begining , Sir I hope\
‌  July 14 . 62 .

Lεtter to Sir W . Petty .

Extr . of lεtter from Mon\
‌ conyſ[Monjonys] to Moray .

Paper ẽntitled , Extraict &c

Hugεnſ to Moray 31 Oct . 64 .

Hugeuſ’s lεtter from tranſlated\
‌  Aug . 18 . 62

Copy of lεtter of Hugεnſ\
‌ to Moray 10 Oct . 64 .

Copy of lεtter to Ea[r]ꝉ of\
‌ Sandwich[*over:* ‘Sandwick’] Jan . 17 . 66 .

Copy of Hugεuſ lεtter June\
‌ 2. 61 .1

1 Sheet begining, May it\
pleaſe y^r^ Maj.^ty^

Extr . of Hugεuſ lεtter to\
‌ Moray ffεb . 2 . 63.

Sorbεir’s lεtter to B^p^ of\
‌ Laon . a Copy .

Copy of Hugεuſ to Moray\
‌  29 May 65 .\
‌  21 Nov . 64 .

Extr . of lεtter from Hugεuſ to\
‌ Moray Apr . 9 . 66 .

D^r^ Wilkinſ to Sr[*over:* ‘Mr’] R . Moray .\

Extr . of Hugεuſ to Sr. R. Mo\
‌ ray 24 Dεc . 65 .

S^t^ Clarε to Moray 29[*over:* ‘26’] Dec 63.\
‌   Aug . 13 . 64 .\
‌   Dac . 29 . 62 .\
‌   Apr. 6. 63 .

Pεtty to Sr R. Moray 6 Aug 63.\
‌   Apr. 29 . 63 .\
‌   July 31 . 63 .\
‌   July 8 . 63 .\
‌   ffεb . 25 . 62[*over:* ‘61’]\
‌   Oct . 29 . 62\
‌   Nov. 29 . 62 .

Draught of a Lεtter bεgining ,\
‌ Sεrεniſsime Princεps .

------

\TranscriptionStop

## Halye History of Vegetables (cols. ^B^7–8, ^C^1–2; B2^v^–C1^r^)

\TranscriptionStart

[**Col. ^B^7, B2^v^:**]

[*three lines centered:*] A Bundle inſcribd\
Halyε Hiſtory ^of^ Vege -\
tablεſ .

Treatiſε bεgining That y^ε^\
‌ Hiſtory of Vεgatablεſ. M.S.\
‌ of 24 pageſ written.

D^r^[*canceled:* ‘D~~r~~’] Colεſ Trεatiſε inſcrib\
‌ ed to D^r^ Cox . 6 fol .

Trεatiſe bεgining , Thε Al\
‌ kalicalε &c .[possibly Slare’s] 7 fol .

Printed Paper intitled Aſsar\
‌ tioneſ optica dε Fridε . [possibly Freind]\

Printed half Sheet begining , Que\
‌ ryſ to bε propounded .’

Rεlation of a Swordfiſh $\frac{1}{4}$ Sheet

Ray to Old . Nov. 30. 74 .\
[*canceled:* ‘~~Dr~~’] with a Diſcurſε of yε Seedſ\
‌ of Plantſ . 10 fol .

Liſter’s Lεttεr 12 Mart. 73.\
‌ with a Catalogue of Shellſ .

1 Sheet bεgining , Rεſponſiones .

Paper bεgining , Aug . 30 .

Caſſini Methoduſ investigā di 3\
‌  Sheets

Obſεrvt ofoure Eclipſ of y^ε^ Moone\
‌  8 Sεpt . 71 .

Lεttεr of Trεmont 20 June. 70\

Improvεmt of Corneall by\
Sεa-Sand .

Paper intitled , Mr Rayſ Paper .

D^r^ Brownſ Anſwer to Queryſ .

Grεw to Old 12 Mart. 71. 4 fol.

Giornale dε Lεttεrah

Diſcourſe of Vitriol. 1 Sheet .

Sεvεral ways of analyſing vε\
gεtableſ . 4 fol . [*bracket to line below*]

Diſcourſε continud of Vitriol . ^fol. 7^

Hook of Arithmetical Instru-\
ments .

Printed fig . of y^ε^ Utεruſ ·

Part of D^r^ [*uncertain:*]Cores Diſcours June\
3 . 47 pagεs 6 .

[**Col. ^B^8, B2^v^:**]

Dr Needham , de Sεnſanqui\
niſ 2 Sheets .

Paper inſcribd , the portable Ba\
‌ rometεr .

Fig . of y^ε^ Utεrus with y^ε^\
‌ fætuſ .

Boulinia cεntεuaina .

Answer to Queryſ for Barbary .

Colepreſs’s conſideration about\
‌ ſlate .

Hooks obſεrvations , of a buble\
‌ of ſoapwater . $\frac{1}{2}$ Sheet , be\
‌ gining , ye Tubε

Sturmys Account of Tydeſ\
‌ Hungrode .[Hong Road]

Extr . of Babinſ letters to\
‌ Paridiεſ

Mr Hook of Rεfraction of\
‌ Colours . 1 Sheet . 19 June 72

Draught of a Muſhromſ [*uncertain:*]knee

Paper begining , R. H . [*uncertain:*]Showd\
‌ an Experiment .

Schwter’s[?] account of y^ε^ foyles\
‌ of precious Stones .

Paper of Tεlεſcope lightſ . M^r^\
‌ Hooks .

Earl of Sandwich’s paper, fol. 4

M^r^ Boyl of Shining[*over:* ‘Shineng’] flεsh . fol. 4 .

Frεnch Lεtter of y^ε^ Stone of\
‌ Mεxico . 2 Sheets .

Of ſhineing flεsh 2 Sheets &\
‌ $\frac{1}{2}$ morε .

Paper Inscribd, Excεrpta e[?]\
‌ Borrbεtti Epistola .

Dεscription & Scheme of D^r^\
‌ Wrenſ Inſtrument .

Of y^e^ Satεllites of Saturne\
‌ 2 Sheets , bεgining , [*uncertain:*] Cuare\
‌ Sæpius .

Six lεavs bεgining , Judicio\
‌ Rεader .

[*canceled:* ‘a’] Pεcquets Inuεntion 1 Sheet\

Paper of y^ε^ Giornale dε\
‌ Lεttεrati . [**col. ^C^1, C1^r^:**]\
dε Lattεrati

Dεſcriptio Rεgni Johan . Præs\
‌ bytεis ex Oſorio .

Bullialdus’s Calculation of yε\
‌ placεſ of yε Sun & Moone .

Hookſ obſεrṽ. of an Eclipſ of yε\
‌ Moone Sept . 3 . 71 .

Flamſteads [*canceled:* ‘Letter’] Exphemerideſ\
‌ for , 73 . fol. 7.

Account of y^ε^ Cocua Tree
‌ Mar 1 . 2  71 .

Enquiryſ for Borrbary .

Grεwſ paper ($\frac{1}{2}$ Sheet ) intitled\
ſome further account of ye Pith .

M^r^ Newtonſ Lεttεr July 11 . 72 .
being 5 Sheetſ .

Of Rεfraction 2 Sheets M^r^ Hookſ .

Hookſ anſwer to Voſſius’s Hypothe\
‌ -ſis about y^e^ ſpots in y^e^ Moone .

Voſius’s[?] Lεtter & Diſcourſε about\
‌ burning Glaſsεſ &c . 2 Sheets & $\frac{1}{2}$

Copy of Mr Hooks paper about\
‌ Arithmeticall Inſtrumentſ .

Diſcours of yε Spiral fibεrs of\
yε Gutſ . D^r^ Cole . fol. 4 .

Of yε Currεnt of y^ε^ Tydeſ about\
yε Orcadas .[Orkney Islands]

Obſεrṽ. of an Eclips of y^ε^ Moone\
‌ Sept. 8 . 71 .

Flamſteadſ appulſ’ſ of y^ε^ Moone\
to yε fixt Starrſ. 72 .

Draught of Mr Newton’s\
‌ Rεflεx Tεlεſcope - with a Sheet\
‌ of yε Explication of it .

Paper bεgining , Nov . 10 . 68 .

Anſwεr to Queryſ for Borrbary rε-\
‌coended to L^d^D Howard . 2 Sheets.

Paper of Sr R . Moray Dεc . 9 . 71 .

M^r^ Boyl of ye Powεr of ye Atmo-\
‌ ſpherε on Bodys under water. 6 half\
‌  Sheets .

Bullialdus’s [Boulliau, Ismaël] Calculation of yε Eclipſ\
of yε Moone 8 Sεpt . 71.

[**Col. ^C^2, C1^r^:**]

Mr Hookſ Dεſcript . of yε 60 foot\
‌ Tεleſcope for Hεvεlius.

Brookſ letter of Snakes catching\
‌ Rats 17 . Apr . 67 .

[*canceled:* ‘Vεrnonſ copy of Picard’s /’]

A Counciεl paper bεgining , An\
‌ Extract .

Paper begining on one ſidε\
‌ Of good wεather .

------

\TranscriptionStop

## Letters (col. ^C^2; C1^r^)

\TranscriptionStart

Letter Book of y^e^ R. S.\
‌  N . 1 . beeing 416 pageſ\
‌  w^r^ of p . 194 & 194 re\
‌  writen . And a Table\
‌  of 6 fol .

The originals of which a\
in yε Bundle inſcribd ( Origi\
nalſ of Lεttεrs bεlonging to yε So\
cielty from 61 . to 66 .) εxcεpting\
Lεttεr of M^r^ Birchenſhaw of\
Muſick & Lεttεr of Old . to Sachs\
& are to Hεvεlius .

------

## Originals of letters 1661 to 1666 (col. ^C^2; C1^r^)

‌ A Bundle inſcribed , Origi\
nallſ of Lεtterſ bεlonging to y^e^\
Society from 61 . to 66 . beeing\
εnterd in to yε Lεttεr Booke\
of yε R . S . N . 1 . Except\
ing a Lεtter to Old . from Oxf\
Mart . 21 . 66 .

‌   Lεtter from Pariſ 24 .\
Mart . 66 .

‌  Letter from Jacob to\
Sr Pr Wych .

‌  Some Astronomicall Ob-\
ſεrvation $\frac{1}{2}$ Sheet .

‌  Lεtter of Wallis to Old .\
19 Jan . 66 .

‌  Lεtter from Pariſ 28 . Dεc\
1666 .

‌  Copy of Walliſ’s Lεttεr to\
Hεvεliuſ . Apr . 5. 64 .

------

\TranscriptionStop

## [Letterbooks and files] (col. ^C^3–4; C1^v^)

\TranscriptionStart

‌ Letter Book of ye R. S.\
N . 2 . Conteining pagεſ\
371 . of w^ch^ p . 173 & 174\
arε twice numbεrd .

—

‌  The file of y^ε^ R. S .\
Conteining lεavſ 110 . numberd.\
where of fol . 21 . twice , & bε\
twixt fol . 64 . & 65 ſεvεn\
leavſ inſεrted & fol. 86 &\
‌ 87 wanting .  After fol . 110.\
71 leavs morε writen upon , but\
not numberd .  Alſo betwixt-\
fol . 109 & 110 . 1 Sheet inſcribed .\

—

‌  Looſε paperſ in y^ε^ ſame\
Stiched Book , aſ followεth ,

‌ Calculatio Eclipſis &c 5 lεavſ.

‌ Paper bεgining , An Experiment\
of Wheat .

‌ Queryſ of Prεſerving Timber\
$\frac{1}{2}$ Sheet .

4 Draughtſ of a monſtrous\
birth .

‌ Paper bεgining . A Tranſcript\
of a Lεtter [*canceled:* ‘~~of yε Gεn~~’] $\frac{1}{2}$ Sheet .

‌ Mr Willughby accot of y^ε^ Gε\
neration of Inſectſ .

‌ Johnſons Lεtter July 21. 68 .

‌ Enquiryſ for Africk.

‌ Account of [*canceled:* ‘y^ε^’] a monſtrous\
Birth .

‌ Liſt of Tradεſ

‌ Lεtter of David Thomaſ to\
Mr Boyle .

‌ Boyl’ſ Lεtter to Old . with a\
Draught .

‌ Accountſ of y^ε^ weather 1 Sheet\
with a Small paper.

‌ Extr . of a Lεtter producd &c

[**Col. ^C^4, C1^v^:**]

[*canceled:* ‘~~Liſt of Books~~’]

Catalogue of rare Bookſ .\
in ye Emperorſ Library — 8 leaveſ

—

‌ Letter Book of y^ε^ R. S\
N . 3 . conteining pageſ\
371 . bεſides a Table of 12\
pagεſ . Of w^ch^ pag . 27 &\
28 twice . Bεtween pag.\
34 & 35 inſεrted a Paper of\
Draughtſ . Bεtween pag. 290\
& 291 . another . Bεtween\
pag . 148 & 149 a third .

—

‌ Letter Book of y^e^ R. S\
N . 4 . Conteining writen\
pagεſ 378 . besides‸^Index\ of^ 8 pag\
of .  Bεtween pag. 46 & 47\
a paper of Draughtſ . Bεtween\
p . 56 & 57. y^ε^ conſtellation\
Cignus , Between p . 94 & 95\
2 monſtrous births[?] . Bεtween\
p. [*canceled, uncertain:* ‘112’] 102 & 103 an Eclypſ\
Bεtween p. 294 & 295 . Occul\
tatio ſtεllæ .  Bεtween p. 29[*hidden, probably:*]8\
& 299 . another .  Bεtween p\
326 & 327 . Occultatio Satu\
rni .

—

\TranscriptionStop

## Mathematical papers answered (col. ^D^1–3; D1^r^–D1^v^)

\TranscriptionStart

‌ A Bundle of Mathemati-\
call papers Inscribd yε Pro -\
bleme anſwerd .

—

The Probleme anſwerd , 1 sheet .

De Augmēnt[o] Scientiarũ Mathema\
ticarum . 2 Sheets .

‌ 1 Sheets inſcribd , From Mr\
Barrow .

‌ The Statε of y^ε^ Controverſy\
between Hugεuſ & Gregory. 1 Sh^t^.\
with a Letter to Mr Old .

‌ Problema Austriacū[m] .

‌ Longitudinẽ Arcûs circularis\
indagane. $\frac{1}{2}$ Sheet [*canceled:* ‘$\frac{1}{2}$’]

‌ A diſcourse inscribd , To M^r^\
Old . 2 Sheets $\frac{1}{4}$ .

‌ A Paper inſcribd , About\
Fεrguſon . [*canceled:* ‘2 Sheet .’] 2 half Sheetſ

‌ A Memorandũ of M^r^ Collins\
for Sluſiuſ . $\frac{1}{4}$ Sheet .

‌ Problema Austriacũ $\frac{1}{2}$ Sheet

‌ About printing Mathematicall\
books . 1 Sheet .

‌ D^r^ Walliſ’s Lεtter to Borel .\
‌  lius . 1 Sheet .

1 Sheet $\frac{1}{2}$ begining , Have-\
ing lent to &c

$\frac{1}{2}$ Sheet inscribd , to M^r^ Old .

$\frac{1}{2}$ Sheet begining , Leo Tand\
ſpεaking &c

$\frac{1}{2}$ Sheet inſcribd , Anſwer to\
Du Laurεnce &c

1 Sheet begining , Mich . Angε\
li Ricc^ij^ [*canceled, uncertain:* ‘Ricc~~hee~~’] Exεrcitatio Gεo-\
metica .

‌ Vεra circuli εt Hyperb .\

[TODO-GB-uklors]: # begin checking from here

Quadratura . $\frac{1}{4}$ ſheet .

[**Col. ^D^2, D1^r^:**]

$\frac{1}{2}$ ſheet begining , Taquet\
p . 122 .

$\frac{1}{2}$ ſheet begining, Ex [*over:* ‘ex’] duobus &c

$\frac{1}{4}$ ſheet begining , Gageing pro-\
moted.

[*canceled:* ‘1 Sheet’] 3 half ſheets ^inſcribed^[*canceled:* ‘n scibed’]\
Sluſy [Slusius] Mεsolabum .

1 ſheet & 2 quartεrs begining ,\
Lεt thiſ follow y^ε^ account &c

1 ſheet inſcribed , A lyεbaic\
proposition etc .

—

[*canceled:* ‘2’] $\frac{1}{2}$ ſheet begining , To yε\
Charactar &c [possibly CLP/24/12]

1 ſheet inscribd , A diſcoursε\
of ye Roots of Equations .

1 Sheet begining , Problema tra̅[n]s\
miſſuem &c [probably CLP/24/11]

$\frac{1}{2}$ ſheet begining , Veneran de\
Sluſius .

1 Sheet begining , Memorandu̅[m] [probably CLP/24/20]

A paper begining, The[*over:* ‘y^e^’] Advance\
ment. [possibly a response to EL/O1/49]

$\frac{1}{2}$ Sheet begining , To Sluſius .

1 Sheet directed to M^r^ Ord .

Andræi Taquet Opera Mathe-\
mat . poſhuma . 2 Sheets

Lεttεr from Du Lawrεnce [Du Laurens] to M^r^\
Old .  X [perhaps missing or one of EL/L5/13–16]

$\frac{1}{2}$ sheet[*over:* ‘of’] of Calculations.

$\frac{1}{2}$ ſheet of Agriculturε .

1 Sheet inſcribd, For M^r^ Old .

$\frac{1}{2}$ [*over:* ‘1’] printed ſheet, intitled , Dary’s\
Gaging promoted .

A Lεtter from Mr Collins 2 Sheets

1 sheet of M^r^ Collinſ about Mu -\
ſicall progreſſion .

Extract of M^r^ Grεgorys Lεtter\
$\frac{1}{2}$ ſheet .

A ſheet inſcribd, D^r^ Pεlls Book .

$\frac{1}{2}$ ſheet begining, for M^r^ Sluſius.

Dεſcrption of y^ε^ doublε Horiſon\
tale Dial. 1 ſheet .

$\frac{1}{4}$ ſheet beginnig , Tεrtio , de in\
finitis ſpiralibus invεrsis &c

[**Col. ^D^3, D1^v^:**]

$\frac{1}{2}$ ſheet dirεcted to Mr Old .\
- About Mr[*over, uncertain:* ‘Mo’] Hobs . 2 Sheets .

2 ſheets begining , si sit &c .

$\frac{1}{2}$ [*over self*] ſheet begining , Concerning\
Slusiuſ .

1 ſheet begining , Cum̀ in εuſu̅. [possibly ‘Cum in usum’]

1 ſheet inſcribd , Gregorys Book .

$\frac{1}{4}$ Sheet begining , In a right angle

M^r^ Grεgorys Letter to D^r^ Walliſ. 1 Sh .

Caſſin’s method of obſerving y^ε^\
Altitudε of y^ε^ Sun . $\frac{1}{2}$ ſheet .

Grεgorys propoſalls .  $\frac{1}{2}$ ſheet .

Advεrti sauent. $\frac{1 -}{2}$ sheet .

Gregorys anſwεr to Hugεuſ , ſheet $\frac{1}{2.}$

3 Quεryſ w^th^ y^t^ Answ. 2 papers .

------

\TranscriptionStop

## The Bullet (cols. ^D^3–4; D1^v^)

\TranscriptionStart

[*two lines centered:*]A Bundle inſcribd\
The Bullet .

Sharras Lεtter to Gruban\
dol . Mart. 19 . 70 .

Nεlſons Lεtter to O . Dεc.15‸^70.^ [*canceled:* ‘~~7~~’]

Doεlineurtius[?] to O . June: 74 .

Dodington to O . Jan . 30 . 71 .

Mart . Fogεlius to O.

Fεll to M^r^ Boyl Nov . 1 .

^D^Walliſ to L^d^ Broun^c^ker . 3 ſheets.

D^r^ Wallis to O . Fεb . 18 . 73 .

Hugεns to Grub .[*over:* ‘O’] Aug. 10 . 75 .

D^r^ Walliſ's anſwer to M^r^ Liſters\
Lεtter . Jan . 12 . 7¾ .

‌ J . G . to O.

Alεx. Marſham to M^r^ Maurice

Abrah . Brued.[?] Seganius[?] to D^r^\
moorε 3 Sheets .July 20 . 70 .

A paper to M^r^ Boyl inscribd,\
A Character of y^ε^ Society..

Nicholſon [*canceled:* ‘~~to O~~’] from Dublin\
2 half Sheets. May 10 . 76 .

Lεttεr inscribd , Conserning\
divεrs particularſ of Nature & Ar[t?]\
2 Sheets .

[*canceled:* ‘~~Hεvεliuſ~~’] The 4^th^ & 5^th^ Sheets\
of a lεttεr from Hεvεliuſ .

[**Col. ^D^4, D1^v^:**]

J. G . to O . Nov . 17 .

A paper about a Comer ¼ ſheet .

Caſſini to O . Aug . 7 .[*over?*] 75 .

Hugεnſ to D^r^ Wallis . Nov. 13 . 68 .

Sluſius to y^ε^ R . Society. May 19. 74

Hawly to O . Aug : 8. 76 . together\
with a Sheet begining , Methoduſ\
dirεcta &c .

Newton to[*over?* ‘Newtonro’] O . Jan . 25 . $\frac{75}{6}$ .

Dalence[*over?*] to O . Sεpt . 2 . 76.

Hεvεlius to O . Aug : 18 . 74 .

Extract of Flamſtead to Collins .

Bεnſon to O .

Linus[?] to O .

Leibnit to O . July 15 . 74 .

2 papers , one begining Quando-\
quidem com̅[m]oditas . The other\
Inscribd , Johan . Kεplεri Ma-\
nuſcripta .

Vεrnon to O . Dεc : 24 . 70 .

Hεvεlius to Flamſtead . 3 Sheets\
Jan . 2 . 77 .

1 [*inserted:*]½ Sheet begining , Ephemeriſ No-\
vani̅[?] Stεllaru̅[m?] .

Paper to B . W . Jan . 14 . 6$\frac{2 }{3}$.\
¼ ſheet .

Voyturεſ[?] Lεtter to Card. Maza\
reen .

Mazareen to Voyture .

Paper [*canceled?:* ‘~~inscrbd tite~~’] begining , ſi Fla\
[?] inter alia Scribit . all yε\
rεſt Dutch .

Paper begining , Dε balbuti-\
ente[*over?* ‘enr’] illo .

P. Cherubri to O . Apr . 20. 75.

Dodington to O . Apr . 14 . 71 .

Colpreſſ to Grub . Nov . 20 . 68 .

Fermat to O . Fεb . 4[?]. $\frac{69 .}{70}$ 

[*canceled:* ‘~~¼ sheet~~’]

------

\TranscriptionStop

## Another bundle (col. ^D^5; D2^r^)

\TranscriptionStart

[*centered:*] Another Bundle

Travagini Diſquiſitio .[?]

Lεtεra dεl Roſεtti .

Molinetti Diſsartatio .

Il Bumbardeeno[?] Veneto. imꝑ[er]fεct .

A List of Seedſ ſent Mr Howard.

Flamſteads Appulsεs 71 .

Malpighi’s Draughtſ & Trεatiſε

Rεſponſε dε Mr Marriotte

Hobs Dialogueſ , man . sc .[?]

Elementſ of Speech .

Appendix of pεrſonſ dεaf & dumb.

Lεtter of Hεvεliuſ . 1675 .

M^r^ Evεlyus Sεmbradorε

Flamſteads Epiſtle Nov . 24 . 69 .

Pag . 38 . of a Book [*over?* ‘book’] M. S.

Draught of Patent to R . S .\
Apr . 22 .[*over:* ‘12’] 15 . Car. 2 .

------

\TranscriptionStop

## For the very much etc. (cols. ^D^5–6; D2^r^)

\TranscriptionStart

[*two lines centered:*] A Bundle inſcribd\
For y^ε^ vεry much &c

Old . to Hεveliuſ . imꝑ[er]fεct .

Old . to Sluſiuſ. Jan . 26 .70 .

Flamstead to Old . Dεc : 5 . 70 .

Brook to Collwall . Dec 12 .[*over?* ‘Fer 1’] 68.

Phillips to Walliſ , of Tydεſ.

Fogεlius to Old . Nov . 27. 69 .

Brown to Old . Mart . 21 1 69 .

Yεrbury to Old . Fεbr. 27. 70 .

Childεry to Old . Mart . 29. 70 .

Parεn to Old . Nov . 27. 69 .

Paper beginnig , Clariſſ . vir .

Winthrop to Old . Nov.12.68 .

Clark to Old . Oct . 18 . 69 .

Travagini to Old . Sεpt . 17 . 70 .

Newburg to Old . Aug . 9 . 70 .

Walliſ to Old . Oct . 16 . 69 .

Walliſ to Old . Oct . 17 . 69 .

Hugεnſ to Old . Oct . 15 . 70 .

[**Col. ^D^6, D2^r^:**]

Old . to Ludolph . Dεc : 20. 70 .

Dεniſ to Old . Mart . 22 .

Ludolph to Old . Oct . 31 . 70 .

Du Hamel to Old . May 24 .70.

Axe to Old . Fεb . 26 . 69 .

Draught of a Lεtter by Old .

Corcilly about y^ε^ Mercurial\
εxperiment . Fεb . Cal . 8 . 71 .

Duttamer[?] to Old . Jun . 28 . 70 .

Newburg to Old . Fεb . 28 . 70 .

Willughby to Old July 19 .

Draught of a Lεttεr begining,\
‌ Sεrεne Prince.

Newburg to Old .

Dεs Fontanis to Old . Mart. 8 .\
- 6$\frac{9}{70}$

Paper of D^r^ Mertεt[?] to Old .

Newburg to Old .

Richard to Old . Sεpt . 7 . 70 .

Souza to Old . Cal . Jul . 69 .

Lεtter to Old . from Amſterdam\
Aug : 5 . 70 .

a Notε to Grubend .

½ Sheet begining , Pεtenda\
ex Anglia .

$\frac{1}{4}$ Sheet begining , Pεtenda ex\
Anglia .

------

\TranscriptionStop

## A Monsier Mr Grubendol (cols. ^D^6–7; D2^r^–D2^v^)

\TranscriptionStart

[*three lines centered:*] A Bundle inſcribed\
A Mounſier Mr\
Grubendol.[*over?* ‘O’]

LεwεnHoεck to Grubendole\
‌ Dec : 20 . 75 .

LεwenHoεck to , Old. Fεb . 22 . 76.

2 Sheetſ $\frac{1}{2}$ & $\frac{1}{4}$ .

LεwεnH . to Old . Jan. 22 . 76.

LεwεnH . to Old . June 1. 74 . [*canceled:* ‘~~2 Sheetſ~~’]

2 Sheets with 4[*over:* ‘3’] papers .

LεwεnH . to Old . 3 half Sheets\
‌ Oct . 19 . 74 .

LεwenH . to Old . 2 Sheets July . 6 . 74

LεwenH . to Old . 1 Sheet $\frac{1}{2}$ Sεpt . 7. 74

[**Col ^D^7, D2^v^:**]

LewεnH . to Old . Sheet & ½ Aug.\
7 . 74 .

LεwεnH . to Old . Sεpt . 7 . 74 .

LεweneH . to Ol . Jan . 20 . 75 .\
‌ 2 Sheets & $\frac{1}{2}$ .\
‌    Dεc : 4 . 74. 1 Sheet $\frac{1}{2}$\
‌ with a [*canceled:* ‘~~A~~’] Draught of y^ε^ Opt . N [??]\
‌    Aug . 14 . 75 . 3 Sheets .\
‌    Apr . 21 . 76 . 1 Sheet $\frac{1}{2}$\
‌    May 29 . 76. 2 Sheets\
‌  with a Draught .\
‌    Apr . 16 . 74 .\
‌    Extract [*canceled?*] of LεwεnH .
‌ lεtter .4 half Sheetſ .\
‌    Fεbr . 11 . 75 . 5 Sheetſ\
‌  & 1 Sheet of Draughtſ .

[*canceled?:* ‘~~Grub Grub notes upon Lεw~~’]

Old^s^. Notes upon LewenH .

LevenH . to Old . Mart . 6 : 75 .

1 Sheet & $\frac{1}{2}$ with 3 draughtſ .

‌   1 Sheet begining , Of\
de baudeheus .

------

\TranscriptionStop

## Het Podagra (cols. ^D^7–8; D2^v^)

\TranscriptionStart

[*two lines centered:*] The Bundle marked\
Het Podagra

Hεvεl . to Old . Apr . 7. 74 .

Lεttεr to Vratitius .

2 half Sheets of D^r^ Bεals .

Stεrprni[?] to Old . Fεb . 24 . 71.

Wittys[?] Lεttεrſ to Old . 2 Sheetſ .

Richard to Old . in 2 papers .\
‌ Aug : 27 . 69 .\
‌ January 14 . 70 . 2 paper .

Flamſtead to Old . Dεc : 23 . 71.\
‌ 3 papers .

Copy of Paſchalſ lεtter to Old .

Travaginy to y^ε^ R. Society .\
‌ Mart . 20 . 76 .

Flamſtead of an Eclipſ .

Caſſini to Old . Jun . 25 . 76 .

[**col. ^D^8, D2^v^:**]

Fivε papers of Hεveliuſ pind\
‌ togεther .

Huer to Old . May. 12 . 71.[?]

Kingſton [*over?* ‘Kingſon’] to Old . May . 11 . 71[?]

Jolly[?] to Old . Oct . 29 . 76 .

Fogεl to Old . Apr . 4 . 71 .

Justel to Old . July . 11 .

[*cancelled:* ‘~~Letter beginn'y Sir when a~~’]

Walliſ to Old . Mart . 24. $\frac{73}{4}$

Job on to Old . Fεb . 28. 6$\frac{9.}{70}$

G. H . from Oxford . [*canceled:* ‘~~to~~’]

Turner’s letter .

Paper inſcribd , Dε Origine[?]\
‌ Animaliũ .

Paper in scribd, Tractatuſ\
‌ in comparabiliſ .

Lεttεr from Cave May 2 . 69[?]

Justεl to Old . Mart. 2 . 68

[*canceled:* ‘~~Beal to Evelyn~~’] ·

Lεtter of Old . to - Oct 29 . [??]

Justεl to Old . with 2 papers[?]\
[*canceled:* ‘~~Auz~~’] of Hook to Auzout

Auzouts [*canceled:* ‘~~to~~’] account of Cam[?]\
‌ pany’s Glaſſεſ .

N . N . to Mr C. of y^ε^ steel[?]\
‌ Bow .

Mr [Mc?] Keazie to Gregory. Fεb. 8 . 75

Lεtter to Mr. Huεl Fεb.ult. 76

Sr Will . Pεttyſ Lεtter . Jan . 13. 70

------

\TranscriptionStop

## Two printed books (col. ^D^8; D2^v^)

\TranscriptionStart

[*inserted:*] two printed bookeſ. viz^l^ .

Maniottij Epiſtola Apo

Mr Dεni’ſ Lεtter .

—

The Statutes of y^ε^ R . Socie[?]\
ty . M . S .

[*canceled, uncertain:* ‘~~Lecniſed Bilſ printed R. S.~~’]

\TranscriptionStop

## Deal Box (col. ^E^1; E1^r^)

\TranscriptionStart

[*two lines centered:*] A Deal Box\
conteining

Malpighiuſ’s Anatomy\
of Plantſ of 24 leavs .

Togεther with 61 Tab.\
of Draughts besidεs y^ε^\
ffrontiſpiece .

\TranscriptionStop

## Letter book no 5 (col. ^E^1; E1^r^)

\TranscriptionStart

[*three lines centered:*] Lεttεr Book\
of y^ε^ R . Socie-\
ty N . 5 .

‌ Conteineth 422 payεſ.\
Bεtween pagε 8 & 9 . a\
Draught of y^ε^ Moone . Bε-\
tween p. 30 & 31 . two\
Draughts of Spots in y^e^ Sun .\
Bεtween p . 97. & 98 . a\
Draught of Mr Newtons Tε-
lescope . Bεtween p. [*canceled:* ‘~~140~~’]
104 & 105 . Picarts Trian-\
gles . Bεtween 190 & 191 . two\
leavs of [*canceled:* ‘~~Draugh~~’] Printed figuεs\
of [*canceled:* ‘~~ye~~’] Comets . Bεt . 194 [*over:* ‘195’] & 195\
Mr. Newtons Schemes . Pagε\
204 & 206 twice . Pagε\
387 & 388 wanting . Bεt .\
414 & 415 three Schemeſ .\
A Table of 8 pages .

------

\TranscriptionStop

## Letter book no 6 (cols. ^E^1–2; E1^r^)

\TranscriptionStart

[*three lines centered:*] Letter Book of\
yε R . Society\
N . 6 .

‌ Conteines pagεs 380 .\
[*canceled:* ‘~~B~~’] Pagε 83 . wanting . Bεtween [**col. ^E^2, E1^r^:**]\
Between p . 168 & 169 Con -\
ſtεllatio Ceti . Between p .\
309 . & 310 . a Lanship .\
Bεt . 378 & 379 . a [*canceled:* ‘D’] Sheet\
of Draughtſ of Stones .  A Ta\
ble of Six pages .

------

\TranscriptionStop

## Papers of the Society to be filed up (cols. ^E^2–4; E1^r^–E1^v^)

\TranscriptionStart

[*three lines centered:*] A Bundle inſcribd\
Papers of y^ε^ Society\
to be filed up .

Diſcourſε contεining 36 .\
leavſ beſidεs Title Pagε\
εntitled , Variaſ εcitas[?] &c

Paper begining , Sir R .\
Moray July 22 . 63 .

Paper begining , I dεſire\
‌ to know .

Paper ſuperſcribd To M^r^\
Bogl from Dantsick Aug . 8 .\
68 .

Diſcourſe of D^r^ Timo-\
thy Clark of Alum , contei̅[n]\
ing 16 . leavſ .

Sheet begining , La Grance[?]\
‌ de Scarlate.

Paper of y^ε^ Ld Herbεrt .

Paper begining , An Oxe killd\
‌ at Latham hall Fεb. 13 . 36 .

Du Moulins Paper of Rεform\
ing y^ε^ yεar .

Lεtter to Sir Willia̅m [*canceled:* ‘~~Pεiſal~~’]\
‌ Perſal .

Dεſcriptio monſtri $\frac{1}{2}$ Sheet\
‌ Oct . 26 . 64 .

Draught of y^ε^ Stone in\
‌ Sir Th . Adams’s Bladdεr.

[*canceled:* ‘~~Lettεr of~~’] Italian Letter\
of 4 leavs ſubscribed\
‌ G . G . 21 Oct . 13 .

[**Col. ^E^3, E1^v^:**]

‌ Paper bεgining , upon y^ε^\
mountainſ in North Wales .

‌ Querys & Anſwers about\
Japan 2 half Sheets.

‌ Knigs Paper , beginnig, a\
Relation madε by &c

‌ Browns account of a Pεtrε -\
fyd Bone fféb. 7 . 67 . ½ sheet

‌ Rεſponſεs aux Dεmandeſ\
touchant le Sucres .

‌ ^small^ Paper inſcribd , Hooks obs^r^.\
of y^ε^ Coucel 1664 .

‌ Paper bεgninig , Pour fai\
re dε Sucre candi &c

‌ Sheet of James Oddy . will

a Small paper .

‌ Paper beginnig , a Treue Rε\
lation &c .

Walſh to Sr The. dε Vaux\
‌  Apr . 8 . 68 .

‌ Lεtter to Sr. Th . dε Vaux\
‌  May 27 . 68 .

‌ Paper beginnig , Ruines diſ-\
coverd underground &c

‌ Croonſ diſcourſe of Motion\
5 Sheetſ .

‌ Copy of a Collection of\
y^e^ Alpine Plantſ .

‌ Paper begining At Anche[*over?*]\
na therε is a kind . Another\
with it , beginnig , Conchanal-\
teram longam .

‌ Paper beginnig , Cεra Hiſ-\
panica .

2 half Sheets beginnig , A\
dεſcription of ^a^[*canceled:* ‘~~y^ε^~~’] Diadrome .

‌ Discourse of 30 leavs be-
ginnig a Preambular[?] Letter

[*blotted:* ‘~~L~~’]‌ Lεttεr from Dεl Ga\
‌ [*?*]cts to Dumay . 3 leavs .

[**Col. ^E^4, E1^v^:**] 

Extr . of Letter from Cra -
covia Oct . 26. 69 .\
‌  Another . 26 Nov . 69 .

Kεmp to Old . Aug. 28 . 68 .\
‌  Another of yε ſame date .

Flamſteadſ Paper begining ,\
In Calculo occultationis .

Magalotti to Grub . June.6.\
1668 .

Printed Paper [*canceled:* ‘~~begining~~’], In\
titled Menſuræ Suethiæ .

Sheet of M . S . of y^ε^ ſame\
‌ by Steernhelme .

Liſt of Seedſ ſent to Mr\
Howard from Padua .

Extr . of Walsh's Lεtter of\
‌ Pεtrεfyd Subclameſ.[?] Apr . 9 . 68 .

Catalogue of Mr Howardſ Seedſ.

Sturmy’s’ Obsεrv^s^. of y^ε^ Tidεſ\
at Bristow .

Contentſ of a Box of ſεvε-\
rall Shiffs bought from Al\ 
na[?]

‌ Anſwεr to queryſ about\
mount Ætna . 2 Sheetſ .

‌ Subſidiũ[m] Sεlenographicu̅[m]\
‌  5 leavſ in 4^o^. & 1 Diagram̅.

Portugal Sεcrεtſ of Phyſick\
‌ 4 leavs in 4^o^.

Writεn of y^ε^ Weighſ of\
Mεtals & Liquourſ by Mr\
Collins .

\TranscriptionStop

## Dr Wallis of Deaf & Dumb (cols. ^E^4–6; E1^v^–E2^r^)

\TranscriptionStart

‌   A Bundle inſcribed\
‌ Dr Walliſ of Deaf & Dumb .

Paper inſcribd , Dr Walliſ of\
‌ deaf & dumb men .

Printed Sheet of D^r^ Walliſ’ſ\
con̅cεrciũ Epiſtolicũ[m]

‌ Walliſ to Hugεuſ Aug . 13.68

[**Col. ^E^5, E2^r^:**]

Paper of Walliſ ffεb . 8 . $\frac{67}{8}$

Extr . of Walliſ lεtter to Hu -\
‌ giuſ Nov . 15^o^ . 68 .

Dεſcrption of yε Diamond\
‌ mines , 5 leavſ only .

Boccone’s Lεtter Nov . 20. [*over:* ‘10’] 75 .

Walliſ of y^ε^ Mεrcury ſtand-\
ing higher yn yε ſtandard . 1 sheet\
& a ſcheme , & a letter .

Sheet [*over:* ‘1‘] begining, Rεgiæ Socie -\
‌ tatis .

2 Papers beenig Draughts\
of Inſtrumenteſ chirurgicall .

Paper of Drεtincurtius Apr .\
21 . 76 .

Paper of Querys .

Mεmorialſ for y^ε^ Engliſh Coun-\
cil[*over:* ‘cεl’] at Aleppo .

Mr Hoſkinſ , inquiryſ for y^ε^\
‌ English Councill at Alεppo .

Sr G . Crooks Queryſ for Turkey

Gεnerall headſ of Enquirys\
‌ for all Countrεys , & particu -\
‌ larly for Turkey .

Lεtter to Grub . Mart . 29 .[*over:* ‘19’] 77 .

Read to Old . 27. ffεb . 72 .

Flamſtead to Old .

Hεvεliuſ to Old . 31 . Aug .\
‌ 75 . 2 Sheets.

‌ Grew’s Expeirmtſ [*over:* ‘expeirmtſ’] of yε Solu\
tion of Saltſ in Water . 5 leavſ\
in folio .

‌ New Theory of y^e^ moon[*over:* ‘motion’]
by Caſſini .

‌ Rεmarqueſ out of Chaſtelεt

Lεtter . &c Elεvεn leavs\
‌ in 4^o^.

Flamſtead to Hεvεliuſ\
‌ 1 Sheet . 11 Mart . 76 .

Three half Sheetſ of Dr Walliſ\
‌ bεgining , An Appendix .

LεwεuHεck to Old . Mar .\
‌ 23 . 77 .

[**Col. ^E^6, E2^r^:**]

LεwεnHoεck to Old . Oct . 9 . 76 .\
beenig [*cancelled:* ‘~~one half ſheet~~’] 9 half\
Sheetſ in Dutch , & 5 half Sheets\
English .

1 Sheet of [*canceled:* ‘Shellſ’] Draughtſ.
‌ of Shellſ .

------

\TranscriptionStop

## Dr Clark (col. ^E^6; E2^r^)

\TranscriptionStart

[*two lines centered:*] A Bundle inſcribed\
Dr Clark

[*canceled:* ‘~~A paper~~’] Dεſiderata , from\
Dr Clark .

‌ A liſt of Experimtſ dur -\
nig [*blotted:* ‘to’] Sir  Rob . Moray 's\
Prεſidεntſhip . beenig 14 long\
4^o^ leavs .

A Mathematical Scheme .

Paper bεgninig , Two or 3\
wayſ .

[*canceled:* ‘~~Mr~~’] Stub to Moray . May\
‌ 27. 67 .\
‌  Another Mart . 30 . 68 .\
‌  Another . Mart . 24. 67. 3 Sheetſ\
‌  Part of another . 1 Sheet .

Gagenig Epitomizd . 3 half sheetſ

Draught of Shortgravεſ water\
‌ Lεvεl .

------

\TranscriptionStop

## Bond (cols. ^E^6–8; E2^r^–E2^v^)

\TranscriptionStart

‌  A Bundle inſcribed\
‌   Bond .

Liſt of Books dεdicated to yε\
R . Society .

Draught of an [*canceled:* ‘~~Statue~~’] addition\
‌ all Statute concernnig Elεctionſ .

Paper calld , The State of\
‌ yε Sociεty .

The Corrεſpondεntſ of yε So -\
‌ ciety .

Particular of yε nameſ of yε\
‌ propounderſ of Candidates .[*over:* ‘y^ε^’]

2 quarter Sheetſ begninig Aug.\
‌ 15 . 66 .

Barlow’s Bond.

[**Col. ^E^7, E2^v^:**]

‌ Draugh of yε State of yε\
‌  Society .

Mr Storys Sεnsε upon Dr Holmeſ\
his Rεsurrεction rεvealed in 7\
Bookſ .

Old . to Moray Nov . 7 . 65 .\
‌   Another Oct . 7 . 65 .

‌ Paper inſcribd , morε obsεrvat.\
madε by Mr LewenHoεck .

‌ Paper ‸^begining^ y^ε^ firſt Experi -\
ment ; with a Diagram .

Paper beginnig , To y^ε^ paper\
‌ concεrning yε R . Society .

Paper bεginnig Dε Scriptio Globi &c

[*canceled:* ‘~~Paper begin~~’] Barats Lεttεr 4\
4^to^ leavs . 1 May . 66 .

Old . to Liddel .

Boccone to Grub. 17. Aug. 74 .

Paper begninig , ſome obſεerva -\
‌ blεs .

Printed figure of a Tulip .

Paper bεgining , Opera Taquet .

Propoſals of a Chronological\
‌ History .

Moray to Sir R . Moray Jan .\
‌ 12 . 68 .

Epiſtola Th . Hobbs .

D^r^ Williamſ’s Caphalich\
‌ liquor . $\frac{1}{4}$ sheet .

Part of á Paper bεgninig , At\
‌ Cranebrook in Kεnt .

Paper bεgninig , As to theſε\
‌ Seedſ .

The Uſε & Vεrtue of Trεfoyl

Account of a Magnεtical Ex\
‌ pεriment

Paper bεgining , Then I ſhall\
writε .

Printed Book nititlεd Priſhucti[?]\
‌ ons for yε incrεaſnig of Mulbεrry\
Trεeſ .

[**Col. ^E^8, E2^v^:**]

Paper bεginnig , Boyt dε At\
‌ moſpheeriſ[?]

Paper bεginnig , A Mr Olde[?]

Paper bεgniing, Dans to Com\
‌ mεuſement &c

Voſſiuſ's Paper in anſwer to\
‌ Hook bεgninig , Quod dicit\
‌ Dominuſ Hookius .

‌ Propoſition about a Patεnt Trust[?]

------

\TranscriptionStop

## Bundle B (col. ^E^8; E2^v^)

\TranscriptionStart

[*Two lines centered:*] A Bundlε niscrib[d]\
B .

Beals Poεm of Bacon .

Bloomſ propoſals .

Obſεrvationſ of yε Bara\
‌ meter madε by yε Operator[?]\
‌  An . 64 .

Paper of Tydεſ $\frac{1}{2}$ Sheet\

Hodgſon to Old

------

Chamberlanie to Old. 73

------

\TranscriptionStop

## A bundle within Bullet (cols. ^F^1–4; F1^r^–F1^v^)

\TranscriptionStart

A [*blot*]

------

[*Three lines centered:*] A Bundle dε-\
parated from y^e^ Bun-\
dle incribed y^e^ Bullεt.

2 ſheets of Drawnigs. w^h^\
y^r^ explications, one of a\
boat against y^ε^ Tydε [*over:* ‘Tidε’], yε other\
of a Bath .

Mr Vernors Triangles\
about a degree.

‌ Van - Horns Draught of\
ye Utεrus humanus with yε\
εxplication .

‌ Scheme of y^ε^ Ductus Thora\
cicus with y^ε^ explication. 1 sheet.

Eschinardus’s Letter intitled\
Canturiae Opticae problema.

Book intitled , Diſſεrtatio dε\
‌ Sueiro .[*uncertain*]

[*Cancelled:* ‘~~Paper of Chεlſey Collegε.~~’

3 ſheets of D^r^ Plots dεſigne\
of a Nat . Hist . of England .

Extract of dε la Quintiny's\
Lettεr of Melons. June 15. 63 .

‌  Another Mart . 27 . 63 .

Mr Hooks Optical paper.

Extract of Bεal to Hook .

Rεlation of D^r^ Clarks of a\
man without his ſpleen .

‌ Fig. of Saturn obsεrvd 14 .\
Aug. 1676 .

Draughts of an Engine &\
‌ y^ε^ explication. 2 half Sheels .

Draughts about a microſcope w^th^\
ye εxplication . 1 ſheet . in frεnch.

[**Col. ^F^2, F1^r^:**]

1 ſheet of Draughtſ of raiſ-\
ing water out of Scottuſ, w^th^\
ye Explication in Dutch .  <!--cf. JBO/2/11-->

Draught of a pump with yε Ex-
plication in Dutch .

Draght of [*uncertain:*]Waterworks with\
yε εxplication in Dutch .

[*Cancelled:* ‘~~1  ſheet of an of Hypotheſis phy-~~\
~~sica~~’]

Hypotheſiſ phyſica nova .

One ſheet of yε ſame printed\
at London .

Dεſcription of y^ε^ firy mete-\
or ſeen at Dantsick .

Hiſtory of Ambargrεεſe .

Of yε compreſsion of Air un-\
der water $\frac{1}{2}$ sheet .

Experiment of Colourſ $\frac{1}{2}$ ſtreet .

Copy of Minuteſ for M^r^ Sprat .

Mεlish of Englandſ [*cancelled:* ‘~~Englands~~’]\
Improvement . [*over:* ‘i\~’]

Diſcourſε intitled , Of y^ε^ Mo-\
tivε & man̄εr &c . 1 ſhtet & $\frac{1}{2}$

Mεlish to hiſ Brother. fεb.2. 68.

Enquirys to bε madε in ye W. Indyſ

2 Sheets & $\frac{1}{2}$ of Univεrſal Alphab.

Epigram of y^ε^ touching of yε\
2 Seaſ.

Dirεctions how to tan Lεather .

Conſiderations of transfusion of\
‌ blood .

Grεws Anatomy of Leavſ in\
‌ 3 ſheets .

Sheet of M^r^ Bond .

Order about yε Library .

About yε Rεpoſitory .

Liſt of Arrεarſ for Michael\
mas . 73 .

Paper about yε Repoſitory .

[*Cancelled:* ‘~~Bεa to~~’]

[**Col. ^F^3, F1^v^:**]

Minutes Jan . 23 . 66.\
‌ of Jan . 2 . 66.\
‌ of Jan. 16 . 66 .\
‌ of Fεb. 6. 66.

Paper about y^ε^ Armes of y^ε^\
‌ R. S.

Minutes of y^ε^ Councεll June\
‌ 19 . 68 .\
‌  of June 22 . 68 .\
‌  of May 4 . 68 [*cancelled, uncertain:* ‘~~8~~’]\
‌   May 11 . 68\
‌   May 30 . 68\
‌   April 27 . 68\
‌   Apr. 20. 68\
‌   April. 13 . 68\
‌   Fεb. 17 . 68 .\
‌   Nov. 16. 67 .\
‌   Nov. 5 . 67 [*over:* ‘65’].
‌   April. 25 . 67 .\
‌   Apr. 29 . 67 .\
‌   May 23 .[*over:* ‘15’] 67\
‌   June 3 . 67\
‌   Sεpt . 30 .67 .

Minuteſ of y^ε^ R. S. May 31.\
‌ [*cancelled:* ‘~~1667 .~~’] 1677 .\
‌  of June 7 . 77 .\
‌   June 21 . 77 .

Minuteſ of y^ε^ Council\
‌ Nov. 18 . 68\
‌ Nov. 23 . 68 .\
‌ Aug . 10 . 68 .\
‌ Nov . [*cancelled:* ‘~~10~~’] 5 . 68.\
‌ June 29 . [*over:* ‘27’] 68\
‌ July . [*over:* ‘June’] 6 . 68\
‌ July 13 . 68 .\
‌ Fεb. 24 . 67 .\
‌ Mart . 26 . 68 .

[**Col. ^F^4, F1^v^:**]

Minutes of y^ε^ [*blot*] Council . Dεc.\
‌ 18 . 72\
‌  of Nov . 13 . 72\
‌   Nov . 27. 72

Rεport of Gεofrey Palmer\
‌ of Chlelsey Collegde.

Minutes of y^e^ Council .\
‌ 12 June 72 .

Rεcom̄endatory Lεtter of\
‌ Mr Colepreſs .

------

\TranscriptionStop

## Bundle made out of some others (cols. ^F^4–6; F1^v^–F2^r^)

\TranscriptionStart

[*Two lines centered:*] A Bundle made\
‌ out of ſome others.

Indεnture madε between y^e^\
Society & Mr Hill of a\
Patent .

Minutes of June 4 .\
‌  other Min w^th^ one date .\
‌  of Jan . 29 . 7$\frac{2}{3}$ .

Rough Draught of Hickmans\
‌ Inſtrument for beeing Printer &c  <!--DM/1/110 or DM/5/42-->

Minutes of Oct . 26. 76 .\
‌  of Dec. 14 . 76.\
‌  of Jan . 18 .  76 .\
‌   Jan . 25 .[*over, uncertain:* ‘26’] 76\
‌  of Fεb . 1 . 76 .

Ordεr of y^e^ Councεll Mart .\
‌ 23 . 63 .

Cεrtificate of Priſoners .

A Dεputation to M^r^ Boyl\
‌ for beeing Vice–Pr .

Minutes of June 14 . 77 .

Rough Draught of Auzouts\
[*cancelled:* ‘~~Dip~~’] Diploma

Rough Draught of a Sum̄onſ\
‌ to meet at Grεſham Coll .

Liſt of Bεnefactorſ. 

Paper begining , Munday fεb. 25.\

Rough Draught of a Bond .

[**Col. ^F^5, F2^r^:**]

[*Cancelled:* ‘~~Minutes of a Council .~~]

Liſt of Arrεars for Michael .\
‌ maſ . 73 . [*pen skip*]

Paper indorſd , Argent vive.

Paper bεginning Pliny & Muffεt.  <!--Thomas Muffet?-->

Contributers of fivε [*cancelled:* ‘f’] poundſ .

Minutes of Jan . 14 . $\frac{74}{5}$

Arrεars for Mich . 73 .

Sr Nich . Stuards εngagem^t^.\ <!--Steward? see JBO/1/9 engagement to perform duties?-->
‌ Another of yε ſame .

Sir John Clayton's Lεtter .

Contributors to y^ε^ building for\
‌ yε R. Society .\
‌  Another paper of y^ε^ ſame

Rough Draught of a Dεclara\
‌ tion of y^ε^ Society .

Lεtεr of Sir J. Hoſkins Jan .\
‌ 26. 74 .

Rεport to y^ε^ R. Society Dεc.\
‌ 22 . 73 . cancεlld .

Gilbεrts Lεtter with a Bill .

Liſt of names of y^ε^ Society\
‌ as arε rεmitted ye wεεkly\
‌ contrib . in whole or in part .

Ordεr about Arrεars April .\
‌ 27 . 68. with Childs cεrti-\
‌ ficates

Stanhops Lεtter to M^r^ Col-\
wall .

‌ Subſcriptions to[*over:* ‘for’] M^r^ Old.\
‌ Dεputation to Bp of Excer.\
‌  tεr for beeing V. Pr .

Minuteſ of Mar^t^ . 1 . 76.

New Council 76 .

Ordεr of Council Apr. 27 .\
‌ 70

Forme of a Bond .

Rough Draught of an Ordεr of\
‌ council June 3 . 68 .

Draught of Hickmans Instrūent

[**Col. ^F^6, F2^r^:**]

Mr Howards propoſal of Chel\
‌ sey College .

Liſt of pεrſons in Arrεars .

Paper intitled , Particulars to bε\
‌ rεgiſtred .

Paper bεginning , Query w^t^ name [*uncertain, whether ianue?, a[bou]t isuue?*]

Paper bεgining , Quoū of yε\
‌ Council.

Mr Howardſ propoſals .

Buſineſſ of y^ε^ officiating\  <!--see CMO/1/239-->
Sεcrεtary of yε R. Society .\
‌ Another of yε ſame .

Expεrimts & Obſεrv^tſ^ rεcom\
mended to Mr Hook . 1 ſheet

Paper begining , Paralyſioine[*uncertain*]\
‌ ice[*uncertain*] to bε prεſεnted &c

Paper bεgining , At y^ε^ first &c

Draught of a Lεtter . Dec.\
‌ 17 . 74 .

Minuteſ Nov . 5 . 68 .

Ondarts[*uncertain*] Lεttεr to L^d^ Brounc\
‌ -ker

Paper about yε Charter of\
‌ yε R . S .

Frεnch Comædians Bill .

Weeks his lεtter fεb. 3. 76.

Draught beginnig , London\
‌ . . . . of Jan . 76 .

Paper bεginning , whether yε\
Prεſidεnt appoyntieſ [*possibly:* ‘appoints’] &c 

------

\TranscriptionStop

## Orders and minutes of the Council, &c. (cols. ^F^6–8; F2^r^–F2^v^)

\TranscriptionStart

Ordεrs & Minuteſ of yε Coun-\
‌ cill &c

M[inute]. June 18 . 74\
O[rder]. May 11 . 71 .\
M . Jan 5 . 73\
‌       21 . 73\
‌   Fεb . 26. 73 .\
‌   Mart .18. 68\
‌   Dεc . 22 . 73\
‌   Nov . 27 . 73\
‌        13 . 73 [**Col. ^F^7, F2^v^:**]\
M . Nov. 6 . 73 . <!--TODO continue here, p. 22 of PDF-->

Oct. 22. 7

9.73

Sept 29. 74

Dec. 4. 66

0.

Oct. 15. 74

M

Sept. 7. 74

M

Oct. 30. 74

M

Oct. 21, 69

Inne 3 69

M

begining, Resoliquer & Het Podagra.

Paper

June 28th 70

M

July 26. 70

Oct. 11. 69.

Apr. 27. 69.

Nov. 24. 70

List of Correspondents.

Arrears of some of the scotch Thorlacy Dissurtatio de Is

étabilités.

Arrears of the Nobillity for

73

Arrears of the Physitians. 73. The last ye leavs of an Ita

Paper begining Arrears due &c.

Draught of a 2 Engagement

für Cents

May 16. 65

ffeb 1.68

M

Nov. 23. 74

12. 74

ffeb. 28. 74

Oct 21, 75

Marl. 25. 75

Junl 17, 75

May 20 69

June 24, 69

Jan. 21. 74

Apr. 10. 72

Jan H. 74

[**Col. ^F^8, F2^v^:**]

Jan 10, 69

M

May 11, 71

Nov. 9. 71

9. 74

Oct 11, 69

Nov. 29. 69

\TranscriptionStop

## Books (col. ^F^8; F2^v^)

\TranscriptionStart

Burdle Jcken

Books.

Antwoord van Louis de

Bills.

The fire eaters Bill.

Pigron Sellers Bill

News to ye whole world.

Risposta Di Borelli. p. 3

landia.

Printed that of the Trans:

tion of blood at Paris.

han book.

Malpighins's Papers 51 he

sheets. together with 8 h

sheets & 2/4 sheer, & 7

of Mr Old. writeing, & the

Ordrs of Council for print

my them.

\TranscriptionStop

## A bundle taken out of another, bis (cols. ^G^1–3; G1^r^–G1^v^)

\TranscriptionStart

A Bundle taken

out of mother.

Draught of a Letter to m

vite Subscriptions.

Minutes begining, 1668.

sheet begining, of three

continued proportionals

Relation of a strangearrnds

Clap & Stralsound. 1 sheet

ffive leavs in folo begining

William Burge

Succini Historia. 38 leaus

in 4d. inchen in Dutels

Cowley's Letter with a Poem

in 3 Sheets.

Beal to Evelyn, upon his

furne of ye P. Ode.

Grew’s Anatom & of flowers

five half Sheets.

frew of Essent. & Marine

Salt of Vogetables. 4 half

sheets

Copy of Balles list of Magne

Peal Implements.

New Council for 75

Order of Council Nov. 7. 66.

forme of the Refemnee.

Paper intitled, A Copy of

a letter for granting &c

Report of Mr. Hofkius

Dec. 19. 63

Minutes of Mart. 22. 76

of ye Council Nov. 11. 75. dor. Imperfect

kins, Baul, & Aerckin

[**col. ^G^2, G1^r^**]

Paper beging, At a meeting

of ye Council of ye R.S.

List of undertakers for severat

matters with a Leber to ex

cite them yrte.

Minutes of ye Council Nov. 27, 66

Paper begining, Sir Anthony

Morgau

Minutes of ye Councel Mart

8. 68

Mart 1. 68.

Nov. 5. 66.

Arrears at Mich. 66

Lill of nawles

Ordre for dispensing with

Dr Bathurst & Barrow

Minutes of August Conne. Ang

8.66

29.66

June 4. 66.

Apr. 23. 66

Order to Mr Colwall

Min. of ye Counc. ffeb. 21. 66

Irish Petition.

Capty. of Hevelins’s Diploma

Min. of ye Counc. of Dec. 3. 74

17 Dec. 74

Quarter of sheet begining

Mr. Hook

Minules of May 14, 73

of the Conveil Mart. 1. 76.

Minutes of Mart. 8.76.

of ye Counc. Jan. 11. 67.

Jan. 2. 67

Print of the spavish Sembr

Nov. 29. 75 Draught belonging to the same

July 1876 Draught of a double Goosegg

Mart. 6.75 Some Draughts belonging to Mr.

A report signed by Mr Hof-Newtons discousse. 1 steet

there is not of themings wood

[**column**]

Draught of Buchets for rais

nig water

Mr Anbegs paper of ye hero

Clepsydra &c

Explanation of Sr. Buratti

n 5 Jnstrument. 1 street

The same in Italian.

2 Sheets passed together of Scheny Relation of Tenarif 4 payes

belonging to ye savil.

Cheshire Dairys 2. Sheets

Scheme of Cirdes.

Schewe about yr gravitation of ye Hamller & sin sibl

of water.

Thee Schenes in scribd Feb. 1. Description of Hirta 2 p

Explication of an Instrument. Fyr force of a mans beach 2.7.

1 Sheet

Explanatio notarum &c

Calalogue sent to Newburg.

Nov. 28. 69

small paper in High Dutch

Order of Council Nov. 23. 76.

Paper begining, Smooth platte

of steel &c

A Catalogue of Authorstrat

ing of Colours. M ffrench.

\TranscriptionStop

## Originals for the register book (cols. ^G^3–8, ^H^1–2; G1^v^–H1^r^)

\TranscriptionStart

Original of ye

Reg. of ye R.S.

Quast. for Terceriff.

2 pages.

Othor Quest. for Tanar

2 pages

Exped of ye production

20

Na of Colours payes 3

The Ofyr Teinture of the body

the of a free-3 payes.

the Art of Refineing. 8 payes

not of come in you

[**column**]

Experts of Cupeling 2 paye

Of Barnides 4 payes.

of Refinemy 3 pages more

Of green Copperas 6 pages

Paper beginnig, Mr President

4 pages.

Another 3 payes

of Glass drops 3 payers.

of Alum. 8 payes.

plant, 4 pages.

of finding 2 mean yno porti

onalles 1 paye & a scheme

Frimadonsdoms upon it, p

Othar Trimadver

A scheme of yr Orb of ye

arte & meone.

other primadvers on the for-

mer propleine 2 pages

fyr Oculus mundi 2 pages

Fyr. Barometer Observt 10 pages

makeing of Marbled paper 3. payes

oféeighing a Carp. 1 paye

Directions for observas ye

Ellypses of ye C. 2 pages

Ld. Bronnck. Demon strasion

of ye motion of a Pandulum

3 payes. Schemes) page 1

Of Oyl in ye torig waebe. 1 pag

frefining Gold by Antimony

0

5 payes.

Thetem peating of ye same

hind of expert. 3 pages

of bounding ye duplh of ye

Fra. 2 pages.

furum Musicum. 2 pages

Mercurall Expt. 2 pages

[**page**]

fnother mercurial expt.

1 page.

Walles's account of this ex

periment. 5 payes

Ld. Brounck. account of ye

same 1 paye

in New Engl. 2 pages.

ages

of Cydyr 2 payes

of Colchesse Oysters 3 payer

Zuays for yr East Indys, pays

Morrofyr same by Mr Collwall

ray.

2 payes

Fye diversity of ye parts of

water of payers.

Of ye "Patellites of Jupiter"

5 pages.

b8

my 1 page

Adcomtofye Wispering

place at Glocester 3 pages &

1 page of Schemes,

condienda. 1 page

Account of an Echo, 4 pages

of the rare-faction of ye fer

3 payes

of ye figures of froven unive

3 payes & one of Schemes.

sexhalation saisd from wa-

ige fer. 2 pages.

of Colchester Vysters 5 payes

of adering wine. 8 pages

Enquiry for Ireland. 1 page

several dislances from ye

each. 3 payes.

of para. 9 pages.

& 1 scheme.

[**column**]

Of Windmills in Holland 2 pages

of making Maull in Scotland 3 pages

of insreasing fall in great quantity

1 page

of wrighny bodys in water

& pagers.

Account of building ships of fyr differing vright of warne

& cold water.

of damps 2 pages.

of ye weight of water & ye

spring of the far 4 payers

Mr Hoshus yr same 2 payes Repert of Sr Wm Petty Ship-

12 pages.

Another Report 7 payers.

of ye repachion of Ire. 1 payer

of the fore of falling bodys paye

epe Sulphnous vapour near

Wygau. 1 page.

200 phytal Engraft a things to be further desired

of Sir Ph. Vernatty & some

Qua rys. 4 pages

Expts. of Dr Civone 1 paye.

Quarys of yr Condensary Engine

pages

Paloum ad Ariu cadavera Expis propounded for ye condain

my Eugine 3 pages.

Experts with glas balls, 4 payes Accomt of a Tench in ye

Exhaust ny Eugène 1 payer

Of a Carp in the same. 1 pagre

tyr for enhausted out of

water 2 pages.

Fyr Sulplur & titriol miveral

at Leige 2 pages

of Smutty corne 4 pages

Monecon is about writing

of Liquors. 6 pages.

fyr will bored at Amster.

I paye.

of ye difference of Gravityat of uniting of for & waller

2 pages

of volatiliracy fall of Tar

far 2 pages

The same in French 1 payer

Ehes a/ of the same.

[**page**]

Of Hakbing prgerns 2 pages

Inouet of a Gyant child &

a Herring 1 page

of paper out of ye liguess of

ye Harring.

Of a very dunks upon ye

Coasts of Coromandel. 2 payer

Eff Cyder. 13. pagus.

Of a woman's voyding the bores

franck way of lulms of ye stone

3 payes.

The same in French. 4 pages.

Of Cyder, 4 pages

Description of Cavallerius’s Hy

drontisterium. 1 page

Sye multiplymy of Carps.

1 page

Hyr Tydis at St Hilena 1. p. Expts of May den 3 pages

of nene Caudbershick. 3 papers

Descriptions & Scheme of a

Powder myer 2 pages.

Descript. of yr we set of

Landlestich etc. English 3 pages

Antopsia Corpons Comitis de

Belcarns 1 page

Paper of Mr Digges, paye

of houding yr 18th of ye sea

1 payer Instrumenty.

Instrurts for the same 2 pages

to prevent the not in sheep, paye

Account of Pandul. Walches at

bea. of pages.

further account 2 pages

fuswer to Quaeuys for the Ease

Judys. 2 payes.

observant of ye shining diamond

N.o

of pages.

Of a wasler clock. 1 payer

(the Scheeue & wanting)

o

[**column**]

favent of saffron. 13 pay

& 2 pages of Draughts.

Arouct of Tuinines + pages

Experiments of ffræzing. 2 page

Cfyr Hygrescope 1 payer

Direction for a history of ye W

ther. of payers.

of the wheel Barometer, pay

Eggs. 4 payes

Expect of the weight of the Arn, and

of a child at ye side of & c 2. p. Prince Ruperts way of maken

shott. 3 pages

Expt. of Spir. of Nine nig.

an ex haussed vssel 3 pay

of divinig medic water, 4 pay

Jurint ofyr iseing of Wal-

in a Bott Head &c 1 page.

of yr condensation of ye f

rit of wine & water. Spag

of freering 3 pages

Mr Eaphy of May drie 3 pay

Enquirys for Egypt. 3 pages

of ye Inimines of Droouth.

2 pages

Paper of Subtervan. fire.

oféeighing ℥rass above &

under ground. 2 pages,

Bral of Parchment 6 pag

Of a prterfyd Elme 1 page.

of yr effects of Thurder 2 pay

Inswere out of E. Judys to

several Auriges. 20 pages

of Rattle snakes. 1 page

beservt of the Long Sube. 1p

Enquiys for Guiny. 1 page

Account of a Piper. 2 page

of frog spaun. 2 page

Directions for finding the re

sistance of the four to bodys

moved through it. page.

[**page**]

moved through it. 1 page 1

considerat. of a Universel

Mensum. 3 payes.

M. Panificiis 5 pages

effectu of ye Oyl of Tabac

to 3 pages.

Appo ye Colebean poy.

fort. 2 pages.

of Deme. 8 pages, with

1 small draught of ye fur

was.

of ye Juflection of direct

A Scheme

Account of ye duach of Mr.

Brooks. 1 page

Paper of Sr Gill. Talbo of

sulpher titriol flem, &c

Minium. 2 payes.

bea. 25 pages.

A new pignifich Expor. 12

passes.

Letter of Mr Townly, 2 pages of the Ewelt Earle dry up

of Ant. 10 pages.

History of Alkernies 3 pages

Account of Prudulums 1 payg

& 1 daheme.

Of ye Transfusion of Blood

2 pages.

Propotals of Transfusing

. blood. 3 pages

A new Level, page

Eclips of ye Moore June 22 of Horsis Eyrs. 2 payes.

1666 1 payer.

Figurer of Saturne through a Of Tydes, 1 page

Sixty foot gless. 1 paye.

[**column**]

Descriptiare of ye Camp-poyte

1 paye & 3 figuses.

Descrption of the Instrument for

makeing all manner of Diats.

2 pages, 1 scheme.

Figures of Hail 1 payer.

Account of Transfusion

3 pages.

History of you in ye first Boue voyde out of ye Blad

er &c 2 payas.

Exp. of Transful. 1 page

Another of ye same 2 payes.

önother of ye same of pages.

motion nite a Curve. 31 pages, Curtius's Letter to Haac;

2 pages

of meting a Day draw his

breatle vite a broken vinded

horsz. 2 pages

of blowny through yr lung

of a day. 2 pages.

Of ye Ebbing & (flowing of the Melled of Transfus) 1 page

Demonstr. of yr Indian period

13. pagst.

C. S. Transfasion 3 pages

ol Hogsden 2 payer.

Cf. Transfusion 1 page

of Respiration 2 pages.

obseror of shining wood &

furning goal. 8 payes.

Exp of ye Deed. Chyliferns

2 pages.

of an Hermaphrodik 2 pay

The Barque of Treas. 2 pay svay of finding the pressum of

yr for at sea. 1 payr.

A secret in pointing 18 pages

rage

Of Light & far.13

------

[**page: blank**]

[**page: blank**]

[**page: blank**]

\TranscriptionStop
