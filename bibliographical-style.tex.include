%% basic setup

\definefontfeature[default][default][protrusion=quality,expansion=quality,script=latn]

\setupindenting[yes,0.5in]
\setupinterlinespace[big]
\setupheads[indentnext=yes]
\setupwhitespace[none]
\setupfootnotes[location=page]
\setupalign[flushleft,nothyphenated]
\enabletrackers[fonts.missing]
\setupheadertexts[\hfill][Ascher, \pagenumber]
%\def\blankpages[#1]{%
%    \dontleavehmode \page[yes]
%    \dontleavehmode \vfil
%    \hfil {\ssc \bi Approximately #1 pages, not yet prepared.} \hfil
%    \vfil
%    \page[yes]}

%% special features

\definefontfeature[no-ligatures][liga=no,dlig=no,hlig=no,calt=yes,ckrn=yes]

\def\StartTranscription{\stopnarrower\setupindenting[no]\setupnarrower[left=2cm]\startnarrower[left]\tfx\setupinterlinespace[big]\feature[+][no-ligatures]}

\def\StopTranscription{\stopnarrower\BiblHangingIndentsStart\tf\setupinterlinespace[big]\feature[+][no-ligatures]}

\def\BiblHangingIndentsStart{\setupnarrower[left=0.5cm]\startnarrower[left]\setupindenting[yes,-\leftskip,first]\feature[+][no-ligatures]}

\def\BiblHangingIndentsStop{\stopnarrower\setupindenting[yes]\feature[-][no-ligatures]}

\usetypescript[junicode]

\starttypescript[junicode]

    \definefontfallback[SerifBoldFallbacks][file:AGaramondPro-Bold.otf][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldFallbacks][name:dejavuserifbold][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldFallbacks][name:symbola][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldFallbacks][name:Apple Symbols][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldFallbacks][name:FoulisGreek][0-FFFFF][check=yes,force=no]

    \definefontfallback[SerifFallbacks][file:AGaramondPro-Regular.otf][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifFallbacks][name:dejavuserif][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifFallbacks][name:symbola][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifFallbacks][name:Apple Symbols][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifFallbacks][name:FoulisGreek][0-FFFFF][check=yes,force=no]

    \definefontfallback[SerifItalicFallbacks][file:AGaramondPro-Italic.otf][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifItalicFallbacks][name:dejavuserifitalic][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifItalicFallbacks][name:symbola][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifItalicFallbacks][name:Apple Symbols][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifItalicFallbacks][name:FoulisGreek][0-FFFFF][check=yes,force=no]

    \definefontfallback[SerifBoldItalicFallbacks][file:GaramondPremrPro-BdIt.otf][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldItalicFallbacks][name:dejavuserifbolditalic][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldItalicFallbacks][name:symbola][0-FFFFF][check=yes,force=no]
    \definefontfallback[SerifBoldItalicFallbacks][name:Apple Symbols][0-FFFFF][check=yes,force=no]    
    \definefontfallback[SerifBoldItalicFallbacks][name:FoulisGreek][0-FFFFF][check=yes,force=no]


    \definefontsynonym[Junicode-Regular][file:Junicode.ttf][features=default,fallbacks=SerifFallbacks]
    \definefontsynonym[Junicode-Italic][file:junicodeitalic][features=default,fallbacks=SerifItalicFallbacks]
    \definefontsynonym[Junicode-Bold][file:junicodebold][features=default,fallbacks=SerifBoldFallbacks]
    \definefontsynonym[Junicode-BoldItalic][file:junicodebolditalic][features=default,fallbacks=SerifBoldItalicFallbacks]

\stoptypescript

\setupbodyfont[junicode,12pt]

\definefontfeature[swashes][always][swsh=yes]
\definefont[itgaramond][AGaramondPro-Italic*swashes]
\definefont[itwarnock][WarnockPro-It*swashes]
\definefont[itjunicodeRX][file:JunicodeRX-Italic.ttf*swashes]
\definefont[ithoefler][HoeflerText-Italic]
\def\swash{\feature[+][swashes]}
\def\swashA{{\itjunicodeRX A}}
\def\swashB{{\itjunicodeRX B}}
\def\swashC{{\itjunicodeRX C}}
\def\swashD{{\itjunicodeRX D}}
\def\swashG{{\itjunicodeRX G}}
\def\swashJ{{\itjunicodeRX J}}
\def\swashK{{\itjunicodeRX K}}
\def\swashk{{\itjunicodeRX k}}
\def\swashM{{\itjunicodeRX M}}
\def\swashN{{\itjunicodeRX N}}
\def\leftswashN{{\getnamedglyphdirect{name:JunicodeRX-Italic}{N.leftswash}}}
\def\rightswashN{{\getnamedglyphdirect{name:JunicodeRX-Italic}{N.rightswash}}}
\def\swashP{{\itjunicodeRX P}}
\def\swashQ{{\itjunicodeRX Q}}
\def\swashR{{\itjunicodeRX R}}
\def\leftswashR{{\getnamedglyphdirect{name:JunicodeRX-Italic}{R.leftswash}}}
\def\rightswashR{{\getnamedglyphdirect{name:JunicodeRX-Italic}{R.rightswash}}}
\def\swashT{{\itjunicodeRX T}}
\def\swashv{{\itjunicodeRX v}}
\def\swashV{{\itjunicodeRX V}}
\def\swashY{{\itjunicodeRX Y}}
\def\swashz{{\itjunicodeRX z}}

\def\ligas{\getnamedglyphdirect{name:JunicodeRX-Italic}{a_s}}
\def\ligis{\getnamedglyphdirect{name:JunicodeRX-Italic}{i_s}}
\def\ligus{\getnamedglyphdirect{name:JunicodeRX-Italic}{u_s}}

\setupbodyfont[junicode,12pt] 
\setupindenting[yes,0.5in]
\setupinterlinespace[big] 
\setupheads[indentnext=yes]
