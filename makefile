pdftargets := $(patsubst %.md,%.pdf,$(wildcard *.md))
mdxtargets := $(patsubst %.md,%.mdx,$(wildcard *.md))

listpdftargets : ; @echo $(pdftargets)
listmdxtargets : ; @echo $(mdxtargets)

all: final.pdf
.PHONY : all

allmdx : $(mdxtargets)
.PHONY : allmdx

%.mdx : %.md
	pandoc -t markdown+simple_tables+multiline_tables-citations -f markdown --citeproc -o $@ $<

final.tex : $(mdxtargets)
	pandoc -s -f markdown+simple_tables+multiline_tables -t context-smart -o final.tex headers.yaml $(mdxtargets)

%.tex : %.md headers.yaml
	pandoc -s -f markdown+simple_tables+multiline_tables -t context-smart --citeproc -o $@ headers.yaml $<

%.endnote.tex : %.tex
	sed 's/\footnote{/\endnote{/' $< > $@

%.pdf : %.tex standard-style.tex.include custom-style.tex.include custom-libertine-typescript.mkiv
	context $<

%.docx : %.md
	pandoc -s -f markdown+simple_tables+multiline_tables --citeproc -o $@ $<

%.odt : %.md
	pandoc -s -f markdown+simple_tables+multiline_tables --citeproc -o $@ $<

%.html : %.md
	pandoc -f markdown+simple_tables+multiline_tables -t html --citeproc -o $@ $<

.PHONY : clean
clean :
	find . \( -name "*.tuc" -o -name "*.log" -o -name "*.tex" -o -name "*.mdx" \) \! -iregex ".*rev.*" -delete

.PHONY : cleanall
cleanall :
	find . \( -name "*.pdf" -o -name "*.tuc" -o -name "*.log" -o -name "*.tex" -o -name "*.mdx" -o -name "*.docx" -o -name "*.odt" -o -name "*.html" \) \! -iregex ".*rev.*" -delete
